// jQuery Autocomplete plugin 1.2.2 by Jörn Zaefferer
;(function($){$.fn.extend({autocomplete:function(urlOrData,options){var isUrl=typeof urlOrData=="string";options=$.extend({},$.Autocompleter.defaults,{url:isUrl?urlOrData:null,data:isUrl?null:urlOrData,delay:isUrl?$.Autocompleter.defaults.delay:10,max:options&&!options.scroll?10:150,noRecord:""},options);options.highlight=options.highlight||function(value){return value};options.formatMatch=options.formatMatch||options.formatItem;return this.each(function(){new $.Autocompleter(this,options)})},result:function(handler){return this.bind("result",handler)},search:function(handler){return this.trigger("search",[handler])},flushCache:function(){return this.trigger("flushCache")},setOptions:function(options){return this.trigger("setOptions",[options])},unautocomplete:function(){return this.trigger("unautocomplete")}});$.Autocompleter=function(input,options){var KEY={UP:38,DOWN:40,DEL:46,TAB:9,RETURN:13,ESC:27,COMMA:188,PAGEUP:33,PAGEDOWN:34,BACKSPACE:8};var globalFailure=null;if(options.failure!=null&&typeof options.failure=="function"){globalFailure=options.failure}var $input=$(input).attr("autocomplete","off").addClass(options.inputClass);var timeout;var previousValue="";var cache=$.Autocompleter.Cache(options);var hasFocus=0;var lastKeyPressCode;var config={mouseDownOnSelect:false};var select=$.Autocompleter.Select(options,input,selectCurrent,config);var blockSubmit;navigator.userAgent.indexOf("Opera")!=-1&&$(input.form).bind("submit.autocomplete",function(){if(blockSubmit){blockSubmit=false;return false}});$input.bind((navigator.userAgent.indexOf("Opera")!=-1&&!'KeyboardEvent'in window?"keypress":"keydown")+".autocomplete",function(event){hasFocus=1;lastKeyPressCode=event.keyCode;switch(event.keyCode){case KEY.UP:if(select.visible()){event.preventDefault();select.prev()}else{onChange(0,true)}break;case KEY.DOWN:if(select.visible()){event.preventDefault();select.next()}else{onChange(0,true)}break;case KEY.PAGEUP:if(select.visible()){event.preventDefault();select.pageUp()}else{onChange(0,true)}break;case KEY.PAGEDOWN:if(select.visible()){event.preventDefault();select.pageDown()}else{onChange(0,true)}break;case options.multiple&&$.trim(options.multipleSeparator)==","&&KEY.COMMA:case KEY.TAB:case KEY.RETURN:if(selectCurrent()){event.preventDefault();blockSubmit=true;return false}break;case KEY.ESC:select.hide();break;default:clearTimeout(timeout);timeout=setTimeout(onChange,options.delay);break}}).focus(function(){hasFocus++}).blur(function(){hasFocus=0;if(!config.mouseDownOnSelect){hideResults()}}).click(function(){if(options.clickFire){if(!select.visible()){onChange(0,true)}}else{if(hasFocus++>1&&!select.visible()){onChange(0,true)}}}).bind("search",function(){var fn=(arguments.length>1)?arguments[1]:null;function findValueCallback(q,data){var result;if(data&&data.length){for(var i=0;i<data.length;i++){if(data[i].result.toLowerCase()==q.toLowerCase()){result=data[i];break}}}if(typeof fn=="function")fn(result);else $input.trigger("result",result&&[result.data,result.value])}$.each(trimWords($input.val()),function(i,value){request(value,findValueCallback,findValueCallback)})}).bind("flushCache",function(){cache.flush()}).bind("setOptions",function(){$.extend(true,options,arguments[1]);if("data"in arguments[1])cache.populate()}).bind("unautocomplete",function(){select.unbind();$input.unbind();$(input.form).unbind(".autocomplete")});function selectCurrent(){var selected=select.selected();if(!selected)return false;var v=selected.result;previousValue=v;if(options.multiple){var words=trimWords($input.val());if(words.length>1){var seperator=options.multipleSeparator.length;var cursorAt=$(input).selection().start;var wordAt,progress=0;$.each(words,function(i,word){progress+=word.length;if(cursorAt<=progress){wordAt=i;return false}progress+=seperator});words[wordAt]=v;v=words.join(options.multipleSeparator)}v+=options.multipleSeparator}$input.val(v);hideResultsNow();$input.trigger("result",[selected.data,selected.value]);return true}function onChange(crap,skipPrevCheck){if(lastKeyPressCode==KEY.DEL){select.hide();return}var currentValue=$input.val();if(!skipPrevCheck&&currentValue==previousValue)return;previousValue=currentValue;currentValue=lastWord(currentValue);if(currentValue.length>=options.minChars){$input.addClass(options.loadingClass);if(!options.matchCase)currentValue=currentValue.toLowerCase();request(currentValue,receiveData,hideResultsNow)}else{stopLoading();select.hide()}};function trimWords(value){if(!value)return[""];if(!options.multiple)return[$.trim(value)];return $.map(value.split(options.multipleSeparator),function(word){return $.trim(value).length?$.trim(word):null})}function lastWord(value){if(!options.multiple)return value;var words=trimWords(value);if(words.length==1)return words[0];var cursorAt=$(input).selection().start;if(cursorAt==value.length){words=trimWords(value)}else{words=trimWords(value.replace(value.substring(cursorAt),""))}return words[words.length-1]}function autoFill(q,sValue){if(options.autoFill&&(lastWord($input.val()).toLowerCase()==q.toLowerCase())&&lastKeyPressCode!=KEY.BACKSPACE){$input.val($input.val()+sValue.substring(lastWord(previousValue).length));$(input).selection(previousValue.length,previousValue.length+sValue.length)}};function hideResults(){clearTimeout(timeout);timeout=setTimeout(hideResultsNow,200)};function hideResultsNow(){var wasVisible=select.visible();select.hide();clearTimeout(timeout);stopLoading();if(options.mustMatch){$input.search(function(result){if(!result){if(options.multiple){var words=trimWords($input.val()).slice(0,-1);$input.val(words.join(options.multipleSeparator)+(words.length?options.multipleSeparator:""))}else{$input.val("");$input.trigger("result",null)}}})}};function receiveData(q,data){if(data&&data.length&&hasFocus){stopLoading();select.display(data,q);autoFill(q,data[0].value);select.show()}else{hideResultsNow()}};function request(term,success,failure){if(!options.matchCase)term=term.toLowerCase();var data=cache.load(term);if(data){if(data.length){success(term,data)}else{var parsed=options.parse&&options.parse(options.noRecord)||parse(options.noRecord);success(term,parsed)}}else if((typeof options.url=="string")&&(options.url.length>0)){var extraParams={timestamp:+new Date()};$.each(options.extraParams,function(key,param){extraParams[key]=typeof param=="function"?param():param});$.ajax({mode:"abort",port:"autocomplete"+input.name,dataType:options.dataType,url:options.url,data:$.extend({q:lastWord(term),limit:options.max},extraParams),success:function(data){var parsed=options.parse&&options.parse(data)||parse(data);cache.add(term,parsed);success(term,parsed)}})}else{select.emptyList();if(globalFailure!=null){globalFailure()}else{failure(term)}}};function parse(data){var parsed=[];var rows=data.split("\n");for(var i=0;i<rows.length;i++){var row=$.trim(rows[i]);if(row){row=row.split("|");parsed[parsed.length]={data:row,value:row[0],result:options.formatResult&&options.formatResult(row,row[0])||row[0]}}}return parsed};function stopLoading(){$input.removeClass(options.loadingClass)}};$.Autocompleter.defaults={inputClass:"ac_input",resultsClass:"ac_results",loadingClass:"ac_loading",minChars:1,delay:400,matchCase:false,matchSubset:true,matchContains:false,cacheLength:100,max:1000,mustMatch:false,extraParams:{},selectFirst:true,formatItem:function(row){return row[0]},formatMatch:null,autoFill:false,width:0,multiple:false,multipleSeparator:" ",inputFocus:true,clickFire:false,highlight:function(value,term){return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi,"\\$1")+")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong>$1</strong>")},scroll:true,scrollHeight:180,scrollJumpPosition:true};$.Autocompleter.Cache=function(options){var data={};var length=0;function matchSubset(s,sub){if(!options.matchCase)s=s.toLowerCase();var i=s.indexOf(sub);if(options.matchContains=="word"){i=s.toLowerCase().search("\\b"+sub.toLowerCase())}if(i==-1)return false;return i==0||options.matchContains};function add(q,value){if(length>options.cacheLength){flush()}if(!data[q]){length++}data[q]=value}function populate(){if(!options.data)return false;var stMatchSets={},nullData=0;if(!options.url)options.cacheLength=1;stMatchSets[""]=[];for(var i=0,ol=options.data.length;i<ol;i++){var rawValue=options.data[i];rawValue=(typeof rawValue=="string")?[rawValue]:rawValue;var value=options.formatMatch(rawValue,i+1,options.data.length);if(typeof(value)==='undefined'||value===false)continue;var firstChar=value.charAt(0).toLowerCase();if(!stMatchSets[firstChar])stMatchSets[firstChar]=[];var row={value:value,data:rawValue,result:options.formatResult&&options.formatResult(rawValue)||value};stMatchSets[firstChar].push(row);if(nullData++<options.max){stMatchSets[""].push(row)}};$.each(stMatchSets,function(i,value){options.cacheLength++;add(i,value)})}setTimeout(populate,25);function flush(){data={};length=0}return{flush:flush,add:add,populate:populate,load:function(q){if(!options.cacheLength||!length)return null;if(!options.url&&options.matchContains){var csub=[];for(var k in data){if(k.length>0){var c=data[k];$.each(c,function(i,x){if(matchSubset(x.value,q)){csub.push(x)}})}}return csub}else if(data[q]){return data[q]}else if(options.matchSubset){for(var i=q.length-1;i>=options.minChars;i--){var c=data[q.substr(0,i)];if(c){var csub=[];$.each(c,function(i,x){if(matchSubset(x.value,q)){csub[csub.length]=x}});return csub}}}return null}}};$.Autocompleter.Select=function(options,input,select,config){var CLASSES={ACTIVE:"ac_over"};var listItems,active=-1,data,term="",needsInit=true,element,list;function init(){if(!needsInit)return;element=$("<div/>").hide().addClass(options.resultsClass).css("position","absolute").appendTo(document.body).hover(function(event){if($(this).is(":visible")){input.focus()}config.mouseDownOnSelect=false});list=$("<ul/>").appendTo(element).mouseover(function(event){if(target(event).nodeName&&target(event).nodeName.toUpperCase()=='LI'){active=$("li",list).removeClass(CLASSES.ACTIVE).index(target(event));$(target(event)).addClass(CLASSES.ACTIVE)}}).click(function(event){$(target(event)).addClass(CLASSES.ACTIVE);select();if(options.inputFocus)input.focus();return false}).mousedown(function(){config.mouseDownOnSelect=true}).mouseup(function(){config.mouseDownOnSelect=false});if(options.width>0)element.css("width",options.width);needsInit=false}function target(event){var element=event.target;while(element&&element.tagName!="LI")element=element.parentNode;if(!element)return[];return element}function moveSelect(step){listItems.slice(active,active+1).removeClass(CLASSES.ACTIVE);movePosition(step);var activeItem=listItems.slice(active,active+1).addClass(CLASSES.ACTIVE);if(options.scroll){var offset=0;listItems.slice(0,active).each(function(){offset+=this.offsetHeight});if((offset+activeItem[0].offsetHeight-list.scrollTop())>list[0].clientHeight){list.scrollTop(offset+activeItem[0].offsetHeight-list.innerHeight())}else if(offset<list.scrollTop()){list.scrollTop(offset)}}};function movePosition(step){if(options.scrollJumpPosition||(!options.scrollJumpPosition&&!((step<0&&active==0)||(step>0&&active==listItems.size()-1)))){active+=step;if(active<0){active=listItems.size()-1}else if(active>=listItems.size()){active=0}}}function limitNumberOfItems(available){return options.max&&options.max<available?options.max:available}function fillList(){list.empty();var max=limitNumberOfItems(data.length);for(var i=0;i<max;i++){if(!data[i])continue;var formatted=options.formatItem(data[i].data,i+1,max,data[i].value,term);if(formatted===false)continue;var li=$("<li/>").html(options.highlight(formatted,term)).addClass(i%2==0?"ac_even":"ac_odd").appendTo(list)[0];$.data(li,"ac_data",data[i])}listItems=list.find("li");if(options.selectFirst){listItems.slice(0,1).addClass(CLASSES.ACTIVE);active=0}if($.fn.bgiframe)list.bgiframe()}return{display:function(d,q){init();data=d;term=q;fillList()},next:function(){moveSelect(1)},prev:function(){moveSelect(-1)},pageUp:function(){if(active!=0&&active-8<0){moveSelect(-active)}else{moveSelect(-8)}},pageDown:function(){if(active!=listItems.size()-1&&active+8>listItems.size()){moveSelect(listItems.size()-1-active)}else{moveSelect(8)}},hide:function(){element&&element.hide();listItems&&listItems.removeClass(CLASSES.ACTIVE);active=-1},visible:function(){return element&&element.is(":visible")},current:function(){return this.visible()&&(listItems.filter("."+CLASSES.ACTIVE)[0]||options.selectFirst&&listItems[0])},show:function(){var offset=$(input).offset();element.css({width:typeof options.width=="string"||options.width>0?options.width:$(input).width(),top:offset.top+input.offsetHeight,left:offset.left}).show();if(options.scroll){list.scrollTop(0);list.css({maxHeight:options.scrollHeight,overflow:'auto'});if(navigator.userAgent.indexOf("MSIE")!=-1&&typeof document.body.style.maxHeight==="undefined"){var listHeight=0;listItems.each(function(){listHeight+=this.offsetHeight});var scrollbarsVisible=listHeight>options.scrollHeight;list.css('height',scrollbarsVisible?options.scrollHeight:listHeight);if(!scrollbarsVisible){listItems.width(list.width()-parseInt(listItems.css("padding-left"))-parseInt(listItems.css("padding-right")))}}}},selected:function(){var selected=listItems&&listItems.filter("."+CLASSES.ACTIVE).removeClass(CLASSES.ACTIVE);return selected&&selected.length&&$.data(selected[0],"ac_data")},emptyList:function(){list&&list.empty()},unbind:function(){element&&element.remove()}}};$.fn.selection=function(start,end){if(start!==undefined){return this.each(function(){if(this.createTextRange){var selRange=this.createTextRange();if(end===undefined||start==end){selRange.move("character",start);selRange.select()}else{selRange.collapse(true);selRange.moveStart("character",start);selRange.moveEnd("character",end);selRange.select()}}else if(this.setSelectionRange){this.setSelectionRange(start,end)}else if(this.selectionStart){this.selectionStart=start;this.selectionEnd=end}})}var field=this[0];if(field.createTextRange){var range=document.selection.createRange(),orig=field.value,teststring="<->",textLength=range.text.length;range.text=teststring;var caretAt=field.value.indexOf(teststring);field.value=orig;this.selection(caretAt,caretAt+textLength);return{start:caretAt,end:caretAt+textLength}}else if(field.selectionStart!==undefined){return{start:field.selectionStart,end:field.selectionEnd}}}})(jQuery);

// cookie
(function(d){"function"===typeof define&&define.amd?define(["jquery"],d):d(jQuery)})(function(d){function n(a){return a}function p(a){return decodeURIComponent(a.replace(k," "))}function l(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return e.json?JSON.parse(a):a}catch(c){}}var k=/\+/g,e=d.cookie=function(a,c,b){if(void 0!==c){b=d.extend({},e.defaults,b);if("number"===typeof b.expires){var g=b.expires,f=b.expires=new Date;f.setDate(f.getDate()+g)}c=e.json?
JSON.stringify(c):String(c);return document.cookie=[e.raw?a:encodeURIComponent(a),"=",e.raw?c:encodeURIComponent(c),b.expires?"; expires="+b.expires.toUTCString():"",b.path?"; path="+b.path:"",b.domain?"; domain="+b.domain:"",b.secure?"; secure":""].join("")}c=e.raw?n:p;b=document.cookie.split("; ");for(var g=a?void 0:{},f=0,k=b.length;f<k;f++){var h=b[f].split("="),m=c(h.shift()),h=c(h.join("="));if(a&&a===m){g=l(h);break}a||(g[m]=l(h))}return g};e.defaults={};d.removeCookie=function(a,c){return void 0!==
d.cookie(a)?(d.cookie(a,"",d.extend({},c,{expires:-1})),!0):!1}});


function isMobile() {
	return window.matchMedia("only screen and (max-width: 768px)").matches;
}

var window_height = $(window).outerHeight(true);
function scrolling() {
	if(!isMobile()) {
		if($(window).scrollTop() > window_height) {
			$('#scroll-top').fadeIn().css('display', 'inline-block');
		}
		else {
			$('#scroll-top').fadeOut();
		}

		if($(window).scrollTop() > 250) {
			$('a#next-post').fadeIn().css('display', 'inline-block');
		}
		else {
			$('a#next-post').fadeOut();
		}

	}
}

$(window).scroll(function(e) {
	scrolling();
});

$(function() {
	scrolling();

	// hash url helyre rántása, mivel a bannerek töltődése elcseszi
	scrollTopHash();

	if (typeof adblock_notification !== 'undefined') {
		OVERLAY.notification(adblock_notification);
	}

});

OVERLAY = {
	debug: true,

	init: function() {
		if($.cookie('overlay_notification')) {
			OVERLAY.notification($.cookie('overlay_notification'));
			$.removeCookie('overlay_notification');
		}

		if($.cookie('overlay_dialog')) {
			OVERLAY.dialog($.cookie('overlay_dialog'));
			$.removeCookie('overlay_dialog');
		}
	},
	notification: function(message) {
		OVERLAY.remove();

		$('body').prepend('<div id="overlay-box" class="notification"><div class="content">'+message+'</div></div>');
		$('#overlay-box > .content').click(OVERLAY.remove);

	},
	dialog: function(message, options) {
		options = options || {};
		if(!options.primaryLabel) options.primaryLabel = 'Rendben';
		if(!options.width) options.width = 450;
		OVERLAY.remove();

		$('body').prepend('<div id="overlay-box" class="dialog" role="dialog"><div class="content">'+message+'</div><div class="buttons"></div></div>');
		$('body').prepend('<div id="overlay-mask"></div>');

		// gombok
		$('#overlay-box .buttons').append('<button class="primary">'+options.primaryLabel+'</button>');
		if(options.secondaryLabel) $('#overlay-box .buttons').append('<button class="secondary">'+options.secondaryLabel+'</button>');
		$('#overlay-box .buttons button').click(OVERLAY.remove);

		// callback -ek
		if(options.primary) $('#overlay-box .buttons .primary').click(options.primary);
		if(options.secondary) $('#overlay-box .buttons .secondary').click(options.secondary);

		var height = $('#overlay-box').height();
		$('#overlay-box').css({'width': options.width, 'top': '25%', 'left': '50%', 'margin-left': -options.width/2, 'margin-top': -height/4});
	},
	remove: function() {
		$('html').css({ 'overflow': 'auto', 'overflow-y': 'scroll'});
		$('#overlay-box').remove();
		$('#overlay-mask').remove();
	}
}

$.fn.defaultValue = function(def, options) {
options = options || {};
options['class'] = options['class'] || 'defaultValue';
$(this).each(function() {
	if ($(this).val() == '' || $(this).val() == def) {
	    $(this).addClass(options['class']);
	    $(this).val(def)
	}

	$(this).focus(function() {
	    if ($(this).val() == def) {
	        $(this).val('');
	        $(this).removeClass(options['class']);
	    }
	});
	$(this).blur(function() {
	    if ($(this).val() == '') {
	        $(this).addClass(options['class']);
	        $(this).val(def);
	    }
	});

	var inp = $(this);
	$(this).parents('form').submit(function() {
	    if (inp.val() == def) {
	        inp.val('');
	    }
	});
});
}

// aktivitás olvasottnak jelölése (callback)
function setAcitvityViewed(id, thref) {
	$.ajax({
		type: 'POST',
		url: '/requests/content/act_viewed',
		data: {'id': id},
		dataType: 'text',
		success: function(data) {
			if(window.location.href.split('#')[0] == thref.split('#')[0]) {
				window.location.reload();
			}
			else {
				window.location = thref;
			}
		}
	});
}




// hír törlése
function deleteContent(content_id) {

	OVERLAY.dialog('Biztos, hogy törölni szeretné ezt beküldött a hírt a Propellerről? Nem vonható vissza és a hozzászólások is elvesznek!', {
		primaryLabel: 'Igen, törlöm',
		primary: function() {
			$.ajax({
				type: 'GET',
				url: '/requests/content/delete_content',
				data: {'content_id': content_id},
				dataType: 'text',
				success: function (data) {
				//	$('#feed-item-'+content_id).remove();
					$('#c'+content_id).fadeOut(400).hide();
					OVERLAY.notification('A hír törlése megtörtént.');
				}
			});
		},
		secondaryLabel: 'Mégsem',
		secondary: function() {
			return false;
		}
	});

}

// privát üzenet törlése
function deleteMessage(message_id) {

	OVERLAY.dialog('Biztos, hogy törölni szeretné ezt a privát üzenetet? Nem vonható vissza!', {
		primaryLabel: 'Igen, törlöm',
		primary: function() {
			$.ajax({
				type: 'GET',
				url: '/requests/users/delete_message',
				data: {'message_id': message_id},
				dataType: 'text',
				success: function (data) {
		            $('#m'+message_id).fadeOut(400).hide();
				}
			});
		},
		secondaryLabel: 'Mégsem',
		secondary: function() {
			return false;
		}
	});

}

							function toggle(what) { // ez mi?
								$('#'+what).toggle();
								return;
							}

// beállításoknál a politikai nézet változtatása
function togglePolOther(e) {
	$('#politics-wrapper').css('display', ((e == 'other') ? 'block' : 'none'));
}







function comments_resize(parse) {
	//$('.fb-comments').attr('data-width', $('.fb-comments-wrapper').width());
	//	if(parse) FB.XFBML.parse($('.fb-comments-wrapper')[0]);
}

$(window).bind('resize', function(e) {
	window.resizeEvt;
	$(window).resize(function() {
		clearTimeout(window.resizeEvt);
		window.resizeEvt = setTimeout(function() {
			comments_resize(true);
		}, 150);
	});
});

$(function() {
	OVERLAY.init();

	//OVERLAY.notification('habalababalala');
	//OVERLAY.dialog('habalababalala?', { secondaryLabel: 'nem'});


	$('#scroll-top').click(function(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, 800);
	});

	$('.site-navigation .menu-toggle a').click(function(e) {
		e.preventDefault();
		if($('.site-navigation ul').hasClass('toggled')) {
			$('.site-navigation ul').removeClass('toggled');
		}
		else {
			$('.site-navigation ul').addClass('toggled');
		}
	});

	$('#show-comments').click(function(e) {
		e.preventDefault();
		if($('.all-comments').hasClass('toggled')) {
			$('.all-comments').removeClass('toggled');
		}
		else {
			$('.all-comments').addClass('toggled');
		}
	});


/// GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA GA ///

	// kimenő linkek mérése
	$('a.outgoing').click(function() {
		ga('send', 'event', 'outgoing', 'click', $(this).attr('href'));
		$(this).attr('target', '_blank');
	});

	// kapcsolódók a cikkoldalon
	$('.related a').click(function() {
		ga('send', 'event', 'related', 'click', $(this).html(), {'nonInteraction': 1});
	});

	$('.topstories.bottom a').click(function() {
		ga('send', 'event', 'topstories-bottom', 'click', $(this).attr('title'), {'nonInteraction': 1});
	});

	$('.topstories.header a').click(function() {
		ga('send', 'event', 'topstories-header', 'click', $(this).attr('title'), {'nonInteraction': 1});
	});

	$('.topstories.aside a').click(function() {
		ga('send', 'event', 'topstories-sidebar', 'click', $(this).attr('title'), {'nonInteraction': 1});
	});

    $('.topstories.content a').click(function() {
        ga('send', 'event', 'topstories-content', 'click', $(this).attr('title'), {'nonInteraction': 1});
    });


    $('aside.sidebar .popular-box a').click(function() {
		var str = $(this).html().replace(/(<([^>]+)>)/ig,"");
        ga('send', 'event', 'popular-sidebar', 'click', str, {'nonInteraction': 1});
    });

    $('aside.sidebar .commented-box a').click(function() {
		var str = $(this).html().replace(/(<([^>]+)>)/ig,"");
        ga('send', 'event', 'commented-sidebar', 'click', str, {'nonInteraction': 1});
    });

    $('#next-post').click(function() {
		var str = $(this).html().replace(/(<([^>]+)>)/ig,"");
        ga('send', 'event', 'next-post', 'click', 'sajat', 1, {'nonInteraction': 1});
    });



	$('.ga-event a').click(function() {
	    var data = $(this).closest('.ga-event');

	    ga('send', 'event', 'gae-' + $(data).data('category'), $(data).data('action'), $(data).data('label'), {'nonInteraction': 1});
	});

	// tulajdonnevek kiemelése a hírcímekben
	$('.list-box a').each(function() {
		$(this).html($(this).html().replace(new RegExp('([A-ZŰÁÉÍÚŐÓÜÖ0-9][-a-zA-Z0-9öüóőúéáäűíŰÁÉÍÚŐÓÜÖ!?\']+)', "g"), '<b>$1</b>'));
	});


	function numberFrm(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	// share számok
	var get_fb_container = [];
	$('.get-fb-count').each(function(key, value) {
		var url = $(this).data('url').replace('.dev', '.hu');;
		get_fb_container.push(url);

/*		$.ajax({
		    url: 'https://graph.facebook.com/?id=' + url + '&callback=?',
		    dataType: "json",
		    success: function(result) {
		    		if(result.share) {
		    			var str = '<i class="material-icons material-hot">whatshot</i>'+numberFrm(parseInt(result.share.share_count));

						var elm = $(".get-fb-count[data-url='" +url+ "']");

						$(elm).children('span').html(str+' megosztás');
						//console.log(url + ' > ' + count);
		    		}
		    }
		});*/
	});


	if (get_fb_container.length) {
		$.ajax({
		    url: 'https://graph.facebook.com/?ids=' + get_fb_container.join(',') + '&callback=?',
		    dataType: "json",
		    success: function(data) {
				$.each(data, function(key, value) {
					var url = value.id;
					var count = value.share.share_count;

					var str = '<i class="material-icons material-hot">whatshot</i>'+numberFrm(parseInt(count));
					var elm = $(".get-fb-count[data-url='" +url+ "']");

					$(elm).children('span').html(str+' megosztás');
					//console.log(key + ' > ' + count);
				});
		    }
		});
	}


	$('.get-fb-count, .fb-buttons .facebook').click(function(e) {
		e.preventDefault();
		var left = (screen.width/2)-(600/2);
		var top = (screen.height/3)-(350/2);
		window.open($(this).attr('href'), 'Megosztás', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=350, top='+top+', left='+left);
		ga('send', 'event', 'share_button', 'click', window.location.href, {'nonInteraction': 1});
	});



	// a címlapi "most beszédtéma" doboz és a fejléc aktivitások értesítő-buborékjának frissítése
	var checking_interval = logged_in ? 90000 : 180000; // 90 és 180 másodperc
	var previous_comment_num = 0;

	if(typeof commented_since !== 'undefined' || logged_in) {
		setTimeout(checkCommentsAndActivities, checking_interval);
	}

	function checkCommentsAndActivities() {
		$.ajax({
			type: 'POST',
			url: '/requests/content/check',
			data: 'commented_since='+((typeof commented_since !== 'undefined') ? commented_since : ''),
			dataType: 'json',
			success: function(data) {
				if(!data) return;

				// friss kommentek
				if(typeof data.comments !== 'undefined') {
					newCommentedItems = '';

					var newc = 0;
					$.each(data.comments, function(key, value){
						if(value.datetime > commented_since) newc++;
					});

					commented_since = data.comments[0].datetime;

					if(newc > 0) { // vannak új kommentek
						//var alert_str = (previous_comment_num+newc)+' új hozzászólás';

						//$('#commented-message-wrapper').html(alert_str).unbind('click').click(function() {
							//$('#commented-message-wrapper').fadeOut();

							$('#commented-box > li').each(function(key, value) {
								currId = $(this).attr('id');
								newId = 'c_'+data.comments[key].id;

								tit = data.comments[key].title;
								tit = tit.replace(new RegExp('([A-ZŰÁÉÍÚŐÓÜÖ0-9][-a-zA-Z0-9öüóőúéáäűíŰÁÉÍÚŐÓÜÖ!?\']+)', "g"), '<b>$1</b>');

								$(this).attr('id', newId).html('<a href="'+data.comments[key].link+'#v'+data.comments[key].id+'">'+tit+'</a>');

								if(currId != newId) {
									//$(this).fadeOut().fadeIn();
								}
							});
			                previous_comment_num = 0;
						//});

						// eddig 0 új volt
						//if(previous_comment_num == 0) $('#commented-message-wrapper').fadeIn();

						previous_comment_num = previous_comment_num+newc;
					}

				}

				// aktivitások
				if(typeof data.activities !== 'undefined') {
					var a = data.activities;
					var html = '';
					if(a.num == 0) { // már nincsenek olvasatlanok
						$('#activity').hide();
						var w_title = $(document).attr('title').replace(/^\s*\([^\)]*\)\s*/, '');
						$(document).attr('title', w_title);
					}

					// nincsenek aktivitások, vége
					if(typeof(a.items) == 'undefined') return false;

					$.each(a.items, function(key, value){
						html += '<li data-id="'+value.id+'">'+value.message+'</li>';
					});
					//html += '<li class="all"><a>Összesen '+a.num+' új értesítés</a></li>';
					$('#activity-list').html(html);

					//$('#activity .all a').attr('href', $('#activity > a').attr('href'));
					// aktivitások kattintásra olvasottak legyeneke
					$('#activity-list li a').unbind('click').click(function(e) {
						e.preventDefault();
						setAcitvityViewed($(this).parents('li').data('id'), this.href);
					});

					if($('#activity').html() != '<i class="material-icons">public</i><span>'+a.num+'</span>') {
						$('#activity').hide();
					}
					$('#activity').html('<i class="material-icons">public</i><span>'+a.num+'</span>').show();

					// címsorba a számláló
					var w_title = $(document).attr('title').replace(/^\s*\([^\)]*\)\s*/, '');
					var w_counter = $(document).attr('title').replace(/[^0-9]+/g, '');

					$(document).attr('title', '('+a.num+') '+w_title);

				}

			},
			error: function () {
			}
		});

		setTimeout(checkCommentsAndActivities, checking_interval);
	}

 	// hozzászólás küldés
	$('#comment-form').submit(function() {
		if(!logged_in) {
        	OVERLAY.notification('Hozzászólás írásához regisztráció szükséges. <a href="/regisztracio">Regisztráljon</a> vagy használja a <a href="/belepes" rel="nofollow">belépést</a>!');
			return false;
		}

		if($('#comment').val() == '') return false;

		$('#submit').attr('disabled', 'disabled');
		OVERLAY.notification('Pillanat...');
	});

	// ha fókusz kerül a komment mezőre
	if(!logged_in) {
		$('#comment-form #comment').focus(function() {
        	OVERLAY.notification('Hozzászólás írásához regisztráció szükséges. <a href="/regisztracio">Regisztráljon</a> vagy használja a <a href="/belepes" rel="nofollow">belépést</a>!');
		});
	}

	// komment aktivitás: lájk
	$('.ca-like').click(function() {
		if(!logged_in) {
			OVERLAY.notification('A hozzászólások kedveléséhez regisztráció szükséges. <a href="/regisztracio">Regisztráljon</a> vagy használja a <a href="/belepes" rel="nofollow">belépést</a>!');
			return false;
		}

		var parent = $(this).parent().parent().parent();
		var comment_id = $(parent).data('commentid');
		var content_id = $(parent).data('contentid');
		var user_id = $(parent).data('userid');

		$.ajax({
			type: 'GET',
			url: '/requests/content/like_comment',
			data: {'content_id': content_id, 'comment_id': comment_id, 'user_id': user_id},
			dataType: 'text',
			success: function(data) {
				if(data == 'OK') {
					var wrap = $('.c-'+comment_id+' .ca-like span');
					if($(wrap).length) {
						var n = ($(wrap).html() == '') ? 1 : parseInt($(wrap).html()) + 1;
						$(wrap).html(n);
					}
					$(wrap).hide().fadeIn();
				}
			}
		});
		return false;
	});

	// komment aktivitás: jelentés
	$('.ca-report').click(function() {
		if(!logged_in) {
			OVERLAY.notification('A hozzászólások jelentéséhez regisztráció szükséges. <a href="/regisztracio">Regisztráljon</a> vagy használja a <a href="/belepes" rel="nofollow">belépést</a>!');
			return false;
		}

		var parent = $(this).parent().parent().parent();
		var comment_id = $(parent).data('commentid');
		var content_id = $(parent).data('contentid');

		OVERLAY.dialog('Biztos, hogy jelenteni szeretné ezt a hozzászólást a moderátornak?', {
			primaryLabel: 'Igen, jelentem',
			primary: function() {
				$.ajax({
					type: 'GET',
					url: '/requests/content/report_comment',
					data: {'content_id': content_id, 'comment_id': comment_id },
					dataType: 'text',
					success: function (data) {
						OVERLAY.notification('Köszönjük az értesítést, a moderátor hamarosan megvizsgálja a hozzászólást!');
					}
				});
			},
			secondaryLabel: 'Mégsem',
			secondary: function() {
				return false;
			}
		});
		return false;
	});

	// komment aktivitás: válasz
	$('.ca-reply').click(function() {
		var comment_id = $(this).parent().parent().parent().data('commentid');
		$('#comment').focus().attr('scrollTop', 0).val($('#comment').val()+'@'+$('.c-'+comment_id+' .username').html()+': ');
		return false;
	});

	// fejléc aktivitások buborékja
	$('#activity').mouseenter(function() {
	   $('#activity-list').slideDown(100);
	});
	$('#activity-list').mouseleave(function() {
	   $('#activity-list').slideUp(200);
	});
	$('#activity').click(function(e) { // meghagyjuk ezt is touch miatt
		e.preventDefault();
		$('#activity-list').slideToggle(150);
	});


	// aktivitások kattintásra olvasottak legyenek
    $('#activity-list a').click(function(e) {
    	e.preventDefault();
		setAcitvityViewed($(this).parents('li').data('id'), this.href);
	});





	// téma hozzáadásnál: link ellenőrzése
	$('#add-link').blur(function() {
		var l = $('#add-link');
		if(l.val() == '' || l.val() == 'http://') return;

		OVERLAY.notification('Pillanat...');

		$.ajax({
			type: 'GET',
			url: '/requests/content/get_title',
			data: {'title': l.val()},
			dataType: 'json',
			success: function(data) {
				OVERLAY.remove();

				if(data == '404') { // nincs semmi
					$('#add-link').focus();
					OVERLAY.notification('Nem sikerült feldolgozni a linket.');
					return false;
				}


				if(data.duplicatedlink) { //van már ilyen tartalom
					$('#add-title').after('<p class="error">Ezt a hírt már beküldte valaki, <a href="'+data.duplicatedlink+'" target="_blank">itt találja</a>.</p>');
				}
				if(data.link) {
					$('#add-link').val(data.link);
					$('#add-title').focus();
				}

				$('#add-title').val(data.title);
				if(data.title) {
					$('#add-description').focus();
				}

				$('#add-description').val(data.description);
				var descl = data.description;
				if(descl.length < 210) {
					OVERLAY.notification('Túl rövid bevezető, legalább 200 karaktert írjon, vagy másoljon be néhány bekezdést!');
				}

				if(data.description) {
					$('#category_id').focus();
				}

				$('#add-tags').val(data.tags);
				if(data.tags) {
					$('#add-tags').removeClass('defaultValue');
				}
			}
		});
	});

	// téma hozzáadásnál: címkék
	$('#add-tags').autocomplete('/requests/content/tags_autocomplete', {
		width: 410,
		minChars: 2,
		selectFirst: true,
        scroll: false,
		highlight: false,
		multiple: true,
		multipleSeparator: ", "
	});









    // üzenet küldés számláló
    var keyHandler = function() {
        var len = 1000 - $('#message').val().length;
        var td = len < 10 ? 'blink' : 'none';
        $('#message-counter').html(len).css('text-decoration', td).show();
    }
    $('#message').keyup(keyHandler).keyup();

    var delayedKeyHandler = function() {
        setTimeout(function() { $('#message').trigger('keyup') }, 1);
    }
    $('#message').bind('paste', delayedKeyHandler).bind('cut', delayedKeyHandler);

    // komment küldés számláló
    var keyHandler = function() {
        var len = 2000 - $('#comment').val().length;
        var td = len < 10 ? 'blink' : 'none';
        $('#comment-counter').html(len).css('text-decoration', td).show();
    }
    $('#comment').keyup(keyHandler).keyup();

    var delayedKeyHandler = function() {
        setTimeout(function() { $('#message').trigger('keyup') }, 1);
    }
    $('#message').bind('paste', delayedKeyHandler).bind('cut', delayedKeyHandler);




	// üzeneteknél a címzett
	$('#message-recipient').autocomplete('/requests/users/recipient_autocomplete', {
		width: 410,
		minChars: 2,
		selectFirst: true,
        scroll: false,
		highlight: false,
		multiple: false
	});

	// beállítások lapon a település választó
	$("#city-change").autocomplete("/requests/content/get_cities", {
		width: 300,
		minChars: 2,
		selectFirst: true,
      scroll: false,
		highlight: false
	});

	$('#city-change').result(function(event, data, formatted) {
		if(!data) return;
		$('#city_id').val(data[1]);
	});


	$('#search-q').defaultValue('Keresendő hír...');
	$('#mm-name').defaultValue('Kovács János');
	$('#mm-mail').defaultValue('nev@valami.hu');
	$('#message-recipient').defaultValue('Címzett neve (kezdje el gépelni...)');
	$('#message').defaultValue('Üzenet szövege...');
	$('#message-reply').defaultValue('Válasz szövege...');
	$('#add-link').defaultValue('http://');
	$('#add-tags').defaultValue('Kezdjen el gépelni egy szót...');

});

window.fbAsyncInit = function() {

	FB.init({
		appId: "247823682023773",
		status: true,
		xfbml: true
	});

	FB.Event.subscribe('edge.create', function(response) { // like
		if(response == "http://www.facebook.com/propellerhu") {
			//_gaq.push(['_trackEvent', 'facebook_like', document.title, $('link[rel=canonical]').attr('href'), 1, true]);
			ga('send', 'event', 'facebook_like', 'click', document.title, {'nonInteraction': 1});
			$.cookie('facebook_liked', 1, { expires: 180, path: '/' });
		}
	});

	FB.Event.subscribe('edge.remove', function(href) { // dislike
		if(href == "http://www.facebook.com/propellerhu") {
			$.removeCookie('facebook_liked');
		}
	});

	/*comments_resize(false);
	FB.Event.subscribe('xfbml.render', function(response) {
		if($('#fb-comments-count').length) {
			if($('#fb-comments-count').hasClass('fb_comments_count_zero')) {
				$('.fb-comments > span').height(110).css('overflow', 'auto');
				$('.fb-comments > span > iframe').height(110).css('overflow', 'auto');
				scrollTopHash();
			}
		}
		scrollTopHash();
	});*/

};

function scrollTopHash() {
	// tartalmi oldalon hashhez ugrás
	if(window.location.hash) { // van hash
		var anchor = window.location.hash.substring(1);
		var pref = window.location.hash.substring(1,2);
		if($('#'+anchor).length) {
			if(pref == 'c') {
				$(window).scrollTop(($('#'+anchor).offset().top)-4);
			}
		}
	}
}


/*
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/hu_HU/all.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));
*/



$(function() {
	if (isMobile()) {
		$('#fb-ad').html('<fb:ad placementid="247823682023773_642553142550823" format="320x50" testmode="false"></fb:ad>');
	}
});

window.fbAsyncInit = function() {
		FB.Event.subscribe(
	    'ad.loaded',
	    function(placementId) {
	        console.log('Audience Network ad loaded');
	    }
	);
	FB.Event.subscribe(
	    'ad.error',
	    function(errorCode, errorMessage, placementId) {
	        console.log('Audience Network error (' + errorCode + ') ' + errorMessage);
	    }
	);
};

(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk/xfbml.ad.js#xfbml=1&version=v2.5&appId=247823682023773";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));





/* ==========================================================================
   COOKIE ALERT
   ========================================================================== */

jQuery(document).ready(function($) {

   var cookieAcceptedByUser = $.cookie("agreement");
   var specpage = false;
   if($('body').hasClass('postid-53616')){
    specpage = true;
   }
  
   console.log('cookie: ' + cookieAcceptedByUser + ' specpage: ' + specpage);

  if(!specpage){
    if( !cookieAcceptedByUser){
        $('#cookie_block').prop('hidden', false);
    }
  }

  $(document).on('click', '#accept_cookie_btn', function(event) {
      event.preventDefault();
      $.cookie("agreement", 1, { expires : 60, path: '/' });
      $('#cookie_block').prop('hidden', true);
  });

});//ready end