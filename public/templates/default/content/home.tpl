{include file="./header.tpl"}

<main class="main" role="main">
<div>

	{if $misc.is_breaking}
		{section name=i loop=$blocks.left max=1}
		<div class="block block-{$smarty.section.i.index+1}{if $blocks.left[i].image} block-img{else} block-noimg{/if} block-breaking ga-event"
		data-category="cimlap-breaking" data-action="{$blocks.left[i].title|escape}" data-label="1. hely">
			{if $blocks.left[i].image}<a href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} title="{$blocks.left[i].title|escape}" style="background-image: url({$blocks.left[i].image});">{if $misc.is_breaking_subtitle}<span>{$misc.is_breaking_subtitle|escape}</span>{/if}</a>{/if}

			<h2><a href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.left[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.left[i].title|escape}{/if}</a></h2>
			{if $blocks.left[i].description}<p>{$blocks.left[i].description|strip_tags|escape}</p>{/if}

			{if $blocks.left[i].sublink}<ul>
			{section name=j loop=$blocks.left[i].sublink}
			<li><a href="{$blocks.left[i].sublink[j].link}"{if !$blocks.left[i].sublink[j].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.left[i].sublink[j].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.left[i].sublink[j].title|escape}{/if}</a></li>
			{/section}
			</ul>
			{/if}
		</div>
		{/section}
		{assign var="left_col_start" value=1}
	{else}
		{assign var="left_col_start" value=0}
	{/if}

	<div class="col left">
		{section name=i loop=$blocks.left start=$left_col_start max=28}
		<div class="block block-{$smarty.section.i.index+1}{if $blocks.left[i].image} block-img{else} block-noimg{/if} ga-event"
		data-category="cimlap-bal" data-action="{$blocks.left[i].title|escape}" data-label="{$smarty.section.i.index+1}. hely">
			{if $blocks.left[i].image}<a href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} title="{$blocks.left[i].title|escape}" style="background-image: url({$blocks.left[i].image});"></a>{/if}

			<h2><a href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.left[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.left[i].title|escape}{/if}</a></h2>
			{if $blocks.left[i].description}<p>{$blocks.left[i].description|strip_tags|escape}</p>{/if}

			{if $blocks.left[i].sublink}<ul>
			{section name=j loop=$blocks.left[i].sublink}
			<li><a href="{$blocks.left[i].sublink[j].link}"{if !$blocks.left[i].sublink[j].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.left[i].sublink[j].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.left[i].sublink[j].title|escape}{/if}</a></li>
			{/section}
			</ul>
			{/if}
		</div>

		{if $smarty.section.i.index == 5}
			<!-- /21667127856/propeller_nyito_6 -->
			<div id='propeller_nyito_6'>
			<script>{literal}
			googletag.cmd.push(function() { googletag.display('propeller_nyito_6'); });
			</script>{/literal}
			</div>
		{/if}

		{/section}


	</div>

	<div class="col right">

		<div class="list-box">
		<h4 class="heading1"><span>Most beszédtéma</span></h4>
		<script>var commented_since = '{$commented[0].datetime}';</script>
		<ul id="commented-box">
			{section name=i loop=$commented max=8}
			<li id="c_{$commented[i].id}"><a href="{$env->base}/{$commented[i].link}#v{$commented[i].id}" rel="bookmark">{$commented[i].title|escape}</a></li>
			{/section}
		</ul>
		</div>

		{section name=i loop=$blocks.right max=30}
		<div class="block block-{$smarty.section.i.index+1}{if $blocks.right[i].image} block-img{else} block-noimg{/if} ga-event"
		data-category="cimlap-jobb" data-action="{$blocks.right[i].title|escape}" data-label="{$smarty.section.i.index+1}. hely">
			{if $blocks.right[i].image}<a href="{$blocks.right[i].link}"{if !$blocks.right[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} title="{$blocks.right[i].title|escape}" style="background-image: url({$blocks.right[i].image});"></a>{/if}

			<h2><a href="{$blocks.right[i].link}"{if !$blocks.right[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.right[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.right[i].title|escape}{/if}</a></h2>
			{if $blocks.right[i].description}<p>{$blocks.right[i].description|strip_tags|escape}</p>{/if}

			{if $blocks.right[i].sublink}<ul>
			{section name=j loop=$blocks.right[i].sublink}
			<li><a href="{$blocks.right[i].sublink[j].link}"{if !$blocks.right[i].sublink[j].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.right[i].sublink[j].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.right[i].sublink[j].title|escape}{/if}</a></li>
			{/section}
			</ul>
			{/if}
		</div>

		{if $smarty.section.i.index == 1}
			<!-- /21667127856/propeller_nyito_3 -->
			<div id='propeller_nyito_3'>
			<script>{literal}
			googletag.cmd.push(function() { googletag.display('propeller_nyito_3'); });
			</script>{/literal}
			</div>
		{/if}
		{/section}

	</div>


	<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

	{include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}