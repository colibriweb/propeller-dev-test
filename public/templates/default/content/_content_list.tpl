
		{section name=i loop=$list}

			{*if $smarty.section.i.index == 2}
			    <div id="banner-lead" class="banner">
        			{$env->getBanner('slave', $ad.category, 'nyito', 'roadblock_felso')}
			    </div>
			{/if*}

			<div class="hentry" id="c{$list[i].id}">
				<h2 class="entry-title">{if $SCRIPT_NAME == '/modules/content/search.php'}{math equation="(x * 30)-30 + y + 1" x=$p.current_page y=$smarty.section.i.index}. {/if}<a href="{$env->base}/{$list[i].category_alias}/{$list[i].alias}" rel="bookmark" title="{$list[i].title|escape}">{$list[i].title|escape}</a></h2>

				{if $SCRIPT_NAME == '/modules/content/search.php'}
				<p class="entry-summary">{if $list[i].description}{$list[i].description}{/if}</p>
				{else}
				<p class="entry-summary">{if $list[i].description}{$list[i].description|strip_tags|truncate:360:"..."|escape}{/if}</p>
				{/if}

				<div class="entry-meta"><div>
					{*if $list[i].link}
					<span class="vcard source-org copyright">
					<span class="org fn">{$list[i].site|default:$list[i].orighost}</span></span>{else}
					<span class="vcard author"><span class="fn">Propeller</span></span>{/if*}

					<span><time class="entry-date published updated" datetime="{$content.date_c}"><i class="material-icons">access_time</i>{$list[i].date}</time></span>

					<span class="comments-count"><a href="{$env->base}/{$list[i].category_alias}/{$list[i].alias}"><i class="material-icons">comment</i>{$list[i].comments} hozzászólás</a></span>

					<span class="fb-count"><a class="get-fb-count" data-url="http://propeller.hu/{$list[i].category_alias}/{$list[i].alias}" href="https://www.facebook.com/sharer.php?app_id=247823682023773&sdk=joey&u=http://propeller.hu/{$list[i].category_alias}/{$list[i].alias}&display=popup" rel="nofollow"><span></span></a></span>

					{if ($env->u[3] == 'bekuldott') && ($smarty.session.user.id == $user.id)}
						<span><a onclick="deleteContent({$list[i].id});"><i class="material-icons">delete</i> Törlés</a></span>
					{/if}
				</div></div><!-- .entry-meta -->

				{if $list[i].tags}
					<div class="entry-tags">
						<i class="material-icons">local_offer</i>{section name=j loop=$list[i].tags}<a href="{$env->base}/tag/{$list[i].tags[j]|escape:'html'|replace:' ':'+'}" rel="tag">{$list[i].tags[j]|escape}</a>{if !$smarty.section.j.last}, {/if}{/section}
					</div>
				{/if}
			</div>
		{/section}
