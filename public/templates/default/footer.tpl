

	<div class="clear"></div>

	</div><!-- #primary -->
	</div><!-- #content -->

</div><!-- #wrapper -->


<div class="clear"></div>

<footer class="site-footer">
	<nav class="footer-navigation">
	<ul>
		<li>&copy; 2016 IKO Digital Kft.</li><!--
		--><li><a href="/{$env->l.pages.url}/{$env->l.pages.url_contact}">{$env->l.pages.contact}</a></li><!--
		--><li><a href="mailto:szerk@propeller.hu">Írjon a szerkesztőknek!</a></li><!--
		--><li><a href="/adatvedelmi_tajekoztato.pdf">{$env->l.pages.privacy}</a></li><!--
		--><li><a href="/statikus/doboz">Ingyenes hírek</a></li><!--
		--><li><a href="{$env->base}/feed/content-sajat.xml">RSS</a></li>
	</ul>
	</nav><!-- .footer-navigation -->
</footer><!-- .site-footer -->

<a id="scroll-top" href="#top"><i class="material-icons">keyboard_arrow_up</i></a>
{if ($SCRIPT_NAME == '/modules/content/content.php')}
<a id="next-post" href="/kovetkezo-hir" rel="nofollow" data-counter="{$next_post_counter}" title="Következő hír"><i class="material-icons">keyboard_arrow_right</i></a>
{/if}

<div id="cookie_block" hidden>
	<div class="container">
		<div class="toCenter">
			<p class="text-left">
				<strong>
					Az oldalon sütiket használunk, hogy biztonságos böngészés mellett minél értékesebb felhasználói élményt adhassunk!
				</strong>

				<a id="accept_cookie_btn" class="cookie-button cta" href="">Rendben</a>
				<a id="more_info" class="cookie-button cta cta-outline" href="http://propeller.hu/adatvedelmi_tajekoztato.pdf">Részletek</a>
			</p>
		</div>
	</div>
</div>

<script>var logged_in={if $smarty.session.user}true{else}false{/if};</script>

<!-- Facebook SDK -->
<div id="fb-root"></div>
<script>{literal}(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.7&appId=247823682023773";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));{/literal}</script>

<script src="{$smarty.const.STTC}/js/jquery-1.9.0.min.js"></script>
<script src="{$smarty.const.STTC}/js/script.{$smarty.const.BUILD}.js"></script>
<script src="{$smarty.const.STTC}/js/stickyscroller.js"></script>

</body>
</html>