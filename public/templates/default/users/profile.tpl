{if is_array($message_thread)}
{include file="./header.tpl" title="`$titleplus`"}
{elseif isset($titleplus)}
{include file="./header.tpl" title="`$user.name` – `$titleplus`"}
{else}
{include file="./header.tpl" title="`$user.name`"}
{/if}

<main class="main" role="main">
<div>

	<h1><span><a href="/{$env->l.users.url_profile}/{$user.alias}"><img src="{$user.avatarsrc}" style="margin-right:.4em;display:inline;" width="32" height="32" alt="">{$user.name|escape}</a></span></h1>

	<ul class="submenu">
		<li><a href="/{$env->l.users.url_profile}/{$user.alias}"{if empty($env->u[3])} class="active"{/if}>Hozzászólások</a></li><!--
		--><li><a href="/{$env->l.users.url_profile}/{$user.alias}/adatlap"{if $env->u[3] == 'adatlap'} class="active"{/if}>Adatlap</a></li><!--
		--><li><a href="/{$env->l.users.url_profile}/{$user.alias}/bekuldott"{if $env->u[3] == 'bekuldott'} class="active"{/if}>Beküldött hírek</a></li><!--
		{if $user.id == $smarty.session.user.id}
		--><li><a href="/{$env->l.users.url_settings}">{$env->l.users.settings}</a></li><!--
		{else}
		--><li><a href="/{$env->l.users.url_messages}/{$user.alias}">Új személyes üzenet</a></li><!--
		{/if}
	--></ul>

		{if $env->u[3] == 'bekuldott'}
			{if !$list}
				<p>{$user.name|escape} még nem küldött be egyetlen hírt sem.</p>
			{else}

			<div class="content-list">
				{include file="./content/_content_list.tpl"}
			</div>
			{/if}
		{elseif empty($env->u[3])}{* hozzászólások *}
			{if !$list}
				<p>{$user.name|escape} nem írt mostanában egyetlen hozzászólást sem.</p>
			{else}

				<div class="comment-wrapper">
				<div class="comment-list">
				{section name=i loop=$list}
				<div id="c{$list[i].comment_id}" class="c-{$list[i].comment_id} clear" data-contentid="{$list[i].id}" data-commentid="{$list[i].comment_id}" data-userid="{$user.id|default:'0'}">
					<a href="/{$env->l.users.url_profile}/{$user.alias}" class="avatar"><img src="{$user.avatarsrc}" width="50" height="50" alt=""></a>
					<div class="comment-right">
						<a href="/{$list[i].category_alias}/{$list[i].alias}" class="username">{$list[i].title|escape}</a>
						<div class="comment-text">
							{$list[i].description|nl2br}
						</div>
						<div class="comment-foot">
							<a class="ca-like" title="Tetszik"><i class="material-icons">thumb_up</i><span>{if $list[i].likes}{$list[i].likes}{/if}</span></a>
							<a href="/{$list[i].category_alias}/{$list[i].alias}" title="Válasz"><i class="material-icons">reply</i>Válasz</a>
							{if in_array($smarty.session.user.comment_id, array(1,317,181,44,16889,3400))}<a href="/admin/?op=comments&mode=edit&id={$list[i].comment_id}&category_id={$content.category_id}" target="_blank">Moderálás</a>{/if}
							<a class="ca-report" title="Jelentem a moderátornak"><i class="material-icons">error</i></a>
							<a class="ca-date" href="/{$content.category_alias}/{$content.alias}#c{$list[i].comment_id}"><i class="material-icons">access_time</i>{$list[i].date}</a>
						</div>
					</div><!-- .comment-right -->
				</div>
				{/section}
				</div>
				</div>

			{/if}
		{elseif $env->u[3] == 'adatlap'}
		<div class="content-list">
			<div>
				{$env->l.users.profession}:
				<b>{$user.profession|escape|default:'Nincs megadva'}</b>
			</div>
			<div>
				Életkor:
				<b>{if $user.age}{$user.age} éves{else}Nincs megadva{/if}</b>
			</div>
			<div>
				{$env->l.users.city}:
				<b>{$user.city|escape|default:'Nincs megadva'}</b>
			</div>
			<div>
				{$env->l.users.website}:
				<b>{if $user.website}<a href="{$user.website|escape:'html'}" rel="nofollow" target="_blank">{$user.website|truncate:46:"..."|escape}</a>{else}-{/if}</b>
			</div>
			<div>
				{$env->l.users.motto}:
				<b>{$user.motto|escape|default:'Nincs megadva'}</b>
			</div>
			<div>
				{$env->l.users.t_like}:
				<b>{$user.t_like|escape|default:'Nincs megadva'}</b>
			</div>
			<div>
				{$env->l.users.t_dislike}:
				<b>{$user.t_dislike|escape|default:'Nincs megadva'}</b>
			</div>
			<div>
				{$env->l.users.topics}:
				<b>{$user.topics|escape|default:'Nincs megadva'}</b>
			</div>
			<!--div>
				{$env->l.users.politics}:
				<b>{$user.pol|escape|default:'Nincs megadva'}</b>
			</div-->
		</div>
		{/if}


	<div class="clear"></div>

	{if $p.paging_all > 1}
	<ul class="pagination">
		<li> </li>
		<li>{if $p.current_page == 1}<span>Előző</span>{else}<a href="/{$env->l.users.url_profile}/{$user.alias}{if !empty($env->u[3])}/{$env->u[3]|escape:'html'|replace:' ':'+'}{/if}?page={$p.current_page-1}">Előző</a>{/if}</li>
		{section name=pagination start=$p.paging_from loop=$p.paging_to+1}
			<li{if $smarty.section.pagination.index == $p.current_page} class="active"{/if}><a href="/{$env->l.users.url_profile}/{$user.alias}{if !empty($env->u[3])}/{$env->u[3]|escape:'html'|replace:' ':'+'}{/if}?page={$smarty.section.pagination.index}">{$smarty.section.pagination.index}</a></li>
		{/section}
		<li>{if $p.current_page == $p.paging_all}<span>Következő</span>{else}<a href="/{$env->l.users.url_profile}/{$user.alias}{if !empty($env->u[3])}/{$env->u[3]|escape:'html'|replace:' ':'+'}{/if}?page={$p.current_page+1}">Következő</a>{/if}</li>
	</ul>
	{/if}



<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}