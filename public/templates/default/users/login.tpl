{include file="./header.tpl" title=$env->l.users.login}

<main class="main" role="main">
<div>

	<h1><span>{$env->l.users.login}</span></h1>

	{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

	<p class="lead">{$env->l.users.loginlead}</p>

	<form id="form" action="{$env->l.users.url_login}{if $redirect}?redirect={$redirect}{/if}" method="post">
	<fieldset>

		<input type="hidden" name="action" value="do" />
		<input type="hidden" name="redirect" value="{$redirect}" />

		<label for="mail">{$env->l.users.mail}:</label>
		<input type="text" id="mail" class="text required validate-email" name="user[mail]" maxlength="255" value="{if isset($form.mail)}{$form.mail|escape}{elseif isset($smarty.cookies.mail)}{$smarty.cookies.mail|escape}{else}{$smarty.get.mail|escape}{/if}" title="{$env->l.users.help_loginmail}" tabindex="1" /><br class="clear" />

		<label for="pass">{$env->l.users.pass}:</label>
		<input type="password" id="pass" class="text required" name="user[pass]" maxlength="32" title="{$env->l.users.help_loginpass}" tabindex="2" /><br class="clear" />

		<div class="checkbox-wrapper">
			<label for="note"><input type="checkbox" id="note" class="radio" name="note" value="1" {if $form.note eq 1}checked="checked" {/if}tabindex="3" />{$env->l.users.notelogin}</label>
		</div>

		<p class="note">{$env->l.users.notelogintext}</p>

		<div class="button-wrapper">
			<input type="submit" id="submit" class="primary" value="{$env->l.button.login}" tabindex="5" />
			<a href="{$env->l.users.url_lostpass}{if $form.mail}?mail={$form.mail|escape}{/if}">Ha elfelejtette a jelszavát, kattintson ide!</a>
		</div>

	</fieldset>
	</form>

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}
