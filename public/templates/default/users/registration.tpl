{include file="./header.tpl" title=$env->l.users.registration}

<script src='https://www.google.com/recaptcha/api.js?render=6Lf1a3wUAAAAAA9wIcrhR_Acnmvwj7i553vA-56L'></script>

<main class="main" role="main">
<div>

	<h1><span>{$env->l.users.registration}</span></h1>
	{if isset($smarty.get.ok)}
		<p class="lead">{if !$env->c.users.mailvalidation}{$env->l.users.registrationlead_none}{else}{$env->l.users.registrationlead_valid}{/if}</p>
		<br />

		{if $smarty.session.registration_user_id}
		<p class="lead">Ha kis idő elteltével sem érkezik meg az aktiváló levél, érdemes megnézni a postafiók spam (levélszemét) mappájában is, hátha odakerült. Ha ott sem található, <a href="/{$env->l.users.url_resend}">ide kattinva kérheti</a> az aktiváló e-mail újraküldését.</p>{/if}

	{else}
	{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

	<p class="lead">{$env->l.users.registrationlead}</p>

	<form id="form" action="{$env->l.users.url_registration}" method="post">

	<fieldset>

		<input type="hidden" name="action" value="do" />

		<label for="mail">{$env->l.users.mail}:</label>
		<input type="text" id="mail" class="text required validate-email" name="user[mail]" maxlength="255" value="{if $form.mail}{$form.mail|escape}{else}{$smarty.get.mail|escape}{/if}" title="{$env->l.users.help_mail}" tabindex="1" /><br class="clear" />

		<label for="pass">{$env->l.users.pass}:</label>
		<input type="password" id="pass" class="text required" name="user[pass]" maxlength="32" title="{$env->l.users.help_pass}" tabindex="2" /><br class="clear" />

		<label for="repass">{$env->l.users.repass}:</label>
		<input type="password" id="repass" class="text required" name="user[repass]" maxlength="32" title="{$env->l.users.help_repass}" tabindex="3" /><br class="clear" />

		<label for="name">{$env->l.users.name}:</label>
		<input type="text" id="name" class="text required" name="user[name]" maxlength="32" value="{$form.name|escape}" title="{$env->l.users.help_name}" tabindex="4" /><br class="clear" />

	<!--{if $env->c.users.usecaptcha}

		<label for="recaptcha">{$env->l.users.recaptcha}:</label>
		<img id="captcha" src="requests/users/captcha_registration?{$smarty.now}" alt="{$env->l.users.captcha}" title="{$env->l.users.captcha}" style="vertical-align:middle;">
		<input type="text" id="recaptcha" class="text required" name="recaptcha" title="{$env->l.users.help_captcha}" style="width: 167px;" tabindex="6" /><br class="clear" />
	{/if}-->
		<input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
		<div class="checkbox-wrapper">
			<label for="newsletter"><input type="checkbox" id="newsletter" class="radio" name="user[newsletter]" value="1" {if $form AND empty($form.newsletter)}{else}checked="checked" {/if}tabindex="7" />{$env->l.users.newsletter}</label>
			<label for="agree"><input type="checkbox" id="agree" class="radio" name="agree" value="1" {if $form.agree eq 1}checked="checked" {/if}tabindex="8" />Elfogadom az <a
			href="/adatvedelmi_tajekoztato.pdf" target="_blank">Adatvédelmi szabályzatot és Felhasználási feltételeket</a></label>
		</div>

		<p class="note">{$env->l.users.agreetext}</p>

		<div class="button-wrapper">
			<input type="submit" id="submit" class="primary" value="{$env->l.button.registration}" tabindex="9" />
			<a href="{$env->l.users.url_login}{if $form.mail}?mail={$form.mail|escape}{/if}">Ha már regisztrált, kattintson ide!</a>
		</div>

	</fieldset>
	</form>
	{/if}
	
<script>{literal}
grecaptcha.ready(function() {
	grecaptcha.execute('6Lf1a3wUAAAAAA9wIcrhR_Acnmvwj7i553vA-56L', { action: 'registration' } )
	.then(function(token) { 
		document.getElementById('g-recaptcha-response').value =    token;
	});
});
{/literal}</script>

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}