{if is_array($message_thread)}
{include file="./header.tpl" title="`$titleplus`"}
{elseif isset($titleplus)}
{include file="./header.tpl" title="`$user.name` – `$titleplus`"}
{else}
{include file="./header.tpl" title="`$user.name`"}
{/if}

<main class="main" role="main">
<div>

	<h1><span><a href="/{$env->l.users.url_messages}">Üzeneteim</a></span></h1>

	<ul class="submenu">
		<li><a href="/{$env->l.users.url_messages}"{if empty($env->u[2])} class="active"{/if}>Üzenetváltások</a></li><!--
		{if !empty($env->u[2]) && ($env->u[2] != $env->l.users.url_messages_compose)}--><li><a href="/{$env->l.users.url_messages}/{$env->u[2]|escape:'url'}" class="active">{$titleplus|escape}</a></li><!--{/if}
		--><li><a href="/{$env->l.users.url_messages}/{$env->l.users.url_messages_compose}"{if $env->u[2] == $env->l.users.url_messages_compose} class="active"{/if}>Új személyes üzenet</a></li><!--
	--></ul>

	{if empty($env->u[2])}
		{if $message_list}
			<div class="content-list">
			{section name=i loop=$message_list}
				<div>
					<h2>{if $message_list[i].viewed == 0}(új) {/if}<a href="/{$env->l.users.url_messages}/{$message_list[i].alias}">{$message_list[i].name|escape} <span style="font-weight:normal">üzenetei</span></a></h2>

					<div class="entry-meta"><div>
						<span><time class="entry-date published"><i class="material-icons">access_time</i>{$message_list[i].date}</time></span>
					</div></div><!-- .entry-meta -->
				</div>
			{/section}
			</div>

			<div class="clear"></div>

			{if $p.paging_all > 1}
			<ul class="pagination">
				<li> </li>
				<li>{if $p.current_page == 1}<span>Előző</span>{else}<a href="/{$env->l.users.url_messages}?page={$p.current_page-1}">Előző</a>{/if}</li>
				{section name=pagination start=$p.paging_from loop=$p.paging_to+1}
					<li{if $smarty.section.pagination.index == $p.current_page} class="active"{/if}><a href="/{$env->l.users.url_messages}?page={$smarty.section.pagination.index}">{$smarty.section.pagination.index}</a></li>
				{/section}
				<li>{if $p.current_page == $p.paging_all}<span>Következő</span>{else}<a href="/{$env->l.users.url_messages}?page={$p.current_page+1}">Következő</a>{/if}</li>
			</ul>
			{/if}
		{else}
			<div class="content-list">
				<p>Még nem kapott vagy küldött egyetlen privát üzenetet sem.</p>
			</div>
		{/if}
	{elseif $env->u[2] == $env->l.users.url_messages_compose}

		<form id="message-form" action="/{$env->l.users.url_messages}/{$env->l.users.url_messages_compose}" method="post">
		<fieldset>
			{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}
			<input type="text" id="message-recipient" name="message[recipient]" class="text required" title="{$env->l.users.error_norecipient}" value="{$form.recipient|escape}">

			<textarea name="message[message]" id="message" rows="6" cols="18" class="text required" title="{$env->l.users.error_nomessage}">{$form.message|escape}</textarea><br class="clear">
			<input type="submit" id="submit" class="primary" value="Üzenet küldése"><span id="message-counter-wrapper">Még <span id="message-counter"></span> karakter írhat</span>
		</fieldset>
		</form>

	{else}

		<form id="message-form" action="/{$env->l.users.url_messages}/{$env->u[2]|escape:'url'}" method="post">
		<fieldset>
			{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

			<textarea name="message[message]" id="message-reply" rows="6" cols="18" class="text required" title="{$env->l.users.error_nomessage}">{$form.message|escape}</textarea><br class="clear">
			<input type="submit" id="submit" class="primary" value="Válasz küldése"><span id="message-counter-wrapper">Még <span id="message-counter"></span> karakter írhat</span>
		</fieldset>
		</form>


		<div class="content-list">
		{section name=i loop=$message_thread}
			<div id="m{$message_thread[i].msg_id}">
				<p style="{if $message_thread[i].name == $smarty.session.user.name}text-align:right;padding-left:10%;{else}padding-right:10%;{/if}">{if $message_thread[i].message}<!--span style="font-weight:600">{$message_thread[i].name|escape}</span> írta:<br-->{$message_thread[i].message|escape|nl2br}{/if}</p>

				<div class="entry-meta"><div>
					<span><time class="entry-date published"><i class="material-icons">access_time</i>{$message_thread[i].date}</time></span>
					<span><a onclick="deleteMessage({$message_thread[i].msg_id});" title="Törlés"><i class="material-icons">delete</i></a></span>
				</div></div><!-- .entry-meta -->
			</div>
		{/section}
		</div>

		<div class="clear"></div>

		{if $p.paging_all > 1}
		<ul class="pagination">
			<li> </li>
			<li>{if $p.current_page == 1}<span>Előző</span>{else}<a href="/{$env->l.users.url_messages}/{$env->u[2]|escape:'url'}?page={$p.current_page-1}">Előző</a>{/if}</li>
			{section name=pagination start=$p.paging_from loop=$p.paging_to+1}
				<li{if $smarty.section.pagination.index == $p.current_page} class="active"{/if}><a href="/{$env->l.users.url_messages}/{$env->u[2]|escape:'url'}?page={$smarty.section.pagination.index}">{$smarty.section.pagination.index}</a></li>
			{/section}
			<li>{if $p.current_page == $p.paging_all}<span>Következő</span>{else}<a href="/{$env->l.users.url_messages}/{$env->u[2]|escape:'url'}?page={$p.current_page+1}">Következő</a>{/if}</li>
		</ul>
		{/if}

	{/if}

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}