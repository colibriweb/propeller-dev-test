{include file="./header.tpl" title=$env->l.users.validation}

<main class="main" role="main">
<div>

	<h1><span>{$env->l.users.validation}</span></h1>
	{if isset($smarty.get.ok)}
		<p class="lead">{$env->l.users.validationlead}</p>

	{else}
		{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

		<p class="lead">{$env->l.users.validationlead_error}</p>
	{/if}

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}