

<div class="topstories aside">
{if $misc.is_breaking && $blocks.left[0].image}
	{section name=i loop=$blocks.left max=1}
	<div class="topstories-breaking">
		<a href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} style="background-image: url({$blocks.left[i].image});">{if $misc.is_breaking_subtitle}<span>{$misc.is_breaking_subtitle|escape}</span>{/if}</a>
		<span class="title"><a href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.left[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else} rel="bookmark">{$blocks.left[i].title|escape}{/if}</a></span>
		<span class="share"><a class="get-fb-count" title="Megosztás" data-url="{$blocks.left[i].link}" href="https://www.facebook.com/sharer.php?app_id=247823682023773&sdk=joey&u={$blocks.left[i].link}&display=popup" rel="nofollow"><span></span></a></span>
		<div class="clear"></div>
	</div>
	{/section}
{/if}



{section name=i loop=$blocks.topstories start=3 max=8}
<div{if ($SCRIPT_NAME == '/modules/content/content.php' && $smarty.section.i.index == 3)
		|| ($topstories_first_big && $smarty.section.i.index == 3)
		|| $topstories_all_big
		} class="big"{/if}>
	<a href="{$blocks.topstories[i].link}"{if !$blocks.topstories[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} style="background-image: url({$blocks.topstories[i].image});"></a>
	<span class="title"><a href="{$blocks.topstories[i].link}"{if !$blocks.topstories[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.topstories[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else} rel="bookmark">{$blocks.topstories[i].title|escape}{/if}</a></span>
	<span class="share"><a class="get-fb-count" title="Megosztás" data-url="{$blocks.topstories[i].link}" href="https://www.facebook.com/sharer.php?app_id=247823682023773&sdk=joey&u={$blocks.topstories[i].link}&display=popup" rel="nofollow"><span></span></a></span>
	<div class="clear"></div>
</div>
{/section}

{if ($SCRIPT_NAME == '/modules/content/content.php')}
	<!-- /21667127856/propeller_cikk_6 -->
	<div id='propeller_cikk_6'>
	<script>{literal}
	googletag.cmd.push(function() { googletag.display('propeller_cikk_6'); });
	</script>{/literal}
	</div>
{/if}

{if ($SCRIPT_NAME == '/modules/content/home.php')}
	<!-- /21667127856/propeller_nyito_7 -->
	<div id='propeller_nyito_7'>
	<script>{literal}
	googletag.cmd.push(function() { googletag.display('propeller_nyito_7'); });
	</script>{/literal}
	</div>
{/if}

{if ($SCRIPT_NAME == '/modules/content/category.php')}
	<!-- /21667127856/propeller_rovat_6 -->
	<div id='propeller_rovat_6'>
	<script>{literal}
	googletag.cmd.push(function() { googletag.display('propeller_rovat_6'); });
	</script>{/literal}
	</div>
{/if}


</div>



