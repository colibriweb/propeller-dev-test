
    <div class="list-box">
    <h4 class="heading1"><span>Friss hírek</span></h4>
    <ul>
        {if ($SCRIPT_NAME == '/modules/content/category.php') && $content.category_id == 1}{assign var="lateststart" value="0"}
        {elseif ($SCRIPT_NAME == '/modules/content/category.php') && $content.category_id == 2}{assign var="lateststart" value="12"}
        {elseif ($SCRIPT_NAME == '/modules/content/category.php') && $content.category_id == 6}{assign var="lateststart" value="24"}
        {elseif ($SCRIPT_NAME == '/modules/content/category.php') && $content.category_id == 4}{assign var="lateststart" value="36"}
        {elseif ($SCRIPT_NAME == '/modules/content/category.php') && $content.category_id == 7}{assign var="lateststart" value="48"}
        {else}{assign var="lateststart" value="60"}{/if}

        {section name=i loop=$latest start=$lateststart max=8}
        <li class="ga-event" data-category="box-friss" data-action="{$latest[i].title|truncate:98:"..."|escape}" data-label="{$smarty.section.i.index-$lateststart+1}"><span>{$latest[i].datetime|date_format:"%H:%M"}</span><a href="{$env->base}/{$latest[i].category_alias}/{$latest[i].alias}">{$latest[i].title|truncate:98:"..."|escape}</a></li>
        {/section}

        {if ($SCRIPT_NAME == '/modules/content/content.php')}<li><span> </span><a href="{$env->base}/{$content.category_alias}" style="font-weight:600;">Tovább az összes friss hírhez...</a></li>{/if}
    </ul>


        {if ($SCRIPT_NAME == '/modules/content/content.php')}
            <!-- /21667127856/propeller_cikk_1 -->
            <div id='propeller_cikk_1'>
            <script>{literal}
            googletag.cmd.push(function() { googletag.display('propeller_cikk_1'); });
            </script>{/literal}
            </div>
        {/if}

        {if ($SCRIPT_NAME == '/modules/content/home.php')}
            <!-- /21667127856/propeller_nyito_2 -->
            <div id='propeller_nyito_2'>
            <script>{literal}
            googletag.cmd.push(function() { googletag.display('propeller_nyito_2'); });
            </script>{/literal}
            </div>
        {/if}

        {if ($SCRIPT_NAME == '/modules/content/category.php')}
            <!-- /21667127856/propeller_rovat_3 -->
            <div id='propeller_rovat_3'>
            <script>{literal}
            googletag.cmd.push(function() { googletag.display('propeller_rovat_3'); });
            </script>{/literal}
            </div>
        {/if}

    </div>


