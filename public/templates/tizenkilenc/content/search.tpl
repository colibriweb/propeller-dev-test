{include file="./header.tpl" title=$search_title}

<main class="main" role="main">
<div>

	<h1><span><a href="/kereses">Keresés</a></span></h1>


	<div class="site-search" style="display:block;width:100%;margin-bottom:15px;text-align:left;background:#eee;padding:10px;">
		<form action="/kereses" method="get">
			<input type="text" id="search-q" name="q" value="{$smarty.get.q|escape}"><!--
			--><button type="submit" style="right:12px;"><i class="material-icons">search</i></button><br>
			<label for="n" style="vertical-align:middle;"><input type="checkbox" style="vertical-align:middle;" id="n" value="1" name="n" {if $smarty.get.n == '1'}checked="checked" {/if}/> Szótöredékre is szeretnék keresni</label>
		</form>
	</div><!-- .site-search -->

{if $list && $smarty.get.q}
	<ul class="submenu">
		<li><a href="{if $smarty.get.q == 'időjárás'}/idojaras{else}/kereses?q={$smarty.get.q|escape:'html'|replace:' ':'+'}{if $smarty.get.n == '1'}&amp;n=1{/if}{/if}">{$p.results_num} találat</a> az elmúlt néhány hónapból:</li><!--
	--></ul>
{/if}

	<div class="content-list">
		{if !$list && $smarty.get.q}
			<b>Sajnos nincs a keresett kifejezésnek megfelelő tartalom az elmúlt 1 hónapban.</b><br><br>
			Próbálkozzon a következőkkel:<br>
			- Ellenőrizze, hogy helyesen írta-e a keresett szót<br>
			- Pontosítsa, egyszerűsítse a kifejezést, adjon meg más szavakat<br>
			- Ügyeljen rá, hogy a szóköz "ÉS" kapcsolatként működik<br><br>
			<a href="/"><b>Végső esetben nézze meg a címlapot, hátha ott van, amit keres!</b></a><br>

		{/if}

		{include file="./content/_content_list.tpl"}
	</div>

	<div class="clear"></div>

	{if $p.paging_all > 1}
	<ul class="pagination">
		<li> </li>
		<li>{if $p.current_page == 1}<span>Előző</span>{else}<a href="/{$env->l.content.url_search}?q={$smarty.get.q|escape:'html'|replace:' ':'+'}{if $smarty.get.n == '1'}&amp;n=1{/if}&amp;page={$p.current_page-1}">Előző</a>{/if}</li>
		{section name=pagination start=$p.paging_from loop=$p.paging_to+1}
			<li{if $smarty.section.pagination.index == $p.current_page} class="active"{/if}><a href="/{$env->l.content.url_search}?q={$smarty.get.q|escape:'html'|replace:' ':'+'}{if $smarty.get.n == '1'}&amp;n=1{/if}&amp;page={$smarty.section.pagination.index}">{$smarty.section.pagination.index}</a></li>
		{/section}
		<li>{if $p.current_page == $p.paging_all}<span>Következő</span>{else}<a href="/{$env->l.content.url_search}?q={$smarty.get.q|escape:'html'|replace:' ':'+'}{if $smarty.get.n == '1'}&amp;n=1{/if}&amp;page={$p.current_page+1}">Következő</a>{/if}</li>
	</ul>
	{/if}


	<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

	{include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}