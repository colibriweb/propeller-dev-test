{include file="./header.tpl" title=$content.title}
{if $smarty.session.user.admin}<a href="/admin/?op=content&amp;mode=edit&amp;id={$content.id}&amp;category_id={$content.category_id}" target="_blank" class="primary" style="position:fixed;bottom:12px;right:10px;background:#aaa;-webkit-appearance:none;color:#fff;">{if $smarty.session.user.admin && (strtotime($content.datetime) > time())}
FIGYELEM! EZ A CIKK JELENLEG MÉG NEM ÉLES, CSAK AZÉRT LÁTHATOD, MERT SZERKESZTŐ VAGY – {/if}Szerkesztés</a>{/if}

<article class="hnews hentry item">

{if '18+'|in_array:$content.tags}
<div id="overlay-mask" class="age-limit"></div>
<div id="overlay-box" class="dialog age-limit" role="dialog">
    <div class="content">
        <img src="{$smarty.const.STTC}/images/default/18.png" width="100" height="100">
        <h3>Figyelem!</h3>
        <p>Ez a tartalom kiskorúakra káros elemeket is tartalmazhat.</p>
        <p>Elmúltál már 18 éves?</p>
    </div>
    <div class="buttons">
        <button class="primary" onclick="javascript:OVERLAY.remove();">Igen</button>
        <a href="/" class="secondary">Nem</a>
    </div>
</div>
{/if}

<h1 class="entry-title"><a href="{$env->base}/{$content.category_alias}/{$content.alias}">{$content.title|escape}</a></h1>

<div class="entry-meta"><div>

    {if $content.link}
    <span class="vcard source-org copyright">
    <span class="org fn">{$content.site|default:$content.orighost}{if !$content.content_type && $content.user_id} / <a href="/{$env->l.users.url_profile}/{$content.user_alias}" title="Ezt a hír {$content.user_name|escape} beküldése"><i class="material-icons">person</i>{$content.user_name|escape}</a> által{/if}</span></span>{else}
    <span class="vcard author"><span class="fn">{if $content.authors}
{section name=j loop=$content.authors}<a href="/szerzo/{$content.authors[j].alias}"><i class="material-icons">person</i>{$content.authors[j].name|escape}</a>{if !$smarty.section.j.last}, {/if}{/section}
    {else}

<i class="material-icons">person</i>Propeller


    {/if}</span></span>{/if}

    <iframe src="//www.facebook.com/plugins/like.php?href=http://propeller.hu/{$content.category_alias}/{$content.alias}&amp;width=130&amp;layout=button&amp;action=recommend&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=247823682023773&amp;locale=hu_HU" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

    <span><time class="entry-date published updated" datetime="{$content.date_c}"><i class="material-icons">access_time</i>{$content.date}</time></span>

    <span class="comments-count"><a href="{$env->base}/{$content.category_alias}/{$content.alias}#comments"><i class="material-icons">comment</i>{$content.comments} hozzászólás</a></span>

    <span class="fb-count"><a class="get-fb-count add" title="Megosztás" data-url="http://propeller.hu/{$content.category_alias}/{$content.alias}" href="https://www.facebook.com/sharer.php?app_id=247823682023773&sdk=joey&u=http://propeller.hu/{$content.category_alias}/{$content.alias}&display=popup" rel="nofollow"><span></span></a></span>
</div></div><!-- .entry-meta -->

<main class="main" role="main">
<div>

    {if $content.fullembed}
        <!-- külső videó -->
        <div class="video-embed">
            {$content.fullembed}
        </div>
    {/if}

    <!-- beküldés, feed tartalom, saját írás -->

    <div class="entry-content">
    {if $content.link && !$content.fullembed}
        {$content.description}
        <p>(Forrás: <a href="{$content.link|escape:'html'}" rel="nofollow" class="outgoing">{$content.site|default:$content.orighost}</a>{if !$content.content_type && $content.user_id}, <a href="/{$env->l.users.url_profile}/{$content.user_alias}" title="Ezt a hír {$content.user_name|escape} beküldése">{$content.user_name|escape}</a> által{/if})</p>
    {else}
        {$content.description}
    {/if}

        <!--div style="padding:20px;margin-top:20px;border-left: 4px solid #e71000;background:#f6f6f6;">
            <h2>FinTechRadar 2.0 konferencia - van már jegyed?</h2>

            <p>Témáink: PSD2/Mobilfizetés, Adatbiztonság, UX, Big data, Állam és fintech, Insurtech.
            2 nap, 30 előadó és válaszok a fintech aktuális kérdéseire október 17-18-án a Sugár Moziban.</p>

            <a href="http://fintechradar.hu/konferencia2?utm_source=propeller.hu&utm_medium=textlink&utm_campaign=FTR_konf2"
            class="button primary" target="_blank" style="color:#fff;margin-top:15px;background: #e71000;">Program és jegyvásárlás →</a>
        </div-->
    </div>


    {if $img}
        <!-- nagykép -->
        {section name=i loop=$img}
        <div class="bigpicture-item" id="p{$smarty.section.i.index+1}">
            <a href="{$env->base}/{$content.category_alias}/{$content.alias}#p{$smarty.section.i.index+1}" title="{$img[i].title|escape}"><img src="{$smarty.const.STTC}/images/bigpicture/{$img[i].filepath}" alt="{$img[i].title|escape}" title="{$img[i].title|escape}"></a>
            {if $img[i].author || $img[i].title}<div class="taken clear"><span>{$img[i].author|escape}</span>{$img[i].title|escape}</div>{/if}
        </div>
        {/section}
    {/if}


    <div class="social-buttons">
        <iframe src="//www.facebook.com/plugins/like.php?href=http://propeller.hu/{$content.category_alias}/{$content.alias}&amp;width=130&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21&amp;appId=247823682023773&amp;locale=hu_HU" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        <div>További friss hírek: <iframe src="//www.facebook.com/plugins/like.php?href=http://www.facebook.com/propellerhu&amp;width=130&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=247823682023773&amp;locale=hu_HU" scrolling="no" frameborder="0" allowTransparency="true" style="float:none;vertical-align:top;width:125px;margin-left:5px;margin-top:0;"></iframe></div>
    </div><!-- .social-buttons -->

<div style="float:left;width:100%;padding:1rem 0;">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle"
         style="display:block;"
         data-ad-format="autorelaxed"
         data-ad-client="ca-pub-8809360823023723"
         data-ad-slot="9874569029"></ins>
    <script>{literal}
         (adsbygoogle = window.adsbygoogle || []).push({});
    {/literal}</script>
</div>
    {if !$content.link}
<!-- /21667127856/propeller_cikk_2 -->
<div id='propeller_cikk_2'>
<script>{literal}
googletag.cmd.push(function() { googletag.display('propeller_cikk_2'); });
</script>{/literal}
</div>

    <div id="banner-bottom" class="banner">
        {$env->getBanner('slave', $ad.category, 'cikkoldal', 'roadblock_also')}
    </div>
    {/if}



    {if $related}
        <div class="clear"></div>
        <h4 class="heading1 related-title"><span>Kapcsolódó cikkek</span></h4>
        <div class="related">
            <ul>
            {section name=i loop=$related max=4}
            <li><a href="{$env->base}/{$related[i].category_alias}/{$related[i].alias}" title="{$related[i].title|escape}">{$related[i].title|escape}</a></li>
            {/section}
            </ul>
        </div>
    {/if}



    {if $bigpic_sug}
        <div class="bigpic-list">
        <h4 class="heading1"><span>További nagyképek</span></h4>
        <div class="bigpic-sug">
        {section name=i loop=$bigpic_sug max=5}
        <a href="{$env->base}/{$bigpic_sug[i].category_alias}/{$bigpic_sug[i].alias}" title="{$bigpic_sug[i].title|escape}" rel="bookmark">
            <span class="img" style="background-image:url('{$smarty.const.STTC}/images/bigpicture/{$bigpic_sug[i].filepath}');"></span>
            <span class="title">{$bigpic_sug[i].title|escape}</span>
        </a>
        {/section}
        </div>
        </div>
    {/if}


    <div class="clear"></div>
    <!--h4 class="heading1 related-title"><span>További ajánlatok</span></h4-->
    <div class="topstories content clear" style="padding-top:1.3rem;">
        <div>
    {section name=i loop=$topstories_content max=3}
    <div>
        <a title="{$topstories_content[i].title|escape}" href="{$topstories_content[i].link}"{if !$topstories_content[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} style="background-image: url({$topstories_content[i].image});"></a>
        <span class="title"><a href="{$topstories_content[i].link}" title="{$topstories_content[i].title|escape}"{if !$topstories_content[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$topstories_content[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else} rel="bookmark">{$topstories_content[i].title|escape}{/if}</a></span>
    </div>
    {/section}
<!-- /21667127856/propeller_cikk_3 -->
<div id='propeller_cikk_3'>
<script>{literal}
googletag.cmd.push(function() { googletag.display('propeller_cikk_3'); });
</script>{/literal}
</div>
        </div>
    </div>



    {if $content.link}
    <div id="banner-bottom" class="banner">
        {$env->getBanner('slave', $ad.category, 'cikkoldal', 'roadblock_also')}
    </div>
    {/if}

    {if $content.tags}
        <div class="entry-tags">
            <i class="material-icons">local_offer</i><a href="{$env->base}/{$content.category_alias}" title="{$content.category}">{$content.category}</a>, {section name=j loop=$content.tags}<a href="/tag/{$content.tags[j]|escape:'html'|replace:' ':'+'}" title="{$content.tags[j]|escape}" rel="tag">{$content.tags[j]|escape}</a>{if !$smarty.section.j.last}, {/if}{/section}
        </div>
    {/if}


<div class="fb-comments-wrapper">
    <div class="fb-comments" data-href="http://propeller.hu/{$content.category_alias}/{$content.alias}" data-num-posts="1" data-width="100%"></div>
</div>


{if isset($hirkereso)}
    <div class="hirkereso-wrapper">
    <h4 class="heading1 popular-on-web"><span>Népszerű az Interneten</span></h4>
    <div class="hirkereso">
        <ul>
        {section name=i loop=$hirkereso max=4}
        <li><a href="{$hirkereso[i].link}" rel="nofollow" class="outgoing">{$hirkereso[i].title|escape}</a></li>
        {/section}
        </ul>
    </div>
    </div>
{/if}

{if $content.category_alias == 'itthon' && !$env->isMobile}{literal}
<div id="banner-bottom" class="banner">
    <!-- csak 'itthon' rovat, csak desktop -->
    <div id="adoceangemhulejrimklxp"></div>
    <script type="text/javascript">
    /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_640x360300x250 */
    ado.slave('adoceangemhulejrimklxp', {myMaster: 'EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7' });
    </script>{/literal}
</div>
{/if}

<div class="on-frontpage-wrapper" style="padding-top:1rem;">

    <div class="topstories bottom clear">
        <div>
    {section name=i loop=$blocks.left start=0 max=3}
    <div>
        <a title="{$blocks.left[i].title|escape}" href="{$blocks.left[i].link}"{if !$blocks.left[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} style="background-image: url({$blocks.left[i].image});"></a>
        <span class="title"><a href="{$blocks.left[i].link}" title="{$blocks.left[i].title|escape}"{if !$blocks.left[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.left[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else} rel="bookmark">{$blocks.left[i].title|escape}{/if}</a></span>
    </div>
    {/section}
<!-- /21667127856/propeller_cikk_5 -->
<div id='propeller_cikk_5'>
<script>{literal}
googletag.cmd.push(function() { googletag.display('propeller_cikk_5'); });
</script>{/literal}
</div>

        </div>
    </div>

</div><!-- .on-frontpage-wrapper -->


<div id="comments" class="clear"></div>
<div class="comment-wrapper">

{if $comments[0]}
    <h4 class="heading1 comments-title"><span>{$content.comments} hozzászólás</span><span id="show-comments"><i class="material-icons">arrow_drop_down_circle</i></span></h4>

    <div class="all-comments">
    {if $best.id}
    <div class="popular-comment" style="padding-top:15px;">Legnépszerűbb hozzászólás:</div>

    <div class="comment-list">
    <div class="c-{$best.id} clear best" data-contentid="{$content.id}" data-commentid="{$best.id}" data-userid="{$best.user_id|default:'0'}">
        <a href="/{$env->l.users.url_profile}/{$best.alias}" class="avatar"><img src="{$best.avatarsrc}" width="50" height="50" alt=""></a>
        <div class="comment-right">
            <a href="/{$env->l.users.url_profile}/{$best.alias}" class="username">{$best.name|escape}</a>
            <div class="comment-text">
                {$best.comment|nl2br}
            </div>
            <div class="comment-foot">
                <a class="ca-like" title="Tetszik"><i class="material-icons">thumb_up</i><span>{if $best.likes}{$best.likes}{/if}</span></a>
                <a class="ca-reply" title="Válasz"><i class="material-icons">reply</i>Válasz</a>
                {if $smarty.session.user.admin}<a href="/admin/?op=comments&mode=edit&id={$best.id}&category_id={$content.category_id}" target="_blank">Moderálás</a>{/if}
                <a class="ca-report" title="Jelentem a moderátornak"><i class="material-icons">error</i></a>
                <a class="ca-date" href="/{$content.category_alias}/{$content.alias}#c{$best.id}"><i class="material-icons">access_time</i>{$best.date}</a>
            </div>
        </div><!-- .comment-right -->
    </div>
    </div>
    {/if}

    {if $cturn == 1}
    <div class="comment-pager">Ez itt nem minden hozzászólás, csak az utolsó néhány. <a href="/{$content.category_alias}/{$content.alias}?all#comments">Kattintson, ha mindet olvasná!</a></div>
    {elseif $cturn == 2}
    <div class="comment-pager">Ez itt az összes hozzászólás. <a href="/{$content.category_alias}/{$content.alias}#comments">Kattintson, ha csak a legfrissebbeket olvasná!</a></div>
    {/if}

    <div class="comment-list">
    {section name=i loop=$comments}
    <div id="c{$comments[i].id}" class="c-{$comments[i].id} clear" data-contentid="{$content.id}" data-commentid="{$comments[i].id}" data-userid="{$comments[i].user_id|default:'0'}">
        <a href="/{$env->l.users.url_profile}/{$comments[i].alias}" class="avatar"><img src="{$comments[i].avatarsrc}" width="50" height="50" alt=""></a>
        <div class="comment-right">
            <a href="/{$env->l.users.url_profile}/{$comments[i].alias}" class="username">{$comments[i].name|escape}</a>
            <div class="comment-text">
                {$comments[i].comment|nl2br}
            </div>
            <div class="comment-foot">
                <a class="ca-like" title="Tetszik"><i class="material-icons">thumb_up</i><span>{if $comments[i].likes}{$comments[i].likes}{/if}</span></a>
                <a class="ca-reply" title="Válasz erre"><i class="material-icons">reply</i>Válasz</a>
                {if $smarty.session.user.admin}<a href="/admin/?op=comments&mode=edit&id={$comments[i].id}&category_id={$content.category_id}" target="_blank">Moderálás</a>{/if}
                <a class="ca-report" title="Jelentem a moderátornak"><i class="material-icons">error</i></a>
                <a class="ca-date" href="/{$content.category_alias}/{$content.alias}#c{$comments[i].id}"><i class="material-icons">access_time</i>{$comments[i].date}</a>
            </div>
        </div><!-- .comment-right -->
    </div>
    {/section}
    </div><!-- .comment-list -->
    </div><!-- .all-comments -->

{/if}

    <h4 class="heading1" id="add-comment"><span>Új hozzászólás</span></h4>
    <form id="comment-form" action="/{$content.category_alias}/{$content.alias}#add-comment" method="post">
    <fieldset>
        {if !$smarty.session.user}<p style="margin-bottom: 12px;">Hozzászólás írásához regisztráció szükséges. <a href="/{$env->l.users.url_registration}"><b>Regisztráljon</b></a> vagy használja a <a href="/{$env->l.users.url_login}?redirect=/{$content.category_alias}/{$content.alias}" rel="nofollow"><b>belépést</b></a>!</p>{/if}
        {if isset($error)}<p class="error">{$env->l.content.$error}</p>{/if}

        <input type="hidden" name="action" value="comment">
        <input type="hidden" name="id" value="{$content.id}">{if $content.user_id}
        <input type="hidden" name="comment[owner_uid]" value="{$content.user_id}">
        {/if}

        <textarea name="comment[comment]" id="comment" rows="6" cols="18" class="text required" title="{$env->l.content.error_nocomment}">{$form.comment|escape}</textarea><br class="clear">
        <input type="submit" id="submit" class="primary" value="{$env->l.button.comment}"><span id="comment-counter-wrapper">Még <span id="comment-counter"></span> karakter írhat</span><span class="moderalas">Elolvastam és elfogadom a <a href="/statikus/terms" rel="nofollow" target="_blank">moderálási elveket</a>.</span>

    </fieldset>
    </form>
</div>


{if !$smarty.session.user}
<h4 class="heading1" style="margin-top:20px;"><span>Ingyenes hírlevél</span></h4>

<div class="subscribebox">

    <form class="subscribe-form" action="http://propeller.hu/hirlevel/subscribe" method="post" target="_blank" novalidate>
        <p class="desc">
        <b>Iratkozzon fel a Propeller ingyenes hírlevelére</b>! Naponta elküldjük Önnek
        a legfontosabb híreket egyenesen az email fiókjába! Ha nem tetszik, később bármikor leiratkozhat.</p>

        <div class="field-group">
            <label for="mail">Név:</label>
            <input type="text" value="" name="mail_name" class="text" id="mm-name">
            <label for="mail">Email:</label>
            <input type="email" value="" name="mail" class="text" id="mm-mail"><input type="submit" value="Feliratkozás" class="primary">
        </div>

        <div style="position:absolute; left:-9999px;"><input type="text" name="f_d1926beb3e866034a45b5cb4dcef0cbb" value=""></div>
    </form>
</div><!-- .subscribebox -->
{/if}




<div class="on-frontpage-wrapper">
    <h4 class="heading1 on-frontpage"><span>További ajánlatok</span></h4>

    <div class="topstories bottom clear">
        <div>
    {section name=i loop=$blocks.right start=3 max=3}
    <div>
        <a title="{$blocks.right[i].title|escape}" href="{$blocks.right[i].link}"{if !$blocks.right[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} style="background-image: url({$blocks.right[i].image});"></a>
        <span class="title"><a href="{$blocks.right[i].link}" title="{$blocks.right[i].title|escape}"{if !$blocks.right[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.right[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else} rel="bookmark">{$blocks.right[i].title|escape}{/if}</a></span>
    </div>
    {/section}

        </div>
    </div>
</div><!-- .on-frontpage-wrapper -->

<div class="clear"></div>
</div>
</main><!-- .main -->
</article><!-- .hentry -->

<aside class="sidebar" role="complementary">
<div>

    {include file="./sidebar.tpl"}

    <div class="list-box commented-box">
    <h4 class="heading1"><span>Most beszédtéma</span></h4>
    <ul>
        {section name=i loop=$commented max=8}
        <li id="c_{$commented[i].id}"><span>{$commented[i].datetime|date_format:"%H:%M"}</span><a href="{$env->base}/{$commented[i].link}#v{$commented[i].id}">{$commented[i].title|escape}</a></li>
        {/section}
    </ul>
    </div>

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}