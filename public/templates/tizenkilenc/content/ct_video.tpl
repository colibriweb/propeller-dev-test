{include file="./header.tpl" title="`$category.title``$titleplus`"}

<main class="main" role="main">
<div>

	<h1>
		<span><a href="/{$category.alias}">{if $tagtitle}{$tagtitle|escape}{else}{$category.title}{/if}</a></span>
	</h1>

	<div class="content-list">
		{include file="./content/_content_list.tpl"}
	</div>

	<div class="clear"></div>

	{if $p.paging_all > 1}
	<ul class="pagination">
		<li> </li>
		<li>{if $p.current_page == 1}<span>Előző</span>{else}<a href="/{$category.alias}?page={$p.current_page-1}">Előző</a>{/if}</li>
		{section name=pagination start=$p.paging_from loop=$p.paging_to+1}
			<li{if $smarty.section.pagination.index == $p.current_page} class="active"{/if}><a href="/{$category.alias}?page={$smarty.section.pagination.index}">{$smarty.section.pagination.index}</a></li>
		{/section}
		<li>{if $p.current_page == $p.paging_all}<span>Következő</span>{else}<a href="/{$category.alias}?page={$p.current_page+1}">Következő</a>{/if}</li>
	</ul>
	{/if}

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

	{include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}