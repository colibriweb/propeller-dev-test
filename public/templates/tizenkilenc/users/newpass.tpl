{include file="./header.tpl" title=$env->l.users.lostpass}

<main class="main" role="main">
<div>

	<h1><span>{$env->l.users.newpass}</span></h1>
	{if isset($smarty.get.ok)}
		<p class="lead">{$env->l.users.newpasslead}</p>
		<br />

		<p class="lead">
		<a href="{$env->l.users.url_login}{if isset($smarty.session.newpass_mail)}?mail={$smarty.session.newpass_mail}{/if}">
		<strong>{$env->l.users.gotologin}</strong></a>
		</p>
	{else}
		{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

		<p class="lead">{$env->l.users.newpasslead_error}</p>
	{/if}

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}
