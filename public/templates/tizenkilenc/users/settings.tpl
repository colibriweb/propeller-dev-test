{include file="./header.tpl" title=$env->l.users.settings}

<main class="main" role="main">
<div>

	<h1><span><a href="/{$env->l.users.url_profile}/{$user.alias}"><img src="{$user.avatarsrc}" style="margin-right:.4em;display:inline;" width="32" height="32" alt="">{$user.name|escape}</a></span></h1>

	<ul class="submenu">
		<li><a href="/{$env->l.users.url_profile}/{$user.alias}">Hozzászólások</a></li><!--
		--><li><a href="/{$env->l.users.url_profile}/{$user.alias}/informaciok">Adatlap</a></li><!--
		--><li><a href="/{$env->l.users.url_profile}/{$user.alias}/bekuldott">Beküldött hírek</a></li><!--
		{if $user.id == $smarty.session.user.id}
		--><li><a href="/{$env->l.users.url_settings}"{if $env->u[1] == $env->l.users.url_settings} class="active"{/if}>{$env->l.users.settings}</a></li><!--
		{else}
		--><li><a href="/{$env->l.users.url_messages}/{$user.alias}">Új személyes üzenet</a></li><!--
		{/if}
	--></ul>


	{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

	<p class="lead">{$env->l.users.settingslead}</p>

	<form id="form" action="{$env->l.users.url_settings}" method="post" enctype="multipart/form-data">

	<fieldset>

		<input type="hidden" name="action" value="do" />
		<input type="hidden" name="user[city_id]" value="{$user.city_id|escape}" id="city_id" />
	{*
		<label for="lastname">{$env->l.users.lastname}:</label>
		<input type="text" id="lastname" class="text" name="user[lastname]" maxlength="64" value="{$user.lastname|escape}" tabindex="1" /><br class="clear" />

		<label for="firstname">{$env->l.users.firstname}:</label>
		<input type="text" id="firstname" class="text" name="user[firstname]" maxlength="64" value="{$user.firstname|escape}" tabindex="2" /><br class="clear" />
	*}


		<label for="profession">{$env->l.users.profession}:</label>
		<input type="text" id="profession" class="text" name="user[profession]" maxlength="64" value="{$user.profession|escape}" tabindex="6" /><br class="clear" />

		<label for="year">{$env->l.users.birthdate}:</label>

		<select id="year" name="user[year]" style="width: 80px;" tabindex="3">
		<option label="{$env->l.year}" value="">{$env->l.year}</option>
		{html_options values=$year output=$year selected=$user.birthyear}</select>

		<select id="month" name="user[month]" style="width: 110px;" tabindex="4">
		<option label="{$env->l.month}" value="">{$env->l.month}</option>
		{html_options values=$month output=$env->l.months selected=$user.birthmonth}</select>

		<select id="day" name="user[day]" style="width: 60px;" tabindex="5">
		<option label="{$env->l.day}" value="">{$env->l.day}</option>
		{html_options values=$day output=$day selected=$user.birthday}</select><br class="clear" />

		<label for="city-change">{$env->l.users.city}:</label>
		<input type="text" id="city-change" class="text" name="user[city]" value="{$user.city|escape}" tabindex="7" /><br class="clear" />

		<label for="website">{$env->l.users.website}:</label>
		<input type="text" id="website" class="text required" name="user[website]" maxlength="250" value="{if empty($user.website)}http://{else}{$user.website|escape}{/if}" /><br class="clear" />

		<label for="motto">{$env->l.users.motto}:</label>
		<textarea name="user[motto]" id="motto" rows="6" cols="18" class="text">{$user.motto|escape}</textarea><br class="clear" />

		<label for="t_like">{$env->l.users.t_like}:</label>
		<textarea name="user[t_like]" id="t_like" rows="6" cols="18" class="text">{$user.t_like|escape}</textarea><br class="clear" />

		<label for="t_dislike">{$env->l.users.t_dislike}:</label>
		<textarea name="user[t_dislike]" id="t_dislike" rows="6" cols="18" class="text">{$user.t_dislike|escape}</textarea><br class="clear" />

		<label for="topics">{$env->l.users.topics}:</label>
		<textarea name="user[topics]" id="topics" rows="6" cols="18" class="text">{$user.topics|escape}</textarea><br class="clear" />

		<!--label for="politics">{$env->l.users.politics}:</label>
		<select id="politics" class="item-list" name="user[politics]" onchange="togglePolOther(this.value);">
		<option label="- {$env->l.users.notfilled} -" value=""{if empty($user.politics)} selected="selected"{/if}>- {$env->l.users.notfilled} -</option>
		<option label="{$env->l.users.pol1}" value="1"{if $user.politics == '1'} selected="selected"{/if}>{$env->l.users.pol1}</option>
		<option label="{$env->l.users.pol2}" value="2"{if $user.politics == '2'} selected="selected"{/if}>{$env->l.users.pol2}</option>
		<option label="{$env->l.users.pol3}" value="3"{if $user.politics == '3'} selected="selected"{/if}>{$env->l.users.pol3}</option>
		<option label="{$env->l.users.pol4}" value="4"{if $user.politics == '4'} selected="selected"{/if}>{$env->l.users.pol4}</option>
		<option label="{$env->l.users.pol5}" value="5"{if $user.politics == '5'} selected="selected"{/if}>{$env->l.users.pol5}</option>
		<option label="{$env->l.users.pol6}" value="6"{if $user.politics == '6'} selected="selected"{/if}>{$env->l.users.pol6}</option>
		<option label="{$env->l.users.pol7}" value="7"{if $user.politics == '7'} selected="selected"{/if}>{$env->l.users.pol7}</option>
		<option label="Egyéb..." value="other"{if !empty($user.politics) && $user.politics != '1' && $user.politics != '2' && $user.politics != '3' && $user.politics != '4' && $user.politics != '5' && $user.politics != '6' && $user.politics != '7'} selected="selected"{/if}>Egyéb...</option>

		</select><br class="clear" /-->

		<div id="politics-wrapper"{if empty($user.politics) || $user.politics == '1' || $user.politics == '2' || $user.politics == '3' || $user.politics == '4' || $user.politics == '5' || $user.politics == '6' || $user.politics == '7'} style="display: none;"{/if}>
			<label for="politicsother">&nbsp;</label>
			<input type="text" id="politicsother" class="text" name="user[politicsother]" maxlength="64" value="{if !empty($user.politics) && $user.politics != '1' && $user.politics != '2' && $user.politics != '3' && $user.politics != '4' && $user.politics != '5' && $user.politics != '6' && $user.politics != '7'}{$user.politics|escape}{/if}" /><br class="clear" />
		</div>

		<label id="account-details-toggle"><a onclick="toggle('account-details');">{$env->l.users.changeacc}</a></label>
		<div class="clear"></div>
		<div id="account-details" style="display: none;">

			<label for="mail">{$env->l.users.mail}:</label>
			<input type="text" id="mail" class="text" name="user[mail]" value="{$user.mail|escape}"  disabled="disabled" /><p class="note">Az e-mail cím nem módosítható. Megváltoztatásáért írjon az info@propeller.hu címre!</p>


			<label for="pass">Új jelszó:</label>
			<input type="password" autocomplete="off" value="" id="pass" class="text" name="user[pass]" maxlength="32" title="{$env->l.users.help_pass}" tabindex="10" /><br class="clear" />

			<label for="repass">Új jelszó ismét:</label>
			<input type="password" autocomplete="off" value="" id="repass" class="text" name="user[repass]" maxlength="32" title="{$env->l.users.help_repass}" tabindex="11" /><br class="clear" />

		</div>


		<label id="avatar-change-toggle"><a onclick="toggle('avatar-change');">{$env->l.users.changeavatar}</a></label>
		<div class="clear"></div>
		<div id="avatar-change" style="display: none;">
			<label for="avatar">{$env->l.users.avatarpath}:</label>
			<input type="file" accept="image/gif, image/png, image/jpeg, image/jpg" id="avatar" name="avatar" class="text" size="30" style="margin-bottom:0;">
			<p class="note">A feltöltendő profilkép JPG, GIF, vagy PNG formátumú legyen, lehetőleg négyzet alakú!</p>
		</div>

		<div class="checkbox-wrapper">
			<label for="notify"><input type="checkbox" id="notify" class="radio" name="user[notify]" value="1" {if $user.notify}checked="checked" {/if}tabindex="15" />{$env->l.users.notify}</label>
			<label for="notify_act"><input type="checkbox" id="notify_act" class="radio" name="user[notify_act]" value="1" {if $user.notify_act}checked="checked" {/if}tabindex="16" />Kérek e-mail értesítést az aktivitásokról</label>
		</div>

		<div class="button-wrapper">
			<input type="submit" id="submit" class="primary" value="{$env->l.button.save}" tabindex="18" />
		</div>

	</fieldset>
	</form>

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}