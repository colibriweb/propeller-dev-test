{include file="./header.tpl" title=$env->l.users.lostpass}

<main class="main" role="main">
<div>

	<h1><span>{$env->l.users.lostpass}</span></h1>
	{if isset($smarty.get.ok)}
	<p class="lead">{$env->l.users.lostpasslead_ok}</p>

	{else}
	{if isset($error)}<p class="error">{$env->l.users.$error}</p>{/if}

	<p class="lead">{$env->l.users.lostpasslead}</p>

	<form id="form" action="{$env->l.users.url_lostpass}" method="post">
	<fieldset>

		<input type="hidden" name="action" value="do" />

		<label for="mail">{$env->l.users.mail}:</label>
		<input type="text" id="mail" class="text required validate-email" name="user[mail]" maxlength="255" value="{if $form.mail}{$form.mail|escape}{else}{$smarty.get.mail|escape}{/if}" title="{$env->l.users.help_loginmail}" tabindex="1" /><br class="clear" />

		<div class="button-wrapper">
			<input type="submit" id="submit" class="primary" value="{$env->l.button.send}" tabindex="3" />
		</div>

	</fieldset>

	</form>
	{/if}

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}
