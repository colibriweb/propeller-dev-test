{include file="./header.tpl" title=$env->l.content.add}

<main class="main" role="main">
<div>

	<h1><span>{$env->l.content.add}</span></h1>

	{if isset($error)}<p class="error">{$env->l.content.$error}</p>{/if}

	<p class="lead">{$env->l.content.addlead}</p>

	<form id="form" action="/{$env->l.content.url_add}" method="post">
	<fieldset>

		<input type="hidden" id="action" name="action" value="{$action}" />

		<label for="link">{$env->l.content.link}:</label>
		<input type="text" id="add-link" class="text required validate-url" name="content[link]" maxlength="200" value="{if isset($form.link)}{$form.link|escape}{elseif isset($smarty.get.link)}{$smarty.get.link|escape}{else}{/if}" title="{$env->l.content.help_link}" tabindex="1" /><br class="clear" />

		<div id="add-loading"></div>

		<label for="title">{$env->l.content.title}:</label>
		<input type="text" id="add-title" class="text required" name="content[title]" maxlength="200" value="{if isset($form.title)}{$form.title|escape}{elseif isset($smarty.get.title)}{$smarty.get.title|escape}{/if}" title="{$env->l.content.help_title}" tabindex="2" /><br class="clear" />

		<label for="description">{$env->l.content.description}:</label>
		<textarea id="add-description" rows="6" cols="20" class="text required" style="height:180px;" name="content[description]" title="{$env->l.content.help_description}" tabindex="3">{if isset($form.description)}{$form.description|escape}{/if}</textarea><br class="clear" />

		<label for="category_id">{$env->l.content.category}:</label>
		<select id="category_id" class="required" name="content[category_id]" title="{$env->l.content.help_category}" tabindex="4">
		<option label="{$env->l.content.choose}" value="">{$env->l.content.choose}</option>
		{section name=i loop=$categories}
		<option label="{$categories[i].category|escape}" value="{$categories[i].id}"{if $form.category_id == $categories[i].id} selected="selected"{/if}>{$categories[i].category|escape}</option>
		{/section}
		</select><br class="clear" />

		<label for="tags">{$env->l.content.tags} vesszővel elválasztva:</label>
		<input type="text" id="add-tags" class="text" name="content[tags]" maxlength="255" value="{if isset($form.tags)}{$form.tags|escape}{/if}" title="{$env->l.content.help_tags}" tabindex="5" />

		<p class="note">{$env->l.content.notecontenttext}</p>

		<div class="button-wrapper">
			<input type="submit" id="submit" class="primary" value="{$env->l.button.add}" tabindex="6" />
		</div>

	</fieldset>
	</form>


<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

	<div class="list-box">
	<h4 class="heading1"><span>Így működik</span></h4>
		Másolja be a hír linkjét (ha van), írja be a címet, a szöveget, válassza ki a megfelelő rovatot és
		néhány jellemző címkét, majd küldje be a hírt! Azonnal megjelenik.
	</div>

	<div class="list-box">
	<h4 class="heading1"><span>Hány hírt küldhetek?</span></h4>
		Óránként meghatározott számú hír küldhető.
		<br><br>

		<div class="alert">{if $content_num > 0}Ön még <b style="background:#888;padding:0 2px;color:#fff;">{$content_num}</b> hírt küldhet ebben az órában.{else}
		<b style="background:#888;padding:0 2px;color:#fff;">Elérte a limitet</b>, legközelebb a következő
		órában küldhet hírt. Köszönjük türelmét!{/if}</div>
	</div>

	<div class="list-box">
	<h4 class="heading1"><span>Van egy blogja?</span></h4>
		Helyezze el a Propeller mindig friss híreket szállító hírdobozát a blogján, és cserébe a legjobb
		beküldéseit mi is kiemeljük! <a href="/statikus/doboz"><b>Részletek...</b></a>
	</div>

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}