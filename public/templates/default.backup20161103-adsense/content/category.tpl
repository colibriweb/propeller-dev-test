{include file="./header.tpl" title="`$category.title``$titleplus`"}

<main class="main" role="main">
<div>

	<h1>
		<span>{if $tagtitle}<a href="{$env->base}/{$category.alias}/{$env->u[2]|escape:'html'|replace:' ':'+'}">{$tagtitle|truncate:55:"..."|escape}</a>
		{else}<a href="{$env->base}/{$category.alias}">{$category.title} – friss hírek</a>{/if}</span>
	</h1>

	{if !$list}<!-- rovatcímlap -->
	<div class="col left">
		{section name=i loop=$blocks_cat max=25}
		<div class="block block-{$smarty.section.i.index+1}{if $blocks_cat[i].image} block-img{else} block-noimg{/if}">
			{if $blocks_cat[i].image}<a href="{$blocks_cat[i].link}"{if !$blocks_cat[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} title="{$blocks_cat[i].title|escape}" style="background-image: url({$blocks_cat[i].image});"></a>{/if}

			<h2><a href="{$blocks_cat[i].link}"{if !$blocks_cat[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks_cat[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks_cat[i].title|escape}{/if}</a></h2>
			{if $blocks_cat[i].description}<p>{$blocks_cat[i].description|strip_tags|escape}</p>{/if}

			{if $blocks_cat[i].sublink}<ul>
			{section name=j loop=$blocks_cat[i].sublink}
			<li><a href="{$blocks_cat[i].sublink[j].link}"{if !$blocks_cat[i].sublink[j].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks_cat[i].sublink[j].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks_cat[i].sublink[j].title|escape}{/if}</a></li>
			{/section}
			</ul>
			{/if}
		</div>
		{/section}
	</div><!-- .left -->


	<div class="col right">

		<div class="list-box">
		<h4 class="heading1"><span>Most beszédtéma</span></h4>
		<ul>
			{section name=i loop=$commented max=8}
			<li id="c_{$commented[i].id}"><a href="{$env->base}/{$commented[i].link}#v{$commented[i].id}" rel="bookmark">{$commented[i].title|escape}</a></li>
			{/section}
		</ul>
		</div>

		<div class="list-box partner-box">
		<h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain={$feeds[0].domain}" alt="">{$feeds[0].source} hírek</span></h4>
		<ul>
			{section name=i loop=$feeds start=0 max=7}
			<li><a href="{$env->base}/{$feeds[i].category_alias}/{$feeds[i].alias}">{$feeds[i].title|truncate:98:"..."|escape}</a></li>
			{/section}
		</ul>
		</div>

		{if $feeds[8]}
		<div class="list-box partner-box">
		<h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain={$feeds[8].domain}" alt="">{$feeds[8].source} hírek</span></h4>
		<ul>
			{section name=i loop=$feeds start=8 max=7}
			<li><a href="{$env->base}/{$feeds[i].category_alias}/{$feeds[i].alias}">{$feeds[i].title|truncate:98:"..."|escape}</a></li>
			{/section}
		</ul>
		</div>
		{/if}

		{if $feeds[16]}
		<div class="list-box partner-box">
		<h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain={$feeds[16].domain}" alt="">{$feeds[16].source} hírek</span></h4>
		<ul>
			{section name=i loop=$feeds start=16 max=7}
			<li><a href="{$env->base}/{$feeds[i].category_alias}/{$feeds[i].alias}">{$feeds[i].title|truncate:98:"..."|escape}</a></li>
			{/section}
		</ul>
		</div>
		{/if}

		{if $feeds[24]}
		<div class="list-box partner-box">
		<h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain={$feeds[24].domain}" alt="">{$feeds[24].source} hírek</span></h4>
		<ul>
			{section name=i loop=$feeds start=24 max=7}
			<li><a href="{$env->base}/{$feeds[i].category_alias}/{$feeds[i].alias}">{$feeds[i].title|truncate:98:"..."|escape}</a></li>
			{/section}
		</ul>
		</div>
		{/if}

		<div class="list-box tags-box">
		<h4 class="heading1"><span>Fontosabb témák</span></h4>
		<ul>
			<li>
			{section name=i loop=$tags}
			<a href="{$env->base}/tag/{$tags[i].tag|escape:'html'|replace:' ':'+'}" style="font-size:{$tags[i].size}px;line-height:{$tags[i].size+10}px;" rel="tag">{$tags[i].tag|escape}</a>
			{/section}
			</li>
		</ul>
		</div>

		<br>
        <a href="{$env->base}/{$category.alias}/friss" style="font-weight:600;">Tovább az összes friss hírhez...</a>
	</div><!-- .right -->


	{else}<!-- minden friss hír vagy címke oldal -->

		<div class="content-list">
			{include file="./content/_content_list.tpl"}
		</div>

		<div class="clear"></div>

		{if $p.paging_all > 1}
		<ul class="pagination">
			<li> </li>
			<li>{if $p.current_page == 1}<span>Előző</span>{else}<a href="{$env->base}/{$category.alias}/{if !empty($env->u[2])}{$env->u[2]|escape:'html'|replace:' ':'+'}{/if}?page={$p.current_page-1}">Előző</a>{/if}</li>
			{section name=pagination start=$p.paging_from loop=$p.paging_to+1}
				<li{if $smarty.section.pagination.index == $p.current_page} class="active"{/if}><a href="{$env->base}/{$category.alias}/{if !empty($env->u[2])}{$env->u[2]|escape:'html'|replace:' ':'+'}{/if}?page={$smarty.section.pagination.index}">{$smarty.section.pagination.index}</a></li>
			{/section}
			<li>{if $p.current_page == $p.paging_all}<span>Következő</span>{else}<a href="{$env->base}/{$category.alias}/{if !empty($env->u[2])}{$env->u[2]|escape:'html'|replace:' ':'+'}{/if}?page={$p.current_page+1}">Következő</a>{/if}</li>
		</ul>
		{/if}

	{/if}



	<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

	{include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}