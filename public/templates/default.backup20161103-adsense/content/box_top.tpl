
{if $top}
<div class="list-box popular-box">
<h4 class="heading1"><span>Top hírek</span></h4>
<ul>
    {section name=i loop=$top max=8}
    <li><span><b style="background:rgba(246,17,0,0.99{*math equation="-1 * (x*5 - 90)" x=$smarty.section.i.index*})">{$smarty.section.i.index+1}</b></span><a href="{$env->base}/{$top[i].category_alias}/{$top[i].alias}">{$top[i].title|truncate:98:"..."|escape}</a></li>
    {/section}
</ul>
</div>
{/if}

{if isset($smarty.get.miniad)}
<!-- Goa3 beépítés: propeller.hu kep + szoveg 1., 3765609 -->
<div id="zone3765609" class="goAdverticum"></div>
<!-- A g3.js-t oldalanként egyszer, a </body> zárótag elõtt kell meghívni -->
{/if}
