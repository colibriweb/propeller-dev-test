{include file="./header.tpl" title=$env->l.users.logout}

<main class="main" role="main">
<div>

	<h1><span>{$env->l.users.logout}</span></h1>

	<p class="lead">{$env->l.users.logoutlead}</p>

<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

    {assign var="short_sidebar" value=true}
    {include file="./sidebar.tpl"}

</div>
</aside><!-- .sidebar -->

{include file="./footer.tpl"}
