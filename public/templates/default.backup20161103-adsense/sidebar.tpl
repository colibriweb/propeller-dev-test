
{if $SCRIPT_NAME == '/modules/content/home.php'}

    {include file="./content/box_latest.tpl"}

    <div id="banner-sidebar" class="banner">
        <!-- propeller-sidebar -->
        <ins class="adsbygoogle"
            data-ad-client="ca-pub-6617051563727120"
            data-ad-slot="9332679292"
            data-override-format="true"></ins>
        <script>
        {literal}(adsbygoogle = window.adsbygoogle || []).push({});{/literal}
        </script>
    </div>

    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=168ora.hu" alt="168ora.hu">168 óra</span></h4>
    <ul>
        {section name=p loop=$partners start=0 max=5}
        <li><a href="{$partners[p].link}" rel="nofollow" class="outgoing">{$partners[p].title|truncate:98:"..."|escape}</a></li>
        {/section}
    </ul>
    </div>


    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=hvg.hu" alt="hvg.hu">HVG</span></h4>
    <ul>
        {section name=p loop=$partners start=10 max=3}
        <li><a href="{$partners[p].link}" rel="nofollow" class="outgoing">{$partners[p].title|truncate:98:"..."|escape}</a></li>
        {/section}
    </ul>
    </div>

    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=abcug.hu" alt="abcug.hu">Abcúg</span></h4>
    <ul>
        {section name=p loop=$partners start=20 max=3}
        <li><a href="{$partners[p].link}" rel="nofollow" class="outgoing">{$partners[p].title|truncate:98:"..."|escape}</a></li>
        {/section}
    </ul>
    </div>

    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=nemzetisport.hu" alt="nemzetisport.hu">Nemzeti Sport</span></h4>
    <ul>
        {section name=p loop=$partners start=30 max=3}
        <li><a href="{$partners[p].link}" rel="nofollow" class="outgoing">{$partners[p].title|truncate:98:"..."|escape}</a></li>
        {/section}
    </ul>
    </div>


    {assign var="topstories_first_big" value=true}
{else}<!-- rovatoldal, bármi más (cikkoldal kivételével) -->

    <div id="banner-sidebar" class="banner">
        <!-- propeller-sidebar -->
        <ins class="adsbygoogle"
            data-ad-client="ca-pub-6617051563727120"
            data-ad-slot="9332679292"
            data-override-format="true"></ins>
        <script>
        {literal}(adsbygoogle = window.adsbygoogle || []).push({});{/literal}
        </script>
    </div>

    {if !$short_sidebar}
        {include file="./content/box_latest.tpl"}
    {/if}

    {assign var="topstories_all_big" value=true}

{/if}

{if !$short_sidebar}

    {include file="./content/box_topstories.tpl"}

{/if}


{if $SCRIPT_NAME == '/modules/content/home.php'}
    {include file="./content/box_top.tpl"}
{/if}

<div class="list-box" style="padding-top:0;text-align:center;">
    <div class="fb-page" data-href="https://www.facebook.com/propellerhu" data-hide-cta="true" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"></div>
</div>

