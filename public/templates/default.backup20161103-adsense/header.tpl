<!DOCTYPE html>
<html lang="hu">
<head>
<meta charset="utf-8">
<title>{if $activities.num > 0}({$activities.num}) {/if}{if $title}{$title|escape}{if isset($p.current_page) && $p.current_page != 1} ({$p.current_page}. oldal){/if} - {/if}{$env->l.title}{if $SCRIPT_NAME == '/modules/content/home.php'} - Friss hírek{/if}</title>
<meta name="description" content="{if $content.description}{$content.description|strip_tags|truncate:170:"..."|replace:"\n":""|replace:"   ":" "|replace:"  ":" "|trim|escape}{elseif $tagtitle}&quot;{$tagtitle|escape}&quot; témájú friss hírek {$p.results_num} forrásból. - {$list[0].description|strip_tags|truncate:280:"..."|escape}{else}Friss hírek, fontos témák - kritikus politikai-közéleti újság 2008 óta. Azoknak, akiket még érdekel, mi történik az országban és a világban.{/if}">
{if ($SCRIPT_NAME == '/modules/content/content.php') && $content.tags}<meta name="keywords" content="{section name=j loop=$content.tags}{$content.tags[j]|escape}{if !$smarty.section.j.last}, {/if}{/section}">
{/if}
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800%7COpen+Sans+Condensed:700&amp;subset=latin,latin-ext">
<link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="{$smarty.const.STTC}/css/style.{$smarty.const.BUILD}.css">
<link rel="icon" href="{$smarty.const.STTC}/images/default/apple-touch-icon.{$smarty.const.BUILD}.png">
<link rel="apple-touch-icon" href="{$smarty.const.STTC}/images/default/apple-touch-icon.{$smarty.const.BUILD}.png">
{if ($SCRIPT_NAME == '/modules/content/content.php') && isset($content.id)}<link rel="canonical" href="{$env->base}/{$content.category_alias}/{$content.alias}">
{/if}
{if ($SCRIPT_NAME == '/modules/content/content.php') && $content.link}
{/if}
{if isset($noindex)}<meta name="robots" content="noindex, follow">
{/if}
{if ($SCRIPT_NAME == '/modules/content/content.php') && $content.link}<meta name="Googlebot-News" content="noindex, nofollow">
{/if}
{if $SCRIPT_NAME == '/modules/content/add.php'}<link rel="canonical" href="{$env->base}/bekuldes">
{/if}
{if $env->u[1] == 'belepes'}<link rel="canonical" href="{$env->base}/belepes">
{/if}
{if $SCRIPT_NAME == '/modules/content/home.php'}<meta http-equiv="refresh" content="1200">
{/if}
{if ($SCRIPT_NAME == '/modules/content/content.php') && isset($content.id)}<meta property="og:type" content="article">
<meta property="og:title" content="{$title|escape}">
<meta property="og:description" content="{if ($SCRIPT_NAME == '/modules/content/content.php')}{$content.description|strip_tags|truncate:100:"..."|replace:"\n":""|trim|escape} {/if}Kattintson a részletekért vagy ossza meg!">
<meta property="og:url" content="{$env->base}/{$content.category_alias}/{$content.alias}">
{/if}
<meta property="og:image" content="{if $bigpicture}{$smarty.const.STTC}/images/bigpicture/{$img[0].filepath}{elseif $share_image}{$share_image}{else}{$smarty.const.SHARE_IMAGE}{/if}">
<meta property="fb:app_id" content="247823682023773">
<meta property="fb:pages" content="244064559374">
{if ($SCRIPT_NAME == '/modules/content/content.php') && $content.tags}
{section name=j loop=$content.tags}<meta property="article:tag" content="{$content.tags[j]|escape}">
{/section}
<meta property="article:publisher" content="https://www.facebook.com/propellerhu">
<meta property="article:published_time" content="{$content.date_c}">
{section name=j loop=$content.authors}{if $content.authors[j].facebook_url}<meta property="article:author" content="{$content.authors[j].facebook_url}">{/if}
{/section}

{/if}
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
<link rel="alternate" type="application/rss+xml" href="{$env->base}/feed/content-sajat.xml">

{literal}<script type="text/javascript" src="//gemhu.adocean.pl/files/js/ado.js"></script>
<script type="text/javascript">
/* (c)AdOcean 2003-2016 */
    if(typeof ado!=="object"){ado={};ado.config=ado.preview=ado.placement=ado.master=ado.slave=function(){};}
    ado.config({mode: "old", xml: false, characterEncoding: true});
    ado.preview({enabled: true, emiter: "gemhu.adocean.pl", id: "uESaTC3oo4RWFYK0Pi7N40dYf6fwGW9odHhhEhar3_7.p7"});
</script>{/literal}

<base href="{$env->base}">
</head>
<body class="default{if isset($is_adult)} is-adult{/if}{if
    isset($body_404)} static{elseif $SCRIPT_NAME
    == '/modules/content/content.php'} content content-{$content.id|default:'0'}{if $content.link} content-rovid{else} content-sajat{/if}{if $content.fullembed} content-video{/if}{if $content.tags && in_array('a szerk.', $content.tags)} tag-a-szerk{/if}{elseif $SCRIPT_NAME
    == '/modules/content/add.php'} bekuldes{elseif $SCRIPT_NAME
    == '/modules/content/home.php'} home{elseif $SCRIPT_NAME
    == '/modules/pages/index.php'} static{elseif $env->u[1]
    == 'tag'} tag{elseif $env->u[1]
    == 'video'} video category{elseif $env->u[1]
    == $env->l.users.url_messages} messages{elseif $env->u[1]
    == $env->l.users.url_settings} settings{elseif $SCRIPT_NAME
    == '/modules/content/category.php'} category{/if}">
<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
{literal}<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6617051563727120",
    enable_page_level_ads: true
  });
</script>{/literal}
<script>{literal}
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');{/literal}
var loc = (window.location.search.indexOf('fb_') != -1) ? window.location.pathname : window.location.pathname+window.location.search;
ga('create', 'UA-80777149-1', 'auto');
ga('create', 'UA-241542-5', 'auto', {literal}{'name': 'regi'}{/literal});
ga('send', 'pageview', loc);
ga('regi.send', 'pageview', loc);
ga('set', 'dimension1', '{if $smarty.session.user}Member{else}Visitor{/if}');
{if ($SCRIPT_NAME == '/modules/content/content.php')}
ga('set', 'contentGroup1', '{if !$content.link}Saját tartalom{else}RSS tartalom{/if}');
{/if}
if(top!=self) top.location.replace(document.location);
</script>

<div id="wrapper">

{*if $smarty.session.user}
{if !$env->newPolicyAnswer($smarty.session.user.mail)}
    <div style="background:#fff9cb;font-family:Arial,Helvetica,sans-serif;width:100%;padding:15px 15px 30px;margin:10px 0 20px 0;font-size:13px;">
Tisztelt Felhasználónk!<br><br>

A Propellert a jövőben az <a href="http://ikodigital.hu/" target="_blank">IKO Digital Kft</a>. adja ki, emiatt
a weboldal adatvédelmi tájékoztatója megváltozott és a weboldalt a továbbiakban csak ennek megerősítésével
lehet használni. A módosított szöveget itt olvashatod el részletesen: <a
href="http://propeller.hu/statikus/adatvedelmi-tajekoztato"
target="_blank">http://propeller.hu/statikus/adatvedelmi-tajekoztato</a>.<br>
A módosítás 2016. szeptember 15-én lép hatályba.<br><br>

Bízunk abban, hogy a Propeller regisztrált felhasználója szeretnél maradni a jövőben is, és ha így van, akkor
arra kérünk, kattints a „megerősítem” gombra, ezzel kijelented, hogy a propeller.hu weboldal módosított
adatvédelmi tájékoztatóját elolvastad, megértetted, azt magadra nézve kötelezőnek fogadtad el, az adataidnak
a módosított adatvédelmi tájékoztató szerinti kezeléséhez kifejezetten hozzájárultál.<br><br>

Amennyiben nem erősíted meg, hogy elfogadtad volna a módosított adatvédelmi tájékoztatót, akkor azt úgy
értelmezzük, hogy nem járultál hozza az adataid további kezeléséhez, ebben az esetben <b>15 napon belül
töröljük a felhasználói fiókodat, és az adataidat</b>.<br><br>

Köszönjük!<br><br>

A szerkesztőség

        <div style="text-align:center;padding-top:20px;">
            <a href="http://propeller.hu/?new_policy={$smarty.session.user.mail}&hash={$env->getNewPolicyHash($smarty.session.user.mail)}" style="padding:.6em 2em;background:#2ba700;border-radius:3px;color:#fff;font-size:18px;font-weight:bold;text-transform:uppercase;margin:5px 5px 5px 0;text-decoration:none;">Megerősítem</a>
        </div>

    </div>
{/if}
{/if*}

    <header class="site-header">
    <div>
        <div class="site-title"><a rel="home" href="/"><span>Propeller</span></a></div>

        <div class="site-tools">
            <ul><!--
            {if !$smarty.session.user}
                --><li class="newsletter"><a href="http://propeller.hu/hirlevel/subscribe" rel="nofollow" target="_blank"><i class="material-icons">email</i><span>Hírlevél</span></a></li><!--
                --><li class="registration"><a href="/{$env->l.users.url_registration}"><i class="material-icons">account_circle</i><span>{$env->l.users.registration}</span></a></li><!--
                --><li class="login"><a href="/{$env->l.users.url_login}"><i class="material-icons">lock</i><span>Belépés</span></a></li><!--
                --><li class="bekuldes"><a href="/{$env->l.content.url_add}"><i class="material-icons">add_circle</i><span>Beküldés</span></a></li><!--
            {else}
                --><li class="activities"><a id="activity" title="Értesítések"{if $activities.num == 0} style="display:none;"{/if}><i class="material-icons">public</i><span>{$activities.num}</span></a>
                    <ul id="activity-list"{if $activities.num == 0} style="display:none;"{/if}>
                        {section name=i loop=$activities.items}<li data-id="{$activities.items[i].id}">{$activities.items[i].message}</li>{/section}
                    </ul></li><!--
                --><li class="pofile"><a href="/{$env->l.users.url_profile}/{$smarty.session.user.alias}"><i class="material-icons">person</i><span>Profilom</span></a></li><!--
                --><li class="messages"><a href="/{$env->l.users.url_messages}"><i class="material-icons">question_answer</i><span>{$env->l.users.messages}</span></a></li><!--
                --><li class="bekuldes"><a href="/{$env->l.content.url_add}"><i class="material-icons">add_circle</i><span>Beküldés</span></a></li><!--
                --><li class="logout"><a href="/{$env->l.users.url_logout}"><i class="material-icons">lock</i><span>Kilépés</span></a></li><!--
            {/if}
                --><li class="like-button"><iframe src="//www.facebook.com/plugins/like.php?href=http://www.facebook.com/propellerhu&amp;width=130&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=247823682023773&amp;locale=hu_HU" scrolling="no" frameborder="0" allowTransparency="true"></iframe></li><!--
            --></ul>
        </div><!-- .site-tools -->

        <div class="site-search">
            <form action="/kereses" method="get">
                <input type="text" id="search-q" name="q" value="{$smarty.get.q|escape}"><button type="submit"><i class="material-icons">search</i></button>
            </form>
        </div><!-- .site-search -->
    </div>
    </header><!-- #header -->

    <nav class="site-navigation clear">
        <ul>
            <li class="menu-toggle"><a href=""><i class="material-icons">view_headline</i></a></li><!--
    {foreach from=$misc.navigation_links key=k item=v name=foo}
    --><li><a href="{$v}">{$k|escape}</a></li><!--
    {/foreach}
        --></ul>
    </nav><!-- .site-navigation -->

{if $SCRIPT_NAME != '/modules/content/content.php'}
    <div class="topstories header clear">
        <div>
    {section name=i loop=$blocks.topstories start=0 max=3}
    <div class="ga-event" data-category="cimlap-fejlec" data-action="{$blocks.topstories[i].title|escape}" data-label="{$smarty.section.i.index+1}. hely">
        <a href="{$blocks.topstories[i].link}"{if !$blocks.topstories[i].link|strstr:$env->base} class="thumb outgoing" rel="nofollow"{else} class="thumb"{/if} style="background-image: url({$blocks.topstories[i].image});"></a>
        <span class="title"><a href="{$blocks.topstories[i].link}"{if !$blocks.topstories[i].link|strstr:$env->base} class="outgoing" rel="nofollow">{$blocks.topstories[i].title|escape}<i class="material-icons material-external">exit_to_app</i>{else}>{$blocks.topstories[i].title|escape}{/if}</a></span>
    </div>
    {/section}
        </div>
    </div>
{/if}

    <div id="content" class="hfeed site clear">
    <div id="primary">

