<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>{$subject|escape}</title>
<style type="text/css">
{literal}
#outlook a {padding:0;}
body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
.ExternalClass {width:100%;}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; }
img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
a img {border:none;}
.image_fix {display:block;}
p {margin: 1em 0;  }
table td {border-collapse: collapse; }
{/literal}
</style>
<base target="_blank" />
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="background: #f6f6f6; font: normal 12px Arial, Helvetica, sans-serif;  color: #222;">
<tr>
	<td valign="top" style="padding: 8px;">

	<table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;">
	<tr>
	<td valign="top" style="background:white;padding:15px;">
		<h1 style="margin: 0; padding: 0 0 10px 0; margin: 0; font-size: 14px; font-weight: bold; border-bottom: 1px solid #eee; "><a href="http://propeller.hu/" target="_blank" style="color: black !important; text-decoration: none;">{$subject|escape}</a></h1>

		{if $name}<h2 style="color: #bbb !important; margin: 20px 0 0 0; padding: 0; font-size: 26px; font-weight: bold; letter-spacing: -0.4px; line-height: 28px;">Kedves {$name|escape}!</h2>{/if}

		<p style="margin: 20px 0 0 0; padding: 0 10px 0 0; line-height: 16px;">{$body}</p>

		<p style="margin: 20px 0 0 0; padding: 0 10px 0 0; line-height: 16px;">Üdvözlettel,<br />Propeller</p>

		<p style="border-top: 1px solid #eee; color: #bbb; font-size: 11px; margin: 40px 0 0 0; padding: 10px 10px 0 0; line-height: 14px;">Ezt a levelet automatikus üzenetküldő rendszerünk kézbesítette, kérjük ne válaszoljon rá. Ha szeretné felvenni velünk a kapcsolatot,
		az Impresszum menüpontban található elérhetőségeinken keresztül teheti meg. E-mail értesítéseit a <a href="http://propeller.hu/beallitasaim" target="_blank" style="color: #aaa;">Beállításaim</a> menüpontban szabályozhatja.</p>
	</td>
	</tr>
	</table>

	</td>
</tr>
</table>
</body>
</html>