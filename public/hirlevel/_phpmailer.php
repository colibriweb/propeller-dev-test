<?php
require('PHPMailer/class.phpmailer.php');

$m = new PHPMailer();
$m->XMailer = 'Propeller-Hirlevel';
$m->CharSet = 'UTF-8';

/*$m->Sender = 'bounce@mail-manage.swen.hu';
$m->ReturnPath = 'bounce@mail-manage.swen.hu';
*/
$m->Sender = 'hirlevel-bounce@propeller.hu';
$m->ReturnPath = 'hirlevel-bounce@propeller.hu';
/*
$m->IsSMTP();
$m->SMTPKeepAlive = true;
$m->Host = 'localhost';
//$m->SMTPDebug  = 2;
*/

$m->SetFrom($config['mail_from'], $config['mail_from_name']);

?>