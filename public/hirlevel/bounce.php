#!/usr/bin/php5

<?php

// adatbázis
require('DB.class.php');
$db = DB::getInstance();

$fp = fopen('php://stdin', 'r');
while (!feof($fp)) {
	$buffer .= fread($fp, 4096);
}
fclose($fp);
unset($fp);
$email_data = $buffer;
unset($buffer);

$email_data_arr = explode(chr(10), $email_data);
foreach($email_data_arr as $v) {
	if(substr($v, 0, 28) == 'X-Propeller-Hirlevel-Bounce:') {
		$id = explode(':', $v);
		$parts = explode('@', $id[1]);


// debug miatt csak
//mail('zsolt.liebig@gmail.com', 'Bounce script', 'ez pattant vissza: ' . $parts[0]);


		$res = $db->prepare("INSERT INTO newsletter_bounce
            (list_name, unsubscription_key, mail_data, `datetime`)
            VALUES (?, ?, ?, UTC_TIMESTAMP())");
		$res->execute(array(trim($parts[1]), trim($parts[0]), $email_data));
		break;
	}
}

unset($email_data);
echo 'OK';

?>