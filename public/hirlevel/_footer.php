

	<div id="footer-container">
		<footer class="clearfix"><?= $config['list_footer'] ?></footer>
	</div>

</div><!-- #container -->

<?php if(isset($config['ga_account'])): ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '<?= $config['ga_account'] ?>', 'auto', {
	'allowLinker': true
});
<?php if(isset($ga_event)): ?>
ga('send', 'event', 'propeller-hirlevel', '<?= $ga_event[0] ?>', '<?= $ga_event[1] ?>');
<? endif; ?>
</script>
<? endif; ?>

</body>
</html>