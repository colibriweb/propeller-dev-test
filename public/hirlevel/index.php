<?php
/**
    PROPELLER.HU HÍRLEVÉLKÜLDŐ ÉS LISTAKEZELŐ TOOL
    Author: LIEBIG ZSOLT <zsolt.liebig@gmail.com>

    API endpoint-ok (pl. mobilapp-os fel/leiratkoztatáshoz):

        feliratkozás:   http://propeller.hu/hirlevel/subscribe?api=1&mail=&mail_name=
        leiratkozás:    http://propeller.hu/hirlevel/unsubscribe?api=1&mail=

    Teszt hírlevél küldése egy email-címre:

        http://propeller.hu/hirlevel/cron/test_campaign/foo@bar.com
        (előtte a "http://propeller.hu/hirlevel/cron/create_campaign" futtatása szükséges)

    Napi hírlevél küldés cron-ból:
*/

# kampány elkészítése
# 58 19 * * * root wget -q -O /dev/null http://propeller.hu/hirlevel/cron/create_campaign

# kiküldés (6 percenkénti csomagokban)
# */6 20 * * * root wget -q -O /dev/null http://propeller.hu/hirlevel/cron/send_campaign

# visszapattanó hírlevelek feldolgozása
# 0 7 * * * root wget -q -O /dev/null http://propeller.hu/hirlevel/cron/remove_bounced


define('DEBUG', false);

date_default_timezone_set('UTC');
mb_internal_encoding('UTF-8');

// hibakezelés
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 0);

if(DEBUG) {
    ini_set('display_errors', 1);
}

$base = 'http://propeller.hu/hirlevel';

$request_uri = mb_strtolower(
    str_replace(
        parse_url($base, PHP_URL_PATH), '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
        )
    );
$route = array_filter(explode('/', $request_uri));
array_push($route, null, null, null); // teszünk bele még, hogy ne kelljen isset-tel szenvedni

// config betöltés
$config = require('config.php');

$now = new DateTime();
$now->setTimezone(new DateTimeZone($config['time_zone']));

// nyelv betöltése
$langs = require('langs.php');
$lang = $langs[$config['locale']];
putenv('LANG='.$config['locale'].'.UTF-8');
setlocale(LC_ALL, $config['locale'].'.UTF-8');

// adatbázis
require('DB.class.php');
$db = DB::getInstance();

function p($var) { // debughoz
    echo '<pre>';
    print_r($var);
    die('</pre>');
}

function esc_str($string) { // string escape-elése
    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8', true);
}

function esc_url($string) { // string escape-elése url-ekhez
    return htmlspecialchars(urlencode(urldecode($string)), ENT_QUOTES, 'UTF-8');
}

function error404($message = '404 Not Found') { // hibaoldal
    if(!headers_sent()) {
        header('HTTP/1.0 404 Not Found');
    }
    die($message);
}

function logError($error_message, $level = E_USER_WARNING) { // saját rendszerhibakezelő
    //$call = next(debug_backtrace());
    $call = current(debug_backtrace());
    trigger_error($error_message.' in '.$call['file'].' on line '.$call['line'], $level);
}

function checkMailFormat($mail) {
    return (bool)preg_match("/^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$/i", $mail);
}

function getRandomKey($length) { // pseudo-random string generálás
    $bytes = '';

    if(function_exists('openssl_random_pseudo_bytes')) { // openssl
        $bytes = openssl_random_pseudo_bytes($length, $strong);
    }

    if($bytes === 0 && is_readable('/dev/urandom')) { // ha nincs, akkor urand (*nix)
        $fp = fopen('/dev/urandom','rb');
        if($fp !== false) {
            $urandom = fread($fp, $length);
            fclose($fp);
        }
    }

    $string = base64_encode($bytes); // itt 33%-kal hosszabb lesz a string úgyis, az eredetinél
    $string = str_replace(['+', '/', '='], '', $string);
    $string = substr($string, 0, $length);

    if(strlen($string) < $length) { // semmi nincs
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $string = '';
        for($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(0, strlen($chars)-1), 1);
        }
    }
    return $string;
}

function getIP() {
    return isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
}

function getCampaignContent($date) {
    global $config, $db;

    $res = $db->prepare("SELECT campaign_content FROM ".$config['list_db_prefix']."campaign WHERE campaign_date = ? LIMIT 1");
    $res->execute(array($date));
    if($res->rowCount()) { // megvan a kampány adat
        $row = $res->fetch();
        return unserialize($row->campaign_content);
    }
    return false;
}

function getCampaignHTML($campaign_content, $subject, $mail, $unsubscription_key) {
    global $config, $db, $route, $base, $now;

/*
    $adatved = false;
    $res = $db->prepare("SELECT mail FROM _adatvedelmi_valaszok WHERE mail = ? LIMIT 1");
    $res->execute(array($mail));
    if($res->rowCount()) {
        $adatved = 'valaszolt';
    }

    $adatved_mail = $mail;
    $adatved_hash = sha1('salted'.$mail.'new_policy_hash');
*/

    $unsubscribe_link = $base.'/unsubscribe/'.$unsubscription_key.'/'.$mail;
    $unsubscribe = sprintf($config['unsubscribe_footer_html'], $unsubscribe_link);


    ob_start();
    include('_template_propeller.php');
    //include('_template_propeller_gdpr.php');
    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}

function getCampaignAlt($campaign_content, $subject, $mail, $unsubscription_key) {
    global $config, $base;

    $unsubscribe_link = $base.'/unsubscribe/'.$unsubscription_key.'/'.$mail;

    $alt = $subject."\n\n";

    foreach($campaign_content as $key => $value) {
        $alt .= esc_str($value['title'])."\n";
        //$alt .= esc_str($value['description'])."\n";
        $alt .= esc_str($value['link'])."\n\n";
    }

    $alt .= "* * *\n";
    $alt .= sprintf($config['unsubscribe_footer_alt'], $unsubscribe_link);

    return $alt;
}

switch($route[1]) {
case 'subscribe': // ------------------------------------------------------- feliratkozás
    $title = $lang['subscribe_title'];

    if(empty($route[2])) { // form
        if(isset($_POST['mail'])) { // kiértékelés
            $mail = trim($_POST['mail']);
            $mail_name = !empty($_POST['mail_name']) ? trim($_POST['mail_name']) : null;

            if(!checkMailFormat($mail)) {
                $response = $lang['invalid_email'];
            }

            if(!empty($_POST['f_'.md5($config['list_key'])])) { // ki van töltve a csali mező (spambot lehet)
                error404();
            }

            if(!isset($_POST['terms']) || ($_POST['terms'] != '1')) {
                $response = $lang['no_terms'];
            }

            $res = $db->prepare("SELECT mail FROM ".$config['list_db_prefix']."list
                WHERE mail COLLATE utf16_general_ci = ?
                LIMIT 1");
            $res->execute(array($mail));
            if($res->rowCount()) {
                $response = $lang['already_subscribed'];
            }

            if(!isset($response)) { // nincs hiba
                $unsubscription_key = mb_strtolower(getRandomKey(20));

                if(isset($_POST['api']) && ($_POST['api'] == '1')) { // [API-hívás] rögtön érvényessé válik
                    $res = $db->prepare("INSERT INTO ".$config['list_db_prefix']."list
                    (mail, mail_name, confirmed, subscribed_at, subscribed_ip, unsubscription_key) VALUES (?, ?, 1, UTC_TIMESTAMP(), ?, ?)");
                    $res->execute(array($mail, $mail_name, getIP(), $unsubscription_key));
                    die('OK');
                }
                else { // nem érvényes még, ki kell küldeni a levelet
                    $confirmation_key = mb_strtolower(getRandomKey(20));

                    require('_phpmailer.php');

                    $m->AddAddress($mail, $mail_name);
                    $m->Subject = $title.": ".$config['list_title'];

                    $m->Body = mb_strtoupper($title.": ".$config['list_title'])."\n\n";
                    $m->Body .= $lang['subscribe_mail']."\n\n";
                    $m->Body .= $base.'/subscribe/'.$confirmation_key."\n\n";
                    $m->Body .= $lang['subscribe_mail_footer']."\n";
                    $m->Body .= $config['list_footer'];

                    if(!$m->Send()) {
                        logError('Could not send e-mail to: '.$mail);
                        $response = 'Could not send e-mail to: '.$mail;
                    }
                    else {
                        $res = $db->prepare("INSERT INTO ".$config['list_db_prefix']."list
                        (mail, mail_name, confirmed, confirmation_key, subscribed_at, subscribed_ip, unsubscription_key) VALUES (?, ?, 0, ?, UTC_TIMESTAMP(), ?, ?)");
                        $res->execute(array($mail, $mail_name, $confirmation_key, getIP(), $unsubscription_key));

                        header('Location: '.$base.'/subscribe/success');
                        exit;
                    }
                }
            }
        }

        require('_header.php');
        require('_subscribe.php');
    }
    elseif($route[2] == 'success') {
        $response = $lang['subscribe_success'];
        //$ga_event = ['subscribe_success', 1];
        require('_header.php');
    }
    elseif(strlen($route[2]) == 20) { // feliratkozás megerősítése
        $res = $db->prepare("SELECT mail FROM ".$config['list_db_prefix']."list WHERE confirmed = 0 AND confirmation_key = ? LIMIT 1");
        $res->execute(array($route[2]));
        if(!$res->rowCount()) {
            $response = $lang['subscribe_confirmation_failed'];
        }
        else { // nincs hiba
            $row = $res->fetch();
            $res = $db->prepare("UPDATE ".$config['list_db_prefix']."list SET confirmed = 1, confirmation_key = NULL WHERE mail = ? LIMIT 1");
            $res->execute(array($row->mail));

            $response = $lang['subscribe_confirmed'];
            $ga_event = ['subscribe_confirmed', 1];
        }
        require('_header.php');
    }
    else {
        error404();
    }

    require('_footer.php');

break;
case 'unsubscribe': // ------------------------------------------------------- leiratkozás
    $title = $lang['unsubscribe_title'];

    if(isset($_POST['api']) && ($_POST['api'] == '1')) { // [API-hívás] rögtön leiratkozik
        $res = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list WHERE mail COLLATE utf16_general_ci = ? LIMIT 1");
        $res->execute(array(urldecode(trim($_POST['mail']))));
        die('OK');
    }
    elseif((strlen($route[2]) == 20) && !empty($route[3])) { // leiratkozás megerősítése
        if(isset($_POST['confirm'])) { // rendben
            $mail = urldecode(trim($route[3]));

            if(!isset($response)) { // nincs hiba
                $res = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list WHERE mail COLLATE utf16_general_ci = ? AND unsubscription_key = ? LIMIT 1");
                $res->execute(array($mail, $route[2]));

                header('Location: '.$base.'/unsubscribe/success');
                exit;
            }
        }

        require('_header.php');
        require('_unsubscribe.php');
    }
    elseif($route[2] == 'success') {
        $response = $lang['unsubscribe_success'];
        $ga_event = ['unsubscribe_success', 1];
        require('_header.php');
    }
    else {
        error404();
    }

    require('_footer.php');

break;
case 'archives':

    if($campaign_content = getCampaignContent($route[2])) {
        echo getCampaignHTML($campaign_content, $config['subject_template_poor'], 'test@propeller-hirlevel', 'nokey');
    }
    else {
        error404();
    }

break;
case 'cron':
    if(!in_array($now->format('N'), $config['campaign_days'])) { // ma nincs kampány nap, leáll
        die('No campaign for this day.');
    }

    if($route[2] == 'create_campaign') { // hírlevél összeállítása és elmentése
        $campaign_content = '';

        if(!empty($config['campaign_rss'])) { // rss kampány készítés
            require_once('Feed.class.php');
            $feed = new Feed($config['campaign_rss']);

            if($items = $feed->getFetched()) {
                $campaign_content = serialize($items);
            }
        }

        // van kampány adat, el kell készíteni
        if(!empty($campaign_content)) {
            // elkészült a levél tartalma, adatbázisba mentés
            $res = $db->prepare("INSERT INTO ".$config['list_db_prefix']."campaign (campaign_date, campaign_content) VALUES (?, ?)
            ON DUPLICATE KEY UPDATE campaign_content = ?");
            $res->execute(array($now->format('Y-m-d'), $campaign_content, $campaign_content));

            // email lista áttöltése az átmeneti tárba
            $res = $db->query("TRUNCATE TABLE ".$config['list_db_prefix']."list_queue");
            if($res) {
                $db->query("INSERT INTO ".$config['list_db_prefix']."list_queue
                    SELECT mail, mail_name, unsubscription_key
                    FROM ".$config['list_db_prefix']."list WHERE confirmed = 1");
            }

            die('Campaign and temporary list saved.');
        }
        else {
            die('No content for this campaign.');
        }
    }
    elseif($route[2] == 'preview_campaign') { // hírlevél megnézése a böngészőben
        if($campaign_content = getCampaignContent($now->format('Y-m-d'))) {
            echo getCampaignHTML($campaign_content, sprintf($config['subject_template'], 'Vezetéknév Keresztnév'), 'test@propeller-hirlevel', 'nokey');
        }
    }
    elseif($route[2] == 'test_campaign') { // teszt kiküldés
        if($campaign_content = getCampaignContent($now->format('Y-m-d'))) { // van mára kampány
            require('_phpmailer.php');
            $m->IsHTML(true);

            $mail = checkMailFormat($route[3]) ? $route[3] : $config['campaign_test_mail'];
            $m->AddAddress($mail);
            $m->AddCustomHeader('X-Propeller-Hirlevel-Bounce: nokey@propeller');
            $subject = sprintf($config['subject_template'], 'Vezetéknév Keresztnév');
            $m->Subject = $subject;

            $m->Body = getCampaignHTML($campaign_content, $subject, $mail, 'nokey');
            $m->AltBody = getCampaignAlt($campaign_content, $subject, $mail, 'nokey');

            if(!$m->Send()) {
                logError('Could not send email to: '.$mail.' ('.$m->ErrorInfo.')');
                die('Could not send email to: '.$mail.' ('.$m->ErrorInfo.')');
            }
            die('Test mail just sent to: '.$mail);
        }
    }
    elseif($route[2] == 'send_campaign') { // kiküldés indítása
        if($campaign_content = getCampaignContent($now->format('Y-m-d'))) { // van mára kampány
            require('_phpmailer.php');
            $m->IsHTML(true);

            $res = $db->query("SELECT mail, mail_name, unsubscription_key FROM ".$config['list_db_prefix']."list_queue ORDER BY rand() LIMIT 500");
            while($row = $res->fetch()) {
                $m->AddAddress($row->mail);
                $m->AddCustomHeader('X-Propeller-Hirlevel-Bounce: '.$row->unsubscription_key.'@propeller');

                if(empty($row->mail_name)) {
                    $subject = $config['subject_template_poor'];
                }
                else {
                    $subject = sprintf($config['subject_template'], $row->mail_name);
                }

                $m->Subject = $subject;

                $m->Body = getCampaignHTML($campaign_content, $subject, $row->mail, $row->unsubscription_key);
                $m->AltBody = getCampaignAlt($campaign_content, $subject, $row->mail, $row->unsubscription_key);

                if(!$m->Send()) {
                    logError('Could not send email to: '.$row->mail.' ('.$m->ErrorInfo.')');
                    //die('Could not send email to: '.$row->mail.' ('.$m->ErrorInfo.')');
                }
                echo $row->mail.'<br>';

                $m->ClearAddresses();
                $m->ClearCustomHeaders();

                $res_del = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list_queue WHERE mail = ? LIMIT 1");
                $res_del->execute(array($row->mail));
            }
        }
    }
    elseif($route[2] == 'remove_bounced') { // halott email címek kipucolása

        if(isset($_GET['unsubscription_key'])) {

            $res_del1 = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list WHERE unsubscription_key = ? LIMIT 1");
            $res_del1->execute(array($_GET['unsubscription_key']));

            $res_del2 = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list_queue WHERE unsubscription_key = ? LIMIT 1");
            $res_del2->execute(array($_GET['unsubscription_key']));

            $res_del3 = $db->prepare("DELETE FROM ".$config['list_db_prefix']."bounce WHERE unsubscription_key = ?");
            $res_del3->execute(array($_GET['unsubscription_key']));

        }

        // utóbbi 20 napban legalább 5x visszapattant levelek törlése
        $res = $db->prepare("SELECT list_name, b.unsubscription_key, count(datetime) AS mail_count, mail, mail_name, subscribed_at
        FROM ".$config['list_db_prefix']."bounce b
        LEFT JOIN ".$config['list_db_prefix']."list l ON b.unsubscription_key = l.unsubscription_key
        WHERE list_name = ? AND TIMESTAMPDIFF(DAY, datetime, UTC_TIMESTAMP()) <= 20
        GROUP BY unsubscription_key
        ORDER BY mail_count DESC
        LIMIT 100");
        $res->execute(array('propeller'));
        while($row = $res->fetch()) {
            if($row->mail_count >= 5) { // 5 visszapattant levélnél törlés
                $res_del1 = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list WHERE unsubscription_key = ? LIMIT 1");
                $res_del1->execute(array($row->unsubscription_key));

                $res_del2 = $db->prepare("DELETE FROM ".$config['list_db_prefix']."list_queue WHERE unsubscription_key = ? LIMIT 1");
                $res_del2->execute(array($row->unsubscription_key));

                $res_del3 = $db->prepare("DELETE FROM ".$config['list_db_prefix']."bounce WHERE unsubscription_key = ?");
                $res_del3->execute(array($row->unsubscription_key));

                echo '(REMOVED) ';
            }
            else {
                echo '(<a href="'.$base.'/cron/remove_bounced?unsubscription_key='.$row->unsubscription_key.'">remove</a>) ';
            }

            echo $row->mail_count.'x - '.$row->mail.' ('.$row->mail_name.') - '.$row->subscribed_at.' - '.$row->unsubscription_key.'<br>';

        }

    }
    else {
        error404();
    }

break;
default: error404();
break;
}

?>