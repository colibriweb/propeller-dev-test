<?php

return [
    'hu_HU' => [
        'subscribe_title' =>            'Feliratkozás',
        'subscribe_your_name' =>        'Név:',
        'subscribe_your_email' =>       'Email cím:',
        'mail_placeholder' =>           'nev@valami.hu',
        'button_subscribe' =>           'Feliratkozás',
        'invalid_email' =>              'Nem megfelelő email cím, kérjük, ellenőrizze!',
        'no_terms' =>              'A feltételek elfogadása nélkül nem lehetséges feliratkozni.',
        'subscribe_mail' =>             'A feliratkozás megerősítéséhez kérjük, kattintson az alábbi linkre, vagy a böngészőbe másolva nyissa meg:',
        'subscribe_mail_footer' =>      'Ha nem Ön kezdeményezte a hírlevél feliratkozást, elnézést kérünk, és tekintse tárgytalannak ezt a levelet!',
        'subscribe_success' =>          'Köszönjük! A feliratkozás véglegesítéséhez kérjük, látogassa meg az email fiókját!',
        'subscribe_confirmation_failed' =>  'Hiba! Nem megfelelő érvényesítő kód, vagy a feliratkozás már érvényesítve lett. Kérjük, ellenőrizze az emailben kapott linket!',
        'subscribe_confirmed' =>        'Köszönjük! A feliratkozás véglegesítése megtörtént, a következő hírlevelet már Önnek is küldjük!',
        'already_subscribed' =>         'Ez az email cím már szerepel a listán.',

        'unsubscribe_title' =>          'Leiratkozás',
        'button_unsubscribe' =>         'Igen, leiratkozom',
        'unsubscribe_success' =>        'A leiratkozás sikeresen megtörtént.',
        //'unsubscribe_failed' =>         'Hiba! Nem megfelelő leiratkozó kód. Kérjük, ellenőrizze a leiratkozáshoz használt linket!',
    ]
];

?>