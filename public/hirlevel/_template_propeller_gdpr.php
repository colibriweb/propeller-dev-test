<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title><?= esc_str($subject) ?></title>
<style type="text/css">
  .ExternalClass { width: 100%; }
  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

  body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
  table { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
  table td { border-collapse: collapse; }

  img { outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
  a img { border: none; }

  p { margin: 0; padding: 0; margin-bottom: 0; }
  .forApple a { color: #666666 !important; text-decoration: none !important; }

  [data-browser="useragent"] { display: none; }
</style>
</head>
<body bgcolor="#ffffff">
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" width="100%" style="font-size:14px;font-family:Arial, Helvetica, sans-serif;">
<!-- preheader -->
<tr>
	<td algin="center" bgcolor="#ffffff" style="background-color: #ffffff; padding-right: 0; padding-top: 10px; padding-bottom: 0; padding-left: 0;">
		<div style="max-width: 600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; padding-top: 0; padding-right: 10px; padding-bottom: 0; padding-left: 10px;">
			<table width="100%" cellspacing="0" cellpadding="0" border="0" align="left" style="margin-right: 0; margin-bottom: 0; margin-left: 0; margin-top: 0; padding-right: 0; padding-bottom: 0; padding-top: 0; padding-left: 0; background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-size: 14px; line-height: 18px; color: #222222;" bgcolor="#ffffff">
			<tr>
				<td width="100%" style="padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0; font-size: 13px;">
					<div data-browser="useragent"><a href="<?= $base.'/archives/'.$now->format('Y-m-d') ?>?utm_source=mm-<?= $now->format('Y-m-d') ?>&amp;utm_medium=email&amp;utm_campaign=archive" style="color: #0055bb; text-decoration: none;">Hírlevél megnyitása a böngészőben</a></div>
				</td>
			</tr>
			</table>
		</div>
	</td>
</tr>
 <!-- content -->
<tr>
	<td algin="center" bgcolor="#ffffff" style="background-color: #ffffff; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
<!-- Target Outlook 2003 -->
<!--[if lte mso 7]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->
<!-- Target Outlook 2007 and 2010 -->
<!--[if gte mso 9]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->

	<div style="max-width: 600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; padding-top: 0; padding-right: 10px; padding-bottom: 0; padding-left: 10px;">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" style="max-width: 600px; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-left: 0; padding-bottom: 0; padding-right: 0; background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-size: 14px; line-height: 18px; color: #222222; text-align: left;">

		<!-- adatved -->
		<tr>
			<td width="100%" style="padding-right: 0; padding-left: 0; padding-bottom: 20px; padding-top: 0; background-color: #ffffff; border-bottom-style: solid; border-bottom-color: #eeeeee; border-bottom-width: 1px;" bgcolor="#ffffff">
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="left" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
				<tr><td width="100%" style="padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">

    <div style="background:#fff9cb;font-family:Arial,Helvetica,sans-serif;padding-right:15px;padding-left:15px;padding-bottom:30px;padding-top:15px;margin:0;font-size:13px;">
Tisztelt Felhasználó!<br><br>
Ezúton is tájékoztatjuk, hogy az Általános Adatvédelmi Rendeletre (GDPR) tekintetettel
2018. május 25-e óta elérhető oldalunkon a frissített adatvédelmi tájékoztató. Átdolgoztuk
a weboldal adatvédelmi szabályzatát, és adatkezelési irányelveinket, amelyek az Ön adatainak
védelmét szolgálják.<br><br>
A tájékoztatóban megtalálható az általunk követett gyakorlat leírása, valamint annak bemutatása,
hogy milyen lehetőségek állnak az Ön rendelkezésére az adatok frissítése, kezelése, törlése,
tájékoztatás kérése tekintetében.<br><br>
A szükséges intézkedéseket az Ön adatai védelmében hoztuk meg, kérjük tekintse meg a honlapon
található adatvédelmi tájékoztatót (<a href="http://propeller.hu/adatvedelmi_tajekoztato.pdf" target="_blank">Adatvédelmi szabályzat és felhasználási feltételek</a>).

    </div>

				</td></tr>
				</table>
			</td>
		</tr>
		</table>
	</div>

<!--[if lte mso 7]>
</td></tr></table>
<![endif]-->
<!--[if gte mso 9]>
</td></tr></table>
<![endif]-->
	</td>
</tr>
<!-- footer -->
<tr>
	<td algin="center" bgcolor="#ffffff">
<!-- Target Outlook 2003 -->
<!--[if lte mso 7]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->
<!-- Target Outlook 2007 and 2010 -->
<!--[if gte mso 9]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->
	<div style="max-width: 600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; padding-top: 0; padding-right: 10px; padding-bottom: 0; padding-left: 10px;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" style="max-width: 600px; margin-left: 0; margin-bottom: 0; margin-right: 0; margin-top: 0; padding-left: 0; padding-bottom: 0; padding-top: 0; padding-right: 0; background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-size: 11px; line-height: 18px; color: #888888; text-align: left;">
		<!-- legal -->
		<tr>
			<td width="100%" align="left" style="padding-top: 20px; padding-right: 0; padding-bottom: 0; padding-left: 0; text-align: left; border-top-style: solid; border-top-color: #eeeeee; border-top-width: 1px;">
			<?= $config['list_footer'] ?>
			</td>
		</tr>
		<tr>
			<td width="100%" style="padding-top: 0; padding-right: 0; padding-bottom: 20px; padding-left: 0;">
			<?= $unsubscribe ?>
			</td>
		</tr>
	</table>
	</div>
<!--[if lte mso 7]>
</td></tr></table>
<![endif]-->
<!--[if gte mso 9]>
</td></tr></table>
<![endif]-->
	</td>
</tr>
</table>

</body>
</html>