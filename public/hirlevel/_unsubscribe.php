
<div id="main-container">

	<form class="unsubscribe-form" action="<?= $base ?>/unsubscribe/<?= esc_url($route[2]) ?>/<?= esc_url($route[3]) ?>" method="post" novalidate>
		<?= '<p class="desc">'.sprintf($config['unsubscribe_desc'], esc_str(urldecode($route[3]))).'</p>'."\n"; ?>

		<div class="field-group">
			<input type="submit" value="<?= $lang['button_unsubscribe'] ?>">
		</div>

		<input type="hidden" name="confirm" value="1">
	</form>

</div>
