<!DOCTYPE html>
<html lang="<?= str_replace('_', '-', $config['locale']) ?>">
<head>
<meta charset="utf-8">
<title><?= isset($title) ? esc_str($title).": ".$config['list_title'] : $config['list_title'] ?></title>
<link rel="stylesheet" href="<?= $base ?>/style.css?v=2">
</head>
<body class="<?= $config['locale'] ?><?php foreach($route as $k => $r): if(!empty($r) && ($k <= 2)) echo ' '.$r; endforeach ?>">

<div id="container">

	<div id="header-container">
	<header class="clearfix">
		<h1><?= isset($title) ? esc_str($title).": ".$config['list_title'] : $config['list_title'] ?></h1>
	</header>
	</div>

	<?php if(isset($response)): ?>
		<div id="response-container">
			<p><?= $response ?></p>
		</div>
	<? endif; ?>
