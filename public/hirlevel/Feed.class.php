<?php
/*
használat, pl:

	require_once('Feed.class.php');
	$feed = new Feed($url);
	$feed->getItems = 5;

	if($items = $feed->getFetched()) {
		// sikeres volt a feldolgozás
	}
	else {
		die($feed->error);
	}
	
*/

class Feed {

	/**
	* Feldolgozandó XML url-je
	* @var string
	*/
	var $feed;

	/**
	* Feldolgozáshoz szükséges séma
	* @var string
	*/
	var $schema = 'rss';

	/**
	* Lekért elemek száma
	* @var integer
	*/
	var $getItems = 50;

	/**
	* HTML és egyéb entitások eltávolítása
	* @var boolean
	*/
	var $strip = true;

	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Feed($feed) {

		$this->feed = $feed;

	}

	function getFetched() {

		return $this->{'schema_'.$this->schema}();

	}

	function schema_rss() {

		$source = $this->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';
	
		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);
	
			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeText($description[1][0], $encoding);
	
			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<enclosure url=[\'\"](.*?)[\'\"]|si", $value, $enclosure);
			if(isset($enclosure[1][0])) $assoc[$i]['image'] = $enclosure[1][0];

			if(empty($assoc[$i]['image'])) { // ha nincs, keressünk a descriptionben képeket
				preg_match("|img(.*?)src=[\'\"](.*?)[\'\"]|si", $value, $src); 
				if(!empty($src[2])) {
					$assoc[$i]['image'] = $src[2];
				}
			}

			$i++;
			if($i == $this->getItems) { break; }
		}
	
		return $assoc;

	}

	function schema_atom() {

		$source = $this->getFileContent($this->feed);
		preg_match_all("|<entry(.*?)>(.*?)</entry>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';
	
		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);
	
			preg_match_all("|<content(.*?)>(.*?)</content>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeText($description[2][0], $encoding);
	
			preg_match_all("|href=[\'\"](.*?)[\'\"]|si", $value, $link);
			$assoc[$i]['link'] = $link[1][0];

			preg_match_all("|<published>([^<]+)</published>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			$i++;
			if($i == $this->getItems) { break; }
		}
	
		return $assoc;
	
	}

	function getSafeText($string, $encoding = 'UTF-8') {

		$string = iconv($encoding, 'utf-8//TRANSLIT', $string);
		$string = strtr($string, array('<![CDATA[' => '', ']]>' => ''));

		if($this->strip)
		$string = strip_tags(html_entity_decode(strip_tags($string), ENT_QUOTES, 'UTF-8'));

		$string = str_replace(array('&#8211;', '&#8222;', '&#8221;', '&#8230;', '&#337;', '&apos;', '&#369;', '#8217;', '&#039;'), array('-', '"', '"', '...', 'ő', '"', 'ű', "'", "'"), $string);
		return trim(preg_replace("/\s\s+/", " ", $string));

	}

	function getFileContent($url) {
/*
		$handle = fopen($url, 'rb');
		$string = '';
		while(!feof($handle)) {
		$string .= fread($handle, 8192);
		}
		fclose($handle);
*/

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 7);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1');

		ob_start();
		
		curl_exec($ch);
		curl_close($ch);
		$string = ob_get_contents();
		
		ob_end_clean();

		return $string;

	}

}


?>
