<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title><?= esc_str($subject) ?></title>
<style type="text/css">
  .ExternalClass { width: 100%; }
  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

  body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
  table { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
  table td { border-collapse: collapse; }

  img { outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
  a img { border: none; }

  p { margin: 0; padding: 0; margin-bottom: 0; }
  .forApple a { color: #666666 !important; text-decoration: none !important; }

  [data-browser="useragent"] { display: none; }
</style>
</head>
<body bgcolor="#ffffff">
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" width="100%" style="font-size:14px;font-family:Arial, Helvetica, sans-serif;">
<!-- preheader -->

 <!-- content -->
<tr>
	<td algin="center" bgcolor="#ffffff" style="background-color: #ffffff; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
<!-- Target Outlook 2003 -->
<!--[if lte mso 7]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->
<!-- Target Outlook 2007 and 2010 -->
<!--[if gte mso 9]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->

	<div style="max-width: 600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; padding-top: 0; padding-right: 10px; padding-bottom: 0; padding-left: 10px;">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" style="max-width: 600px; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-left: 0; padding-bottom: 0; padding-right: 0; background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-size: 14px; line-height: 18px; color: #222222; text-align: left;">

		<!-- adatved -->
		<tr>
			<td width="100%" style="padding-right: 0; padding-left: 0; padding-bottom: 20px; padding-top: 0; background-color: #ffffff; border-bottom-style: solid; border-bottom-color: #eeeeee; border-bottom-width: 1px;" bgcolor="#ffffff">
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="left" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
				<tr><td width="100%" style="padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">

    <div style="background:#fff9cb;font-family:Arial,Helvetica,sans-serif;padding-right:15px;padding-left:15px;padding-bottom:30px;padding-top:15px;margin:0;font-size:13px;">
Tisztelt Felhasználónk!<br><br>

A Propellert a jövőben az <a href="http://ikodigital.hu/" target="_blank">IKO Digital Kft</a>. adja ki, emiatt
a weboldal adatvédelmi tájékoztatója megváltozott és a weboldalt a továbbiakban csak ennek megerősítésével
lehet használni. A módosított szöveget itt olvashatod el részletesen: <a
href="http://propeller.hu/statikus/adatvedelmi-tajekoztato"
target="_blank">http://propeller.hu/statikus/adatvedelmi-tajekoztato</a>.<br>
A módosítás 2016. szeptember 15-én lép hatályba.<br><br>

Bízunk abban, hogy a Propeller regisztrált felhasználója szeretnél maradni a jövőben is, és ha így van, akkor
arra kérünk, kattints a „megerősítem” gombra, ezzel kijelented, hogy a propeller.hu weboldal módosított
adatvédelmi tájékoztatóját elolvastad, megértetted, azt magadra nézve kötelezőnek fogadtad el, az adataidnak
a módosított adatvédelmi tájékoztató szerinti kezeléséhez kifejezetten hozzájárultál.<br><br>

Amennyiben nem erősíted meg, hogy elfogadtad volna a módosított adatvédelmi tájékoztatót, akkor azt úgy
értelmezzük, hogy nem járultál hozza az adataid további kezeléséhez, ebben az esetben <b>15 napon belül
töröljük a felhasználói fiókodat és az adataidat</b>.<br><br>

Köszönjük, a szerkesztőség

        <div style="text-align:center;padding-top:20px;">
            <a href="http://propeller.hu/?new_policy=<?= $adatved_mail ?>&hash=<?= $adatved_hash ?>" target="_blank" style="padding:.6em 2em;background:#2ba700;border-radius:3px;color:#fff;font-size:18px;font-weight:bold;text-transform:uppercase;margin:5px 5px 5px 0;text-decoration:none;">Megerősítem</a>
        </div>

    </div>

				</td></tr>
				</table>
			</td>
		</tr>

		</table>
	</div>

<!--[if lte mso 7]>
</td></tr></table>
<![endif]-->
<!--[if gte mso 9]>
</td></tr></table>
<![endif]-->
	</td>
</tr>
<!-- footer -->
<tr>
	<td algin="center" bgcolor="#ffffff">
<!-- Target Outlook 2003 -->
<!--[if lte mso 7]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->
<!-- Target Outlook 2007 and 2010 -->
<!--[if gte mso 9]>
<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; table-width:fixed;" align="center">
<tr><td style="width:600px !important;">
<![endif]-->
	<div style="max-width: 600px; margin-top: 0; margin-right: auto; margin-bottom: 0; margin-left: auto; padding-top: 0; padding-right: 10px; padding-bottom: 0; padding-left: 10px;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" style="max-width: 600px; margin-left: 0; margin-bottom: 0; margin-right: 0; margin-top: 0; padding-left: 0; padding-bottom: 0; padding-top: 0; padding-right: 0; background-color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-size: 11px; line-height: 18px; color: #888888; text-align: left;">
		<!-- legal -->
		<tr>
			<td width="100%" align="left" style="padding-top: 20px; padding-right: 0; padding-bottom: 0; padding-left: 0; text-align: left; border-top-style: none; border-top-color: #eeeeee; border-top-width: 1px;">
			<?= $config['list_footer'] ?>
			</td>
		</tr>
		<tr>
			<td width="100%" style="padding-top: 0; padding-right: 0; padding-bottom: 20px; padding-left: 0;">
			Ezt a levelet azért kaptad, mert korábban regisztráltál a Propelleren.
			</td>
		</tr>
	</table>
	</div>
<!--[if lte mso 7]>
</td></tr></table>
<![endif]-->
<!--[if gte mso 9]>
</td></tr></table>
<![endif]-->
	</td>
</tr>
</table>

</body>
</html>