<?php

return [
    'list_db_prefix' =>          'newsletter_',
    'locale' =>                  'hu_HU',
    'list_key' =>                'wn387wn84vbut5vbwirv',
    'ga_account' =>              'UA-80777149-1', // régi: 'UA-241542-5',
    'mail_from' =>               'noreply@propeller.hu',
    'mail_from_name' =>          'Propeller',
    'campaign_test_mail' =>      'zsolt.liebig@gmail.com',
    'campaign_rss' =>            'http://propeller.hu/feed/newsletter.php',
    'list_title' =>              'Propeller hírlevél',
    'list_footer' =>             '© '.date('Y').' IKO Digital Kft. – <a href="http://propeller.hu/adatvedelmi_tajekoztato.pdf" target="_blank">Adatkezelési tájékoztató és a felhasználási feltételek</a>',
    'subscribe_desc' =>          '<b>Iratkozzon fel a Propeller ingyenes hírlevelére</b>, és rendszeresen<br>elküldjük a legfontosabb híreket egyenesen az email fiókjába!',
    'subscribe_subdesc' =>       'Email címét soha nem adjuk ki harmadik félnek és bármikor kérheti a feliratkozásának törlését.',

    'unsubscribe_desc' =>        'Biztos, hogy leiratkozik a(z) \'%s\' email címmel erről a hírlevélről?',

    'campaign_days' =>           [1, 2, 3, 4, 5, 6, 7], // ISO-8601 numeric representation of the day of the week: 1 (for Monday) through 7 (for Sunday)
    'subject_template' =>        '%s, ezek a nap legfontosabb hírei – '.date('Y. m. d.'),
    'subject_template_poor' =>   'A nap legfontosabb hírei – '.date('Y. m. d.'),

    'unsubscribe_footer_html' => 'Ezt az emailt azért kapta, mert feliratkozott a Propeller hírlevélre.<br>Ha nem szeretne a továbbiakban ilyen leveleket kapni, <a href="%s" target="_blank" style="color: #0055bb; text-decoration: none;">ide kattintva</a> bármikor leiratkozhat.',
    'unsubscribe_footer_alt' =>  "Ezt az emailt azért kapta, mert feliratkozott a Propeller hírlevélre. Ha nem szeretne a továbbiakban ilyen leveleket kapni, ide kattintva bármikor leiratkozhat:\n%s",

    'time_zone' =>               'Europe/Budapest'
];

?>