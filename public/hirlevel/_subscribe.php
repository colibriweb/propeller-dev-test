
<div id="main-container">

	<form class="subscribe-form" action="<?= $base ?>/subscribe" method="post" novalidate>
		<?php if(!empty($config['subscribe_desc'])) echo '<p class="desc">'.$config['subscribe_desc'].'</p>'."\n"; ?>
		<?php if(!empty($config['subscribe_subdesc'])) echo '<p class="subdesc">'.$config['subscribe_subdesc'].'</p>'."\n"; ?>

		<div class="field-group">
			<label for="mail"><?= $lang['subscribe_your_name'] ?></label>
			<input type="text" value="<?= esc_str(@$_POST['mail_name']) ?>" name="mail_name" id="name" placeholder="">
			<br>
			<label for="mail"><?= $lang['subscribe_your_email'] ?></label>
			<input type="email" value="<?= esc_str(@$_POST['mail']) ?>" name="mail" id="mail" placeholder="<?= $lang['mail_placeholder'] ?>">
			<br>
			<label for="terms">&nbsp;</label>
			<input type="checkbox" name="terms" <?= (isset($_POST['terms']) ? 'checked="checked" ' : '' ) ?>id="terms" value="1"><span> Elfogadom az <a href="http://propeller.hu/adatvedelmi_tajekoztato.pdf" target="_blank">adatkezelési tájékoztatót és a felhasználási feltételeket</a></span>
			<br>
			<label for="button">&nbsp;</label><input type="submit" value="<?= $lang['button_subscribe'] ?>">
		</div>

		<div style="position:absolute; left:-9999px;"><input type="text" name="f_<?= md5($config['list_key']) ?>" value=""></div>
	</form>

</div>
