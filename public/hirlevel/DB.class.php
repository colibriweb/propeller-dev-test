<?php

class DB {

 	static protected $instance;

	protected $obj;

	public function __construct() {

		self::$instance =& $this;

	}

	private function _connect() {

		if(!$this->obj) {
			try {
				//echo 'dbconnect ';
				$this->obj = new PDO('mysql:host=localhost;port=3306;dbname=propeller', 'propeller', 'GZtfsd675fde', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				$this->obj->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
				$this->obj->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			}
			catch(PDOException $e) {
				logError('Could not connect to MySQL server: '.$e->getMessage());
				exit('A szolgáltatás átmenetileg nem elérhető. Értesültünk a hibáról, a helyreállításig egy kis türelmet kérünk!');
			}
		}

	}

	public static function getInstance() { // instance kérésnél csak a default configgal csatlakozhat

		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;

	}
/*
	public function query($query) {

		$this->_connect();

		return $this->obj->query($query);

	}

	public function prepare($query, $options = array()) {

		$this->_connect();

		return $this->obj->prepare($query, $options);

	}
*/
	public function __call($method, $args) { // a nemlétező metódosukat egyből a $this->obj -n hívjuk

		$this->_connect();

		$callable = array($this->obj, $method);
		if(is_callable($callable)) {
			return call_user_func_array($callable, $args);
		}

	}

}

?>