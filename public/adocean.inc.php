<?php

return array(

'Propeller_cimlap' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.nyito */
        ado.master({id: "HAfrn3AQiOeKc_4SU1WfhelhfR1g4websrzi.xhfBTD.B7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_cimlap_leaderboard' => '<div id="adoceangemhumnljexnggy"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.nyito.leaderboard */
        ado.slave("adoceangemhumnljexnggy", {myMaster: "HAfrn3AQiOeKc_4SU1WfhelhfR1g4websrzi.xhfBTD.B7" });
        </script>',

        'Propeller_cimlap_halfpage_felso' => '<div id="adoceangemhuslnmnmknre"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.nyito.halfpage_felso */
        ado.slave("adoceangemhuslnmnmknre", {myMaster: "HAfrn3AQiOeKc_4SU1WfhelhfR1g4websrzi.xhfBTD.B7" });
        </script>',

        'Propeller_cimlap_medium_rectangle_also' => '<div id="adoceangemhuzncqhuhlqt"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.nyito.medium_rectangle_also */
        ado.slave("adoceangemhuzncqhuhlqt", {myMaster: "HAfrn3AQiOeKc_4SU1WfhelhfR1g4websrzi.xhfBTD.B7" });
        </script>',

        'Propeller_cimlap_interstitial' => '<div id="adoceangemhupmetqjesmz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.nyito.interstitial */
        ado.slave("adoceangemhupmetqjesmz", {myMaster: "HAfrn3AQiOeKc_4SU1WfhelhfR1g4websrzi.xhfBTD.B7" });
        </script>',
    )
),
'Propeller_itthon_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.itthon_nyito */
        ado.master({id: "Je2mK3BZ8k6EwVTfs.LkXmPRwmKCwp5nopC9kRR3ovP.e7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_itthon_nyito_leaderboard' => '<div id="adoceangemhuqggffvkfpc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_nyito.leaderboard */
        ado.slave("adoceangemhuqggffvkfpc", {myMaster: "Je2mK3BZ8k6EwVTfs.LkXmPRwmKCwp5nopC9kRR3ovP.e7" });
        </script>',

        'Propeller_itthon_nyito_halfpage_felso' => '<div id="adoceangemhuweiiokhmai"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_nyito.halfpage_felso */
        ado.slave("adoceangemhuweiiokhmai", {myMaster: "Je2mK3BZ8k6EwVTfs.LkXmPRwmKCwp5nopC9kRR3ovP.e7" });
        </script>',

        'Propeller_itthon_nyito_medium_rectangle_also' => '<div id="adoceangemhunhnlisekzx"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_nyito.medium_rectangle_also */
        ado.slave("adoceangemhunhnlisekzx", {myMaster: "Je2mK3BZ8k6EwVTfs.LkXmPRwmKCwp5nopC9kRR3ovP.e7" });
        </script>',

        'Propeller_itthon_nyito_interstitial' => '<div id="adoceangemhutfpobirqkd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_nyito.interstitial */
        ado.slave("adoceangemhutfpobirqkd", {myMaster: "Je2mK3BZ8k6EwVTfs.LkXmPRwmKCwp5nopC9kRR3ovP.e7" });
        </script>',
    )
),
'Propeller_itthon_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.itthon_cikkoldal */
        ado.master({id: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_itthon_cikkoldal_leaderboard' => '<div id="adoceangemhulpglgoslhj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.leaderboard */
        ado.slave("adoceangemhulpglgoslhj", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_halfpage_felso' => '<div id="adoceangemhuumrhmweoxu"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuumrhmweoxu", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhuypnrjlmqge"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhuypnrjlmqge", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_roadblock_felso' => '<div id="adoceangemhupcdfetjomt"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhupcdfetjomt", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_roadblock_also' => '<div id="adoceangemhuvafinigfhd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.roadblock_also */
        ado.slave("adoceangemhuvafinigfhd", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_interstitial' => '<div id="adoceangemhurnioptoshp"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.interstitial */
        ado.slave("adoceangemhurnioptoshp", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_textlink' => '<div id="adoceangemhumdklhqdtwo"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.textlink */
        ado.slave("adoceangemhumdklhqdtwo", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',

        'Propeller_itthon_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhuoopedrihxo"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.itthon_cikkoldal.kepes_ajanlo */
        ado.slave("adoceangemhuoopedrihxo", {myMaster: "zaLnkuMRd5ZgPAvSCCW23y2XDeyWYqb__zbVp5k_42r.v7" });
        </script>',
    )
),
'Propeller_bulvar_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.bulvar_nyito */
        ado.master({id: "ceFAD8_xh1ll0EcyjW9EtXdV89nAk7KaxzFmo036WIL.D7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_bulvar_nyito_leaderboard' => '<div id="adoceangemhutiohludhep"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_nyito.leaderboard */
        ado.slave("adoceangemhutiohludhep", {myMaster: "ceFAD8_xh1ll0EcyjW9EtXdV89nAk7KaxzFmo036WIL.D7" });
        </script>',

        'Propeller_bulvar_nyito_halfpage_felso' => '<div id="adoceangemhuzgqkekqnpv"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_nyito.halfpage_felso */
        ado.slave("adoceangemhuzgqkekqnpv", {myMaster: "ceFAD8_xh1ll0EcyjW9EtXdV89nAk7KaxzFmo036WIL.D7" });
        </script>',

        'Propeller_bulvar_nyito_medium_rectangle_also' => '<div id="adoceangemhuqjfoornlok"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_nyito.medium_rectangle_also */
        ado.slave("adoceangemhuqjfoornlok", {myMaster: "ceFAD8_xh1ll0EcyjW9EtXdV89nAk7KaxzFmo036WIL.D7" });
        </script>',

        'Propeller_bulvar_nyito_interstitial' => '<div id="adoceangemhuwhhrhxjszq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_nyito.interstitial */
        ado.slave("adoceangemhuwhhrhxjszq", {myMaster: "ceFAD8_xh1ll0EcyjW9EtXdV89nAk7KaxzFmo036WIL.D7" });
        </script>',
    )
),
'Propeller_bulvar_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.bulvar_cikkoldal */
        ado.master({id: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_bulvar_cikkoldal_leaderboard' => '<div id="adoceangemhuobpnmnlnhw"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.leaderboard */
        ado.slave("adoceangemhuobpnmnlnhw", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_halfpage_felso' => '<div id="adoceangemhuxojkcwnpxh"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuxojkcwnpxh", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhulcgeqkfsnr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhulcgeqkfsnr", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_roadblock_felso' => '<div id="adoceangemhuselhksspqg"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhuselhksspqg", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_roadblock_also' => '<div id="adoceangemhuycnkdipgwq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.roadblock_also */
        ado.slave("adoceangemhuycnkdipgwq", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_interstitial' => '<div id="adoceangemhuupqqfthuhc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.interstitial */
        ado.slave("adoceangemhuupqqfthuhc", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_textlink' => '<div id="adoceangemhupfconpmulb"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.textlink */
        ado.slave("adoceangemhupfconpmulb", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',

        'Propeller_bulvar_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhuraihjqrixb"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.bulvar_cikkoldal.kepes_ajanlo */
        ado.slave("adoceangemhuraihjqrixb", {myMaster: "hT9Lv1.mmOhuWOkbzlcvrDOqAaRabwelUPPqqUTvW9f.37" });
        </script>',
    )
),
'Propeller_kulfold_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.kulfold_nyito */
        ado.master({id: "jpo6ZvZmk6GyxqIWmRINV4PxcfNSmX5hmed9LVc6Zb3.D7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_kulfold_nyito_leaderboard' => '<div id="adoceangemhuqdhmliipvq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_nyito.Propeller_kulfold_nyito_leaderboard */
        ado.slave("adoceangemhuqdhmliipvq", {myMaster: "jpo6ZvZmk6GyxqIWmRINV4PxcfNSmX5hmed9LVc6Zb3.D7" });
        </script>',

        'Propeller_kulfold_nyito_halfpage_felso' => '<div id="adoceangemhuwbjpeoegqa"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_nyito.Propeller_kulfold_nyito_halfpage_felso */
        ado.slave("adoceangemhuwbjpeoegqa", {myMaster: "jpo6ZvZmk6GyxqIWmRINV4PxcfNSmX5hmed9LVc6Zb3.D7" });
        </script>',

        'Propeller_kulfold_nyito_medium_rectangle_also' => '<div id="adoceangemhuneosovrtjl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_nyito.Propeller_kulfold_nyito_medium_rectangle_also */
        ado.slave("adoceangemhuneosovrtjl", {myMaster: "jpo6ZvZmk6GyxqIWmRINV4PxcfNSmX5hmed9LVc6Zb3.D7" });
        </script>',

        'Propeller_kulfold_nyito_interstitial' => '<div id="adoceangemhutcqfilokav"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_nyito.Propeller_kulfold_nyito_interstitial */
        ado.slave("adoceangemhutcqfilokav", {myMaster: "jpo6ZvZmk6GyxqIWmRINV4PxcfNSmX5hmed9LVc6Zb3.D7" });
        </script>',
    )
),
'Propeller_kulfold_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.kulfold_cikkoldal */
        ado.master({id: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_kulfold_cikkoldal_leaderboard' => '<div id="adoceangemhulmhsmrpfmb"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.leaderboard */
        ado.slave("adoceangemhulmhsmrpfmb", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_halfpage_felso' => '<div id="adoceangemhuujcpckshym"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuujcpckshym", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhuymoiqojkww"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhuymoiqojkww", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_roadblock_felso' => '<div id="adoceangemhuppdmkwgigl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhuppdmkwgigl", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_roadblock_also' => '<div id="adoceangemhuvnfpdmdpcr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.roadblock_also */
        ado.slave("adoceangemhuvnfpdmdpcr", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_interstitial' => '<div id="adoceangemhurkjfgxlmih"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.interstitial */
        ado.slave("adoceangemhurkjfgxlmih", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_textlink' => '<div id="adoceangemhumalsntqmqg"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.textlink */
        ado.slave("adoceangemhumalsntqmqg", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',

        'Propeller_kulfold_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhuolqljufrsc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.kulfold_cikkoldal.kepes_ajanlo */
        ado.slave("adoceangemhuolqljufrsc", {myMaster: "Em5Xx2bB54I6kFvV6gtgFYYvEp3FoL4IWh9NRavXdQv.S7" });
        </script>',
    )
),
'Propeller_technika_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.technika_nyito */
        ado.master({id: "Ysfhm1bxrM5we3KiwpmzUwZ.7lK1cZtM1.5rppFmBVv.77", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_technika_nyito_leaderboard' => '<div id="adoceangemhumeoohrfuis"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_nyito.Propeller_technika_nyito_leaderboard */
        ado.slave("adoceangemhumeoohrfuis", {myMaster: "Ysfhm1bxrM5we3KiwpmzUwZ.7lK1cZtM1.5rppFmBVv.77" });
        </script>',

        'Propeller_technika_nyito_halfpage_felso' => '<div id="adoceangemhuscqrqwrkhc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_nyito.Propeller_technika_nyito_halfpage_felso */
        ado.slave("adoceangemhuscqrqwrkhc", {myMaster: "Ysfhm1bxrM5we3KiwpmzUwZ.7lK1cZtM1.5rppFmBVv.77" });
        </script>',

        'Propeller_technika_nyito_medium_rectangle_also' => '<div id="adoceangemhuzefflopicr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_nyito.Propeller_technika_nyito_medium_rectangle_also */
        ado.slave("adoceangemhuzefflopicr", {myMaster: "Ysfhm1bxrM5we3KiwpmzUwZ.7lK1cZtM1.5rppFmBVv.77" });
        </script>',

        'Propeller_technika_nyito_interstitial' => '<div id="adoceangemhupdhieulpyx"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_nyito.Propeller_technika_nyito_interstitial */
        ado.slave("adoceangemhupdhieulpyx", {myMaster: "Ysfhm1bxrM5we3KiwpmzUwZ.7lK1cZtM1.5rppFmBVv.77" });
        </script>',
    )
),
'Propeller_technika_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.technika_cikkoldal */
        ado.master({id: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_technika_cikkoldal_leaderboard' => '<div id="adoceangemhukmhofndgli"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.leaderboard */
        ado.slave("adoceangemhukmhofndgli", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_cikkoldal_halfpage_felso' => '<div id="adoceangemhuqkjrospmao"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuqkjrospmao", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhuxmoejknkkd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhuxmoejknkkd", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_cikkoldal_roadblock_felso' => '<div id="adoceangemhuopdidskius"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhuopdidskius", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_cikkoldal_roadblock_also' => '<div id="adoceangemhuunflmxgpuy"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.roadblock_also */
        ado.slave("adoceangemhuunflmxgpuy", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_cikkoldal_interstitial' => '<div id="adoceangemhunlqhcqjrgj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.interstitial */
        ado.slave("adoceangemhunlqhcqjrgj", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_cikkoldal_textlink' => '<div id="adoceangemhuyacfkmorzi"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.textlink */
        ado.slave("adoceangemhuyacfkmorzi", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',

        'Propeller_technika_kepes_ajanlo' => '<div id="adoceangemhulalogpenpn"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.technika_cikkoldal.Propeller_technika_kepes_ajanlo */
        ado.slave("adoceangemhulalogpenpn", {myMaster: "v6KXBdjhN89t5qZvvQcTz2ilwq2YbZ55gr_9D0OnpZP.77" });
        </script>',
    )
),
'Propeller_sport_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.sport_nyito */
        ado.master({id: "93_RRusxbIx6ryHpPmN9R3NOgtCq7h678PPw22TqRBn.w7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_sport_nyito_leaderboard' => '<div id="adoceangemhumkmqkklqmm"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_nyito.leaderboard */
        ado.slave("adoceangemhumkmqkklqmm", {myMaster: "93_RRusxbIx6ryHpPmN9R3NOgtCq7h678PPw22TqRBn.w7" });
        </script>',

        'Propeller_sport_nyito_halfpage_felso' => '<div id="adoceangemhusiotdqhhhw"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_nyito.halfpage_felso */
        ado.slave("adoceangemhusiotdqhhhw", {myMaster: "93_RRusxbIx6ryHpPmN9R3NOgtCq7h678PPw22TqRBn.w7" });
        </script>',

        'Propeller_sport_nyito_medium_rectangle_also' => '<div id="adoceangemhuzkdhoxefgl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_nyito.medium_rectangle_also */
        ado.slave("adoceangemhuzkdhoxefgl", {myMaster: "93_RRusxbIx6ryHpPmN9R3NOgtCq7h678PPw22TqRBn.w7" });
        </script>',

        'Propeller_sport_nyito_interstitial' => '<div id="adoceangemhupjfkhnrlcr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_nyito.interstitial */
        ado.slave("adoceangemhupjfkhnrlcr", {myMaster: "93_RRusxbIx6ryHpPmN9R3NOgtCq7h678PPw22TqRBn.w7" });
        </script>',
    )
),
'Propeller_sport_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.sport_cikkoldal */
        ado.master({id: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_sport_cikkoldal_leaderboard' => '<div id="adoceangemhuxcngmtsgox"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.leaderboard */
        ado.slave("adoceangemhuxcngmtsgox", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_halfpage_felso' => '<div id="adoceangemhuqaitbmfjli"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuqaitbmfjli", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhuudenpqmljs"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhuudenpqmljs", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_roadblock_felso' => '<div id="adoceangemhulgjqjikjth"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhulgjqjikjth", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_roadblock_also' => '<div id="adoceangemhureltcogqen"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.roadblock_also */
        ado.slave("adoceangemhureltcogqen", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_interstitial' => '<div id="adoceangemhunbpjfjpnvd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.interstitial */
        ado.slave("adoceangemhunbpjfjpnvd", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_textlink' => '<div id="adoceangemhuygqgnvdosc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.textlink */
        ado.slave("adoceangemhuygqgnvdosc", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',

        'Propeller_sport_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhukcgqiwisfy"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.sport_cikkoldal.kepes_ajanlo */
        ado.slave("adoceangemhukcgqiwisfy", {myMaster: "2WCmi0Fn8l5X5VUfRPaEQ2V0sOlNwYLejzRwTUbF__j.37" });
        </script>',
    )
),
'Propeller_video_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.video_nyito */
        ado.master({id: "ir.hlpRjrAWZsUGWT.4muRtKIfajt160musmuEpIHtH.n7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_video_nyito_leaderboard' => '<div id="adoceangemhumbpfousnnk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_nyito.leaderboard */
        ado.slave("adoceangemhumbpfousnnk", {myMaster: "ir.hlpRjrAWZsUGWT.4muRtKIfajt160musmuEpIHtH.n7" });
        </script>',

        'Propeller_video_nyito_halfpage_felso' => '<div id="adoceangemhuspqihkpuyq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_nyito.halfpage_felso */
        ado.slave("adoceangemhuspqihkpuyq", {myMaster: "ir.hlpRjrAWZsUGWT.4muRtKIfajt160musmuEpIHtH.n7" });
        </script>',

        'Propeller_video_nyito_medium_rectangle_also' => '<div id="adoceangemhuzbgmbsmsif"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_nyito.medium_rectangle_also */
        ado.slave("adoceangemhuzbgmbsmsif", {myMaster: "ir.hlpRjrAWZsUGWT.4muRtKIfajt160musmuEpIHtH.n7" });
        </script>',

        'Propeller_video_nyito_interstitial' => '<div id="adoceangemhupaipkxijdp"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_nyito.interstitial */
        ado.slave("adoceangemhupaipkxijdp", {myMaster: "ir.hlpRjrAWZsUGWT.4muRtKIfajt160musmuEpIHtH.n7" });
        </script>',
    )
),
'Propeller_video_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.video_cikkoldal */
        ado.master({id: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_video_cikkoldal_leaderboard' => '<div id="adoceangemhuxjplpnkufr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.leaderboard */
        ado.slave("adoceangemhuxjplpnkufr", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_halfpage_felso' => '<div id="adoceangemhuqhkifwmgbg"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuqhkifwmgbg", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhuukgsclejvq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhuukgsclejvq", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_roadblock_felso' => '<div id="adoceangemhulnlfnsrgjf"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhulnlfnsrgjf", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_roadblock_also' => '<div id="adoceangemhurlnigionfl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.roadblock_also */
        ado.slave("adoceangemhurlnigionfl", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_interstitial' => '<div id="adoceangemhuniroitgllb"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.interstitial */
        ado.slave("adoceangemhuniroitgllb", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_textlink' => '<div id="adoceangemhuyncmqpllta"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.textlink */
        ado.slave("adoceangemhuyncmqpllta", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',

        'Propeller_video_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhukjifmqqpgw"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.video_cikkoldal.kepes_ajanlo */
        ado.slave("adoceangemhukjifmqqpgw", {myMaster: "YqZcS1banRSguEmecjFbqR6CA6Ox4VKmpqhW3wRUK.X.B7" });
        </script>',
    )
),
'Propeller_tag_nyito' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.tag_nyito */
        ado.master({id: ".kZR_9ZQHDK8DcHMG5vNezVYTnv95VuNjJO4Kv4Ea4..j7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_tag_nyito_leaderboard' => '<div id="adoceangemhumhnhboikne"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_nyito.leaderboard */
        ado.slave("adoceangemhumhnhboikne", {myMaster: ".kZR_9ZQHDK8DcHMG5vNezVYTnv95VuNjJO4Kv4Ea4..j7" });
        </script>',

        'Propeller_tag_nyito_halfpage_felso' => '<div id="adoceangemhusfpkkternk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_nyito.halfpage_felso */
        ado.slave("adoceangemhusfpkkternk", {myMaster: ".kZR_9ZQHDK8DcHMG5vNezVYTnv95VuNjJO4Kv4Ea4..j7" });
        </script>',

        'Propeller_tag_nyito_medium_rectangle_also' => '<div id="adoceangemhuzheoelsomz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_nyito.medium_rectangle_also */
        ado.slave("adoceangemhuzheoelsomz", {myMaster: ".kZR_9ZQHDK8DcHMG5vNezVYTnv95VuNjJO4Kv4Ea4..j7" });
        </script>',

        'Propeller_tag_nyito_interstitial' => '<div id="adoceangemhupggrnqofhj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_nyito.interstitial */
        ado.slave("adoceangemhupggrnqofhj", {myMaster: ".kZR_9ZQHDK8DcHMG5vNezVYTnv95VuNjJO4Kv4Ea4..j7" });
        </script>',
    )
),
'Propeller_tag_cikkoldal' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.tag_cikkoldal */
        ado.master({id: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_tag_cikkoldal_leaderboard' => '<div id="adoceangemhuxpnncxpqjl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.leaderboard */
        ado.slave("adoceangemhuxpnncxpqjl", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_halfpage_felso' => '<div id="adoceangemhuqnikipssvw"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.halfpage_felso */
        ado.slave("adoceangemhuqnikipssvw", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_medium_rectangle_also' => '<div id="adoceangemhuuafegujfkk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.medium_rectangle_also */
        ado.slave("adoceangemhuuafegujfkk", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_roadblock_felso' => '<div id="adoceangemhuldkhqlhtzv"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.roadblock_felso */
        ado.slave("adoceangemhuldkhqlhtzv", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_roadblock_also' => '<div id="adoceangemhurbmkjrdkuf"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.roadblock_also */
        ado.slave("adoceangemhurbmkjrdkuf", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_interstitial' => '<div id="adoceangemhunopqlmmhpv"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.interstitial */
        ado.slave("adoceangemhunopqlmmhpv", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_textlink' => '<div id="adoceangemhuydrndjrhiu"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.textlink */
        ado.slave("adoceangemhuydrndjrhiu", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',

        'Propeller_tag_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhukpghpjgmvq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.tag_cikkoldal.kepes_ajanlo */
        ado.slave("adoceangemhukpghpjgmvq", {myMaster: "rok1OKX73bGMV07jS7ueoYXAIMzFkONWWrlDPLZomcj.37" });
        </script>',
    )
),
'Propeller_mobil' => array(
    'master' =>
        '<script type="text/javascript">
        /* (c)AdOcean 2003-2017, MASTER: IKO_Digital.propeller.hu.mobil */
        ado.master({id: "1VZQQX.858IeVMIMLi__FzmvThHQiluBKBq46m2pC2b.t7", server: "gemhu.adocean.pl", keys: myAdoceanKeys, vars: myAdoceanVars });
        </script>',

    'slaves' => array(
        'Propeller_mobil_felso' => '<div id="adoceangemhuoirspxskmu"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.mobil.felso */
        ado.slave("adoceangemhuoirspxskmu", {myMaster: "1VZQQX.858IeVMIMLi__FzmvThHQiluBKBq46m2pC2b.t7" });
        </script>',

        'Propeller_mobil_kozepso' => '<div id="adoceangemhuljijdvmpdp"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.mobil.kozepso */
        ado.slave("adoceangemhuljijdvmpdp", {myMaster: "1VZQQX.858IeVMIMLi__FzmvThHQiluBKBq46m2pC2b.t7" });
        </script>',

        'Propeller_mobil_also' => '<div id="adoceangemhurhkmmkjgyz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.mobil.also */
        ado.slave("adoceangemhurhkmmkjgyz", {myMaster: "1VZQQX.858IeVMIMLi__FzmvThHQiluBKBq46m2pC2b.t7" });
        </script>',
    )
)
);

