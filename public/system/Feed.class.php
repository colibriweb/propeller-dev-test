<?php
/*
	RSS-feldolgozó class
	(a hírgyűjtő crawler nem ezt használja)

	használat, pl:

	require_once('../../system/Feed.class.php');
	$feed = new Feed($url);
	$feed->schema = 'rss20';
	$feed->cacheTime = 600;
	$feed->getItems = 5;

	if($items = $feed->getFetched()) {
		// sikeres volt a feldolgozás
	}
	else {
		$smarty->assign('error', $feed->error);
	}

*/

class Feed {

	/**
	* Feldolgozandó XML url-je
	* @var string
	*/
	var $feed;

	/**
	* Feldolgozáshoz szükséges séma
	* @var string
	*/
	var $schema = 'rss';

	/**
	* Cache élettartama
	* @var integer
	*/
	var $cacheTime = 0;

	/**
	* Cache-könyvtár elérési útvonala
	* @var string
	*/
	var $cacheDir = '../../system/cache/feed/';

	/**
	* Az RSS-ből képzett cachefile neve
	* @var string
	*/
	var $cacheFile;

	/**
	* Feldolgozandó elemek száma
	* @var integer
	*/
	var $maxItems = 20;

	/**
	* Szükséges elemek száma
	* @var integer
	*/
	var $getItems = 10;

	/**
	* HTML és egyéb entitások eltávolítása
	* @var boolean
	*/
	var $strip = true;



	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Feed($feed) {

		$this->feed = $feed;
		$this->cacheFile = md5($feed);

	}


	/**
	* Feed elemeienek lekérése a feldolgozó sémán keresztül
	* @return mixed Sikeres művelet esetén a feed elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function getFetched() {

		if(!$this->cacheTime) {
			$res = $this->{'schema_'.$this->schema}();
			return array_slice($res, 0, $this->getItems);
		}
		else {
			$diff = @(time() - filemtime($this->cacheDir.$this->cacheFile));
			if($diff > $this->cacheTime) { // nem elég friss

				$res = $this->{'schema_'.$this->schema}();

				$serialized = serialize($res);
				if($f = @fopen($this->cacheDir.$this->cacheFile, 'w')) {
					fwrite($f, $serialized, strlen($serialized));
					fclose($f);
				}

				return array_slice($res, 0, $this->getItems);

			}
			else { // elég friss
				$res = unserialize(join('', file($this->cacheDir.$this->cacheFile)));
				return array_slice($res, 0, $this->getItems);
			}
		}

	}

	/**
	* RSS feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az RSS elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_rss() {

		$source = $this->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeText($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<enclosure url=[\'\"](.*?)[\'\"]|si", $value, $enclosure);
			$assoc[$i]['enclosure'] = $enclosure[1][0];

			if(empty($assoc[$i]['enclosure'])) {
				preg_match_all("|<media:content url=[\'\"](.*?)[\'\"]|si", $value, $enclosure);
				$assoc[$i]['enclosure'] = $enclosure[1][0];
			}

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Puding.hu XML feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az XML elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_xml_puding() {

		$source = $this->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);

			preg_match_all("|<imgurl>(.*?)</imgurl>|si", $value, $imgurl);
			$assoc[$i]['imgurl'] = $imgurl[1][0];

			preg_match_all("|<url>(.*?)</url>|si", $value, $url);
			$assoc[$i]['url'] = $url[1][0];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Atom feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az Atom elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_atom() {

		$source = $this->getFileContent($this->feed);
		preg_match_all("|<entry(.*?)>(.*?)</entry>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);

			preg_match_all("|<content(.*?)>(.*?)</content>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeText($description[2][0], $encoding);

			preg_match_all("|href=[\'\"](.*?)[\'\"]|si", $value, $link);
			$assoc[$i]['link'] = $link[1][0];

			preg_match_all("|<published>([^<]+)</published>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Metropolita RSS feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az RSS elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_metropolita() {

		$source = $this->getFileContent($this->feed);

		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);

//			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
//			$assoc[$i]['description'] = $this->getSafeText($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = $link[1][0];

//			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
//			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<enclosure>(.*?)</enclosure>|si", $value, $enclosure);
			$assoc[$i]['enclosure'] = $enclosure[1][0];

			if(!$enclosure[1][0]) unset($assoc[$i]);
			else $i++;

			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Zene.hu RSS feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az RSS elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_zenehu() {

		$source = $this->getFileContent($this->feed);

		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();

		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeText($title[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = $link[1][0];

			preg_match_all("|<enclosure url=\"(.*?)\"|si", $value, $enclosure);
			$assoc[$i]['enclosure'] = $enclosure[1][0];

			if(!$enclosure[1][0]) unset($assoc[$i]);
			else $i++;

			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* HTML elemeket és speciális karaktereket tartalmazó string plain textté konvertálása
	* @param string $string Szövegdarab
	* @param string $string Szövegdarab kódolása
	* @return string Sikeres művelet esetén a plain stringgel, egyébként false-sal tér vissza
	*/
	function getSafeText($string, $encoding = 'UTF-8') {

		$string = iconv($encoding, 'utf-8//TRANSLIT', $string);
		$string = strtr($string, array('<![CDATA[' => '', ']]>' => ''));

		if($this->strip)
		$string = strip_tags(html_entity_decode(strip_tags($string), ENT_QUOTES, $encoding));

		$string = str_replace(array('&#8211;', '&#8222;', '&#8221;', '&#8230;', '&#337;', '&apos;', '&#369;'), array('-', '"', '"', '...', 'ő', '"', 'ű'), $string);
		return trim(preg_replace("/\s\s+/", " ", $string));

	}

	/**
	* Visszaadja egy helyi- vagy távoli szerveren lévő fájl tartalmát
	* @param string $url Elérési útvonal
	* @return string Sikeres művelet esetén a fájl tartalmával, egyébként false-sal tér vissza
	*/
	function getFileContent($url) {
/*
		$handle = fopen($url, 'rb');
		$string = '';
		while(!feof($handle)) {
		$string .= fread($handle, 8192);
		}
		fclose($handle);
*/

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 7);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1');

		ob_start();

		curl_exec($ch);
		curl_close($ch);
		$string = ob_get_contents();

		ob_end_clean();

		return $string;

	}

}


?>
