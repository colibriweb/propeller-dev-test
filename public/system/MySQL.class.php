<?php
// karbantartás idejére a site lezárása
/*if(!in_array($_SERVER['REMOTE_ADDR'], array('5.187.205.61'))) {
	$protocol = "HTTP/1.0";
	if( "HTTP/1.1" == $_SERVER["SERVER_PROTOCOL"] ) $protocol = "HTTP/1.1";
	header( "$protocol 503 Service Unavailable", true, 503 );
	header( "Retry-After: 1800" );
	die('A Propeller átmenetileg nem elérhető, de már dolgozunk a hiba kijavításán. Köszönjük a türelmet!');
}*/

define('BUILD', 325); // verzió-szám a css, js-fájlok végére (a böngészőcache-törlés kikényszerítése miatt)

define('ENV', 'local'); // prod | dev | local

define('SHARE_IMAGE', 'http://static.propeller.hu/images/default/fb_share_image.png');

if(ENV == 'dev') { // local fejleszőtkörnyezet
	define('ROOT', '/usr/local/var/www/propeller/public');
	define('STTC', 'http://static.propeller.dev'); // static host
	define('HOST', 'propeller.dev'); // host

	ini_set('error_reporting', E_ALL ^ E_DEPRECATED);
	ini_set('display_errors', 1);

	define('_DBNAME', 'propeller');
	define('_DBUSER', 'root');
	define('_DBPASS', 'RReqw3');
}
elseif (ENV == 'local') { // local win fejleszőtkörnyezet
	define('ROOT', 'c:\wamp64/www/colibrimedia/prop/public');
	define('STTC', 'http://localhost/colibrimedia/prop/public_static'); // static host
	define('HOST', 'http://localhost/colibrimedia/prop'); // host

	ini_set('error_reporting', E_ALL ^ E_DEPRECATED);
	ini_set('display_errors', 1);

	define('_DBNAME', 'propeller');
	define('_DBUSER', 'root');
	define('_DBPASS', '');
}
else { // éles szerveren
	define('ROOT', '/var/www/propeller/public');
	define('STTC', 'http://static.propeller.hu'); // static host
	define('HOST', 'propeller.hu'); // host

	ini_set('error_reporting', 0);
	ini_set('display_errors', 0);

	// ha változik a DB-hozzáférés, a /hirlevel/DB.class.php -ben is módosítani kell
	define('_DBNAME', 'propeller');
	define('_DBUSER', 'propeller');
	define('_DBPASS', 'GZtfsd675fde');
}

define('_DBHOST', 'localhost');
define('_DBPORT', '3306');
define('_DBPREF', '');

define('_ERROR', 'zsolt.liebig@gmail.com'); // adatbázis hiba esetén riasztás megy ide

// swen_content tábla gyorsítása
// ahol lehet, a WHERE -hez be van írva, egy "id > _SPD_CONTENT_ID" feltétel, így nem a teljes táblát túrja
// időnként érdemes kézzel átírni, kb. 2000-rel legyen kisebb ez a szám, mint a "swen_content" tábla legnagyobb ID-je
define('_SPD_CONTENT_ID', 0);

// swen_content_comments tábla gyorsítása
// (lásd feljebb), a profil oldalon, a hozzászólásoknál van csak jelentősége
// kb. 10000-rel legyen kisebb ez a szám, mint a "swen_content_comments" tábla legnagyobb ID-je
define('_SPD_COMMENT_ID', 1780000);

$time_start = microtime(true);

class Database {

	var $connection;
	var $dbhost;
	var $dbport;
	var $dbuser;
	var $dbpass;
	var $dbname;
	var $persistency;
	var $cacheDir;
	var $exitOnError = true;
	var $errorMessage = true;

	/**
	* Adatbáziskapcsolat felépítese, adatbázis kiválasztása és a kapcsolat erőforrásmutatójának létrehozása
	* @param string $dbhost Adatbázis hosztnév
	* @param integer $dbport Kapcsolódási port
	* @param string $dbuser Felhasználónév az adatbázishoz
	* @param string $dbpass Jelszó az adatbázishoz
	* @param string $dbname Adatbázis neve
	* @param boolean $persistency Perzisztens kapcsolat
	* @return resource Sikeres csatlakozás és adatbáziskiválasztás esetén az adatbáziskapcsolat erőforrásmutatójával, egyébként false-sal tér vissza
	*/
	function Database($dbhost, $dbport, $dbuser, $dbpass, $dbname, $persistency) {

		$this->dbhost = $dbhost;
		$this->dbport = $dbport;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dbname = $dbname;
		$this->persistency = $persistency;
		$this->cacheDir = ROOT.'/system/cache/db/';

	}

	function doConnection() {

		if(!$this->persistency) {
			$this->connection = @mysql_connect($this->dbhost.':'.$this->dbport, $this->dbuser, $this->dbpass);
		}
		else {
			$this->connection = @mysql_pconnect($this->dbhost.':'.$this->dbport, $this->dbuser, $this->dbpass);
		}

		if(!$this->connection) { // nincs kapcsolat
			header('Location: /503.php');
			exit;
		}
		else {
			if(!mysql_select_db($this->dbname)) { // nincs ilyen adatbázis
				mysql_close($this->connection);
				die('Unknown database.');
				exit;
			}
			mysql_query("SET NAMES utf8", $this->connection);
			return $this->connection;
		}
		return false;

	}

	function Query($query) {

		if(!$this->connection) $this->doConnection();

//		$sq_start = microtime(true);
		$result = mysql_query($query, $this->connection);
		$this->errorHandler($query);
/*
        $sq_diff = number_format((microtime(true) - $sq_start), 4);
		if($sq_diff > (float)0.4) {
			$fh = fopen('/var/www/propeller/public/slowquery.txt', 'a');
			$str = "---------- ".date('Y-m-d H:i:s')."\t".$sq_diff."\n".$query."\n\n";
			fwrite($fh, $str);
			fclose($fh);
		}
*/
		return $result;

	}

	/**
	* Cachelt adatbázisművelet futtatása
	* @param string $query Adatbázisműveletet
	* @param string $cacheID Cache azonosító
	* @param string $lifeTime Cache élettartam
	* @return array Sikeres művelet esetén az adatbázisművelet eredményhalmazával (tömbben), egyébként false-sal tér vissza
	*/
	function QueryCache($query, $cacheID, $lifeTime = 3600) {

		if(!file_exists($this->cacheDir.$cacheID) || (filemtime($this->cacheDir.$cacheID) < time()-(int)$lifeTime)) { // lejárt cachefájl
			$assoc = array();
			$res = $this->Query($query);
			while($row = $this->fetchAssoc($res)) {
				$assoc[] = $row;
			}
			touch($this->cacheDir.$cacheID);
			$f = fopen($this->cacheDir.$cacheID, "w");
			fwrite($f, serialize($assoc));
			fclose($f);
			return $assoc;
		}
		else {
			$bc = file_get_contents($this->cacheDir.$cacheID);
			return unserialize($bc);
		}
	}

	/**
	* Cachelt adatbázisműveletet eldobása
	* @param string $cacheID Cache azonosító
	* @return void
	*/
	function DropQueryCache($cacheID) {

		@unlink($this->cacheDir.$cacheID);

	}

	/**
	* Megtisztítja a szövegdarabot az adatbázis számára veszélyes karakterektől
	* @param mixed $string Szövegdarab
	* @return string Megtisztított szövegdarabbal tér vissza
	*/
	function escape($string) {

		if(!$this->connection) $this->doConnection();
		//return is_array($string) ? array_map('mysql_escape_string', $string) : mysql_escape_string($string);
		return is_array($string) ? array_map('mysql_real_escape_string', $string) : mysql_real_escape_string($string);

	}

	/**
	* Adatbázisművelet végrehajtása és lekérdezése sorszámozott és asszociatív tömbbe
	* @param string $result Adatbázisművelet erőforrásmutatója {@link Query()}
	* @return array Sikeres művelet esetén az adatbázisművelet eredményhalmazával (tömbben), egyébként false-sal tér vissza
	*/
	function fetchArray($result) {

		return mysql_fetch_array($result);

	}

	/**
	* Adatbázisművelet végrehajtása és lekérdezése asszociatív tömbbe
	* @param string $result Adatbázisművelet erőforrásmutatója {@link Query()}
	* @return array Sikeres művelet esetén az adatbázisművelet eredményhalmazával (tömbben), egyébként false-sal tér vissza
	*/
	function fetchAssoc($result) {

		return mysql_fetch_assoc($result);

	}

	/**
	* Adatbázisművelet végrehajtása és lekérdezése objektumként
	* @param string $result Adatbázisművelet erőforrásmutatója {@link Query()}
	* @return obj Sikeres művelet esetén az adatbázisművelet eredményhalmazával (objektumként), egyébként false-sal tér vissza
	*/
	function fetchObject($result) {

		return mysql_fetch_object($result);

	}

	/**
	* Adatbázisművelet végrehajtása és lekérdezése sorszámozott tömbbe
	* @param string $result Adatbázisművelet erőforrásmutatója {@link Query()}
	* @return array Sikeres művelet esetén az adatbázisművelet eredményhalmazával (asszociatív tömbben), egyébként false-sal tér vissza
	*/
	function fetchRow($result) {

		return mysql_fetch_row($result);

	}

	/**
	* Adatbázisművelet végrehajtása és az eredményrekordok számának lekérdezése
	* @param string $result Adatbázisművelt erőforrásmuatója {@link Query()}
	* @return integer Sikeres művelet esetén az adatbázisművelet eredményrekordjaink darabszámával, egyébként false-sal tér vissza
	*/
	function numRows($result) {

		return mysql_num_rows($result);

	}

	/**
	* Adatbázisművelet végrehajtása során érdekelt rekordok száma
	* @param string $result Adatbázisművelt erőforrásmuatója {@link Query()}
	* @return integer Sikeres művelet esetén az adatbázisművelet végrehajtása során érdekelt rekordok darabszámával, egyébként false-sal tér vissza
	*/
	function affectedRows($result) {
		/*
		update esetén szart se ad vissza a mysql_affected_rows()
		ezért preg_match kell neki...

		if($result) {
			return mysql_affected_rows();
		}
		return false;
		*/

		preg_match("/^[^0-9]+([0-9]+)[^0-9]+([0-9]+)[^0-9]+([0-9]+).*$/", mysql_info($this->connection), $array);

		return $array[1];

	}

	/**
	* A következő AUTO_INCREMENT érték lekérdezése
	* @param string $table Adatbázistábla neve
	* @param string $field AUTO_INCREMENT típusú mező neve
	* @return integer Sikeres művelet esetén az AUTO_INCREMENT mező következő értékével, egyébként false-sal tér vissza
	*/
	function nextID($table, $field) {

		return NULL;

	}

	/**
	* Az utolsó INSERT adatbázisművelet által létrehozott AUTO_INCREMENT érték lekérdezése
	* @param string $result AUTO_INCREMENT típusű mező neve
	* @param string $serialField AUTO_INCREMENT típusű mező neve
	* @param string $table Adatbázistábla neve
	* @return integer Sikeres művelet esetén az utolsó INSERT adatbázisművelet AUTO_INCREMENT mezőtípusának utolsó értékével, egyébként false-sal tér vissza
	*/
	function lastInsertID($result, $serialField, $table) {

		return mysql_insert_id();

	}

	/**
	* Adatbázisművelet és adatbáziskapcsolat erőforrásmutatójának felszabadítása és lezárása
	* @return void
	*/
	function closeConnection() {

		if(isset($this->connection)) {

	//		mysql_free_result();

			mysql_close();
		}
/*

		if(isset($this->connection)) {
			if(isset($this->result)) {
				mysql_free_result($this->result);
			}
			$result = mysql_close($this->connection);
			$this->connection = null;
			return $result;
		}
		return false;
*/
	}

	/**
	* Jelentés készítése az esetleges hibás adatbázisműveletről
	* @return void
	*/
	function errorHandler($query) {

		if(mysql_errno($this->connection) != 0) {
			if(ENV == 'dev') {
				echo $query.'<br><br>'.mysql_errno($this->connection)." - ".mysql_error($this->connection);
			}
			else { // éles szerveren nincs kiírás, mailben küldjük a hibát
				if($this->errorMessage) {
					$msg = date('r')."\n\n".$query."\n\n".mysql_errno($this->connection)." - ".mysql_error($this->connection);
					@mail(_ERROR, 'Adatbázis hiba: propeller.hu', $msg, 'From: '._ERROR);
				}
			}

			if($this->exitOnError) {
				die('A Propeller átmenetileg nem elérhető. Értesültünk a hibáról, a helyreállításig egy kis türelmet kérünk!');
				exit;
			}
		}

	}

}

?>
