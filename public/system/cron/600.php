<?php

require('../MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);
$db->exitOnError = false;
$db->errorMessage = false;

require('../Environment.class.php');
$env = new Environment();
$env->db = $db;

//ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
set_time_limit(600);

require('../../modules/content/Content.class.php');
$content = new Content();
$content->env = $env;



	if(isset($_GET['debug'])) {
	?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="hu-HU">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>cron - 600</title>
<style type="text/css">
* { font: normal 11px Courier New, Courier, monospace; }
hr { height: 8px; background-color: #e8edef; border: none; }
</style>
</head>
<body>

	600.php
	<?php
	}

	// feedek feldolgozása
	require('cron_fetch.php');


	// content_temp táblából a counter értékek átmozgatása
	$res_ct = $db->Query("UPDATE "._DBPREF."content, (SELECT count(content_id) db, content_id FROM "._DBPREF."content_counter_temp GROUP BY content_id) temp SET counter = counter + temp.db WHERE temp.content_id = "._DBPREF."content.id");
	$res_ct_t = $db->Query("TRUNCATE TABLE "._DBPREF."content_counter_temp");

	// partner dobozok legenerálása
	require('cron_partner.php');


	// címlapi partner feedek
	$content->q_partners(400);
	echo 'partners-ok<br>';

	// hírkereső doboz számára a lefrissítő
	require_once('../Feed.class.php');
	$feed = new Feed('http://rss.hirkereso.hu/rss.fc/topoldal/84.xml?partner=propeller');
	$feed->cacheTime = 60;
	$feed->maxItems = 8;
	$feed->getItems = 8;
	$feed->schema = 'rss';
	if($items = $feed->getFetched()) {
		echo 'hirkereso-ok<br>';
	}

	if(isset($_GET['debug'])) {
	?>
</body>
</html>
	<?php
	}
?>