<?php

class FeedCron extends Environment {

	/**
	* Feldolgozandó XML url-je
	* @var string
	*/
	var $feed;

	/**
	* Feldolgozáshoz szükséges séma
	* @var string
	*/
	var $schema = 'rss';

	/**
	* Feldolgozandó elemek száma
	* @var integer
	*/
	var $maxItems = 8; // élesen: 8

	/**
	* HTML és egyéb entitások eltávolítása
	* @var boolean
	*/
	var $strip = true;

	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function FeedCron($feed) {

		$this->feed = $feed;

	}


	/**
	* Feed elemeienek lekérése a feldolgozó sémán keresztül
	* @return mixed Sikeres művelet esetén a feed elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function getFetched() {

		return $this->{'schema_'.$this->schema}();

	}

	/**
	* Szabványos RSS feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az RSS elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_rss() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		if($encoding == 'utf-16') $encoding = 'utf-8'; // hir24 -es rss -ek utf16-nak hazudják magukat

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);

			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description(.*?)>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[2][0], $encoding);

			// wordpress szarsága
			$assoc[$i]['description'] = trim(preg_replace('/The post(.*)appeared first(.*)\./', '', $assoc[$i]['description']));

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = trim(strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>'', '?portalstate=rss'=>'', '#rss'=>'')));

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate); //([^<]+)
			$assoc[$i]['pubDate'] = strtr($pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Szabványos RSS feldolgozása asszociatív tömbbe (kategóriákat egyben tartalmazó)
	* @return mixed Sikeres művelet esetén az RSS elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_rss_category() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>'', '~~Fokusz'=>''));

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = strtr( $pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<category>(.*?)</category>|si", $value, $category);
			$assoc[$i]['category'] = $this->getSafeTextCron($category[1][0], $encoding);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Szabványos RDF feldolgozása asszociatív tömbbe
	* @return mixed Sikeres művelet esetén az RDF elemeit tartalmazó tömbbel, egyébként false-sal tér vissza
	*/
	function schema_rdf() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item rdf(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<dc:date>([^<]+)</dc:date>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}


	function schema_atv() {

		$this->maxItems = 20;

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		if($encoding == 'utf-16') $encoding = 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);

			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description(.*?)>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[2][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = trim(strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>'', '?source=hirkereso'=>'')));

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate); //([^<]+)
			$assoc[$i]['pubDate'] = strtr($pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			if(!preg_match("/(IMF|orbán|kövér|matolcsy|baleset|BKV|mszp|fidesz|lmp|jobbik|budapest|magyar)/ieu", $assoc[$i]['title'].' '.$assoc[$i]['description'])) {

				unset($assoc[$i]);
			}

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* AS lapok (feedportla.com -os elbaszott feed -ek)
	*/
	function schema_aslapok() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description(.*?)>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[2][0], $encoding);

			preg_match_all("|link=(.*?)\"|si", $value, $link);
			$assoc[$i]['link'] = trim(urldecode($link[1][0]));

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate); //([^<]+)
			$assoc[$i]['pubDate'] = strtr($pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* AS lapok (feedportla.com -os elbaszott feed -ek)
	*/
	function schema_origo() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description(.*?)>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[2][0], $encoding);

			preg_match_all("|isPermaLink=\"false\">(.*?)</guid>|si", $value, $link);
			$assoc[$i]['link'] = trim(urldecode($link[1][0]));

			if(empty($assoc[$i]['link'])) {
				preg_match_all("|;link=(.*?)[\"]|si", $value, $link);
				$assoc[$i]['link'] = trim(urldecode($link[1][0]));
			}

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate); //([^<]+)
			$assoc[$i]['pubDate'] = strtr($pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}


	/**
	* Hírszerző videók
	*/
	function schema_youtube() {

		$this->maxItems = 20;

		$source = $this->env->getFileContent($this->feed);
		$source = html_entity_decode($source);

		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);
			$assoc[$i]['title'] = str_replace(' - 444', '', $assoc[$i]['title']);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description2);
			preg_match_all("|<span>(.*?)</span>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = str_replace('&feature=youtube_gdata', '', $link[1][0]);

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|\=(.*?)\&feature|si", $link[1][0], $video);
			$assoc[$i]['video_url'] = $video[1][0];

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Index videók 4:3
	*/

	function schema_mediamatters_video() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		if($encoding == 'utf-16') $encoding = 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);

			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description(.*?)>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[2][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = trim(strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>'')));

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate); //([^<]+)
			$assoc[$i]['pubDate'] = strtr($pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<videoembed>(.*?)</videoembed>|si", $value, $video); //([^<]+)
			$assoc[$i]['video_url'] = strtr($video[1][0], array('<![CDATA['=>'', ']]>'=>'', '"600"' => '"590"', '"338"'=>'"332"'));

			$assoc[$i]['video_url'] = preg_replace('/(<br.*?)<\/a>/', '', $assoc[$i]['video_url']);


			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Index videók 16:9
	*/
/*	function schema_index169() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$description[1][0] = preg_replace("/(\[indexvideo[A-Za-z0-9:\-_\/]+\])/", "", $description[1][0]);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|\[indexvideo169:(.*?)\]|si", $value, $video);
			$assoc[$i]['video_url'] = $video[1][0];

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}
*/
	/**
	* Index videók a videobomb-on keresztül
	*/
	function schema_index_indavideo() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);
			$assoc[$i]['description'] = str_replace('Kövesse az IndexVideót a Facebookon is!', '', $assoc[$i]['description']);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>(.*?)</pubDate>|si", $value, $pubDate); //([^<]+)
			$assoc[$i]['pubDate'] = strtr($pubDate[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<embed>(.*?)</embed>|si", $value, $video);
			$assoc[$i]['video_url'] = strtr($video[1][0], array('<![CDATA['=>'', ']]>'=>'', '"460"' => '"590"', '"300"'=>'"332"'));

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* VideoSmart videók
	*/
	function schema_videosmart() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron(strtr($description[1][0], array('<![CDATA[' => '', ']]>' => '')), $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<object(.*)<\/object>|si", $value, $video_url);
			$assoc[$i]['video_url'] = '<object'.$video_url[1][0].'</object>';

			preg_match_all("|<category>(.*?)</category>|si", $value, $category);
			$assoc[$i]['category'] = $this->getSafeTextCron($category[1][0], $encoding);

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* juj.hu videók
	*/
	function schema_nemzetisport() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<guid>http://www.nemzetisport.hu/([^\/]+)\/|si", $value, $rovat);
			$assoc[$i]['rovat'] = $rovat[1][0];

			if(!in_array($assoc[$i]['rovat'], array('bajnokok_ligaja','foci_eb_2012','london_2012','amerikai_sportok','labdarugo_nb_i'))) {
				unset($assoc[$i]);
			}

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* RTL Hírek videók
	*/
	function schema_rtl() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$title[1][0] = $this->getSafeTextCron($title[1][0], $encoding);
			$assoc[$i]['title'] = str_replace(array('NC – ', 'NC - '), array('', ''), $title[1][0]);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|videogallery/\d+\:(.*)</link>|si", $value, $video);
			$assoc[$i]['video_url'] = $video[1][0];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* Hírtv gazdaság
	*/
	function schema_hirtv_gazdasag() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<link>http\:\/\/www\.hirtv\.hu\/gazdasag\/(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}


	/**
	* Hírtv belföld
	*/
	function schema_hirtv_belfold() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<link>http\:\/\/www\.hirtv\.hu\/belfold\/(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}


	/**
	* Borsonline elbaszott pubdate
	*/
	function schema_bors() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$ex = explode(' ', $pubDate[1][0]);

			$assoc[$i]['pubDate'] = $ex[0].' '.$ex[3].' '.$ex[2].' '.$ex[1].' '.$ex[4].' '.$ex[5];

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* tv2, tenyek.hu
	*/
	function schema_tv2() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$title[1][0] = str_replace(array('Tények - ', ' &#8211; Tények-riport'), array('', ''), $title[1][0]);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<fullarticle>(.*?)</fullarticle>|si", $value, $description);
			$description[1][0] = preg_replace('/<strong>(.*?)<\/strong>/', '', $description[1][0]);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<tv2:videouri>(.*?)</tv2:videouri>|si", $value, $video);
			$assoc[$i]['video_url'] = $video[1][0];

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* tabuTV
	*/
	function schema_tabutv() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|<videoembed>(.*)</videoembed>|si", $value, $video);
			$assoc[$i]['video_url'] = html_entity_decode($video[1][0]);

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}

	/**
	* noltv
	*/
	function schema_noltv() {

		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|link=(.*?)\"|si", $value, $link);
			$assoc[$i]['link'] = trim(urldecode($link[1][0]));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|video/(.*)\.html|si", $assoc[$i]['link'], $video);
			$assoc[$i]['video_url'] = html_entity_decode($video[1][0]);

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}


	/**
	* hvgvideo
	*/
	function schema_hvgvideo() {
// <iframe src="http://player.vimeo.com/video/34516054" width="580" height="326" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
/*
690 388

580 326
*/
		$source = $this->env->getFileContent($this->feed);
		preg_match_all("|<item(.*?)>(.*?)</item>|si", $source, $content);

		preg_match("'encoding=[\'\"](.*?)[\'\"]'si", $source, $encoding);
		$encoding = isset($encoding[1]) ? $encoding[1] : 'utf-8';

		$i = 0;
		$assoc = array();
		foreach($content[0] as $value) {
			preg_match_all("|<title>(.*?)</title>|si", $value, $title);
			$assoc[$i]['title'] = $this->getSafeTextCron($title[1][0], $encoding);

			preg_match_all("|<description>(.*?)</description>|si", $value, $description);
			$assoc[$i]['description'] = $this->getSafeTextCron($description[1][0], $encoding);

			preg_match_all("|<link>(.*?)</link>|si", $value, $link);
			$assoc[$i]['link'] = strtr($link[1][0], array('<![CDATA['=>'', ']]>'=>''));

			preg_match_all("|<pubDate>([^<]+)</pubDate>|si", $value, $pubDate);
			$assoc[$i]['pubDate'] = $pubDate[1][0];

			preg_match_all("|clip_id=([0-9]+)\"|si", $value, $video);
			$assoc[$i]['video_url'] = html_entity_decode($video[1][0]);

			if(empty($assoc[$i]['video_url'])) unset($assoc[$i]);

			$i++;
			if($i == $this->maxItems) { break; }
		}

		return $assoc;

	}


	/**
	* HTML elemeket és speciális karaktereket tartalmazó string plain textté konvertálása
	* @param string $string Szövegdarab
	* @param string $string Szövegdarab kódolása
	* @return string Sikeres művelet esetén a plain stringgel, egyébként false-sal tér vissza
	*/
	function getSafeTextCron($string, $encoding = 'UTF-8') {

		$string = iconv($encoding, 'utf-8//TRANSLIT', $string);
		$string = strtr($string, array('<![CDATA[' => '', ']]>' => ''));

		if($this->strip)
		$string = strip_tags(html_entity_decode(strip_tags($string), ENT_QUOTES, 'UTF-8'));

		$string = str_replace(array('&#8211;', '&#8222;', '&#8221;', '&#8230;', '&#337;', '&apos;', '&#369;', '#8217;', '&#039;'), array('-', '"', '"', '...', 'ő', '"', 'ű', "'", "'"), $string);

		$string = preg_replace('/A (.+) bejegyzés először a Abcug\.hu jelent meg\./i', '', $string);

		return trim(preg_replace("/\s\s+/", " ", $string));

	}

}


?>