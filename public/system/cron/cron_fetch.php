<?php

// rss-ek feldoglozása az automatikus hírhyűjtés számára

require('FeedCron.class.php');

if(isset($_GET['debug'])) echo '<hr />cron_fetch.php<br />'."\n";

function removeFakeTitle($string) { // faszságok kipucolása a címből
	$string = trim($string);
	$string = preg_replace("/^(borzalom|drámai?|hoppá|elképesztő|tragédia|sokkoló|gyász|baleset|túlélte|döbbenet)\!\s/i", "", $string);
	$string = preg_replace("/\s(\-|\–|\—)(\-|\–|\—)?\s(videó|fotó|kép)(k|val|kkal|pel|pekkel)?\!?$/i", "", $string);
	$string = preg_replace("/(18\+)/i", "", $string);
	return trim($string);
}

$sources = array();

// rss-ek lekérése
$res_s = $db->Query("SELECT id, category_id, feed, timediff, feed_schema, video_embed, modtime, lastpubdate, fetchrank FROM "._DBPREF."content_feeds
WHERE public = '1' AND modtime < NOW() - INTERVAL 2 MINUTE ORDER BY rand() DESC LIMIT 0, 20");
$num = $db->numRows($res_s);
while($row_s = $db->fetchAssoc($res_s)) {
	$sources[] = $row_s;
}

$feed_num = 0;
$all_item_num = 0;
if(isset($_GET['debug']))
echo '<pre>MT		Új	Rank	Schema		Feed</pre>'."\n";

// 1 rss feldolgozása
foreach($sources as $source) {
	$feed = new FeedCron($source['feed']);
	$feed->env = $env;
	$feed->schema = $source['feed_schema'];
	$item_num = 0;

	if($items = $feed->getFetched()) { // sikerült feldolgozni a feedet
		$query = '';
		foreach($items as $key) {
			$converted = (int)(strtotime($key['pubDate'])+(int)$source['timediff']);
			if(($converted > strtotime($source['lastpubdate'])) && ($converted < time()) && (mb_strlen($key['description'], 'UTF-8') > 120)) { // az utolsó frissülés óta
				$key = array_map('html_entity_decode', $key);
				$key['description_min'] = mb_substr($key['description'], 0, 120, 'UTF-8');

				$key = $db->escape($key);
				$key['pubDate'] = date("Y-m-d H:i:s"); //date("Y-m-d H:i:s", $converted);

				if(!empty($key['category'])) { // kategóriákat egyben tartalmazó feedből jön
					$catmap = array(
                     // hirado.hu
					'210' => array('belfold' => 1, 'gazdasag' => 1, 'kultura' => 6, 'bulvar' => 6, 'tudomany' => 4, 'auto' => 4, 'baleset_bunugy' => 6),
					// mr1-kossuth.hu
					'211' => array('HÍR - Külhon' => 2, 'HÍR - Itthon' => 1, 'HÍR - Tudomány' => 6, 'HÍR - Magazin' => 6, 'HÍR - Gazdaság' => 1, 'HÍR - Kultúra' => 6),
					// videosmart.hu
					'224' => array('Szabadidő' => 6, 'Számítástechnika' => 4, 'Mobil' => 4, 'Táplálkozás' => 6),
					// MNO
					'327' => array('belfold' => 1, 'tudomany' => 4, 'szines' => 6, 'bitvilag' => 4), //'kulfold' => 2,
					// hir24
					'328' => array('Belföld' => 1, 'Itthon' => 1, 'Külföld' => 2, 'SztárVilág' => 6, 'Szórakozás' => 6, 'Sztori' => 6, 'Színes oldal' => 6, 'Sötét oldal' => 6, 'InfoTech' => 4, 'Tech-Tud' => 4),
					// kisalföld
					'153' => array('Belföld' => 1, 'Sopron' => 1, 'Győr és környéke' => 1, 'Győr' => 1, 'Mosonmagyaróvár' => 1, 'Rábaköz' => 1, 'Sport' => 7)
					);

					if(isset($catmap[$source['id']][$key['category']]))
					$source_catID = $catmap[$source['id']][$key['category']];
					else
					continue;
				}
				else {
					$source_catID = $source['category_id'];
				}
				$alias = $feed->env->getSefUrl($key['title']);
				$key['title'] = removeFakeTitle($key['title']);

				// duplikáció?
				$res_dup = $db->Query("SELECT id FROM "._DBPREF."content_temp
				WHERE (title = '".$key['title']."' OR title LIKE '".$key['title']."%' OR
				description LIKE '".$key['description_min']."%') AND datetime < NOW() - INTERVAL 24 HOUR LIMIT 1");
				if($db->numRows($res_dup)) {
					//$msg = "Ez már volt:\n\n".$key['title']."\n\n".$key['description']."\n\n".$key['link'];
					//@mail('zsolt.liebig@gmail.com', 'Duplikáció', $msg, 'From: zsolt.liebig@gmail.com');
					continue;
				}



				$res = $db->Query("INSERT INTO "._DBPREF."content
				(feed_id, category_id, title, link, description, alias, datetime) VALUES
				('".$source['id']."', '".$source_catID."', '".$key['title']."', '".$key['link']."', '".$key['description']."', '".$alias."', '".$key['pubDate']."')");

				if($res) {
					$content_id = $db->lastInsertID($res, 'id', _DBPREF."content");

					if(!empty($source['video_embed']) && !empty($key['video_url'])) { // videó feed és van is az itemhez video url
						$fullembed = str_replace('@VIDEOURL@', $key['video_url'], $db->escape($source['video_embed']));
						$db->Query("INSERT INTO "._DBPREF."content_videos (content_id, fullembed) VALUES ('".$content_id."', '".$fullembed."')");
						$db->Query("UPDATE "._DBPREF."content SET content_type = '1' WHERE id = '".$content_id."' LIMIT 1");

						// temp tábla írás
						$db->Query("INSERT INTO "._DBPREF."content_temp
						(id, feed_id, category_id, title, link, description, alias, datetime, content_type) VALUES
						('".$content_id."', '".$source['id']."', '".$source_catID."', '".$key['title']."', '".$key['link']."', '".$key['description']."', '".$alias."', '".$key['pubDate']."', '1')");
					}
					else {
						// temp tábla írás
						$db->Query("INSERT INTO "._DBPREF."content_temp
						(id, feed_id, category_id, title, link, description, alias, datetime) VALUES
						('".$content_id."', '".$source['id']."', '".$source_catID."', '".$key['title']."', '".$key['link']."', '".$key['description']."', '".$alias."', '".$key['pubDate']."')");
					}

					if($item_num == 0 && $key['pubDate']) { // ez az első, tehát valszeg a legfrissebb elem benne
						$db->Query("UPDATE "._DBPREF."content_feeds SET lastpubdate = '".$key['pubDate']."' WHERE id = '".$source['id']."' LIMIT 1");
					}
					$item_num++;
					$all_item_num = $all_item_num + $item_num;
				}
				unset($res);
			}
			unset($key);
		}
		$db->Query("INSERT INTO "._DBPREF."content_feeds_stat (feed_id, datetime, item_num) VALUES ('".$source['id']."', NOW(), '".$item_num."')");
		$feed_num++;
	}

	if(isset($_GET['debug']))
	echo '<pre>'.number_format(microtime(true) - $time_start, 4).'		'.$item_num.'	'.$source['fetchrank'].'	'.$source['feed_schema'].'		'.$source['feed'].'</pre>'."\n";

	unset($feed);
	unset($items);

	$db->Query("UPDATE "._DBPREF."content_feeds SET modtime = NOW() WHERE id = '".$source['id']."' LIMIT 1");
}

// log
$time = microtime(true) - $time_start;
$db->Query("INSERT INTO "._DBPREF."content_feeds_log (datetime, microtime, feed_num, item_num) VALUES (NOW(), '".$time."', '".$feed_num."', '".$all_item_num."')");

?>