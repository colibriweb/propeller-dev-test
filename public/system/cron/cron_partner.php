<?php

// partner-oldalainknál beágyazott html-dobozok legenerálása
// pl. propeller.hu/partner/atv.html

if(isset($_GET['debug'])) echo '<hr />cron_partner.php<br />'."\n";

$assoc = array();
$blocks = $content->q_partner_box();

foreach($blocks AS $key => $value) {
	if(strstr($value['link'], $env->base)) {
		 if (empty($value['image'])) {
			$value['image'] = SHARE_IMAGE;
		 }
		$assoc[] = $value;
	}
}

// általános (blogok, kis oldalak) common.html
$common = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<link rel="canonical" href="'.$env->base.'/partner/common.html">
<!--
      SZERETNÉL ILYEN DOBOZT A HONLAPODRA? KATTINTS IDE:
      http://propeller.hu/statikus/doboz

      Alap használat:
      <iframe width="280" height="310" frameborder="0" scrolling="no" src="http://propeller.hu/partner/common.html"></iframe>

      Ha több vagy kevesebb hírcím legyen: növeld vagy csökkentsd az iframe "height" paraméterét!
-->
<title></title>
<style type="text/css">
html, body, div, h1, h2, h3, p, a { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; vertical-align: baseline; }
body { color: #222; }
:focus { outline: 0; }
* { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; }
html { overflow: hidden; }
body, div { font: 14px/16px bold Arial, Helvetica, sans-serif; }
a { color: #222; text-decoration: none; }
a:hover { text-decoration: underline;  }
div {
	width: 100%; max-width: 308px;
	height: 60px; overflow: hidden;
	margin: 0 0 10px 0;
}
div:before, div:after { content: ""; display: table; }
div:after { clear: both; }
div > a {
	display: block; float: left; width: 110px; height: 57px; background-color: #eee; background-repeat: no-repeat; background-position: 50% 50%; background-size: cover;
}
div > span {
	display: block; width: calc(100% - 110px); padding: 3px 0 0 10px; overflow-x: hidden; overflow-y: visible;
	font-size: 14px; line-height: 1.25; font-weight: bold;
}
h3 {
	font-size: 16px; font-weight: bold;
	margin-bottom: 10px;
	border-bottom: 1px solid #eee;
	height: 24px; overflow: hidden;
	line-height: 22px;
	text-transform: uppercase;
}
</style>
<script type="text/javascript">
window.onload=function(){var a=window.location!=window.parent.location?document.referrer:document.location,b=document.createElement("a");b.href=a;if("propeller.hu"!=b.hostname)for(var c=document.getElementsByTagName("a"),a=0;a<c.length;a++)c[a].href=c[a].href+"?utm_source="+b.hostname+"&utm_medium=partner&utm_campaign="+encodeURIComponent(c[a].innerHTML);};
</script>
</head>
<body>
<h3>Hírek a Propellerről</h3>
';
/*
window.onload = function() {
	var s = (window.location != window.parent.location) ? document.referrer: document.location;
	var p = document.createElement("a");
	p.href = s;
	if(p.hostname != "propeller.hu") {
		var a = document.getElementsByTagName("a");
		for(var i=0; i<a.length; i++) {
			a[i].href = a[i].href+"?utm_source="+p.hostname+"&utm_medium=partner&utm_campaign="+encodeURIComponent(a[i].innerHTML);
		}
	}
	var h = document.location.hash.substring(1);
	if((h != "") && h == "tc=0") {
		var d = document.getElementsByTagName("div");
		for(var i=0; i<d.length; i++) {
			if(d[i].className == "tc") d[i].parentNode.removeChild(d[i]);
		}
	}
}
*/
foreach($assoc as $key => $value) {
	$common .= '
	<div>
	<a href="'.$value['link'].'" target="_blank" style="background-image:url('.$value['image'].');"></a>
	<span><a href="'.$value['link'].'" target="_blank">'.$env->strTruncate($value['title'], 70, '...').'</a></span>
	</div>
	';
	if($key == 5) break;
}
$common .= '</body></html>';

touch('../../partner/common.html');
$f = fopen('../../partner/common.html', "w");
fwrite($f, $common);
fclose($f);
if(isset($_GET['debug']))
echo '<pre>partner/common.html</pre>'."\n";







// hvg.html
$hvg = '<html lang="hu"><head>
	<meta charset="UTF-8">
<style>

@font-face {
  font-family: \'aleo\';
  src: url(\'hvg-font/aleo-regular-webfont.eot\'); /* IE9 Compat Modes */
  src: url(\'hvg-font/aleo-regular-webfont.eot?#iefix\') format(\'embedded-opentype\'), /* IE6-IE8 */
       url(\'hvg-font/aleo-regular-webfont.woff2\') format(\'woff2\'), /* Super Modern Browsers */
       url(\'hvg-font/aleo-regular-webfont.woff\') format(\'woff\'), /* Pretty Modern Browsers */
       url(\'hvg-font/aleo-regular-webfont.ttf\')  format(\'truetype\'), /* Safari, Android, iOS */
       url(\'hvg-font/aleo-regular-webfont.svg#svgFontName\') format(\'svg\'); /* Legacy iOS */
}

body{
  margin: 0;
  padding: 0;
}
.articleitem {
  border-bottom: 1px solid #d8d8d8;
  margin: 0;
  padding: 20px 0;
  position: relative;
}
.clear:before,
.clear:after {
  clear: both;
  content: " ";
  display: table;
}
.image-holder {
  float: left;
  margin-right: 20px;
}
.image-holder a {
  display: block; float: left; width: 60px; height: 60px; background-color: #eee; background-repeat: no-repeat; background-position: 50% 50%; background-size: cover;
}
h1 {
  margin: 0;
}
h1 a {
  color: #000;
  font-size: 18px;
  line-height: 120%;
  margin: 0 0 12px;
  font-family: aleo, serif;
  text-decoration: none;
}
h1 a:hover {
  text-decoration: underline;
}

</style>
</head><body>
';
foreach($assoc as $key => $value) {
	$hvg .= '
<div class="articleitem clear">
	<div class="image-holder">
		<a href="'.$value['link'].'?utm_source=hvg.hu&amp;utm_medium=partner&amp;utm_campaign='.rawurlencode($value['title']).'" target="_blank" style="background-image:url('.$value['image'].');"></a>
	</div>
	<div class="text-holder">
		<h1><a href="'.$value['link'].'?utm_source=hvg.hu&amp;utm_medium=partner&amp;utm_campaign='.rawurlencode($value['title']).'" target="_blank">'.$value['title'].'</a></h1>
	</div>
</div>

	';
	if($key == 1) break;
}
$hvg .= '</body></html>';

touch('../../partner/hvg.html');
$f = fopen('../../partner/hvg.html', "w");
fwrite($f, $hvg);
fclose($f);
if(isset($_GET['debug']))
echo '<pre>partner/hvg.html</pre>'."\n";





// atv.html
$atv = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title></title>
<!--
      SZERETNÉL ILYEN DOBOZT A HONLAPODRA? KATTINTS IDE:
      http://propeller.hu/statikus/doboz
-->
<style type="text/css">
html, body, div, h1, h2, h3, p, a { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; vertical-align: baseline; }
body { color: #222; }
:focus { outline: 0; }
* { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; }
html { overflow: hidden; }
body, div { font: 14px/16px bold Arial, Helvetica, sans-serif; }
a { color: #222; text-decoration: none; }
a:hover { text-decoration: underline;  }
div {
	width: 100%; max-width: 308px;
	height: 60px; overflow: hidden;
	margin: 0 0 10px 0;
}
div:before, div:after { content: ""; display: table; }
div:after { clear: both; }
div > a {
	display: block; float: left; width: 110px; height: 57px; background-color: #eee; background-repeat: no-repeat; background-position: 50% 50%; background-size: cover;
}
div > span {
	display: block; width: calc(100% - 110px); padding: 3px 0 0 10px; overflow-x: hidden; overflow-y: visible;
	font-size: 14px; line-height: 1.25; font-weight: bold;
}
h3 {
	font-size: 16px; font-weight: bold;
	margin-bottom: 10px;
	border-bottom: 1px solid #eee;
	height: 24px; overflow: hidden;
	line-height: 22px;
	text-transform: uppercase;
}
</style>
</head>
<body>
<h3>Friss hírek a Propellerről</h3>
';
$ennyi = 0;
foreach($assoc as $key => $value) {
    if (preg_match('/(megbasz|fasz|atv)/iu', $value['title'])) {
        continue;
    }
    $ennyi++;
	$atv .= '
	<div>
	<a href="'.$value['link'].'?utm_source=atv.hu&amp;utm_medium=partner&amp;utm_campaign='.rawurlencode($value['title']).'" target="_blank" style="background-image:url('.$value['image'].');"></a>
	<span><a href="'.$value['link'].'?utm_source=atv.hu&amp;utm_medium=partner&amp;utm_campaign='.rawurlencode($value['title']).'" target="_blank">'.$env->strTruncate($value['title'], 70, '...').'</a></span>
	</div>
	';
	if($ennyi == 4) break;
}
//$atv .= '<img src="http://www.google-analytics.com/collect?v=1&t=event&tid=UA-241542-5&cid=9dbdb1fb-94bf-4f97-bf86-e4ab2ddbce92&ec=partner-box&ea=view&el=atv.hu" width="0" height="0" alt="" border="0">';
$atv .= '</body></html>';

touch('../../partner/atv.html');
$f = fopen('../../partner/atv.html', "w");
fwrite($f, $atv);
fclose($f);
if(isset($_GET['debug']))
echo '<pre>partner/atv.html</pre>'."\n";


?>