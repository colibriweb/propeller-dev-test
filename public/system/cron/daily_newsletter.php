<?php
die;

// nincs használatban, ez a régi hírlevél-küldő


require('../MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require('../Environment.class.php');
$env = new Environment();
$env->db = $db;

require('../../modules/users/Auth.class.php');
$auth = new Auth($env);

require('../libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require('../../modules/content/Content.class.php');
$content = new Content();
$content->env = $env;



	/** Rendszerosztály: e-mail küldés */
	require('../libraries/phpmailer/class.phpmailer.php');
	$phpMailer = new PHPMailer();

	// levélküldő osztály feltöltése és levélküldés
	$phpMailer->CharSet = 'UTF-8';
	$phpMailer->From = $env->l['mail']['from'];
	$phpMailer->FromName = $env->l['mail']['fromname'];
	$phpMailer->AddAddress($row['mail']);
	$phpMailer->IsHTML(true);

	define('UTM', 'utm_source=newsletter_'.date('Ymd').'&utm_medium=mail');
	$smarty->assign('utm_source', UTM);

	//$leader = $content->q_leader();

	$exclude = array();
/*
	if(!empty($leader[0]['image'])) {
		$exclude[] = $leader[0]['link'];
		$leader[0]['link'] = $leader[0]['link'].'?'.UTM.'&utm_campaign='.rawurlencode($subject_lesz);
		$phpMailer->AddEmbeddedImage(ROOT.'_static/images/b/'.$leader[0]['image'], 'leader', 'sdasd.jpg');
		$smarty->assign('leader', $leader[0]);
	}*/

	$blocks = $content->q_blocks_home();

	foreach($blocks['left'] AS $key => $value) {
		if(!empty($value['image'])) {
			$exclude[] = $value['link'];
			$subject_lesz = $value['title'];
			$value['link'] = $value['link'].'?'.UTM.'&utm_campaign='.rawurlencode($value['title']);
			$phpMailer->AddEmbeddedImage(ROOT.'_static/images/b/'.$value['image'], 'leader', 'sdasd.jpg');
			$smarty->assign('leader', $value);
			break;
		}
	}


	$new_blocks = array();
	$i = 0;
	foreach($blocks['left'] AS $key => $value) {
		if(($value['comments'] > 0) && !in_array($value['link'], $exclude)) {
			$exclude[] = $value['link'];
			$value['link'] = $value['link'].'?'.UTM.'&utm_campaign='.rawurlencode($value['title']);
			$new_blocks[] = $value;

			if($i == 8) break;

			$i++;
		}
	}

	$smarty->assign('block', $new_blocks);



	//$smarty->assign('topstories', $content->q_header_topstories());

	$tst = array();
	$res_t = $env->db->Query("SELECT id, content_id, title, link, description, image FROM "._DBPREF."content_topstories WHERE datetime < NOW() AND datetime > '".date('Y-m-d H:i:s', (time()-(3600*20)))."' ORDER BY datetime DESC LIMIT 0, 10");
	while($row_t = $env->db->fetchArray($res_t)) {
		$exclude[] = $row_t['link'];
		$row_t['link'] = $row_t['link'].'?'.UTM.'&utm_campaign='.rawurlencode($row_t['title']);
		$tst[] = $row_t;

	}

	$smarty->assign('topstories', $tst);



	$top = $content->q_newsletter_top24();
	$new_top = array();
	foreach($top AS $key => $value) {
		if($value['t'] == 1) {
			if(!in_array($value['category_alias'].'/'.$value['alias'], $exclude)) {

				$value['link'] = $value['category_alias'].'/'.$value['alias'].'?'.UTM.'&utm_campaign='.rawurlencode($value['title']);
				$new_top[] = $value;
			}

		}

	}

	$smarty->assign('top', $new_top);

	$subject = 'Ezek a nap legfontosabb hírei a Propelleren';

/*
	$body = preg_replace("/<a.+?href=\"([^\"]*?)\"[^>]*?>/i", "<a href=\"\\1?utm_source=newsletter_".date('Ymd')."&utm_medium=mail&utm_campaign=nrp\">", $body);

	$smarty->assign('body', $body);

*/


if(isset($_GET['preview'])) {
	$smarty->assign('subject', $subject);
	echo $smarty->fetch('daily_newsletter.tpl');
	die;
}
elseif(isset($_GET['test'])) {
	$subject = 'Zsolt, ezek a nap legfontosabb hírei a Propelleren';
	$phpMailer->Subject = $subject_lesz;

	$smarty->assign('subject', $subject);
	$phpMailer->AddAddress('zsolt.liebig@gmail.com');
	$phpMailer->Body = $smarty->fetch('daily_newsletter.tpl');

	if(!$phpMailer->Send()) die('Hiba a levélküldés során'); else die('Rendben!');

	$phpMailer->ClearAddresses();
}
elseif(isset($_GET['init'])) {

	$env->db->Query("TRUNCATE TABLE "._DBPREF."newsletter_list_temp");

	$env->db->Query("INSERT INTO "._DBPREF."newsletter_list_temp SELECT * FROM "._DBPREF."newsletter_list");
	echo 'ok';

}
else {

	$res_m = $env->db->Query("SELECT name, mail FROM "._DBPREF."newsletter_list_temp ORDER BY rand() LIMIT 500");
	while($row_m = $env->db->fetchArray($res_m)) {
		$phpMailer->AddAddress($row_m['mail']);

		if(!empty($row_m['name'])) {
			$subject = $row_m['name'].', ezek a nap legfontosabb hírei a Propelleren';
		}

		if(!empty($subject_lesz)) {
			$phpMailer->Subject = $subject_lesz;
		}
		else {
			$phpMailer->Subject = $subject;
		}

		$smarty->assign('subject', $subject);


		$smarty->assign('leiratkozas', array($row_m['mail'], md5('LEIRATKOZ'.$row_m['mail'])));


		$phpMailer->Body = $smarty->fetch('daily_newsletter.tpl');

		if(!$phpMailer->Send()) die('Hiba a levélküldés során: '.$row_m['mail']);

		$phpMailer->ClearAddresses();
		echo 'OK - '.$row_m['mail'].'<br />';

		$env->db->Query("DELETE FROM "._DBPREF."newsletter_list_temp WHERE mail = '".$row_m['mail']."' LIMIT 1");

	}


}




?>
