<?php

// felhasználói aktivitásokról (kommentelés, lájkolás) értesítő email küldése

require('../libraries/smarty/Smarty.class.php');

if(isset($_GET['debug'])) echo '<hr />cron_activities.php<br />'."\n";

$smarty = new Smarty();
$smarty->template_dir = '../../templates/'.$env->template;
$smarty->compile_dir = '../cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = '../cache/smarty_cache_'.$env->template;
$smarty->assign('env', $env);

// utolsó értesítős-küldés ideje
$res_last = $db->Query("SELECT last_act_send FROM "._DBPREF."misc LIMIT 1");
$row_last = $db->fetchArray($res_last);


$res = $db->Query("SELECT a.name As act_name, a.message AS act_message, user_id_affected, a.datetime, u.name, u.alias, u.mail FROM "._DBPREF."users_activities a
LEFT JOIN "._DBPREF."users u ON a.user_id_affected = u.id
WHERE a.datetime > '".$row_last['last_act_send']."' AND action NOT IN (5,8) AND viewed = 0
AND status = '2' AND notify_act = 1
AND modtime < '".date('Y-m-d H:i:s', strtotime('-3 minutes'))."'
ORDER BY a.user_id_affected ASC, a.datetime DESC LIMIT 0, 150");
$assoc = array();
while($row = $db->fetchAssoc($res)) {


	if(isset($_GET['debug'])) {
	//	echo '<pre>'.$row['name']." \t ".$row['mail']." \t ".date('Y. m. d. H:i', strtotime($row['datetime']))." \t ".$row['act_name'].' '.$row['act_message'].'</pre>';

	}
	$assoc[$row['user_id_affected']][] = $row;
}

/** Rendszerosztály: e-mail küldés */
require('../libraries/phpmailer/class.phpmailer.php');
$phpMailer = new PHPMailer();

// levélküldő osztály feltöltése és levélküldés
$phpMailer->CharSet = 'UTF-8';
$phpMailer->From = $env->l['mail']['from'];
$phpMailer->FromName = $env->l['mail']['fromname'];
$phpMailer->IsHTML(true);

foreach($assoc AS $key => $value) {

	// törölt címekre nem küldünk értesítést
	if (strpos($value[0]['mail'], 'TOROLT-') !== false) {
	    continue;
	}

	$phpMailer->Subject = count($value).' új esemény történt a Propelleren';
	//$phpMailer->AddAddress('zsolt.liebig@gmail.com');
	$phpMailer->AddAddress($value[0]['mail']);

	$smarty->assign('subject', $phpMailer->Subject);
	$smarty->assign('name', $value[0]['name']);

	$bdy = 'Ez történt a Propelleren, a legutóbbi látogatás óta:<br /><br />';
	foreach($value AS $k2 => $v2) {
		if(preg_match('/(#[a-zA-Z0-9]+)\"/', $v2['act_message'], $matches)) {
			$v2['act_message'] = str_replace($matches[1], '', $v2['act_message']);
			$v2['act_message'] = preg_replace("/<a.+?href=\"([^\"]*?)\"[^>]*?>/i", "<a href=\"\\1?utm_source=activity_".date('Ymd')."&utm_medium=email&utm_campaign=noreply".$matches[1]."\" target=\"_blank\">", $v2['act_message']);
		}
		else {
			$v2['act_message'] = preg_replace("/<a.+?href=\"([^\"]*?)\"[^>]*?>/i", "<a href=\"\\1?utm_source=activity_".date('Ymd')."&utm_medium=email&utm_campaign=noreply\" target=\"_blank\">", $v2['act_message']);
		}
		$bdy .= '<b>'.$v2['act_name'].'</b> '.$v2['act_message'].'<br /><small>'.$env->dateToString($v2['datetime']).'</small><br />';
	}

	$bdy .= '<br /><a href="http://propeller.hu/belepes?utm_source=activity_'.date('Ymd').'&utm_medium=email&utm_campaign=noreply" target="_blank"><b>Tovább a Propellerre...</b></a>';

	$smarty->assign('body', $bdy);

	$phpMailer->Body = $smarty->fetch('mail.tpl');
	$phpMailer->Send();

	$phpMailer->ClearAddresses();

	if(isset($_GET['debug'])) {
		echo 'Elküldve:';
		p($value);
	}

}

$db->Query("UPDATE "._DBPREF."misc SET last_act_send = NOW() LIMIT 1");

//	mail('zsolt.liebig@gmail.com', 'Elküldve '.count($assoc).' értesítő levél', 'Elment', 'From: zsolt.liebig@gmail.com');

?>