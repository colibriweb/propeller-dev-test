<?php

require('../MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

// 50 napnál régebbi temp content törlése
$db->Query("DELETE FROM "._DBPREF."content_temp WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-50 day"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_temp");

// 1 napnál régebbi találati statisztikák törlése
$db->Query("DELETE FROM "._DBPREF."content_feeds_stat WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-1 day"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_feeds_stat");

// 2 napnál régebbi feldolgozási napló törlése
$db->Query("DELETE FROM "._DBPREF."content_feeds_log WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-2 day"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_feeds_log");

// 2 hétnél régebbi jelszópótlások törlése
$db->Query("DELETE FROM "._DBPREF."users_lostpass WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-2 week"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."users_lostpass");

// 50 napnál régebbi aktivitások törlése
$db->Query("DELETE FROM "._DBPREF."users_activities WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-14 day"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."users_activities");

// 30 napnál régebbi kiemelések törlése
$res = $db->Query("SELECT block_id FROM "._DBPREF."content_blocks WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-30 day"))."' ORDER BY datetime DESC LIMIT 1");
$row = $db->fetchArray($res);
$db->Query("DELETE FROM "._DBPREF."content_blocks WHERE block_id < ".$row['block_id']);
$db->Query("OPTIMIZE TABLE "._DBPREF."content_blocks");

// 3 hétnél régebbi aktiválatlan regisztrációk törlése
$db->Query("DELETE u, r FROM "._DBPREF."users u
    LEFT JOIN "._DBPREF."users_registration r ON r.user_id = u.id
    WHERE u.status = '1' AND u.datetime < '".date('Y-m-d H:i:s', strtotime("-3 week"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."users");
$db->Query("OPTIMIZE TABLE "._DBPREF."users_registration");

// 10 napnál régebbi komment logok törlése
$db->Query("DELETE FROM "._DBPREF."content_comments_log WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-10 day"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_comments_log");

// 10 napnál régebbi bejelentett komment-reportok törlése
$db->Query("DELETE FROM "._DBPREF."content_comments_report WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-10 day"))."'");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_comments_report");


// 4 hétnél régebbi törölt kommentek végleges törlése
//$db->Query("DELETE FROM "._DBPREF."content_comments_del WHERE datetime < '".date('Y-m-d H:i:s', strtotime("-4 week"))."'");
//$db->Query("OPTIMIZE TABLE "._DBPREF."content_comments_del");


// nem használt címkék törlése (a kapcsolótáblából törölve lettek, mikor a tartalom felvétele történt)
$res_d = $db->Query("SELECT content_id, tag_id, id, tag FROM "._DBPREF."content_tags t
	LEFT JOIN "._DBPREF."content_tags_conn c ON t.id = c.tag_id WHERE c.content_id IS NULL");
while($row_d = $db->fetchArray($res_d)) { $query_d .= "id = '".$row_d['id']."' OR "; }
if($db->numRows($res_d)) {
	$query_d = rtrim($query_d, "OR ");
	$db->Query("DELETE FROM "._DBPREF."content_tags WHERE ".$query_d);
}


//$db->Query("OPTIMIZE TABLE "._DBPREF."content");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_tags");
$db->Query("OPTIMIZE TABLE "._DBPREF."content_tags_conn");
//$db->Query("OPTIMIZE TABLE "._DBPREF."content_comments");
$db->Query("OPTIMIZE TABLE "._DBPREF."users_errorlog");


?>