<?php

class Pagination extends Environment {

	var $current_page = 1; 	// aktuálisan látható oldal

	var $results_from = 0; 	// találatok az adatbázisból innen
	var $results_limit; 		// találatok az adatbázisból ennyi (ennyi van egy oldalon)

	var $results_num;		// összes találat

	var $paging_from; 		// lapozógombok innen
	var $paging_to; 		// lapozógombok eddig
	var $paging_all; 		// összes lapszám

	var $paging_max = 50;   // legfeljebb ennyi oldalig lehet lapozni


	function __construct($current_page, $results_limit = 20) {

		if($current_page != (int)$current_page || $current_page < 0 || $current_page > $this->paging_max) { // legfeljebb paging_max oldalig lehet lapozni
			return $this->setHeader(404);
		}

		$this->current_page = ($current_page == 0) ? 1 : (int)$current_page;

		$this->results_limit = (int)$results_limit;

		if($this->current_page == 1) {
			$this->results_from = 0;
		}
		else {
			$this->results_from = $this->current_page * $this->results_limit - $this->results_limit;
		}

	}

	function getPaging() {

		$this->paging_all = ceil($this->results_num / $this->results_limit);

		if($this->paging_all < 1) { // egy oldal van csak
			return;
		}

		if($this->current_page > $this->paging_all) { // nem lehet ilyen oldal
			return $this->setHeader(404);
		}

		if($this->paging_all > $this->paging_max) { // legfeljebb paging_max oldalig lehet oldalszámokat kitenni
			$this->paging_all = $this->paging_max;
		}

		if($this->paging_all <= 7) {
			$this->paging_from = 1;
			$this->paging_to = $this->paging_all;
			return get_object_vars($this);
		}

		// több mint 7 oldal van

		if($this->current_page <= 4) { // eleje
			$this->paging_from = 1;
			$this->paging_to = 7;
		}
		elseif($this->current_page >= ($this->paging_all-3)) { // vége
			$this->paging_from = $this->paging_all-6;
			$this->paging_to = $this->paging_all;
		}
		else { // közepe
			$this->paging_from = $this->current_page-3;
			$this->paging_to = $this->current_page+3;
		}


		return get_object_vars($this);
	}


}

?>
