<?php
/*
	Copyright 2010 Scott MacVicar

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

	Original can be found at https://github.com/scottmac/opengraph/blob/master/OpenGraph.php

*/

class OpenGraph implements Iterator {
	/**
	* Holds all the Open Graph values we've parsed from a page
	*
	*/
	private $_values = array();

	/**
	* Fetches a URI and parses it for Open Graph data, returns
	* false on error.
	*
	* @param $URI    URI to page to parse for Open Graph data
	* @return OpenGraph
	*/
	static public function fetch($URI) {
		$curl = curl_init($URI);

		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 15);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

		$response = curl_exec($curl);

		curl_close($curl);

		if(!empty($response)) {
			return self::_parse($response);
		}
		else {
			return false;
		}
	}

	/**
	* Parses HTML and extracts Open Graph data, this assumes
	* the document is at least well formed.
	*
	* @param $HTML    HTML to parse
	* @return OpenGraph
	*/
	static private function _parse($HTML) {
		$old_libxml_error = libxml_use_internal_errors(true);

		$doc = new DOMDocument();
		$doc->loadHTML('<?xml encoding="UTF-8">'.$HTML);

		libxml_use_internal_errors($old_libxml_error);

		$tags = $doc->getElementsByTagName('meta');
		if(!$tags || $tags->length === 0) {
			return false;
		}

		$page = new self();

		$nonOgDescription = null;

		foreach($tags AS $tag) {
			if($tag->hasAttribute('property') && strpos($tag->getAttribute('property'), 'og:') === 0) {
				$key = strtr(substr($tag->getAttribute('property'), 3), '-', '_');

				if($key === 'image') {
					if(filter_var($tag->getAttribute('content'), FILTER_VALIDATE_URL)) {
						$page->_values[$key][] = $tag->getAttribute('content');
					}
				}
				else {
					$page->_values[$key] = $tag->getAttribute('content');
				}
			}

			// valasz.hu hack
			if($tag->hasAttribute('name') && $tag->hasAttribute('name') && strpos($tag->getAttribute('name'), 'og:') === 0) {
				$key = strtr(substr($tag->getAttribute('name'), 3), '-', '_');

				if($key === 'image') {
					$page->_values[$key][] = $tag->getAttribute('content');
				}
				else {
					$page->_values[$key] = $tag->getAttribute('content');
				}
			}

			if($tag->hasAttribute('property') && strpos($tag->getAttribute('property'), 'article:') === 0) {
				if(substr($tag->getAttribute('property'), 8) === 'tag') {
					$page->_values['tag'][] = $tag->getAttribute('content');
				}
			}

			if($tag->hasAttribute('name') && $tag->getAttribute('name') === 'author') {
				$author = $tag->getAttribute('content');
				if(preg_match('/^[a-zA-Z0-9ÁÉÍÓÖŐÚŰÜáéíóöőúűü\.\s\-\,]+$/i', $author)) {
					$page->_values['author'] = $author;
				}
			}

			//Based on modifications at https://github.com/bashofmann/opengraph/blob/master/src/OpenGraph/OpenGraph.php
			if($tag->hasAttribute('name') && $tag->getAttribute('name') === 'description') {
				$nonOgDescription = $tag->getAttribute('content');
			}

		}

		if(isset($page->_values['site_name'])) { // béna a site_name, dobjuk el
			if(!preg_match('/^[a-zA-Z0-9ÁÉÍÓÖŐÚŰÜáéíóöőúűü\.\s\-\,]+$/i', $page->_values['site_name'])) {
				unset($page->_values['site_name']);
			}
		}

		//Based on modifications at https://github.com/bashofmann/opengraph/blob/master/src/OpenGraph/OpenGraph.php
		if(!isset($page->_values['title'])) {
			$titles = $doc->getElementsByTagName('title');
			if($titles->length > 0) {
				$page->_values['title'] = $titles->item(0)->textContent;
			}
		}

		if(isset($page->_values['title'])) {
			$page->_values['title'] = trim($page->_values['title']);
			// elején buziságok
			$page->_values['title'] = preg_replace("/^(borzalom|drámai?|hoppá|elképesztő|tragédia|sokkoló|gyász|baleset|túlélte|döbbenet|18\+)\!\s/i", "", $page->_values['title']);

			// végén domain
			$page->_values['title'] = preg_replace("/\s(\-|\–|\—|\|)\s\S+\.[a-z]{2,}$/i", "", $page->_values['title']);

			// végén site név
			$page->_values['title'] = preg_replace("/\s(\|\s.+|\-\s\!\!444\!\!\!|-\sNők Lapja Café|- NLCafé|-\s444|-\sépülettár|-\sVárosi Kurír)$/i", "", $page->_values['title']);

			// végén buziságok
			$page->_values['title'] = preg_replace("/\s(\-|\–|\—)(\-|\–|\—)?\s(videó|fotó|kép)(k|val|kkal|pel|pekkel)?\!?$/i", "", $page->_values['title']);
		}

		if(!isset($page->_values['description']) && $nonOgDescription) {
			$page->_values['description'] = $nonOgDescription;
		}

		//Fallback to use image_src if ogp::image isn't set.
		if(!isset($page->_values['image'])) {
			$domxpath = new DOMXPath($doc);
			$elements = $domxpath->query("//link[@rel='image_src']");

			if($elements->length > 0) {
				$domattr = $elements->item(0)->attributes->getNamedItem('href');
				if($domattr) {
					$page->_values['image'] = $domattr->value;
					$page->_values['image_src'] = $domattr->value;
				}
			}
		}

		if(empty($page->_values)) {
			return false;
		}

		return $page;
	}

	/**
	* Helper method to access attributes directly
	* Example:
	* $graph->title
	*
	* @param $key    Key to fetch from the lookup
	*/
	public function __get($key) {
		if(array_key_exists($key, $this->_values)) {
			return $this->_values[$key];
		}
	}

	/**
	 * Iterator code
	 */
	private $_position = 0;
	public function rewind() { reset($this->_values); $this->_position = 0; }
	public function current() { return current($this->_values); }
	public function key() { return key($this->_values); }
	public function next() { next($this->_values); ++$this->_position; }
	public function valid() { return $this->_position < sizeof($this->_values); }
}

function cleanText($string) { // nemkívánatos karakterek eltávolítása a szövegből
	$string = str_replace(array("\r\n", "\r", "\n"), " ", $string);
	$string = str_replace('--', "–", $string);
	$string = preg_replace("/[ |\t]{1,}/", " ", $string);

	$string = preg_replace("/[ |\t]{1,}(\.|,|:|;|'|\?|\!)/", "$1", $string);
	$string = trim($string);

	return $string;
}

function cleanUrl($url) { // kipucolja az URL-ből a felesleget

	$url = preg_replace('/source=hirkereso.*/', '', $url);
	$url = preg_replace('/~~.*/', '', $url);
	$url = preg_replace('/\#rss.*/', '', $url);
	//$url = preg_replace('/atv\.hu\/(belfold|kulfold|bulvar)/', 'atv.hu/cikk', $url);

	$parts = parse_url($url);
	if(!isset($parts['query'])) {
		return $url;
	}

	parse_str($parts['query'], $query);
	foreach($query as $key => $value) {
		if(in_array($key, ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_conzent'])) {
			$url = str_replace("$key=$value", "", urldecode($url));
		}
	}
	$url = rtrim($url, '?&#');

	return $url;
}