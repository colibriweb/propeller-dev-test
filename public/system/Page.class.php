<?php
/**
* Rendszerosztály: lapozásokhoz szükséges oldalszámok kiszámítása
* @package SWEN
* @subpackage _system
* @version 1.0
* @copyright Copyright {@link &copy;} 2006. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/


/**
* Rendszerosztály: lapozásokhoz szükséges oldalszámok kiszámítása
* @version 1.0
* @copyright Copyright {@link &copy;} 2006. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
class Page {

	/**
	* Aktuális oldal
	* @var integer
	*/
	var $current;

	/**
	* Összes oldal
	* @var integer
	*/
	var $all;

	/**
	* Elem oldalanként
	* @var integer
	*/
	var $limit;

	/**
	* Kezdő elem
	* @var integer
	*/
	var $from = 0;



	var $num = 0;

	var $pager_from = 0;

	var $pager_to = 0;

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Page($all, $limit, $current = 0) {

		$this->num = $all;
		$this->all = ceil($all / $limit);
		$this->limit = $limit;

		if($current == 0) { // első oldal
			$this->from = 0;
			$this->current = 1;
		}
		else { // következő oldal
			$this->current = $current;
			$this->from = $current * $limit - $limit;
		}

		if(($this->current-5) <= 0) {
			$this->pager_from = 1;
			$this->pager_to = 11;
		}
		else {
			$this->pager_from = $this->current-4;
			$this->pager_to = $this->current+6;
		}

		if($this->all <= $this->pager_to) {
			$this->pager_to = $this->all+1;
		}

	}

}

?>
