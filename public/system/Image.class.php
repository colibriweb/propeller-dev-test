<?php
/**
* Képkezelés és feltöltés
* @package SWEN
* @subpackage _system
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
/*
használat, pl:

	require_once('../../system/Image.class.php');
	$image = new Image($_FILES['image']);
	$image->imgBase = '../../images/';
	$image->fileID = '654894';

	if(!$image->saveForComments()) {
		$smarty->assign('error', $image->error);
	}
	else {
		// sikeres volt a felöltés
	}

*/

class Image {

	/**
	* Feltöltött kép tulajdonságai tömbben
	* @var array
	*/
	var $image;

	var $types = array('image/gif', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/png');

	var $extensions = array('.gif', '.jpg', '.jpeg', '.png');

	var $maxSize = 10240000;

	var $imgBase = '';

	var $fileID;
	var $ext = '.png';

	var $toStore = '';

	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Image($image) {

		$this->image = $image;

	}

	/**
	* Feltöltendő avatar kép ellenőrzése
	* @param array $upload Feltöltendő kép paraméterei
	* @param integer $size Maximálisan megengedett méret
	* @return array Helyes fájltípus és méret esetén true-val, egyébként false-sal tér vissza
	*/
	function checkImage() {

		if(!in_array($this->image['type'], $this->types)) {
			$this->error = 'Nem megfelelő képformátum.';
			return false;
		}

		$this->ext = strrchr(strtolower($this->image['name']), '.');
		if(!in_array($this->ext, $this->extensions)) {
			$this->error = 'Nem megfelelő kiterjesztés.';
			return false;
		}

		if($this->image['size'] > $this->maxSize) {
			$this->error = 'Nem megfelelő képméret.';
			return false;
		}

		return true;

	}


	function createImage() {

		if($this->image['type'] == 'image/jpg' || $this->image['type'] == 'image/jpeg' || $this->image['type'] == 'image/pjpeg')  {
			$this->ext = '.jpg';
			return imageCreateFromJPEG($this->image['tmp_name']);
		}
		elseif($this->image['type'] == 'image/gif') {
			$this->ext = '.gif';
			return imageCreateFromGIF($this->image['tmp_name']);
		}
		elseif($this->image['type'] == 'image/png') {
			$this->ext = '.jpg';
			return imageCreateFromPNG($this->image['tmp_name']);
		}

	}

/* --- profiler metódusok ---*/

	function saveForAvatar() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$route = floor($this->fileID/1000).'/'.$this->fileID; // max 2*1000 db fájl egy könyvtárban
			if(!is_dir($this->imgBase.$route)) {
				$this->mkdirRecursive($this->imgBase.$route, 0755);
			}
			$this->imagePath = $this->imgBase.$route.'/';

// arányos leméretezés
			list($width, $height) = getimagesize($this->image['tmp_name']);
			//saving the image into memory (for manipulation with GD Library)
			//$myImage = imagecreatefromjpeg($this->image['tmp_name']);
			$myImage = $this->createImage();

			// calculating the part of the image to use for thumbnail
			if ($width > $height) {
			  $y = 0;
			  $x = ($width - $height) / 2;
			  $smallestSide = $height;
			} else {
			  $x = 0;
			  $y = ($height - $width) / 2;
			  $smallestSide = $width;
			}
			// copying the part into thumbnail
			$thumbSize = 100;
			$thumb = imagecreatetruecolor($thumbSize, $thumbSize);
			imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);
			//final output
			$this->fullPath = $this->imagePath.$this->fileID.'_thumb.jpg';
			ImageJPEG($thumb, $this->fullPath, 75);
// arányos leméretezés


/*
			$oldSize = getimagesize($this->image['tmp_name']);
			$newSizeForOriginal = array(600, 600);

			$this->ext = '_original.jpg';
			if($this->copyImage($myImage, $newSizeForOriginal, $oldSize, '', true, true, true)) {
				// sikerült a large avatar
				//echo $this->fullPath.'<br />';
			}
			ImageDestroy($sourceID);
*/

			return true;
		}
	}


	function saveForTopstory() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);
			$newSizeForNormal = array(60, 60);
			$sourceID = $this->createImage();

//			$this->imagePath = $this->imgBase.'topstories/';
			$this->ext = '.jpg';

			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', false, true, true)) {
				chmod($this->fullPath, 0644);
			}

			ImageDestroy($sourceID);
			return true;
		}
	}
/*
	function saveForLeader() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);
//			$newSizeForNormal = $this->calcThumb($oldSize[0], $oldSize[1], 60, 'short');
			$newSizeForNormal = array(190, 160);
			$sourceID = $this->createImage();

			$this->ext = '.jpg';

			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', false, true, true)) {
				chmod($this->fullPath, 0644);
			}

			ImageDestroy($sourceID);
			return true;
		}
	}
*/
/*	function saveForBlock() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);
//			$newSizeForNormal = $this->calcThumb($oldSize[0], $oldSize[1], 60, 'short');
//			$newSizeForNormal = array(270, 170);
			$newSizeForNormal = $this->calcThumb($oldSize[0], $oldSize[1], 300, 'fix-x');
			$sourceID = $this->createImage();

			$this->ext = '.jpg';

			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', false, true, true)) {
				chmod($this->fullPath, 0644);
			}

			ImageDestroy($sourceID);
			return true;
		}
	}
*/
	function saveForBlock() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);
			$newSizeForNormal = array(600, 312);
			$sourceID = $this->createImage();

//			$this->imagePath = $this->imgBase.'topstories/';
			$this->ext = '.jpg';

			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', false, true, true)) {
				chmod($this->fullPath, 0644);
			}

			ImageDestroy($sourceID);
			return true;
		}
	}

	function saveForJegyzet() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);
			$newSizeForNormal = array(50, 50);
			$sourceID = $this->createImage();

//			$this->imagePath = $this->imgBase.'topstories/';
			$this->ext = '.jpg';

			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', false, true, true)) {
				chmod($this->fullPath, 0644);
			}

			ImageDestroy($sourceID);
			return true;
		}
	}

	function saveForBigpicture() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);

			//$newSizeForThumb = array(174, 116);
			$newSizeForThumb = $this->calcThumb($oldSize[0], $oldSize[1], 174, 'fix-x');
			$newSizeForNormal = $this->calcThumb($oldSize[0], $oldSize[1], 620, 'max-x');


			$sourceID = $this->createImage();

			if(!is_dir($this->imagePath)) {
				$this->mkdirRecursive($this->imagePath, 0755);
			}

			$this->imagePath = $this->imagePath.'/';

			$this->ext = '_thumb.jpg';
			if($this->copyImage($sourceID, $newSizeForThumb, $oldSize, '', true, true, false)) {
				// sikerült a thumb kép
				//echo $this->fullPath.'<br />';
			}

			$this->ext = '_normal.jpg';
			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', true, true, false)) {
				// sikerült a normál kép
				//echo $this->fullPath.'<br />';
			}

/*
			$this->ext = '_normal.jpg';
			$this->fullPath = $this->imagePath.$this->fileID.'_normal.jpg';
			$this->toStore = $this->fileID.'_normal.jpg';
			if(copy($this->image['tmp_name'], $this->fullPath)) {
				// sikerült a normál kép
				//echo $this->fullPath.'<br />';
			}
*/
			ImageDestroy($sourceID);
			return true;
		}
	}

	function saveForBrupload() {

		if(!$this->checkImage()) {
			return false;
		}
		else { // elvégezhető a feltöltés
			$oldSize = getimagesize($this->image['tmp_name']);


			if($oldSize[0] > $oldSize[1])
			$newSizeForNormal = $this->calcThumb($oldSize[0], $oldSize[1], 470, 'long');
			else
			$newSizeForNormal = $this->calcThumb($oldSize[0], $oldSize[1], 470, 'short');

			$sourceID = $this->createImage();

			$this->imagePath = $this->imgBase;

			if(!is_dir($this->imagePath)) {
				$this->mkdirRecursive($this->imagePath, 0755);
			}

			$this->imagePath = $this->imagePath.'/';

			$this->ext = '_normal.jpg';

			$this->fullPath = $this->imagePath.$this->fileID.'_normal.jpg';
			$this->toStore = $this->fileID.'_normal.jpg';


			$this->ext = '_normal.jpg';
			if($this->copyImage($sourceID, $newSizeForNormal, $oldSize, '', false, true, true)) {
				// sikerült a normal avatar
				//echo $this->fullPath.'<br />';
			}

			ImageDestroy($sourceID);
			return true;
		}

	}

	function copyImage($sourceID, $newSize, $oldSize, $prefix = '', $overwrite = true, $forceJPG = false, $crop = false) {

		// új kép létrehozása
		$targetID = imagecreatetruecolor($newSize[0], $newSize[1]);

		// méretezés
		if(!$crop) {
		$targetPic = imageCopyResampled($targetID, $sourceID, 0, 0, 0, 0, $newSize[0], $newSize[1], $oldSize[0], $oldSize[1]);
		}
		else {
/*			$wm = $oldSize[0]/$newSize[0];
			$hm = $oldSize[1]/$newSize[1];
			$h_height = $newSize[1]/2;
			$w_height = $newSize[0]/2;

			if($oldSize[0] > $oldSize[1]) { // fekvő
				$adjusted_width = $oldSize[0] / $hm;
				$half_width = $adjusted_width / 2;
				$int_width = $half_width - $w_height;
				//                $dst_image  $src_image  $dst_x  $dst_y  $src_x  $src_y      $dst_w  $dst_h  $src_w  $src_h )
				imagecopyresampled($targetID, $sourceID, -$int_width, 0, 0, 0, $adjusted_width, $newSize[1], $oldSize[0], $oldSize[1]);
			}
			elseif(($oldSize[0] < $oldSize[1]) || ($oldSize[0] == $oldSize[1])) { // álló vagy négyzet
				$adjusted_height = $oldSize[1] / $wm;
				$half_height = $adjusted_height / 2;
				$int_height = $half_height - $h_height;
				imagecopyresampled($targetID, $sourceID, 0, -$int_height, 0, 0, $newSize[0], $adjusted_height, $oldSize[0], $oldSize[1]);
			}
			else {
				imagecopyresampled($targetID, $sourceID, 0, 0,  0, 0, $newSize[0], $newSize[1], $oldSize[0], $oldSize[1]);
			}
*/

			$width_new = $oldSize[1] * $newSize[0] / $newSize[1];
			$height_new = $oldSize[0] * $newSize[1] / $newSize[0];
			//if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
			if($width_new > $oldSize[0]){ // fekvő
				//cut point by height
				$h_point = (($oldSize[1] - $height_new) / 2);
				//copy image
				imagecopyresampled($targetID, $sourceID, 0, 0, 0, $h_point, $newSize[0], $newSize[1], $oldSize[0], $height_new);
			}
			else{ // álló vagy négyzet
				//cut point by width
				$w_point = (($oldSize[0] - $width_new) / 2);
				imagecopyresampled($targetID, $sourceID, 0, 0, $w_point, 0, $newSize[0], $newSize[1], $width_new, $oldSize[1]);
			}

		}

		if($overwrite) { // írja felül, ha már létezik
			$this->fullPath = $this->imagePath.$prefix.$this->fileID.$this->ext;
			$this->toStore = $this->fileID.$this->ext;
		}
		else { // legyen új kép, a létező megtartásával
			$i = 1;
			$this->fullPath = $this->imagePath.$prefix.$this->fileID.'_'.$i.$this->ext;
			$this->toStore = $this->fileID.'_'.$i.$this->ext;

			while(file_exists($this->fullPath)) {
				$this->fullPath = $this->imagePath.$prefix.$this->fileID.'_'.$i.$this->ext;
				$this->toStore = $this->fileID.'_'.$i.$this->ext;
				$i++;
			}
		}

		if($forceJPG) {
			return ImageJPEG($targetID, $this->fullPath, 90);
		}
		else {
			if($this->image['type'] == 'image/jpg' || $this->image['type'] == 'image/jpeg' || $this->image['type'] == 'image/pjpeg' || $this->image['type'] == 'image/png')  {
				return ImageJPEG($targetID, $this->fullPath, 90);
			}
			elseif($this->image['type'] == 'image/gif') {
				return ImageGIF($targetID, $this->fullPath);
			}/* // PNG-t nem csinálunk
			elseif($this->image['type'] == 'image/png') {
				return ImagePNG($targetID, $this->imagePath.$prefix.$this->ext);
			}*/
		}
		return false;

	}




	/**
	* Kiszámolja a leendő kép kiterjedésbeni méretét
	* @param integer $width Jelenlegi szélessége
	* @param integer $height Jelenlegi magassága
	* @param integer $size Legnagyobb megengedett szélesség vagy magasság
	* @param string $rule A méretezés szabálya a $size-t figyelembe véve (short, long, stretch, leave)
	* @return array Az leendő kép méreteivel tér vissza
	*/
	function calcThumb($width, $height, $size = 120, $rule = 'leave') {

		if($rule == 'stretch') { // $size*$size legyen
			return array($size, $size);
		}
		elseif(($width < $size && $height < $size) || $rule == 'leave') { // kisebb mint a $size vagy ne méretezzen
			return array($width, $height);
		}
		elseif($rule == 'short') { // a rövidebbik oldala legyen $size
			if($width < $height) {
				$scale = $width/$size;
			}
			else {
				$scale = $height/$size;
			}
			return array(round($width/$scale), round($height/$scale));
		}
		elseif($rule == 'long') { // a hosszabbik oldala legyen $size
			if($width < $height) {
				$scale = $height/$size;
			}
			else {
				$scale = $width/$size;
			}
			return array(round($width/$scale), round($height/$scale));
		}
		elseif($rule == 'fix-x') { // a szélessége fix, a magassága arányos

			$scale = $width/$size;
			return array($size, ceil($height/$scale));
		}
		elseif($rule == 'max-x') { // a szélessége maximum $size, a magassága arányos
			if($width > $size) {
				$scale = $width/$size;
				return array($size, $height/$scale);
			}
			else {
				return array($width, $height);
			}


		}

	}

	/**
	* Könyvátrútvonallá parse-olja fel egy fájl azonosítóját
	* @param integer $fileID Fájl azonosítója
	* @return string Sikeres művelet esetén a könyvtárútvonallal tér vissza
	*/
	function directoryRoute($fileID) {

		$full = str_pad($fileID, 6, '0', STR_PAD_LEFT);
		return substr($full, 0, 3).'/'.substr($full, 3, 6);

	}

	/**
	* PHP 5.0.0 előtti verziók esetén használatos rekurzív könyvtárkészítés
	* @param string $dirName Könyvtár útvonal
	* @param string $rights Jogok
	* @return void
	*/
	function mkdirRecursive($dirName, $rights = 0755) {

		if(phpversion() < '5.0.0') {
			$dirs = explode('/', $dirName);
			$dir = '';
			foreach($dirs as $part) {
				$dir .= $part.'/';
				if(!is_dir($dir) && strlen($dir) > 0)
					mkdir($dir, $rights);
			}
		}
		else {
			return mkdir($dirName, $rights, true);
		}

	}

}

?>
