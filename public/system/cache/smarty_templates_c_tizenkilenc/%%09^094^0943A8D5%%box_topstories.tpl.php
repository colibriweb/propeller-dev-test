<?php /* Smarty version 2.6.12, created on 2018-11-26 13:16:59
         compiled from ./content/box_topstories.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strstr', './content/box_topstories.tpl', 7, false),array('modifier', 'escape', './content/box_topstories.tpl', 7, false),)), $this); ?>


<div class="topstories aside">
<?php if ($this->_tpl_vars['misc']['is_breaking'] && $this->_tpl_vars['blocks']['left'][0]['image']): ?>
	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['left']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['max'] = (int)1;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
	<div class="topstories-breaking">
		<a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="thumb outgoing" rel="nofollow"<?php else: ?> class="thumb"<?php endif; ?> style="background-image: url(<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']; ?>
);"><?php if ($this->_tpl_vars['misc']['is_breaking_subtitle']): ?><span><?php echo ((is_array($_tmp=$this->_tpl_vars['misc']['is_breaking_subtitle'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</span><?php endif; ?></a>
		<span class="title"><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?> rel="bookmark"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></span>
		<span class="share"><a class="get-fb-count" title="Megosztás" data-url="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
" href="https://www.facebook.com/sharer.php?app_id=247823682023773&sdk=joey&u=<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
&display=popup" rel="nofollow"><span></span></a></span>
		<div class="clear"></div>
	</div>
	<?php endfor; endif; ?>
<?php endif; ?>



<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['topstories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)3;
$this->_sections['i']['max'] = (int)8;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<div<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' && $this->_sections['i']['index'] == 3 ) || ( $this->_tpl_vars['topstories_first_big'] && $this->_sections['i']['index'] == 3 ) || $this->_tpl_vars['topstories_all_big']): ?> class="big"<?php endif; ?>>
	<a href="<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="thumb outgoing" rel="nofollow"<?php else: ?> class="thumb"<?php endif; ?> style="background-image: url(<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['image']; ?>
);"></a>
	<span class="title"><a href="<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?> rel="bookmark"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></span>
	<span class="share"><a class="get-fb-count" title="Megosztás" data-url="<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link']; ?>
" href="https://www.facebook.com/sharer.php?app_id=247823682023773&sdk=joey&u=<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link']; ?>
&display=popup" rel="nofollow"><span></span></a></span>
	<div class="clear"></div>
</div>
<?php endfor; endif; ?>

<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' )): ?>
	<!-- /21667127856/propeller_cikk_6 -->
	<div id='propeller_cikk_6'>
	<script><?php echo '
	googletag.cmd.push(function() { googletag.display(\'propeller_cikk_6\'); });
	</script>'; ?>

	</div>
<?php endif; ?>

<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php' )): ?>
	<!-- /21667127856/propeller_nyito_7 -->
	<div id='propeller_nyito_7'>
	<script><?php echo '
	googletag.cmd.push(function() { googletag.display(\'propeller_nyito_7\'); });
	</script>'; ?>

	</div>
<?php endif; ?>

<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' )): ?>
	<!-- /21667127856/propeller_rovat_6 -->
	<div id='propeller_rovat_6'>
	<script><?php echo '
	googletag.cmd.push(function() { googletag.display(\'propeller_rovat_6\'); });
	</script>'; ?>

	</div>
<?php endif; ?>


</div>


