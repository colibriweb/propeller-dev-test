<?php /* Smarty version 2.6.12, created on 2018-11-26 13:16:59
         compiled from ./sidebar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', './sidebar.tpl', 25, false),array('modifier', 'escape', './sidebar.tpl', 25, false),)), $this); ?>


<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/content.php' && $this->_tpl_vars['content']['fullembed'] )): ?>

    <div id="banner-sidebar" class="banner">
        <?php echo $this->_tpl_vars['env']->getBanner('slave',$this->_tpl_vars['ad']['category'],$this->_tpl_vars['ad']['page'],'halfpage_felso'); ?>

    </div>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./content/box_latest.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  else: ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./content/box_latest.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div id="banner-sidebar" class="banner">
        <?php echo $this->_tpl_vars['env']->getBanner('slave',$this->_tpl_vars['ad']['category'],$this->_tpl_vars['ad']['page'],'halfpage_felso'); ?>

    </div>
<?php endif; ?>



<?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/home.php'): ?>
    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=168ora.hu" alt="168ora.hu">168 óra</span></h4>
    <ul>
        <?php unset($this->_sections['p']);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['loop'] = is_array($_loop=$this->_tpl_vars['partners']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['start'] = (int)0;
$this->_sections['p']['max'] = (int)5;
$this->_sections['p']['show'] = true;
if ($this->_sections['p']['max'] < 0)
    $this->_sections['p']['max'] = $this->_sections['p']['loop'];
$this->_sections['p']['step'] = 1;
if ($this->_sections['p']['start'] < 0)
    $this->_sections['p']['start'] = max($this->_sections['p']['step'] > 0 ? 0 : -1, $this->_sections['p']['loop'] + $this->_sections['p']['start']);
else
    $this->_sections['p']['start'] = min($this->_sections['p']['start'], $this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] : $this->_sections['p']['loop']-1);
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = min(ceil(($this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] - $this->_sections['p']['start'] : $this->_sections['p']['start']+1)/abs($this->_sections['p']['step'])), $this->_sections['p']['max']);
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>
        <li><a href="<?php echo $this->_tpl_vars['partners'][$this->_sections['p']['index']]['link']; ?>
" rel="nofollow" class="outgoing"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['partners'][$this->_sections['p']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 98, "...") : smarty_modifier_truncate($_tmp, 98, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li>
        <?php endfor; endif; ?>
    </ul>
    </div>


    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=napidoktor.hu" alt="napidoktor.hu">Napidoktor</span></h4>
    <ul>
        <?php unset($this->_sections['p']);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['loop'] = is_array($_loop=$this->_tpl_vars['partners']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['start'] = (int)20;
$this->_sections['p']['max'] = (int)3;
$this->_sections['p']['show'] = true;
if ($this->_sections['p']['max'] < 0)
    $this->_sections['p']['max'] = $this->_sections['p']['loop'];
$this->_sections['p']['step'] = 1;
if ($this->_sections['p']['start'] < 0)
    $this->_sections['p']['start'] = max($this->_sections['p']['step'] > 0 ? 0 : -1, $this->_sections['p']['loop'] + $this->_sections['p']['start']);
else
    $this->_sections['p']['start'] = min($this->_sections['p']['start'], $this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] : $this->_sections['p']['loop']-1);
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = min(ceil(($this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] - $this->_sections['p']['start'] : $this->_sections['p']['start']+1)/abs($this->_sections['p']['step'])), $this->_sections['p']['max']);
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>
        <li><a href="<?php echo $this->_tpl_vars['partners'][$this->_sections['p']['index']]['link']; ?>
" rel="nofollow" class="outgoing"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['partners'][$this->_sections['p']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 98, "...") : smarty_modifier_truncate($_tmp, 98, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li>
        <?php endfor; endif; ?>
    </ul>
    </div>

    <div class="list-box partner-box">
    <h4 class="heading1"><span><img src="https://www.google.com/s2/favicons?domain=izeselet.hu" alt="izeselet.hu">Ízes élet</span></h4>
    <ul>
        <?php unset($this->_sections['p']);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['loop'] = is_array($_loop=$this->_tpl_vars['partners']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['start'] = (int)25;
$this->_sections['p']['max'] = (int)3;
$this->_sections['p']['show'] = true;
if ($this->_sections['p']['max'] < 0)
    $this->_sections['p']['max'] = $this->_sections['p']['loop'];
$this->_sections['p']['step'] = 1;
if ($this->_sections['p']['start'] < 0)
    $this->_sections['p']['start'] = max($this->_sections['p']['step'] > 0 ? 0 : -1, $this->_sections['p']['loop'] + $this->_sections['p']['start']);
else
    $this->_sections['p']['start'] = min($this->_sections['p']['start'], $this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] : $this->_sections['p']['loop']-1);
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = min(ceil(($this->_sections['p']['step'] > 0 ? $this->_sections['p']['loop'] - $this->_sections['p']['start'] : $this->_sections['p']['start']+1)/abs($this->_sections['p']['step'])), $this->_sections['p']['max']);
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>
        <li><a href="<?php echo $this->_tpl_vars['partners'][$this->_sections['p']['index']]['link']; ?>
" rel="nofollow" class="outgoing"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['partners'][$this->_sections['p']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 98, "...") : smarty_modifier_truncate($_tmp, 98, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li>
        <?php endfor; endif; ?>
    </ul>
    </div>
<?php endif; ?>



<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/home.php' ) || ( $this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/content.php' ) || ( $this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/category.php' )): ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./content/box_top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div id="banner-sidebar" class="banner">
        <?php echo $this->_tpl_vars['env']->getBanner('slave',$this->_tpl_vars['ad']['category'],$this->_tpl_vars['ad']['page'],'medium_rectangle_also'); ?>

    </div>

    <?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/content.php'): ?>
        <!-- /21667127856/propeller_cikk_4 -->
        <div id='propeller_cikk_4'>
        <script><?php echo '
        googletag.cmd.push(function() { googletag.display(\'propeller_cikk_4\'); });
        </script>'; ?>

        </div>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/colibrimedia/prop/public/modules/content/home.php'): ?>
        <!-- /21667127856/propeller_nyito_5 -->
        <div id='propeller_nyito_5'>
        <script><?php echo '
        googletag.cmd.push(function() { googletag.display(\'propeller_nyito_5\'); });
        </script>'; ?>

        </div>
    <?php endif; ?>

    <iframe src="https://nuus.hu/?wp_embed_box=eaaf549f6ea9" style="margin-top:25px;" width="310" height="300" frameborder="0" scrolling="no"></iframe>
<?php endif; ?>


<?php if (! $this->_tpl_vars['short_sidebar']): ?>
    <?php $this->assign('topstories_first_big', true); ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./content/box_topstories.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  endif; ?>

<div class="list-box" style="padding-top:0;text-align:center;">
    <div class="fb-page" data-href="https://www.facebook.com/propellerhu" data-hide-cta="true" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"></div>
</div>
