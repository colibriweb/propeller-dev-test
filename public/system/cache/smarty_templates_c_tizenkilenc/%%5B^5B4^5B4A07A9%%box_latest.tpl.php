<?php /* Smarty version 2.6.12, created on 2018-11-26 13:16:59
         compiled from ./content/box_latest.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', './content/box_latest.tpl', 13, false),array('modifier', 'escape', './content/box_latest.tpl', 13, false),array('modifier', 'date_format', './content/box_latest.tpl', 13, false),)), $this); ?>

    <div class="list-box">
    <h4 class="heading1"><span>Friss hírek</span></h4>
    <ul>
        <?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' ) && $this->_tpl_vars['content']['category_id'] == 1):  $this->assign('lateststart', '0'); ?>
        <?php elseif (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' ) && $this->_tpl_vars['content']['category_id'] == 2):  $this->assign('lateststart', '12'); ?>
        <?php elseif (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' ) && $this->_tpl_vars['content']['category_id'] == 6):  $this->assign('lateststart', '24'); ?>
        <?php elseif (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' ) && $this->_tpl_vars['content']['category_id'] == 4):  $this->assign('lateststart', '36'); ?>
        <?php elseif (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' ) && $this->_tpl_vars['content']['category_id'] == 7):  $this->assign('lateststart', '48'); ?>
        <?php else:  $this->assign('lateststart', '60');  endif; ?>

        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['latest']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)$this->_tpl_vars['lateststart'];
$this->_sections['i']['max'] = (int)8;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
        <li class="ga-event" data-category="box-friss" data-action="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['latest'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 98, "...") : smarty_modifier_truncate($_tmp, 98, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" data-label="<?php echo $this->_sections['i']['index']-$this->_tpl_vars['lateststart']+1; ?>
"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['latest'][$this->_sections['i']['index']]['datetime'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%H:%M") : smarty_modifier_date_format($_tmp, "%H:%M")); ?>
</span><a href="<?php echo $this->_tpl_vars['env']->base; ?>
/<?php echo $this->_tpl_vars['latest'][$this->_sections['i']['index']]['category_alias']; ?>
/<?php echo $this->_tpl_vars['latest'][$this->_sections['i']['index']]['alias']; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['latest'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 98, "...") : smarty_modifier_truncate($_tmp, 98, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li>
        <?php endfor; endif; ?>

        <?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' )): ?><li><span> </span><a href="<?php echo $this->_tpl_vars['env']->base; ?>
/<?php echo $this->_tpl_vars['content']['category_alias']; ?>
" style="font-weight:600;">Tovább az összes friss hírhez...</a></li><?php endif; ?>
    </ul>


        <?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' )): ?>
            <!-- /21667127856/propeller_cikk_1 -->
            <div id='propeller_cikk_1'>
            <script><?php echo '
            googletag.cmd.push(function() { googletag.display(\'propeller_cikk_1\'); });
            </script>'; ?>

            </div>
        <?php endif; ?>

        <?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php' )): ?>
            <!-- /21667127856/propeller_nyito_2 -->
            <div id='propeller_nyito_2'>
            <script><?php echo '
            googletag.cmd.push(function() { googletag.display(\'propeller_nyito_2\'); });
            </script>'; ?>

            </div>
        <?php endif; ?>

        <?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php' )): ?>
            <!-- /21667127856/propeller_rovat_3 -->
            <div id='propeller_rovat_3'>
            <script><?php echo '
            googletag.cmd.push(function() { googletag.display(\'propeller_rovat_3\'); });
            </script>'; ?>

            </div>
        <?php endif; ?>

    </div>

