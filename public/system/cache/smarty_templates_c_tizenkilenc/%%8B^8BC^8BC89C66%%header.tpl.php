<?php /* Smarty version 2.6.12, created on 2018-11-26 13:16:58
         compiled from ./header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', './header.tpl', 5, false),array('modifier', 'strip_tags', './header.tpl', 6, false),array('modifier', 'truncate', './header.tpl', 6, false),array('modifier', 'replace', './header.tpl', 6, false),array('modifier', 'trim', './header.tpl', 6, false),array('modifier', 'default', './header.tpl', 113, false),array('modifier', 'strstr', './header.tpl', 284, false),)), $this); ?>
<!DOCTYPE html>
<html lang="hu">
<head>
<meta charset="utf-8">
<title><?php if ($this->_tpl_vars['activities']['num'] > 0): ?>(<?php echo $this->_tpl_vars['activities']['num']; ?>
) <?php endif;  if ($this->_tpl_vars['title']):  echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  if (isset ( $this->_tpl_vars['p']['current_page'] ) && $this->_tpl_vars['p']['current_page'] != 1): ?> (<?php echo $this->_tpl_vars['p']['current_page']; ?>
. oldal)<?php endif; ?> - <?php endif;  echo $this->_tpl_vars['env']->l['title'];  if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php'): ?> - Friss hírek<?php endif; ?></title>
<meta name="description" content="<?php if ($this->_tpl_vars['content']['excerpt']):  echo ((is_array($_tmp=$this->_tpl_vars['content']['excerpt'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  elseif ($this->_tpl_vars['content']['description']):  echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['content']['meta_desc'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 170, "...") : smarty_modifier_truncate($_tmp, 170, "...")))) ? $this->_run_mod_handler('replace', true, $_tmp, "\n", "") : smarty_modifier_replace($_tmp, "\n", "")))) ? $this->_run_mod_handler('replace', true, $_tmp, '   ', ' ') : smarty_modifier_replace($_tmp, '   ', ' ')))) ? $this->_run_mod_handler('replace', true, $_tmp, '  ', ' ') : smarty_modifier_replace($_tmp, '  ', ' ')))) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  elseif ($this->_tpl_vars['tagtitle']): ?>&quot;<?php echo ((is_array($_tmp=$this->_tpl_vars['tagtitle'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
&quot; témájú friss hírek <?php echo $this->_tpl_vars['p']['results_num']; ?>
 forrásból. - <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['list'][0]['description'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 280, "...") : smarty_modifier_truncate($_tmp, 280, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  else: ?>Friss hírek, fontos témák - kritikus politikai-közéleti újság 2008 óta. Azoknak, akiket még érdekel, mi történik az országban és a világban.<?php endif; ?>">
<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && $this->_tpl_vars['content']['tags']): ?><meta name="keywords" content="<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['content']['tags']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
 echo ((is_array($_tmp=$this->_tpl_vars['content']['tags'][$this->_sections['j']['index']])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  if (! $this->_sections['j']['last']): ?>, <?php endif;  endfor; endif; ?>">
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800%7COpen+Sans+Condensed:700&amp;subset=latin,latin-ext">
<link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="<?php echo @STTC; ?>
/css/style.<?php echo @BUILD; ?>
.css">
<link rel="icon" href="<?php echo @STTC; ?>
/images/default/apple-touch-icon.<?php echo @BUILD; ?>
.png">
<link rel="apple-touch-icon" href="<?php echo @STTC; ?>
/images/default/apple-touch-icon.<?php echo @BUILD; ?>
.png">
<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && isset ( $this->_tpl_vars['content']['id'] )): ?><link rel="canonical" href="<?php echo $this->_tpl_vars['env']->base; ?>
/<?php echo $this->_tpl_vars['content']['category_alias']; ?>
/<?php echo $this->_tpl_vars['content']['alias']; ?>
">
<?php endif;  if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && $this->_tpl_vars['content']['link']):  endif;  if (isset ( $this->_tpl_vars['noindex'] )): ?><meta name="robots" content="noindex, follow">
<?php endif;  if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && $this->_tpl_vars['content']['link']): ?><meta name="Googlebot-News" content="noindex, nofollow">
<?php endif;  if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/add.php'): ?><link rel="canonical" href="<?php echo $this->_tpl_vars['env']->base; ?>
/bekuldes">
<?php endif;  if ($this->_tpl_vars['env']->u[1] == 'belepes'): ?><link rel="canonical" href="<?php echo $this->_tpl_vars['env']->base; ?>
/belepes">
<?php endif;  if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php'): ?><meta http-equiv="refresh" content="1200">
<?php endif;  if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && isset ( $this->_tpl_vars['content']['id'] )): ?><meta property="og:type" content="article">
<meta property="og:title" content="<?php echo ((is_array($_tmp=$this->_tpl_vars['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
">
<meta property="og:description" content="<?php if ($this->_tpl_vars['content']['excerpt']):  echo ((is_array($_tmp=$this->_tpl_vars['content']['excerpt'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  else:  echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['content']['meta_desc'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp, false) : smarty_modifier_strip_tags($_tmp, false)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 170, "...") : smarty_modifier_truncate($_tmp, 170, "...")))) ? $this->_run_mod_handler('replace', true, $_tmp, "\n", "") : smarty_modifier_replace($_tmp, "\n", "")))) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?>">
<meta property="og:url" content="<?php echo $this->_tpl_vars['env']->base; ?>
/<?php echo $this->_tpl_vars['content']['category_alias']; ?>
/<?php echo $this->_tpl_vars['content']['alias']; ?>
">
<?php endif; ?>
<meta property="og:image" content="<?php if ($this->_tpl_vars['bigpicture']):  echo @STTC; ?>
/images/bigpicture/<?php echo $this->_tpl_vars['img'][0]['filepath'];  elseif ($this->_tpl_vars['share_image']):  echo $this->_tpl_vars['share_image'];  else:  echo @SHARE_IMAGE;  endif; ?>">
<meta property="fb:app_id" content="247823682023773">
<meta property="fb:pages" content="244064559374">
<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && $this->_tpl_vars['content']['tags']):  unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['content']['tags']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?><meta property="article:tag" content="<?php echo ((is_array($_tmp=$this->_tpl_vars['content']['tags'][$this->_sections['j']['index']])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
">
<?php endfor; endif; ?>
<meta property="article:publisher" content="https://www.facebook.com/propellerhu">
<meta property="article:published_time" content="<?php echo $this->_tpl_vars['content']['date_c']; ?>
">
<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['content']['authors']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
 if ($this->_tpl_vars['content']['authors'][$this->_sections['j']['index']]['facebook_url']): ?><meta property="article:author" content="<?php echo $this->_tpl_vars['content']['authors'][$this->_sections['j']['index']]['facebook_url']; ?>
"><?php endif;  endfor; endif; ?>

<?php endif; ?>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
<link rel="alternate" type="application/rss+xml" href="<?php echo $this->_tpl_vars['env']->base; ?>
/feed/content-sajat.xml">

<?php echo '<!-- AdOcean HEAD -->
<script type="text/javascript">
var myAdoceanVars = "&sitewidth="+document.documentElement.clientWidth;
var myAdoceanKeys = "";

if (((window.innerWidth<5000) && (window.innerWidth>=1200)) ||
   ((document.documentElement.clientWidth<5000) && (document.documentElement.clientWidth>=1200)))
    {
    myAdoceanKeys = myAdoceanKeys+ "w_1200";
    }
if (((window.innerWidth<1200) && (window.innerWidth>=1024)) ||
   ((document.documentElement.clientWidth<1200) && (document.documentElement.clientWidth>=1024)))
    {
    myAdoceanKeys = myAdoceanKeys+ "w_970";
    }
if (((window.innerWidth<1024) && (window.innerWidth>=768)) ||
   ((document.documentElement.clientWidth<1024) && (document.documentElement.clientWidth>=768)))
    {
    myAdoceanKeys = myAdoceanKeys+ "w_728";
    }
if (((window.innerWidth<768) && (window.innerWidth>=480)) ||
   ((document.documentElement.clientWidth<768) && (document.documentElement.clientWidth>=480)))
    {
    myAdoceanKeys = myAdoceanKeys+ "w_468";
    }
if ((window.innerWidth<480) || (document.documentElement.clientWidth<480))
    {
    myAdoceanKeys = myAdoceanKeys+ "w_320";
    }
</script>
<script type="text/javascript" src="//gemhu.adocean.pl/files/js/ado.js"></script>
<script type="text/javascript">
/* (c)AdOcean 2003-2017 */
    if(typeof ado!=="object"){ado={};ado.config=ado.preview=ado.placement=ado.master=ado.slave=function(){};}
    ado.config({mode: "old", xml: false, characterEncoding: true});
    ado.preview({enabled: true, emiter: "gemhu.adocean.pl", id: "uESaTC3oo4RWFYK0Pi7N40dYf6fwGW9odHhhEhar3_7.p7"});
</script>
<!-- / AdOcean HEAD -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-NW3FSR\');</script>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NW3FSR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

<!-- Hotjar Tracking Code for http://propeller.hu/ -->
<script>
(function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:372454,hjsv:5};
    a=o.getElementsByTagName(\'head\')[0];
    r=o.createElement(\'script\');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
})(window,document,\'//static.hotjar.com/c/hotjar-\',\'.js?sv=\');
</script>
'; ?>

<base href="<?php echo $this->_tpl_vars['env']->base; ?>
">
</head>
<body class="default<?php if (isset ( $this->_tpl_vars['is_adult'] )): ?> is-adult<?php endif;  if (isset ( $this->_tpl_vars['body_404'] )): ?> static<?php elseif ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php'): ?> content content-<?php echo ((is_array($_tmp=@$this->_tpl_vars['content']['id'])) ? $this->_run_mod_handler('default', true, $_tmp, '0') : smarty_modifier_default($_tmp, '0'));  if ($this->_tpl_vars['content']['link']): ?> content-rovid<?php else: ?> content-sajat<?php endif;  if ($this->_tpl_vars['content']['fullembed']): ?> content-video<?php endif;  if ($this->_tpl_vars['content']['tags'] && in_array ( 'a szerk.' , $this->_tpl_vars['content']['tags'] )): ?> tag-a-szerk<?php endif;  elseif ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/add.php'): ?> bekuldes<?php elseif ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php'): ?> home<?php elseif ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/pages/index.php'): ?> static<?php elseif ($this->_tpl_vars['env']->u[1] == 'tag'): ?> tag<?php elseif ($this->_tpl_vars['env']->u[1] == 'video'): ?> video category<?php elseif ($this->_tpl_vars['env']->u[1] == $this->_tpl_vars['env']->l['users']['url_messages']): ?> messages<?php elseif ($this->_tpl_vars['env']->u[1] == $this->_tpl_vars['env']->l['users']['url_settings']): ?> settings<?php elseif ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php'): ?> category<?php endif; ?>">

<script type="text/javascript">
<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' ) && $this->_tpl_vars['content']['tags']): ?>
myAdoceanKeys = myAdoceanKeys + "<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['content']['tags']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>,<?php echo ((is_array($_tmp=$this->_tpl_vars['content']['tags'][$this->_sections['j']['index']])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html'));  endfor; endif; ?>";
<?php endif; ?>
</script>

<!-- AdOcean MASTER -->
<?php echo $this->_tpl_vars['env']->getBanner('master',$this->_tpl_vars['ad']['category'],$this->_tpl_vars['ad']['page']); ?>

<!-- / AdOcean MASTER -->

<!-- AdOcean interstitial -->
<?php echo $this->_tpl_vars['env']->getBanner('slave',$this->_tpl_vars['ad']['category'],$this->_tpl_vars['ad']['page'],'interstitial'); ?>

<!-- / AdOcean interstitial -->

<?php echo '
<div id="adoceangemhuyjppgsguck"></div>
<script type="text/javascript">
/* (c)AdOcean 2003-2017, IKO_Digital.propeller.hu.mobil.sticky */
ado.slave(\'adoceangemhuyjppgsguck\', {myMaster: \'1VZQQX.858IeVMIMLi__FzmvThHQiluBKBq46m2pC2b.t7\' });
</script>'; ?>



<script><?php echo '
(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');'; ?>

var loc = (window.location.search.indexOf('fb_') != -1) ? window.location.pathname : window.location.pathname+window.location.search;
ga('create', 'UA-80777149-1', 'auto');
ga('create', 'UA-109791684-1', 'auto', <?php echo '{\'name\': \'masik-iko\'}'; ?>
);
ga('create', 'UA-241542-5', 'auto', <?php echo '{\'name\': \'regi\'}'; ?>
);
ga('send', 'pageview', loc);
ga('regi.send', 'pageview', loc);
ga('set', 'dimension1', '<?php if ($_SESSION['user']): ?>Member<?php else: ?>Visitor<?php endif; ?>');
<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' )): ?>
ga('set', 'contentGroup1', '<?php if (! $this->_tpl_vars['content']['link']): ?>Saját tartalom<?php else: ?>RSS tartalom<?php endif; ?>');
<?php endif; ?>
//if(top!=self) top.location.replace(document.location);
</script>


<?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php'): ?>
    <?php $this->assign('gemius', "bQ.rsMLaLIpY_FD6CrBBnJcPLcCIlo9uu22cGnZvirP.K7");  elseif ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' || $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php'): ?>
    <?php if ($this->_tpl_vars['env']->u[1] == 'itthon'): ?>
        <?php $this->assign('gemius', "ciflnwbU0WLje16AGM8k5oXxHbEdbe_oUws56PG54l3.B7"); ?>
    <?php elseif ($this->_tpl_vars['env']->u[1] == 'nagyvilag'): ?>
        <?php $this->assign('gemius', "bO5L_1A8PMlSv.FFYrLd95QKHZF6xe_YYmCvIpw4t6f.F7"); ?>
    <?php elseif ($this->_tpl_vars['env']->u[1] == 'szorakozas'): ?>
        <?php $this->assign('gemius', "ciJL_wcyPMjj21FQ0Kz1NYXxfXAdba7dfRZJC0XVOfn.M7"); ?>
    <?php elseif ($this->_tpl_vars['env']->u[1] == 'sport'): ?>
        <?php $this->assign('gemius', "coHlPwei0eONNjf3GCrJcIXDP_gd9MdFHdaCQPX_PML.17"); ?>
    <?php elseif ($this->_tpl_vars['env']->u[1] == 'technika'): ?>
        <?php $this->assign('gemius', "ciJL_wcyPMjjMPFFGDtVF4Yj354dtAb1fZj8Z0Z2Nzz.t7"); ?>
    <?php else: ?>
        <?php $this->assign('gemius', "0sfrO_b5LBD9GbG8kFeV2rR53whK.wcBAiByQexB5Nr.07"); ?>
    <?php endif;  else: ?>
    <?php $this->assign('gemius', "0sfrO_b5LBD9GbG8kFeV2rR53whK.wcBAiByQexB5Nr.07");  endif; ?>

<script type="text/javascript">
<!--//--><![CDATA[//><!--
var pp_gemius_identifier = '<?php echo $this->_tpl_vars['gemius']; ?>
';<?php echo '
// lines below shouldn\'t be edited
function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+\'_pdata\'] = window[i+\'_pdata\'] || []; x[x.length]=arguments;};};gemius_pending(\'gemius_hit\'); gemius_pending(\'gemius_event\'); gemius_pending(\'pp_gemius_hit\'); gemius_pending(\'pp_gemius_event\');(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l=\'http\'+((location.protocol==\'https:\')?\'s\':\'\'); gt.setAttribute(\'async\',\'async\');gt.setAttribute(\'defer\',\'defer\'); gt.src=l+\'://gahu.hit.gemius.pl/xgemius.js\'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,\'script\');
//--><!]]>
</script>'; ?>



<script src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script><?php echo '
  googletag.cmd.push(function() {
    googletag.defineSlot(\'/21667127856/propeller_cikk_1\', [\'fluid\', [1, 1]], \'propeller_cikk_1\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_cikk_2\', [\'fluid\', [1, 1]], \'propeller_cikk_2\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_cikk_3\', [\'fluid\', [1, 1]], \'propeller_cikk_3\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_cikk_4\', [\'fluid\', [1, 1]], \'propeller_cikk_4\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_cikk_5\', [\'fluid\', [1, 1]], \'propeller_cikk_5\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_cikk_6\', [\'fluid\', [1, 1]], \'propeller_cikk_6\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_1\', [\'fluid\', [1, 1]], \'propeller_nyito_1\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_2\', [\'fluid\', [1, 1]], \'propeller_nyito_2\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_3\', [\'fluid\', [1, 1]], \'propeller_nyito_3\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_4\', [\'fluid\', [1, 1]], \'propeller_nyito_4\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_5\', [\'fluid\', [1, 1]], \'propeller_nyito_5\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_6\', [\'fluid\', [1, 1]], \'propeller_nyito_6\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_nyito_7\', [\'fluid\', [1, 1]], \'propeller_nyito_7\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_rovat_1\', [\'fluid\', [1, 1]], \'propeller_rovat_1\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_rovat_2\', [\'fluid\', [1, 1]], \'propeller_rovat_2\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_rovat_3\', [\'fluid\', [1, 1]], \'propeller_rovat_3\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_rovat_4\', [\'fluid\', [1, 1]], \'propeller_rovat_4\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_rovat_5\', [\'fluid\', [1, 1]], \'propeller_rovat_5\').addService(googletag.pubads());
    googletag.defineSlot(\'/21667127856/propeller_rovat_6\', [\'fluid\', [1, 1]], \'propeller_rovat_6\').addService(googletag.pubads());
    googletag.pubads().enableSyncRendering();
    googletag.enableServices();
  });'; ?>

</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script><?php echo '
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-9029276815386250",
    enable_page_level_ads: true
  });
</script>'; ?>


<div id="wrapper">

    <header class="site-header">
    <div>
        <div class="site-title"><a rel="home" href="/"><span>Propeller</span></a></div>

        <div class="site-tools">
            <ul><!--
            <?php if (! $_SESSION['user']): ?>
                --><li class="newsletter"><a href="http://propeller.hu/hirlevel/subscribe" rel="nofollow" target="_blank"><i class="material-icons">email</i><span>Hírlevél</span></a></li><!--
                --><li class="registration"><a href="<?php echo $this->_tpl_vars['env']->l['users']['url_registration']; ?>
"><i class="material-icons">account_circle</i><span><?php echo $this->_tpl_vars['env']->l['users']['registration']; ?>
</span></a></li><!--
                --><li class="login"><a href="<?php echo $this->_tpl_vars['env']->l['users']['url_login']; ?>
"><i class="material-icons">lock</i><span>Belépés</span></a></li><!--
                --><li class="bekuldes"><a href="<?php echo $this->_tpl_vars['env']->l['content']['url_add']; ?>
"><i class="material-icons">add_circle</i><span>Beküldés</span></a></li><!--
            <?php else: ?>
                --><li class="activities"><a id="activity" title="Értesítések"<?php if ($this->_tpl_vars['activities']['num'] == 0): ?> style="display:none;"<?php endif; ?>><i class="material-icons">public</i><span><?php echo $this->_tpl_vars['activities']['num']; ?>
</span></a>
                    <ul id="activity-list"<?php if ($this->_tpl_vars['activities']['num'] == 0): ?> style="display:none;"<?php endif; ?>>
                        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['activities']['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?><li data-id="<?php echo $this->_tpl_vars['activities']['items'][$this->_sections['i']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['activities']['items'][$this->_sections['i']['index']]['message']; ?>
</li><?php endfor; endif; ?>
                    </ul></li><!--
                --><li class="pofile"><a href="<?php echo $this->_tpl_vars['env']->l['users']['url_profile']; ?>
/<?php echo $_SESSION['user']['alias']; ?>
"><i class="material-icons">person</i><span>Profilom</span></a></li><!--
                --><li class="messages"><a href="<?php echo $this->_tpl_vars['env']->l['users']['url_messages']; ?>
"><i class="material-icons">question_answer</i><span><?php echo $this->_tpl_vars['env']->l['users']['messages']; ?>
</span></a></li><!--
                --><li class="bekuldes"><a href="<?php echo $this->_tpl_vars['env']->l['content']['url_add']; ?>
"><i class="material-icons">add_circle</i><span>Beküldés</span></a></li><!--
                --><li class="logout"><a href="<?php echo $this->_tpl_vars['env']->l['users']['url_logout']; ?>
"><i class="material-icons">lock</i><span>Kilépés</span></a></li><!--
            <?php endif; ?>
                --><li class="like-button"><iframe src="//www.facebook.com/plugins/like.php?href=http://www.facebook.com/propellerhu&amp;width=130&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=247823682023773&amp;locale=hu_HU" scrolling="no" frameborder="0" allowTransparency="true"></iframe></li><!--
            --></ul>
        </div><!-- .site-tools -->

        <div class="site-search">
            <form action="/kereses" method="get">
                <input type="text" id="search-q" name="q" value="<?php echo ((is_array($_tmp=$_GET['q'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><button type="submit"><i class="material-icons">search</i></button>
            </form>
        </div><!-- .site-search -->
    </div>
    </header><!-- #header -->

    <nav class="site-navigation clear">
        <ul>
            <li class="menu-toggle"><a href=""><i class="material-icons">view_headline</i></a></li><!--
    <?php $_from = $this->_tpl_vars['misc']['navigation_links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['foo'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['foo']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['foo']['iteration']++;
?>
    --><li><a href="<?php echo $this->_tpl_vars['v']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['k'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li><!--
    <?php endforeach; endif; unset($_from); ?>
        --></ul>
    </nav><!-- .site-navigation -->



<div id="banner-leaderboard" class="banner">
    <?php echo $this->_tpl_vars['env']->getBanner('slave',$this->_tpl_vars['ad']['category'],$this->_tpl_vars['ad']['page'],'leaderboard'); ?>

</div>


<?php if ($this->_tpl_vars['SCRIPT_NAME'] != '/modules/content/content.php'): ?>
    <div class="topstories header clear">
        <div>
    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['topstories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['max'] = (int)3;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
    <div class="ga-event" data-category="cimlap-fejlec" data-action="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" data-label="<?php echo $this->_sections['i']['index']+1; ?>
. hely">
        <a href="<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="thumb outgoing" rel="nofollow"<?php else: ?> class="thumb"<?php endif; ?> style="background-image: url(<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['image']; ?>
);"></a>
        <span class="title"><a href="<?php echo $this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['topstories'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></span>
    </div>
    <?php endfor; endif; ?>

    <?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php'): ?>
        <!-- /21667127856/propeller_nyito_1 -->
        <div id='propeller_nyito_1'>
        <script><?php echo '
        googletag.cmd.push(function() { googletag.display(\'propeller_nyito_1\'); });
        </script>'; ?>

        </div>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php'): ?>
        <!-- /21667127856/propeller_rovat_1 -->
        <div id='propeller_rovat_1'>
        <script><?php echo '
        googletag.cmd.push(function() { googletag.display(\'propeller_rovat_1\'); });
        </script>'; ?>

        </div>
    <?php endif; ?>

        </div>
    </div>
<?php endif; ?>

    <div id="content" class="hfeed site clear">
    <div id="primary">
