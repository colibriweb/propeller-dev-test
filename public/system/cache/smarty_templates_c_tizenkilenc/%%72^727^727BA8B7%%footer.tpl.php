<?php /* Smarty version 2.6.12, created on 2018-11-26 13:16:59
         compiled from ./footer.tpl */ ?>


	<div class="clear"></div>

	</div><!-- #primary -->
	</div><!-- #content -->

</div><!-- #wrapper -->


<div class="clear"></div>

<footer class="site-footer">
	<nav class="footer-navigation">
	<ul>
		<li>&copy; 2016 IKO Digital Kft.</li><!--
		--><li><a href="/<?php echo $this->_tpl_vars['env']->l['pages']['url']; ?>
/<?php echo $this->_tpl_vars['env']->l['pages']['url_contact']; ?>
"><?php echo $this->_tpl_vars['env']->l['pages']['contact']; ?>
</a></li><!--
		--><li><a href="mailto:szerk@propeller.hu">Írjon a szerkesztőknek!</a></li><!--
		--><li><a href="/adatvedelmi_tajekoztato.pdf"><?php echo $this->_tpl_vars['env']->l['pages']['privacy']; ?>
</a></li><!--
		--><li><a href="/statikus/doboz">Ingyenes hírek</a></li><!--
		--><li><a href="<?php echo $this->_tpl_vars['env']->base; ?>
/feed/content-sajat.xml">RSS</a></li>
	</ul>
	</nav><!-- .footer-navigation -->
</footer><!-- .site-footer -->

<a id="scroll-top" href="#top"><i class="material-icons">keyboard_arrow_up</i></a>
<?php if (( $this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/content.php' )): ?>
<a id="next-post" href="/kovetkezo-hir" rel="nofollow" data-counter="<?php echo $this->_tpl_vars['next_post_counter']; ?>
" title="Következő hír"><i class="material-icons">keyboard_arrow_right</i></a>
<?php endif; ?>

<div id="cookie_block" hidden>
	<div class="container">
		<div class="toCenter">
			<p class="text-left">
				<strong>
					Az oldalon sütiket használunk, hogy biztonságos böngészés mellett minél értékesebb felhasználói élményt adhassunk!
				</strong>

				<a id="accept_cookie_btn" class="cookie-button cta" href="">Rendben</a>
				<a id="more_info" class="cookie-button cta cta-outline" href="http://propeller.hu/adatvedelmi_tajekoztato.pdf">Részletek</a>
			</p>
		</div>
	</div>
</div>

<script>var logged_in=<?php if ($_SESSION['user']): ?>true<?php else: ?>false<?php endif; ?>;</script>

<!-- Facebook SDK -->
<div id="fb-root"></div>
<script><?php echo '(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.7&appId=247823682023773";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));'; ?>
</script>

<script src="<?php echo @STTC; ?>
/js/jquery-1.9.0.min.js"></script>
<script src="<?php echo @STTC; ?>
/js/script.<?php echo @BUILD; ?>
.js"></script>
<script src="<?php echo @STTC; ?>
/js/stickyscroller.js"></script>

</body>
</html>