<?php /* Smarty version 2.6.12, created on 2018-11-26 12:52:45
         compiled from ./content/box_top.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', './content/box_top.tpl', 7, false),array('modifier', 'escape', './content/box_top.tpl', 7, false),)), $this); ?>

<?php if ($this->_tpl_vars['top']): ?>
<div class="list-box popular-box">
<h4 class="heading1"><span>Top hírek</span></h4>
<ul>
    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['top']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['max'] = (int)8;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
    <li><span><b style="background:rgba(246,17,0,0.99)"><?php echo $this->_sections['i']['index']+1; ?>
</b></span><a href="<?php echo $this->_tpl_vars['env']->base; ?>
/<?php echo $this->_tpl_vars['top'][$this->_sections['i']['index']]['category_alias']; ?>
/<?php echo $this->_tpl_vars['top'][$this->_sections['i']['index']]['alias']; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['top'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 98, "...") : smarty_modifier_truncate($_tmp, 98, "...")))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li>
    <?php endfor; endif; ?>
</ul>


        <?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/home.php'): ?>
            <!-- /21667127856/propeller_nyito_4 -->
            <div id='propeller_nyito_4'>
            <script><?php echo '
            googletag.cmd.push(function() { googletag.display(\'propeller_nyito_4\'); });
            </script>'; ?>

            </div>
        <?php endif; ?>

        <?php if ($this->_tpl_vars['SCRIPT_NAME'] == '/modules/content/category.php'): ?>
            <!-- /21667127856/propeller_rovat_4 -->
            <div id='propeller_rovat_4'>
            <script><?php echo '
            googletag.cmd.push(function() { googletag.display(\'propeller_rovat_4\'); });
            </script>'; ?>

            </div>
        <?php endif; ?>
</div>
<?php else: ?>
Nincsenek Top hírek
<?php endif; ?>


<?php if (isset ( $_GET['miniad'] )): ?>
<!-- Goa3 beépítés: propeller.hu kep + szoveg 1., 3765609 -->
<div id="zone3765609" class="goAdverticum"></div>
<!-- A g3.js-t oldalanként egyszer, a </body> zárótag elõtt kell meghívni -->
<?php endif; ?>