<?php /* Smarty version 2.6.12, created on 2018-11-26 12:52:43
         compiled from content/home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'content/home.tpl', 9, false),array('modifier', 'strstr', 'content/home.tpl', 10, false),array('modifier', 'strip_tags', 'content/home.tpl', 13, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<main class="main" role="main">
<div>

	<?php if ($this->_tpl_vars['misc']['is_breaking']): ?>
		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['left']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['max'] = (int)1;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<div class="block block-<?php echo $this->_sections['i']['index']+1;  if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']): ?> block-img<?php else: ?> block-noimg<?php endif; ?> block-breaking ga-event"
		data-category="cimlap-breaking" data-action="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" data-label="1. hely">
			<?php if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']): ?><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="thumb outgoing" rel="nofollow"<?php else: ?> class="thumb"<?php endif; ?> title="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="background-image: url(<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']; ?>
);"><?php if ($this->_tpl_vars['misc']['is_breaking_subtitle']): ?><span><?php echo ((is_array($_tmp=$this->_tpl_vars['misc']['is_breaking_subtitle'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</span><?php endif; ?></a><?php endif; ?>

			<h2><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></h2>
			<?php if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['description']): ?><p><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['description'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p><?php endif; ?>

			<?php if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink']): ?><ul>
			<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
			<li><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></li>
			<?php endfor; endif; ?>
			</ul>
			<?php endif; ?>
		</div>
		<?php endfor; endif; ?>
		<?php $this->assign('left_col_start', 1); ?>
	<?php else: ?>
		<?php $this->assign('left_col_start', 0); ?>
	<?php endif; ?>

	<div class="col left">
		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['left']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)$this->_tpl_vars['left_col_start'];
$this->_sections['i']['max'] = (int)28;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<div class="block block-<?php echo $this->_sections['i']['index']+1;  if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']): ?> block-img<?php else: ?> block-noimg<?php endif; ?> ga-event"
		data-category="cimlap-bal" data-action="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" data-label="<?php echo $this->_sections['i']['index']+1; ?>
. hely">
			<?php if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']): ?><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="thumb outgoing" rel="nofollow"<?php else: ?> class="thumb"<?php endif; ?> title="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="background-image: url(<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['image']; ?>
);"></a><?php endif; ?>

			<h2><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></h2>
			<?php if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['description']): ?><p><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['description'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p><?php endif; ?>

			<?php if ($this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink']): ?><ul>
			<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
			<li><a href="<?php echo $this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['left'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></li>
			<?php endfor; endif; ?>
			</ul>
			<?php endif; ?>
		</div>

		<?php if ($this->_sections['i']['index'] == 5): ?>
			<!-- /21667127856/propeller_nyito_6 -->
			<div id='propeller_nyito_6'>
			<script><?php echo '
			googletag.cmd.push(function() { googletag.display(\'propeller_nyito_6\'); });
			</script>'; ?>

			</div>
		<?php endif; ?>

		<?php endfor; endif; ?>


	</div>

	<div class="col right">

		<div class="list-box">
		<h4 class="heading1"><span>Most beszédtéma</span></h4>
		<script>var commented_since = '<?php echo $this->_tpl_vars['commented'][0]['datetime']; ?>
';</script>
		<ul id="commented-box">
			<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['commented']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['max'] = (int)8;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
			<li id="c_<?php echo $this->_tpl_vars['commented'][$this->_sections['i']['index']]['id']; ?>
"><a href="<?php echo $this->_tpl_vars['env']->base; ?>
/<?php echo $this->_tpl_vars['commented'][$this->_sections['i']['index']]['link']; ?>
#v<?php echo $this->_tpl_vars['commented'][$this->_sections['i']['index']]['id']; ?>
" rel="bookmark"><?php echo ((is_array($_tmp=$this->_tpl_vars['commented'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a></li>
			<?php endfor; endif; ?>
		</ul>
		</div>

		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['right']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['max'] = (int)30;
$this->_sections['i']['show'] = true;
if ($this->_sections['i']['max'] < 0)
    $this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<div class="block block-<?php echo $this->_sections['i']['index']+1;  if ($this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['image']): ?> block-img<?php else: ?> block-noimg<?php endif; ?> ga-event"
		data-category="cimlap-jobb" data-action="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" data-label="<?php echo $this->_sections['i']['index']+1; ?>
. hely">
			<?php if ($this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['image']): ?><a href="<?php echo $this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="thumb outgoing" rel="nofollow"<?php else: ?> class="thumb"<?php endif; ?> title="<?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="background-image: url(<?php echo $this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['image']; ?>
);"></a><?php endif; ?>

			<h2><a href="<?php echo $this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></h2>
			<?php if ($this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['description']): ?><p><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['description'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</p><?php endif; ?>

			<?php if ($this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['sublink']): ?><ul>
			<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['sublink']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
			<li><a href="<?php echo $this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['link']; ?>
"<?php if (! ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['link'])) ? $this->_run_mod_handler('strstr', true, $_tmp, $this->_tpl_vars['env']->base) : strstr($_tmp, $this->_tpl_vars['env']->base))): ?> class="outgoing" rel="nofollow"><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<i class="material-icons material-external">exit_to_app</i><?php else: ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['blocks']['right'][$this->_sections['i']['index']]['sublink'][$this->_sections['j']['index']]['title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp));  endif; ?></a></li>
			<?php endfor; endif; ?>
			</ul>
			<?php endif; ?>
		</div>

		<?php if ($this->_sections['i']['index'] == 1): ?>
			<!-- /21667127856/propeller_nyito_3 -->
			<div id='propeller_nyito_3'>
			<script><?php echo '
			googletag.cmd.push(function() { googletag.display(\'propeller_nyito_3\'); });
			</script>'; ?>

			</div>
		<?php endif; ?>
		<?php endfor; endif; ?>

	</div>


	<div class="clear"></div>
</div>
</main><!-- .main -->

<aside class="sidebar" role="complementary">
<div>

	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</div>
</aside><!-- .sidebar -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "./footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>