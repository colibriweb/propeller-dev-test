<?php

/*
GoogleAnalytics kampány-url paraméterezés séma:

utm_source				utm_medium			utm_campaign

atv.hu					partner 			rawurlencode(Kiborult a bili: kivonulhat Magyarországról a Visa)
newsletter_20121212		mail				rawurlencode(Kiborult a bili: kivonulhat Magyarországról a Visa)
activity_20121212		mail				noreply
messages				mail				noreply
validation				mail				noreply
*/

header('Content-Type: text/html; charset=UTF-8');

if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
    header('X-UA-Compatible: IE=edge,chrome=1');
}

date_default_timezone_set('Europe/Budapest');

session_set_cookie_params(0, '/', '');
session_start();

// bekezdés szétdarabolóhoz a regexp
define('EXCERPTREG', '^([^\.\!\?\…].*?([a-záéíóööőúűü\s\"-]{3,}|[A-Z]{2,})[\.|\!|\?|\…]+\"?(\s+|$))');

class Environment {

	/**
	* Adatbáziskapcsolat referenciája
	* @var Database
	*/
	var $db;

	/**
	* URL route darabok
	* @var array
	*/
	var $u = array();

	/**
	* Nyelvi tömb
	* @var array
	*/
	var $l = array();

	/**
	* Konfigurációs tömb
	* @var array
	*/
	var $c = array();

	/**
	* Rendszerüzenet
	* @var string
	*/
	var $message;

	/**
	* Alapértelmezett template
	* @var string
	*/
	var $template = 'tizenkilenc';

	/**
	* Rendszergyökér webcíme
	* @var string
	*/
	var $base; // base host protokollal, perjel nélkül ("http://propeller.hu")

	var $domainBase; // cookiek hosztja (".propeller.hu")

	var $adslots; // reklámzónákat tartalmazó tömb
	var $isMobile; // mobilról nézik az oldalt?

	function Environment() {
		// url darabok feltöltése
		if(!isset($_REQUEST['qsa'])) {
			$_REQUEST['qsa'] = ltrim($_SERVER['REQUEST_URI'], '/');
		}
		$this->u = array_filter(explode('/', '/'.$_REQUEST['qsa'])); //unset($this->route[0])

		//$this->base = 'http://'.HOST;
		$this->base = 'http://localhost/colibrimedia/prop/';
		//$this->domainBase = '.'.HOST;
		$this->domainBase = 'http://localhost/colibrimedia/prop/';

		$this->adslots = include_once(ROOT.'/adocean.inc.php');
		$this->isMobile = $this->isMobile();

		// nyelv beállítása
		$this->l = include(ROOT.'/modules/hu.php');

		// config
		$this->c['content']['tagsnum'] = 5; // címkék megadható maximális száma
		$this->c['content']['anoncomment'] = false; // névelen kommentelhet?
		$this->c['content']['perpage'] = 15; // lapozós oldalakon a limit
		$this->c['content']['trackback'] = false; // cikk beküldéskor menjen trackback ping?
		$this->c['users']['logincookietime'] = 6048000; // belépés megjegyzése ennyi időre
		$this->c['users']['mailcookietime'] = 259200; // email cím megjegyzése az input mezőn ennyi időre
		$this->c['users']['errorlog'] = true; // form-küldési hibák loggolása
		$this->c['users']['errorlimit'] = 10; // ennyi hibás form-küldés után tiltás
		$this->c['users']['logtime'] = 60; // hibás form-küldéseek miatt tiltás ennyi időre
		$this->c['users']['registration'] = true; // regisztrálni lehet?
		$this->c['users']['usecaptcha'] = true; // capctha kell a regisztrációhoz?
		$this->c['users']['passlength'] = 5; // jelszó min. hossza
		$this->c['users']['mailvalidation'] = true; // emailes érvényesítés kell a regisztrációhoz?
		$this->c['users']['noticeadminreg'] = false; // kapjon az admin értesítést az új regisztrációról?
		$this->c['users']['keylength'] = 8; // érvényesítő token hossza
		$this->c['users']['messages_perpage'] = 10; // privát üzenetek oldalon az üzenet limit
	}

	public function getNewPolicyHash($mail) {
		return sha1('salted'.$mail.'new_policy_hash');
	}

	public function newPolicyAnswer($mail) {
	    $res = $this->db->Query("SELECT mail FROM _adatvedelmi_valaszok WHERE mail = '".$this->db->escape($mail)."' LIMIT 1");
	    if($this->db->numRows($res)) {
	        return true;
	    }
	    return false;
	}

	/**
	* Hibaoldalakra történő átirányítások lekezelése
	* @param integer $status Átirányítás státuszkódja
	* @return void
	*/
	public function setHeader($status) {
		if($status == 404) {

			global $env, $smarty, $content;

			header('HTTP/1.0 404 Not Found');

			$smarty->assign('pages', array('title' => 'Az oldal nem található!', 'alias' => '404', 'body' =>
				'<p>Lehet, hogy eltávolításra került a tartalom, de az is lehet, hogy hibás linkre kattintott.</p>
				<p>Esetleg próbálja meg a <a href="http://propeller.hu/kereses">keresővel</a> megtalálni!</p>'));

			// dolgok
			$smarty->assign('misc', $content->q_misc());

			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$blocks = $content->q_blocks();

			foreach($blocks['left'] AS $key => $value) {
				if(empty($value['image'])) unset($blocks['left'][$key]);
				unset($blocks['left'][$key]['sublink']);
				unset($blocks['left'][$key]['description']);
			}
			$blocks['left'] = array_values($blocks['left']);
			foreach($blocks['right'] AS $key => $value) {
				if(empty($value['image'])) unset($blocks['right'][$key]);
				unset($blocks['right'][$key]['sublink']);
				unset($blocks['right'][$key]['description']);
			}
			$blocks['right'] = array_values($blocks['right']);


			$smarty->assign('blocks', $blocks);

			$smarty->assign('activities', $env->getActivities());
			$smarty->assign('body_404', true);
			$smarty->display('pages/index.tpl');
			exit;

			header('HTTP/1.0 404 Not Found');
			echo '<h1>404 - Az oldal nem található</h1>';
			echo '<h2><a href="http://propeller.hu/">Ugrás a címlapra</a></h2>';
			die;

		}
		elseif($status == 403) {
			header('HTTP/1.0 403 Forbidden');
			exit;
		}

	}

	/**
	* Látogató IP-címének lekérdezése
	* @return string Az IP-címmel tér vissza
	*/
	public function getIpAddress() {

		return $_SERVER['REMOTE_ADDR'];

		/*
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			return $_SERVER['REMOTE_ADDR'];
		}*/

	}

	public function setOverlayNotification($message) { // kliensoldali értesítés beállítása cookie-val

		return setcookie('overlay_notification', $message, time()+5, '/');

	}

	public function setOverlayDialog($message) { // kliensoldali értesítés beállítása cookie-val

		return setcookie('overlay_dialog', $message, time()+5, '/');

	}

	public function isDenied($userID = null) {

		// a cookie-s megbélyegzést lehet ki kéne venni, mert ha ugyanarról a gépről többen propellereznek,
		// a másik is kitiltódik

		if(isset($_SESSION['dck']) || isset($_COOKIE['dck'])) {
			setcookie('dck', 1, time()+8640000, '/');
			$_SESSION['dck'] = 1;
			return true;
		}

		if(isset($userID)) {
			$res_den = $this->db->Query("SELECT status FROM "._DBPREF."users WHERE id = '".$userID."' AND status = '0' LIMIT 1");
			if($this->db->numRows($res_den)) {
				setcookie('dck', 1, time()+8640000, '/');
				$_SESSION['dck'] = 1;
				return true;
			}
		}

		return false;

	}

	/**
	* String-ként átadott címkék szétvágása tömbbe, az elválasztó karakter mentén
	* @param string $string Címkék felsorolva
	* @param string $separator Felsorolást elválasztó karakter
	* @param boolean $lowercase Átalakítás kisebtűssé (false = nem, true = igen)
	* @param integer $number Címkék száma maximálisan
	* @return array Sikeres művelet esetén a címkéket tartalmazó tömbbel, egyébként üres tömbbel tér vissza
	*/
	function getTagArray($string, $separator = ",", $lowercase = false, $number = 10) {

		$lowercase = true;

		$string = strip_tags($string);
		$string = stripslashes($string);
		$string = ($lowercase) ? mb_strtolower($string, 'UTF-8') : $string;
		$string = preg_replace("/[\%]/", "százalék", $string);
		$string = preg_replace("/[ |\t]{1,}/", " ", $string); // // szóközés és tab halmozás
		$tags = explode($separator, $string); // tagek tömbben

		// nem megengedett karakterek kipucolása
		foreach($tags AS $key => $value) {
			$tags[$key] = preg_replace('([^a-záéíóöőúüűÁÉÍÓÖŐÚÜŰèä\-\_\'\.\"A-Z0-9\+\:\(\)\&]+)', ' ', $value);
		}

		$tags = array_map('trim', $tags);
		$tags = array_unique($tags);

		foreach($tags as $i => $value) {
			if(empty($value) || (mb_strlen($value, 'UTF-8') < 2) || preg_match("/^([0-9]+)$/", $value)) {
				unset($tags[$i]);
			}
		}
		sort($tags);

		$tags = (!empty($tags)) ? array_slice($tags, 0, $number) : false;

		return $tags;

	}

	/**
	* Tömbként átadott címkék összefűzése string-nek, elválasztó karakterekkel
	* @param string $array Cimkék tömbben
	* @param string $separator Felsorolást elválasztó karakter
	* @return string Sikeres művelet esetén a címkék felsorolásával, egyébként üres tömbbel tér vissza
	*/
	function getTagString($array, $separator = ", ") {

		return implode($separator, (array)$array);

	}

	/**
	* Keresőbarát url készítése speciális karaktereket tartalmazó stringből
	* @param string $string Megtisztítandó string
	* @return string Sikeres művelet esetén a biztonságos string-gel, egyébként false-sal tér vissza
	*/
	function getSefUrl($string) {

		$string = mb_strtolower($string, 'UTF-8');
		$string = preg_quote($string);

		$accents = array("[í]","[á]","[ä]","[é]","[ö]","[ü]","[ó]","[ő]","[ú]","[ű]");
		$replaces = array("i","a","a","e","o","u","o","o","u","u");
		$string = preg_replace($accents, $replaces, $string);

		$string = preg_replace('([^a-zA-Z0-9]+)', '-', $string);

		$string = '-'.$string.'-';

		// kényes szavak kiszedése az urlből, különben az adsense letilt
		$rep = "meztelen|sz?ex|perverz|porno|anal|milf";
		$string = preg_replace("/(\-az?\-|\-es\-|\-ime\-)/", "-", $string);
		$string = preg_replace("/($rep)([a-zA-Z0-9]+)?/", "-", $string);

		$string = preg_replace("/\-\w{1}\-/", "-", $string);
		$string = preg_replace("/\-{2,}/", "-", $string);
		$string = implode('-', array_slice(explode('-', trim($string, '-')), 0, 8));

		return $string;
	}

	/**
	* HTML elemeket és speciális karaktereket tartalmazó string plain textté konvertálása
	* @param string $text Szövegdarab
	* @param string $encoding Szövegdarab kódolása
	* @param string $leaveNl Sortörések meghagyása
	* @return string Sikeres művelet esetén a plain stringgel, egyébként false-sal tér vissza
	*/
	function getSafeText($text, $encoding = 'UTF-8', $leaveNl = false) {

		$text = iconv($encoding, 'utf-8//TRANSLIT', $text);
		$text = strtr($text, array('<![CDATA[' => '', ']]>' => ''));
		$text = strip_tags($text);

		$text = preg_replace(array("/[!]{4,}/", "/[?]{4,}/", "/[\.]{4,}/"), array("!!!", "???", "..."), $text); // írásjel halmozás
		$text = preg_replace("/[ |\t]{1,}/", " ", $text); // // szóközés és tab halmozás

		if($leaveNl) { // sortörés marad, de a halmozást el kell távolítani
			$text = str_replace(array("\r\n", "\r"), "\n", $text);
			$text = preg_replace("/[\n]{3,}/", "\n\n", $text);
		}

		return trim($text);

	}

	// rövid kötőjelek hosszúra cserélése a szövegben (admin használja asszem)
	function dashReplace($text) {

		$text = str_replace(array(" - ", " — "), " – ", $text);
		$text = str_replace(array(" -, ", " —, "), " –, ", $text);

		//$text = preg_replace('/"(.*?)"/', '„$1”', $text);

		return $text;
	}

	/**
	* Egy webcímből az alap domain vagy subdomain kinyerése
	* @param string $link Webcím
	* @return string Sikeres művelet esetén a domainnel, egyébként false-sal tér vissza
	*/
	function getHost($link) {

//		return preg_replace('/^(http:\/\/)?(www.)?([^\/\?]+)([\/|\?])?(.+)?$/', '$3', $link);
		$a = parse_url($link);
		return str_replace('www.', '', @$a['host']);

	}

	/**
	* Megvizsgál egy szövegrészletet, hogy tartalmaz-e nem megengedett kifejezéseket
	* @param string $string Szöveg
	* @return boolean Nem megengedett kifejezés esetén true-val, egyébként false-sal tér vissza
	*/
	function isSpamExpression($string) {

		$res = $this->db->Query("SELECT expression FROM "._DBPREF."system_spam");
		while($row = $this->db->fetchRow($res)) {
			if(preg_match("#\b$row[0]\b#", $string))
			return true;
		}
		return false;

	}

	// elütések, karakterhibák javítása
	function textCorrect($string) {

		$string = str_replace(' ... ...', '...', $string); // napi.hu
		$string = preg_replace('/(\.|!|\?\,\:\;\))([A-ZÁÉÍÓÖŐÚŰÜ0-9\"(])/', '$1 $2', $string);
		$string = str_replace(array('Tovább »'), '', $string); // szarságok
		return $string;

	}

	// megpróbálja visszaadni egy egybefolyó szövegből a lehetséges első bekezédst (első értelmes mondatot)
	function getExcerpt($string) { // ne használj strip_tag -et előtte!

		$string = preg_replace('#<[^>]+>#', ' ', $string); // html kipucolása és szóközzel helyettesítése
		$string = preg_replace("/[ |\t]{1,}/", " ", $string); // szóközés és tab halmozás
		$string = trim($string);

		if(preg_match("/[\n\r|\n]{1,}/", $string)) { // van benne sortörés, ott szakítsuk meg
			$parts = preg_split("/[\n\r|\n]{1,}/", $string, 4, PREG_SPLIT_DELIM_CAPTURE);
			$return = $parts[0];
		}
		else { // nincs sortörés, keressük meg az első mondatot
			preg_match('/'.EXCERPTREG.'/u', $string, $parts);

			if(!empty($parts[0])) {
				$return = $parts[0];
			}
			else {
				$return = $string;
			}
		}
		//p($return);die;
		return $return;
	}

	// megpróbálja egy egybefolyó szövegben megkeresni az első bekezdéset (első értelmes mondatot)
	// és beszúr utána valamit (reklám html-kódot)
	function getExcerptSplit($string, $insert) { // ne használj strip_tag -et előtte!

		$string = preg_replace('#<[^>]+>#', ' ', $string); // html kipucolása és szóközzel helyettesítése
		$string = preg_replace("/[ |\t]{1,}/", " ", $string); // szóközés és tab halmozás
		$string = trim($string);

		if(preg_match("/[\n\r|\n]{1,}/", $string)) { // van benne sortörés, oda szúrjuk be
			$return = preg_replace("/[\n\r|\n]{1,}/", $insert, $string, 1);
		}
		else { // nincs sortörés, keressük meg az első mondatot
			preg_match('/'.EXCERPTREG.'(.*)?/u', $string, $parts);

			if(!empty($parts[1])) {
				$return = $parts[1].$insert.@$parts[4];
			}
			else { // nem sikerült szétvágni, menjen a végére
				$return = $string.$insert;
			}

		}
		//p($return);die;
		return $return;
	}

	/**
	* Visszaadja egy helyi- vagy távoli szerveren lévő fájl tartalmát
	* @param string $url Elérési útvonal
	* @return mixed Sikeres művelet esetén a fájl tartalmával, egyébként false-sal tér vissza
	*/
	function getFileContent($url) {

		$allowed_content_types = array('text/plain', 'text/html', 'text/xml', 'application/xml', 'application/xhtml+xml', 'application/json', 'application/javascript', 'application/rss+xml');

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 7);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1');
		ob_start();

		curl_exec($ch);
		$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		curl_close($ch);

		preg_match('/([\w\/+]+)\;?/i', mb_strtolower($content_type, 'UTF-8'), $matches);
		if(!isset($matches[1]) || !in_array($matches[1], $allowed_content_types)) { // nem megfelelő MIME típus
			ob_end_clean();
			return false;
		}

		$string = ob_get_contents();
		ob_end_clean();

		return $string;

	}


	// fájlba kiírt cache

	function getFileCache($cacheID, $lifeTime = 3600) {

		if(!file_exists(ROOT.'/system/cache/filecache/'.$cacheID) || filemtime(ROOT.'/system/cache/filecache/'.$cacheID) < time()-(int)$lifeTime) return false;

		$bc = file_get_contents(ROOT.'/system/cache/filecache/'.$cacheID);
		return unserialize($bc);
	}

	function putFileCache($cacheID, $content) {

		touch(ROOT.'/system/cache/filecache/'.$cacheID);
		$f = fopen(ROOT.'/system/cache/filecache/'.$cacheID, "w");
		fwrite($f, serialize($content));
		fclose($f);
		return;

	}

	function dropFileCache($cacheID) {

		unlink(ROOT.'/system/cache/filecache/'.$cacheID);

	}

	/**
	* X-Requested-With kéréstípus ellenőrzése Ajax műveletek elvégzése számára
	* @return boolean XMLHttpRequest elérés esetén true-val, egyébként false-sal tér vissza
	*/
	function isAjax() {

		return $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

	}

	/**
	* Látogató user agent értékének ellenőrzése
	* @return boolean Robot látogató esetén true-val, egyébként false-sal tér vissza
	*/
	function isRobot() {
/*
		$ua = getenv('HTTP_USER_AGENT');
		if(@eregi("[^e]crawler|spider|bot|custo|web(cow|mon|capture)|wysigot|httrack|wget|xenu", $ua)) {
			return true;
		}
		return false;
*/
        return (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/[^e]crawl|spider|bot|slurp|custo|web(cow|mon|capture)|wysigot|httrack|wget|xenu/i', $_SERVER['HTTP_USER_AGENT']));
	}

	/**
	* HTML linkekké konvertálja a hivatkozásokat egy szövegben
	* @param string $string Szöveg
	* @return string A hivatkozásokat tartalmazó stringgel tér vissza
	*/
	function stringToLink($string) {

		return preg_replace("`((http)+(s)?:(//)|(www\.))((\w|\.|\-|_)+)(/)?(\S+)?`ie", "'<a href=\"http\\3://\\5\\6\\8\\9\" rel=\"nofollow\" target=\"_blank\">'.substr('\\0',0,45).'...</a>'", $string);

	}

	/**
	* HTML linkekké konvertálja a felhasználónév válaszokat a kommentben
	* @param string $string Szöveg
	* @return string A hivatkozásokat tartalmazó stringgel tér vissza
	*/
	function stringToUserlink($string) {

		$string = preg_replace("/@([a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ\-_\'\.\"][a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ\-_\'\.\"\s]{1,30}[a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ\-_\'\.\"]):/ie", "'<b>\\0</b> '", $string);
		$string = preg_replace("/\:<\/b>\s+\n+/", ":</b> ", $string);

		return $string;
	}

	function dateToString($datetime) {

		$time = strtotime($datetime);

		if(date('Y-m-d', $time) == date('Y-m-d', time()-86400))
			return 'tegnap, '.date('H:i', $time);
		elseif(date('Y-m-d', $time) == date('Y-m-d'))
			return 'ma, '.date('H:i', $time);
		elseif(date('Y', $time) == date('Y')) // idei
			return $this->l['months']['month'.date('m', $time)].' '.date('j', $time).'., '.date('H:i', $time);
		else
			return date('Y', $time).'. '.$this->l['months']['month'.date('m', $time)].' '.date('j', $time).'., '.date('H:i', $time);

	}

	function dateToPast($datetime) {

		$time = strtotime($datetime);
		$past = time()-$time;

		if($past < 60) return 'most'; // 1 percig
		elseif($past < 1740) return floor($past/60).' perce'; // 29 percig
		elseif($past < 2400) return 'fél órája'; // 40 percig
		elseif($past < 3600) return floor($past/60).' perce'; // 60 percig
		elseif($past < 43200) return floor($past/3600).' órája'; // 12 óráig
		elseif($past < 64800) return 'fél napja'; // 18 óráig
		elseif($past < 86400) return floor($past/3600).' órája'; // 24 óráig
		else return floor($past/86400).' napja'; // végtelen

	}

	function nofollowOutgoing($text) { // nofollow címkék a kifelé mutató linkekre

		preg_match_all('/<a\s.*>/isU', $text, $matches);
		if(count($matches[0]) > 0) {
			for($i = 0; $i < count($matches[0]); $i++) {
				if(!preg_match( '~nofollow~is', $matches[0][$i]) && !preg_match('~\/\/propeller\.hu~', $matches[0][$i])) {
					$result = trim($matches[0][$i],">");
					$result .= ' rel="nofollow" class="outgoing" target="_blank">';
					$text = str_replace($matches[0][$i], $result, $text);
				}
			}
		}
		return $text;

	}

	/**
	* Levágja a szöveget a megadott karakterhosszúság mentén, a szóközöket is figyelembe véve
	* @param string $string A szöveg
	* @param integer $length Megengedett karakterhossz
	* @param str $etc Folytatást jelző string
	* @return string Sikeres művelet esetén az új szövegdarabbal tér vissza
	*/
	function strTruncate($string, $length = 140, $etc = ' [...]') {

		if(mb_strlen($string, 'UTF-8') > $length) {
			$length -= strlen($etc);
			$string = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string, 0, $length+1, 'UTF-8'));
			return mb_substr($string, 0, $length, 'UTF-8').$etc;
		}
		else {
			return $string;
		}

	}

	// mobilos böngásző-e?
	function isMobile() {

		$useragent = $_SERVER['HTTP_USER_AGENT'];

		return (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|'.
			'hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|'.
			'p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|'.
			'windows (ce|phone)|xda|xiino/i', $useragent)
			|| preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|'.
			'al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|'.
			'bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|'.
			'craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|'.
			'ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|'.
			'hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|'.
			'i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|'.
			'kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|'.
			'm50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|'.
			'mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|'.
			'nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|'.
			'pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|'.
			'ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|'.
			'sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|'.
			't6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|'.
			'up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|'.
			'61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|'.
			'zte\-/i', substr($useragent, 0, 4)));
/*
		$mobile_browser = 0;

		if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) $mobile_browser++;

		if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0) || ((isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])))) $mobile_browser++;

		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
		$mobile_agents = array(
		    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		    'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
		    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		    'wapr','webc','winw','winw','xda','xda-');

		if(in_array($mobile_ua,$mobile_agents)) $mobile_browser++;

		if(strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini')>0) $mobile_browser++;

		if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows')>0) $mobile_browser = 0;

		return ($mobile_browser>0);
*/
	}

	/**
	 * Megfelelő banner-kód lekérdezése a rovat, az oldaltípus és a slot neve alapján.
	 * Pl:
	 * $env->getBanner('itthon', 'nyito', 'medium_rectangle_also');
	 *
	 * @param  string      $part     Kódrészlet típusa ('master' vagy 'slave')
	 * @param  string      $category Kategória slug (pl. 'szorakozas') VAGY 'cimlap', 'video', 'tag'
	 * @param  string|null $page     Oldal típusa ('nyito' vagy 'cikkoldal')
	 * @param  string|null $slave     Zóna neve (pl. 'leaderboard')
	 *
	 * @return string Zóna HTML-kódja
	 */
	function getBanner($part, $category, $page = null, $slave = null) {

		$banner = '';

		// Rosszul lettek megadva ezek a rovatnevek a zónák készítésekor, át kell fordítani
		if ($category == 'szorakozas') $category = 'bulvar';
		if ($category == 'nagyvilag') $category = 'kulfold';

		if ($this->isMobile) {
			// Mobilról nézik

			if ($part === 'master') { // mobilon csak 1 közös master van, azt adjuk vissza
				$banner = $this->adslots['Propeller_mobil']['master'];
			}

			if ($part === 'slave') {
				if (in_array($slave, array('roadblock_felso'))) {
					$banner = $this->adslots['Propeller_mobil']['slaves']['Propeller_mobil_felso'];
				} elseif (in_array($slave, array('roadblock_also'))) {
					$banner = $this->adslots['Propeller_mobil']['slaves']['Propeller_mobil_kozepso'];
				} elseif (in_array($slave, array('halfpage_felso'))) {
					$banner = $this->adslots['Propeller_mobil']['slaves']['Propeller_mobil_also'];
				}
			}
		} else {
			// Desktop, keresük ki a rovatnak, oldaltípusnak megfelelő mastert,

			foreach ($this->adslots as $pageName => $script) {
				$name = 'Propeller_' . $category . (!empty($page) ? "_$page" : '');

				if ($pageName == $name) {
					if ($part === 'master') {
						// master kérés, csak a külső tömbön kell végigmenni

						$banner = "\n" . $script['master'] . "\n";
					} elseif ($part === 'slave') {
						// slave kód, bele kell menni a slave tömbbe
						foreach ($script['slaves'] as $slaveName => $html) {
							if ($slaveName == $name . "_$slave") {
								$banner = "\n" . $html . "\n";
							}
						} // foreach
					} // elseif
				} // if
			} // foreach
		} // desktop

		// komment kiszedése
		//$banner = preg_replace('#/\*(?:[^*]*(?:\*(?!/))*)*\*/#','', $banner);

		return $banner;

	}

	// aktivitások lekérdezése az értesítő-buborékhoz a belépett felhasználó számára
	// azért itt van mert az $env-en keresztül így érhető el mindenhonnan (a templateből)
	function getActivities() {

		$acts = array('num' => 0);

		if(!isset($_SESSION['user']['id'])) return;

		$res_acts = $this->db->Query("SELECT SQL_CALC_FOUND_ROWS id, action, name, alias, message FROM "._DBPREF."users_activities
		WHERE datetime > '".date('Y-m-d H:i:s', strtotime('-10 day'))."'
			AND user_id_affected = '".$_SESSION['user']['id']."'
			AND viewed = 0
			ORDER BY datetime
			DESC LIMIT 0, 4");

		$res_num = $this->db->Query("SELECT found_rows()");
		$rows = $this->db->fetchArray($res_num);

		$acts['num'] = $rows[0];

		while($row_acts = $this->db->fetchAssoc($res_acts)) {
			//$row_acts['date'] = $this->dateToPast($row_acts['datetime']);

			if($row_acts['action'] == 5)
			$row_acts['message'] = '<a href="/tagok/'.$row_acts['alias'].'">'.htmlspecialchars($row_acts['name'], ENT_QUOTES).'</a> '.$row_acts['message'];
			else
			$row_acts['message'] = htmlspecialchars($row_acts['name'], ENT_QUOTES).' '.str_replace('<', '<br><', $row_acts['message']);

			$acts['items'][] = $row_acts;
		}

		return $acts;

	}

}


// debughoz
function p($var) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

?>
