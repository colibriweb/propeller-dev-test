<?php

return array(

'Propeller_cimlap' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_cimlap */
    ado.master({id: "1VVQOn.i58IUZ6vDVqYK6jD_0k.nNT3X9fBAP9VHV.3.m7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_cimlap_leaderboard' => '<div id="adoceangemhuppdqqrjpvz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_cimlap.Propeller_cimlap_leaderboard */
        ado.slave("adoceangemhuppdqqrjpvz", {myMaster: "1VVQOn.i58IUZ6vDVqYK6jD_0k.nNT3X9fBAP9VHV.3.m7" });
        </script>',

        'Propeller_cimlap_halfpage_felso' => '<div id="adoceangemhumalgepdumu"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_cimlap.Propeller_cimlap_halfpage_felso */
        ado.slave("adoceangemhumalgepdumu", {myMaster: "1VVQOn.i58IUZ6vDVqYK6jD_0k.nNT3X9fBAP9VHV.3.m7" });
        </script>',

        'Propeller_cimlap_medium rectangle_also' => '<div id="adoceangemhuzacnhmnivt"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_cimlap.Propeller_cimlap_medium rectangle_also */
        ado.slave("adoceangemhuzacnhmnivt", {myMaster: "1VVQOn.i58IUZ6vDVqYK6jD_0k.nNT3X9fBAP9VHV.3.m7" });
        </script>',

        'Propeller_cimlap_interstitial' => '<div id="adoceangemhuwbjtkjhnfo"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_cimlap.Propeller_cimlap_interstitial */
        ado.slave("adoceangemhuwbjtkjhnfo", {myMaster: "1VVQOn.i58IUZ6vDVqYK6jD_0k.nNT3X9fBAP9VHV.3.m7" });
        </script>'
    )
),
'Propeller_itthon_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_itthon_nyito */
    ado.master({id: "c9qRlLnibDVm6xxb5GuJ5l4rLW3ReOd05h04ix63Lvr.37", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_itthon_nyito_leaderboard' => '<div id="adoceangemhuqdhqbukgui"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_nyito.Propeller_itthon_nyito_leaderboard */
        ado.slave("adoceangemhuqdhqbukgui", {myMaster: "c9qRlLnibDVm6xxb5GuJ5l4rLW3ReOd05h04ix63Lvr.37" });
        </script>',

        'Propeller_itthon_nyito_halfpage_felso' => '<div id="adoceangemhuneogfrelpd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_nyito.Propeller_itthon_nyito_halfpage_felso */
        ado.slave("adoceangemhuneogfrelpd", {myMaster: "c9qRlLnibDVm6xxb5GuJ5l4rLW3ReOd05h04ix63Lvr.37" });
        </script>',

        'Propeller_itthon_nyito_medium_rectangle_also' => '<div id="adoceangemhukffnioopzy"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_nyito.Propeller_itthon_nyito_medium_rectangle_also */
        ado.slave("adoceangemhukffnioopzy", {myMaster: "c9qRlLnibDVm6xxb5GuJ5l4rLW3ReOd05h04ix63Lvr.37" });
        </script>',

        'Propeller_itthon_nyito_interstitial' => '<div id="adoceangemhuxfmtlliuyt"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_nyito.Propeller_itthon_nyito_interstitial */
        ado.slave("adoceangemhuxfmtlliuyt", {myMaster: "c9qRlLnibDVm6xxb5GuJ5l4rLW3ReOd05h04ix63Lvr.37" });
        </script>'
    )
),
'Propeller_itthon_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal */
    ado.master({id: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_itthon_cikkoldal_leaderboard' => '<div id="adoceangemhurhkqcwlnnn"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_leaderboard */
        ado.slave("adoceangemhurhkqcwlnnn", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>',

        'Propeller_itthon_cikkoldal_halfpage_felso' => '<div id="adoceangemhuoirggtfsii"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhuoirggtfsii", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>',

        'Propeller_itthon_cikkoldal_roadblock_felso' => '<div id="adoceangemhuljinjqpgch"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhuljinjqpgch", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>',

        'Propeller_itthon_cikkoldal_roadblock_also' => '<div id="adoceangemhuyjptmnjlbc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_roadblock_also */
        ado.slave("adoceangemhuyjptmnjlbc", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>',

        'Propeller_itthon_cikkoldal_interstitial' => '<div id="adoceangemhuvkgkqkdqhx"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_interstitial */
        ado.slave("adoceangemhuvkgkqkdqhx", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>',

        'Propeller_itthon_cikkoldal_textlink' => '<div id="adoceangemhuslnqdinurs"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_textlink */
        ado.slave("adoceangemhuslnqdinurs", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>',

        'Propeller_itthon_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhupmehhvgjwr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_itthon_cikkoldal.Propeller_itthon_cikkoldal_kepes_ajanlo */
        ado.slave("adoceangemhupmehhvgjwr", {myMaster: "EoPqPGbj411rPpMMn1CqcANmEjO6Nb4WEecA8n8glM7.k7" });
        </script>'
    )
),
'Propeller_bulvar_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_bulvar_nyito */
    ado.master({id: "nUfm8XM70rq2f5u8iRt3p7kJ3vrAPC_SqJBkk8zS6sn.f7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_bulvar_nyito_leaderboard' => '<div id="adoceangemhuscqfhsesdq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_nyito.Propeller_bulvar_nyito_leaderboard */
        ado.slave("adoceangemhuscqfhsesdq", {myMaster: "nUfm8XM70rq2f5u8iRt3p7kJ3vrAPC_SqJBkk8zS6sn.f7" });
        </script>',

        'Propeller_bulvar_nyito_halfpage_felso' => '<div id="adoceangemhupdhmkpogxp"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_nyito.Propeller_bulvar_nyito_halfpage_felso */
        ado.slave("adoceangemhupdhmkpogxp", {myMaster: "nUfm8XM70rq2f5u8iRt3p7kJ3vrAPC_SqJBkk8zS6sn.f7" });
        </script>',

        'Propeller_bulvar_nyito_medium_rectangle_also' => '<div id="adoceangemhumeosnmilhk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_nyito.Propeller_bulvar_nyito_medium_rectangle_also */
        ado.slave("adoceangemhumeosnmilhk", {myMaster: "nUfm8XM70rq2f5u8iRt3p7kJ3vrAPC_SqJBkk8zS6sn.f7" });
        </script>',

        'Propeller_bulvar_nyito_interstitial' => '<div id="adoceangemhuzefjbkspcf"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_nyito.Propeller_bulvar_nyito_interstitial */
        ado.slave("adoceangemhuzefjbkspcf", {myMaster: "nUfm8XM70rq2f5u8iRt3p7kJ3vrAPC_SqJBkk8zS6sn.f7" });
        </script>'
    )
),
'Propeller_bulvar_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal */
    ado.master({id: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_bulvar_cikkoldal_leaderboard' => '<div id="adoceangemhutgdgiufjrz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_leaderboard */
        ado.slave("adoceangemhutgdgiufjrz", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>',

        'Propeller_bulvar_cikkoldal_halfpage_felso' => '<div id="adoceangemhuqhkmlrpnqu"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhuqhkmlrpnqu", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>',

        'Propeller_bulvar_cikkoldal_medium rectangle_also' => '<div id="adoceangemhunirsoojsap"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_medium rectangle_also */
        ado.slave("adoceangemhunirsoojsap", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>',

        'Propeller_bulvar_cikkoldal_roadblock_felso' => '<div id="adoceangemhukjijcmdhbo"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhukjijcmdhbo", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>',

        'Propeller_bulvar_cikkoldal_interstitial' => '<div id="adoceangemhuxjppfjnlpj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_interstitial */
        ado.slave("adoceangemhuxjppfjnlpj", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>',

        'Propeller_bulvar_cikkoldal_textlink' => '<div id="adoceangemhuukggjwgqke"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_textlink */
        ado.slave("adoceangemhuukggjwgqke", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>',

        'Propeller_bulvar_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhurlnmmtqujz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_bulvar_cikkoldal.Propeller_bulvar_cikkoldal_kepes_ajanlo */
        ado.slave("adoceangemhurlnmmtqujz", {myMaster: "yhdRLcRnHKlZYkEf1jOmKL77.sFJqjtcGYUb1KrDp2b.y7" });
        </script>'
    )
),
'Propeller_kulfold_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_kulfold_nyito */
    ado.master({id: "TC5AaOBth2jFjw1G.W69sloQY_w7gxH33Tl2CPd3moP.37", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_kulfold_nyito_leaderboard' => '<div id="adoceangemhulnljdoeout"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_nyito.Propeller_kulfold_nyito_leaderboard */
        ado.slave("adoceangemhulnljdoeout", {myMaster: "TC5AaOBth2jFjw1G.W69sloQY_w7gxH33Tl2CPd3moP.37" });
        </script>',

        'Propeller_kulfold_nyito_halfpage_felso' => '<div id="adoceangemhuyncqglosto"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_nyito.Propeller_kulfold_nyito_halfpage_felso */
        ado.slave("adoceangemhuyncqglosto", {myMaster: "TC5AaOBth2jFjw1G.W69sloQY_w7gxH33Tl2CPd3moP.37" });
        </script>',

        'Propeller_kulfold_nyito_medium rectangle_also' => '<div id="adoceangemhuvojgkiihyn"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_nyito.Propeller_kulfold_nyito_medium rectangle_also */
        ado.slave("adoceangemhuvojgkiihyn", {myMaster: "TC5AaOBth2jFjw1G.W69sloQY_w7gxH33Tl2CPd3moP.37" });
        </script>',

        'Propeller_kulfold_nyito_interstitial' => '<div id="adoceangemhuspqmnvrlmi"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_nyito.Propeller_kulfold_nyito_interstitial */
        ado.slave("adoceangemhuspqmnvrlmi", {myMaster: "TC5AaOBth2jFjw1G.W69sloQY_w7gxH33Tl2CPd3moP.37" });
        </script>'
    )
),
'Propeller_kulfold_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal */
    ado.master({id: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_kulfold_cikkoldal_leaderboard' => '<div id="adoceangemhumbpjeqffic"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_leaderboard */
        ado.slave("adoceangemhumbpjeqffic", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_halfpage_felso' => '<div id="adoceangemhuzbgqhnpjhx"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhuzbgqhnpjhx", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_medium rectangle_also' => '<div id="adoceangemhuwcnglkjocs"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_medium rectangle_also */
        ado.slave("adoceangemhuwcnglkjocs", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_roadblock_felso' => '<div id="adoceangemhutdenoxssbn"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhutdenoxssbn", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_roadblock_also' => '<div id="adoceangemhuqeltbvmhgm"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_roadblock_also */
        ado.slave("adoceangemhuqeltbvmhgm", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_interstitial' => '<div id="adoceangemhunfckfsgmmh"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_interstitial */
        ado.slave("adoceangemhunfckfsgmmh", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_textlink' => '<div id="adoceangemhukgjqipqqlc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_textlink */
        ado.slave("adoceangemhukgjqipqqlc", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>',

        'Propeller_kulfold_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhuxgqgmmkffb"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_kulfold_cikkoldal.Propeller_kulfold_cikkoldal_kepes_ajanlo */
        ado.slave("adoceangemhuxgqgmmkffb", {myMaster: "yFmqI_4bA6cVU2f8qQr6Ip3GAHBxf6ORVnWz.R4LWdL.U7" });
        </script>'
    )
),
'Propeller_technika_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_technika_nyito */
    ado.master({id: "PhwxewXqXKR3XYwqpdJfyadXbYFgGGenilhvrrEEaZ3.E7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_technika_nyito_leaderboard' => '<div id="adoceangemhuriotcxnozr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_nyito.Propeller_technika_nyito_leaderboard */
        ado.slave("adoceangemhuriotcxnozr", {myMaster: "PhwxewXqXKR3XYwqpdJfyadXbYFgGGenilhvrrEEaZ3.E7" });
        </script>',

        'Propeller_technika_nyito_halfpage_felso' => '<div id="adoceangemhuojfkguhtfm"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_nyito.Propeller_technika_nyito_halfpage_felso */
        ado.slave("adoceangemhuojfkguhtfm", {myMaster: "PhwxewXqXKR3XYwqpdJfyadXbYFgGGenilhvrrEEaZ3.E7" });
        </script>',

        'Propeller_technika_nyito_medium rectangle_also' => '<div id="adoceangemhulkmqjrrhol"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_nyito.Propeller_technika_nyito_medium rectangle_also */
        ado.slave("adoceangemhulkmqjrrhol", {myMaster: "PhwxewXqXKR3XYwqpdJfyadXbYFgGGenilhvrrEEaZ3.E7" });
        </script>',

        'Propeller_technika_nyito_interstitial' => '<div id="adoceangemhuykdhnolmjg"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_nyito.Propeller_technika_nyito_interstitial */
        ado.slave("adoceangemhuykdhnolmjg", {myMaster: "PhwxewXqXKR3XYwqpdJfyadXbYFgGGenilhvrrEEaZ3.E7" });
        </script>'
    )
),
'Propeller_technika_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal */
    ado.master({id: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_technika_cikkoldal_leaderboard' => '<div id="adoceangemhusmrtdjpfna"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_leaderboard */
        ado.slave("adoceangemhusmrtdjpfna", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_cikkoldal_halfpage_felso' => '<div id="adoceangemhupnikhwikiv"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhupnikhwikiv", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_cikkoldal_medium rectangle_also' => '<div id="adoceangemhumopqktsohq"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_medium rectangle_also */
        ado.slave("adoceangemhumopqktsohq", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_cikkoldal_roadblock_felso' => '<div id="adoceangemhuzoghoqmtcl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhuzoghoqmtcl", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_cikkoldal_roadblock_also' => '<div id="adoceangemhuwpnnbogihk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_roadblock_also */
        ado.slave("adoceangemhuwpnnbogihk", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_cikkoldal_interstitial' => '<div id="adoceangemhutafeflqmnf"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_interstitial */
        ado.slave("adoceangemhutafeflqmnf", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_cikkoldal_textlink' => '<div id="adoceangemhuqbmkiikrxa"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_cikkoldal_textlink */
        ado.slave("adoceangemhuqbmkiikrxa", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>',

        'Propeller_technika_kepes_ajanlo' => '<div id="adoceangemhuncdrlvdgrz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_technika_cikkoldal.Propeller_technika_kepes_ajanlo */
        ado.slave("adoceangemhuncdrlvdgrz", {myMaster: "l_GW0QtQkhFvxqBrQXFNUpfLnCE4oTBTH6avE4uGb7D.R7" });
        </script>'
    )
),
'Propeller_sport_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_sport_nyito */
    ado.master({id: "fKA1Pl.d3VXlf0_cByr4NLvLDVfOZqdmqJqYQOEhj9..L7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_sport_nyito_leaderboard' => '<div id="adoceangemhuxdrncqhplp"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_nyito.Propeller_sport_nyito_leaderboard */
        ado.slave("adoceangemhuxdrncqhplp", {myMaster: "fKA1Pl.d3VXlf0_cByr4NLvLDVfOZqdmqJqYQOEhj9..L7" });
        </script>',

        'Propeller_sport_nyito_halfpage_felso' => '<div id="adoceangemhuueiegnrtgk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_nyito.Propeller_sport_nyito_halfpage_felso */
        ado.slave("adoceangemhuueiegnrtgk", {myMaster: "fKA1Pl.d3VXlf0_cByr4NLvLDVfOZqdmqJqYQOEhj9..L7" });
        </script>',

        'Propeller_sport_nyito_medium rectangle_also' => '<div id="adoceangemhurfpkjkliaj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_nyito.Propeller_sport_nyito_medium rectangle_also */
        ado.slave("adoceangemhurfpkjkliaj", {myMaster: "fKA1Pl.d3VXlf0_cByr4NLvLDVfOZqdmqJqYQOEhj9..L7" });
        </script>',

        'Propeller_sport_nyito_interstitial' => '<div id="adoceangemhuoggrmxenke"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_nyito.Propeller_sport_nyito_interstitial */
        ado.slave("adoceangemhuoggrmxenke", {myMaster: "fKA1Pl.d3VXlf0_cByr4NLvLDVfOZqdmqJqYQOEhj9..L7" });
        </script>'
    )
),
'Propeller_sport_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal */
    ado.master({id: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_sport_cikkoldal_leaderboard' => '<div id="adoceangemhuyheodsigzy"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_leaderboard */
        ado.slave("adoceangemhuyheodsigzy", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_halfpage_felso' => '<div id="adoceangemhuvilehpskjt"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhuvilehpskjt", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_medium rectangle_also' => '<div id="adoceangemhusjclkmmpeo"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_medium rectangle_also */
        ado.slave("adoceangemhusjclkmmpeo", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_roadblock_felso' => '<div id="adoceangemhupkjrnjguoj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhupkjrnjguoj", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_roadblock_also' => '<div id="adoceangemhumlqhbxpiii"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_roadblock_also */
        ado.slave("adoceangemhumlqhbxpiii", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_interstitial' => '<div id="adoceangemhuzlhoeujnsd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_interstitial */
        ado.slave("adoceangemhuzlhoeujnsd", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_textlink' => '<div id="adoceangemhuwmoeirdsny"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_textlink */
        ado.slave("adoceangemhuwmoeirdsny", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>',

        'Propeller_sport_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhutnfllonghx"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_sport_cikkoldal.Propeller_sport_cikkoldal_kepes_ajanlo */
        ado.slave("adoceangemhutnfllonghx", {myMaster: "1Vn25H.oMkGfv0ApOpnNMSEZIjXnMV4ExiZjCFVPw.L.r7" });
        </script>'
    )
),
'Propeller_video_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_video_nyito */
    ado.master({id: "1ORaqgzMU7nMbNg6PLbAFVCp0BWvRMNQY1AgOgM0rZj.j7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_video_nyito_leaderboard' => '<div id="adoceangemhunpdicjrpxn"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_nyito.Propeller_video_nyito_leaderboard */
        ado.slave("adoceangemhunpdicjrpxn", {myMaster: "1ORaqgzMU7nMbNg6PLbAFVCp0BWvRMNQY1AgOgM0rZj.j7" });
        </script>',

        'Propeller_video_nyito_halfpage_felso' => '<div id="adoceangemhukalofwkuhi"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_nyito.Propeller_video_nyito_halfpage_felso */
        ado.slave("adoceangemhukalofwkuhi", {myMaster: "1ORaqgzMU7nMbNg6PLbAFVCp0BWvRMNQY1AgOgM0rZj.j7" });
        </script>',

        'Propeller_video_nyito_medium rectangle_also' => '<div id="adoceangemhuxacfjtejmh"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_nyito.Propeller_video_nyito_medium rectangle_also */
        ado.slave("adoceangemhuxacfjtejmh", {myMaster: "1ORaqgzMU7nMbNg6PLbAFVCp0BWvRMNQY1AgOgM0rZj.j7" });
        </script>',

        'Propeller_video_nyito_interstitial' => '<div id="adoceangemhuubjlmqonlc"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_nyito.Propeller_video_nyito_interstitial */
        ado.slave("adoceangemhuubjlmqonlc", {myMaster: "1ORaqgzMU7nMbNg6PLbAFVCp0BWvRMNQY1AgOgM0rZj.j7" });
        </script>',
    )
),
'Propeller_video_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal */
    ado.master({id: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_video_cikkoldal_leaderboard' => '<div id="adoceangemhuodhidlsglw"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_leaderboard */
        ado.slave("adoceangemhuodhidlsglw", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_halfpage_felso' => '<div id="adoceangemhuleoogimlvr"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhuleoogimlvr", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_medium rectangle_also' => '<div id="adoceangemhuyeffkvfqfm"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_medium rectangle_also */
        ado.slave("adoceangemhuyeffkvfqfm", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_roadblock_felso' => '<div id="adoceangemhuvfmlnspueh"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhuvfmlnspueh", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_roadblock_also' => '<div id="adoceangemhusgdsqpjjjg"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_roadblock_also */
        ado.slave("adoceangemhusgdsqpjjjg", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_interstitial' => '<div id="adoceangemhuphkiendopb"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_interstitial */
        ado.slave("adoceangemhuphkiendopb", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_textlink' => '<div id="adoceangemhumirohknsow"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_textlink */
        ado.slave("adoceangemhumirohknsow", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>',

        'Propeller_video_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhuziiflxghiv"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_video_cikkoldal.Propeller_video_cikkoldal_kepes_ajanlo */
        ado.slave("adoceangemhuziiflxghiv", {myMaster: ".q3lytaXLTNyPnpcmbzCXezpo792DJIJ.jIWfvdEGiz.j7" });
        </script>'
    )
),
'Propeller_tag_nyito' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_tag_nyito */
    ado.master({id: "FFJBgEzsvCFX9fI8ti6D3qXkcOyVKQMQyr.wEWZue1j.a7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_tag_nyito_leaderboard' => '<div id="adoceangemhutkgsbskqnl"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_nyito.Propeller_tag_nyito_leaderboard */
        ado.slave("adoceangemhutkgsbskqnl", {myMaster: "FFJBgEzsvCFX9fI8ti6D3qXkcOyVKQMQyr.wEWZue1j.a7" });
        </script>',

        'Propeller_tag_nyito_halfpage_felso' => '<div id="adoceangemhuqlnifpefsk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_nyito.Propeller_tag_nyito_halfpage_felso */
        ado.slave("adoceangemhuqlnifpefsk", {myMaster: "FFJBgEzsvCFX9fI8ti6D3qXkcOyVKQMQyr.wEWZue1j.a7" });
        </script>',

        'Propeller_tag_nyito_medium rectangle_also' => '<div id="adoceangemhunmepimojcf"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_nyito.Propeller_tag_nyito_medium rectangle_also */
        ado.slave("adoceangemhunmepimojcf", {myMaster: "FFJBgEzsvCFX9fI8ti6D3qXkcOyVKQMQyr.wEWZue1j.a7" });
        </script>',

        'Propeller_tag_nyito_interstitial' => '<div id="adoceangemhuknlfmjioxa"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_nyito.Propeller_tag_nyito_interstitial */
        ado.slave("adoceangemhuknlfmjioxa", {myMaster: "FFJBgEzsvCFX9fI8ti6D3qXkcOyVKQMQyr.wEWZue1j.a7" });
        </script>'
    )
),
'Propeller_tag_cikkoldal' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal */
    ado.master({id: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_tag_cikkoldal_leaderboard' => '<div id="adoceangemhuuojsculhqu"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_leaderboard */
        ado.slave("adoceangemhuuojsculhqu", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_halfpage_felso' => '<div id="adoceangemhurpqigrfmlp"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_halfpage_felso */
        ado.slave("adoceangemhurpqigrfmlp", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_medium rectangle_also' => '<div id="adoceangemhuoaipjopqgk"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_medium rectangle_also */
        ado.slave("adoceangemhuoaipjopqgk", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_roadblock_felso' => '<div id="adoceangemhulbpfnljflj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_roadblock_felso */
        ado.slave("adoceangemhulbpfnljflj", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_roadblock_also' => '<div id="adoceangemhuybgmqidkve"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_roadblock_also */
        ado.slave("adoceangemhuybgmqidkve", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_interstitial' => '<div id="adoceangemhuvcnsdwmouz"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_interstitial */
        ado.slave("adoceangemhuvcnsdwmouz", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_textlink' => '<div id="adoceangemhusdejhtgtau"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_textlink */
        ado.slave("adoceangemhusdejhtgtau", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>',

        'Propeller_tag_cikkoldal_kepes_ajanlo' => '<div id="adoceangemhupelpkqqhjt"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_tag_cikkoldal.Propeller_tag_cikkoldal_kepes_ajanlo */
        ado.slave("adoceangemhupelpkqqhjt", {myMaster: "AY1Bdi_UvJX.YDDtxn72XACJnlh.BXsUiT0YAmgkG0..p7" });
        </script>'
    )
),
'Propeller_mobil' => array(
    'master' => '<script type="text/javascript">
    /* (c)AdOcean 2003-2016, MASTER: IkoDigital_hu.propeller.hu.Propeller_mobil */
    ado.master({id: "d9PhYIuprJA6bcKThBpNKIhr4o5o6d3Zwg1tfu2eZaf.G7", server: "gemhu.adocean.pl" });
    </script>',
    'slave' => array(

        'Propeller_mobil_3d boxleaderboard' => '<div id="adoceangemhuzfjmblerzj"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_mobil.Propeller_mobil_3d boxleaderboard */
        ado.slave("adoceangemhuzfjmblerzj", {myMaster: "d9PhYIuprJA6bcKThBpNKIhr4o5o6d3Zwg1tfu2eZaf.G7" });
        </script>',

        'Propeller_mobil_cikkozi' => '<div id="adoceangemhuwgqseiofii"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_mobil.Propeller_mobil_cikkozi */
        ado.slave("adoceangemhuwgqseiofii", {myMaster: "d9PhYIuprJA6bcKThBpNKIhr4o5o6d3Zwg1tfu2eZaf.G7" });
        </script>',

        'Propeller_mobil_also_sticky' => '<div id="adoceangemhuthhjivhkdd"></div>
        <script type="text/javascript">
        /* (c)AdOcean 2003-2016, IkoDigital_hu.propeller.hu.Propeller_mobil.Propeller_mobil_also_sticky */
        ado.slave("adoceangemhuthhjivhkdd", {myMaster: "d9PhYIuprJA6bcKThBpNKIhr4o5o6d3Zwg1tfu2eZaf.G7" });
        </script>'
    )
)
);
