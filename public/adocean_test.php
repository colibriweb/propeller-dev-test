<?php

$scripts = include_once('adocean.inc.php');

/**
 * Összes zóna listázása.
 */
if (! isset($_GET['zone'])) {
    echo '<pre>';
    foreach ($scripts as $name => $script) {

        echo $name . "\n";
        foreach ($script['slaves'] as $zone => $html) {
            echo "    " . '<a href="?zone=' . urlencode($zone). '">' . $zone . "</a>\n";
        }

    }
    echo '</pre>';
}


if (isset($_GET['zone'])) {
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>AdOcean test</title>

    <!-- head script -->
    <script type="text/javascript" src="//gemhu.adocean.pl/files/js/ado.js"></script>
    <script type="text/javascript">
    /* (c)AdOcean 2003-2016 */
        if(typeof ado!=="object"){ado={};ado.config=ado.preview=ado.placement=ado.master=ado.slave=function(){};}
        ado.config({mode: "old", xml: false, characterEncoding: true});
        ado.preview({enabled: true, emiter: "gemhu.adocean.pl", id: "uESaTC3oo4RWFYK0Pi7N40dYf6fwGW9odHhhEhar3_7.p7"});
    </script>
    <!-- / head script -->

</head>

<body>

<?php

foreach ($scripts as $name => $script) {
    foreach ($script['slaves'] as $zone => $html) {
        if ($zone == urldecode($_GET['zone'])) {
            echo '<h2>' . $name . ' > ' . $zone . '</h2>' . "\n\n";

                echo "<!-- rovat master -->\n" . $script['master'] . "\n<!-- / rovat master -->\n\n";

            echo '<div style="background:#ddd;padding:1em;">' . "\n\n";

                echo "<!-- zóna slave -->\n" . $html . "\n<!-- / zóna slave -->\n\n";

            echo '</div>' . "\n\n";
            break 2;

        }
    }
}

?>

</body>
</html>
<?php
}
