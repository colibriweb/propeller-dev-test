<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

require(ROOT.'/system/Pagination.class.php');

// http://www.propeller100105.dev/kereses?q=Pilvax&n=1&tmp=1


    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));

// dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// toplista
$smarty->assign('top', $content->q_top(0));

// bal és középső hasábok, fejléc kiemelések
$smarty->assign('blocks', $content->q_blocks());


if(mb_strlen(trim(@$_GET['q']), 'utf-8') > 2) {
	$smarty->assign('search_title', $_GET['q']);
/*
	$limit = (int)$env->c['content']['perpage'];
	$current = (int)$_GET['page'];

    if ($current > 100) $env->setHeader(404);

	if(($_GET['page'] * $limit) == 0) { $from = 0; }
	else { $from = $current * $limit - $limit; }*/

	$p = new Pagination((int)@$_GET['page'], 30);


	$query = '';
	$words = str_replace(array('%','?','_'), ' ', $_GET['q']);

	$words = explode(" ", str_replace('-', ' ', $words));
	$words = array_slice($words, 0, 4);

if(isset($_GET['n']) && $_GET['n'] == '1') { // szótöredékre is keres
	while(list($k, $v) = each($words)){
	    $v = $db->escape(trim($v));

		//if(isset($_GET['n']) && $_GET['n'] == '1') $v = ' '.$v.' ';

		if(!isset($j)) {
			$query .= "(c.title COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR c.description COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR c.link COLLATE utf8_hungarian_ci LIKE '%".$v."%'
			OR
			c.title COLLATE utf8_hungarian_ci LIKE '".$v."%' OR c.description COLLATE utf8_hungarian_ci LIKE '".$v."%'
			OR
			c.title COLLATE utf8_hungarian_ci LIKE '%".$v."' OR c.description COLLATE utf8_hungarian_ci LIKE '%".$v."'
			)";
			$j = 1;
		}
		else {
			$query .= " AND (c.title COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR c.description COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR c.link COLLATE utf8_hungarian_ci LIKE '%".$v."%'
			OR
			c.title COLLATE utf8_hungarian_ci LIKE '".$v."%' OR c.description COLLATE utf8_hungarian_ci LIKE '".$v."%'
			OR
			c.title COLLATE utf8_hungarian_ci LIKE '%".$v."' OR c.description COLLATE utf8_hungarian_ci LIKE '%".$v."')";
		}
	}
}
else { // alapértelmezett keresés (teljes szóegyezés)

	while(list($k, $v) = each($words)){
	    $v = $db->escape(trim($v));

		if(!isset($j)) {
			$query .= "(
			c.title COLLATE utf8_hungarian_ci LIKE '% ".$v." %' OR
			c.title COLLATE utf8_hungarian_ci LIKE '".$v." %' OR
			c.title COLLATE utf8_hungarian_ci LIKE '% ".$v."' OR
			c.description COLLATE utf8_hungarian_ci LIKE '% ".$v." %' OR
			c.description COLLATE utf8_hungarian_ci LIKE '".$v." %' OR
			c.description COLLATE utf8_hungarian_ci LIKE '% ".$v."' OR
			c.link COLLATE utf8_hungarian_ci LIKE '%".$v."%'
			)";
			$j = 1;
		}
		else {
			$query .= " AND (
			c.title COLLATE utf8_hungarian_ci LIKE '% ".$v." %' OR
			c.title COLLATE utf8_hungarian_ci LIKE '".$v." %' OR
			c.title COLLATE utf8_hungarian_ci LIKE '% ".$v."' OR
			c.description COLLATE utf8_hungarian_ci LIKE '% ".$v." %' OR
			c.description COLLATE utf8_hungarian_ci LIKE '".$v." %' OR
			c.description COLLATE utf8_hungarian_ci LIKE '% ".$v."' OR
			c.link COLLATE utf8_hungarian_ci LIKE '%".$v."%'
			)";
		}
	}

}

/*
	$table = ($current < 2) ? _DBPREF."content_temp c LEFT JOIN "._DBPREF."content cc ON c.id=cc.id" : _DBPREF."content c";
	if($tmp) $table = _DBPREF."content_temp";*/

// csak a temp -ben keresünk
	$table = _DBPREF."content_temp c LEFT JOIN "._DBPREF."content cc ON c.id=cc.id";

	$select = "c.id, c.category_id, c.title, c.link,
	@pos:=LOCATE('".$db->escape($_GET['q'])."',c.description COLLATE utf8_hungarian_ci)-18,
	@lng:=length(c.description COLLATE utf8_hungarian_ci),
	@pont1:=if(@pos<1,'','...'),
	@pont2:=if((@pos+190)>@lng,'','...'),
	concat(@pont1,SUBSTRING(c.description COLLATE utf8_hungarian_ci, if(@pos<1,1,@pos),if((@pos+210)>@lng,@lng-@pos+1,190)),@pont2) description, CONCAT(c.id,'-',c.alias) AS alias, c.datetime, comments";

	$res_s = $db->Query("SELECT SQL_CALC_FOUND_ROWS ".$select." FROM ".$table." WHERE ".$query." AND c.datetime < NOW() AND (c.content_type IS NULL OR c.content_type <> 7) ORDER BY c.datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);
	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);
	$p->results_num = $rows[0];
	$smarty->assign('p', $p->getPaging());

/*
	if($rows[0] < $limit && $current < 2 && (!$tmp)) { // nincs elég találat, és a temp táblából kerestünk, és nem trend keresés történik
		$res_s = $db->Query("SELECT SQL_CALC_FOUND_ROWS ".$select.", comments FROM "._DBPREF."content c WHERE c.id > ".(_SPD_CONTENT_ID-100000)." AND ".$query." ORDER BY c.datetime DESC LIMIT ".$from.", ".$limit);
		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);

	}
	else {
		$smarty->assign('tmp', true);
	}*/

$list = array();
	while($row_s = $db->fetchArray($res_s)) {

		$row_s['orighost'] = $env->getHost($row_s['link']);
		$row_s['category_alias'] = $env->l['content']['category_alias_'.$row_s['category_id']];
		$row_s['date'] = $env->dateToString($row_s['datetime']);
		$row_s['description'] = highlight(strip_tags($row_s['description']), $_GET['q']);
		$list[] = $row_s;
	}

	//if(!$list) $env->setHeader(404);
	$smarty->assign('list', $list);


	/** Rendszerosztály: lapozásokhoz szükséges oldalszámok kiszámítása */
/*	require_once('../../system/Page.class.php');

	$rows[0] = ($rows[0] > $limit*100) ? $limit*100 : $limit*100;

	$page = new Page($rows[0], $limit, $db->escape($_GET['page']));
	$smarty->assign('page', $page);*/

}
else {
	$smarty->assign('search_title', $env->l['content']['search']);
}


function highlight($text, $keyword) {
	return preg_replace('/'.preg_quote($keyword,'/').'([-a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ]+)?/i', "<b>\\0</b>", $text);
	//return preg_replace('/'.preg_quote($keyword,'/').'[^\s\.\,\"\:\!\?]+/i', "<b>\\0</b>", $text);
}


$smarty->assign('activities', $env->getActivities());
$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('content/search.tpl');

?>
