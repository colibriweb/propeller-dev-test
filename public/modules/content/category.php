<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

require(ROOT.'/system/Pagination.class.php');


$category = array(
'cimlap' => 0,
'tag' => 0,
'szerzo' => 0,

'itthon' => 1,
'nagyvilag' => 2,
'technika' => 4,
'szorakozas' => 6,
'sport' => 7
);

if($env->u[1] == 'tag') {
	$row_cat = array('alias' => $env->u[1], null, 'id' => 0);
}
elseif($env->u[1] == 'szerzo') {
	$row_cat = array('alias' => $env->u[1], null, 'id' => 0);
}
else {
	$row_cat = array('alias' => $env->u[1], 'title' => $env->l['content']['category_'.$category[$env->u[1]]], 'id' => $category[$env->u[1]]);
}

$smarty->assign('category', $row_cat);
$smarty->assign('content', array('category_id' => $category[$env->u[1]]));



// dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// toplista
$smarty->assign('top', $content->q_top($row_cat['id']));

// bal és középső hasábok, fejléc kiemelések
$smarty->assign('blocks', $content->q_blocks());





if(empty($env->u[2])) { // rovat címlap

	$smarty->assign('tags', $content->q_tagcloud($row_cat['id']));

	// most beszédtéma
	$smarty->assign('commented', $content->q_commented($row_cat['id']));

	// kategória kiemelések
	$smarty->assign('blocks_cat', $content->q_blocks_category($row_cat['id']));

	// banner MASTER kód
	$smarty->assign('ad', array('category' => $env->u[1], 'page' => 'nyito'));

	if($row_cat['id'] == 1) { // itthon
		$l_query = "
		#ROVATCIMLAP_ITTHON
		# nol
		#(SELECT 'NOL.hu' AS source, 'nol.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (183) ORDER BY datetime DESC LIMIT 8)

		# atv
		(SELECT 'ATV' AS source, 'atv.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (341) AND category_id = 1 ORDER BY datetime DESC LIMIT 8)

		# index
		UNION (SELECT 'Index - Belföld' AS source, 'index.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (63,59,299) ORDER BY datetime DESC LIMIT 8)

		#hvg
		UNION (SELECT 'HVG' AS source, 'hvg.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (16,302) ORDER BY datetime DESC LIMIT 8)

		# origo
		# UNION (SELECT 'Origo' AS source, 'origo.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (21,300) ORDER BY datetime DESC LIMIT 8)
		";
	}
	elseif($row_cat['id'] == 2) { // nagyvilag
		$l_query = "
		#ROVATCIMLAP_NAGYVILAG

		# nol
		#(SELECT 'NOL.hu' AS source, 'nol.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (184) ORDER BY datetime DESC LIMIT 8)

		# index
		(SELECT 'Index - Külföld' AS source, 'index.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (64) ORDER BY datetime DESC LIMIT 8)

		# origo
		UNION (SELECT 'Origo' AS source, 'origo.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 22 ORDER BY datetime DESC LIMIT 8)

		";
	}
	elseif($row_cat['id'] == 6) { // szórakozás
		$l_query = "
		#ROVATCIMLAP_SZORAKOZAS
		(SELECT 'Blikk' AS source, 'blikk.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (293,181,222,220) ORDER BY datetime DESC LIMIT 8)
		UNION
		(SELECT 'Lemon' AS source, 'lemon.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 350 ORDER BY datetime DESC LIMIT 8)
		UNION
		(SELECT 'Velvet' AS source, 'velvet.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (45,297) ORDER BY datetime DESC LIMIT 8)
		UNION
		(SELECT 'Bors' AS source, 'borsonline.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 180 ORDER BY datetime DESC LIMIT 8)
		";
	}
	elseif($row_cat['id'] == 4) { // technika
		$l_query = "
		#ROVATCIMLAP_TECHNIKA
		(SELECT 'FinTechRadar' AS source, 'fintechradar.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 354 ORDER BY datetime DESC LIMIT 8)
		UNION
		(SELECT 'SG.hu' AS source, 'sg.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 57 ORDER BY datetime DESC LIMIT 8)
		UNION
		(SELECT 'HWSW' AS source, 'hwsw.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 190 ORDER BY datetime DESC LIMIT 8)
		UNION
		(SELECT 'Index - Tech' AS source, 'index.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (13,322,323,324) ORDER BY datetime DESC LIMIT 8)
		";
	}
	elseif($row_cat['id'] == 7) { // sport
		$l_query = "
		#ROVATCIMLAP_SPORT

		# nso
		(SELECT 'Nemzeti Sport' AS source, 'nemzetisport.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (351,295) ORDER BY datetime DESC LIMIT 8)

		# nol
		#(SELECT 'NOL.hu' AS source, 'nol.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (187) ORDER BY datetime DESC LIMIT 8)

		# index
		UNION (SELECT 'Index - Sport' AS source, 'index.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 107 ORDER BY datetime DESC LIMIT 8)

		# f1világ
		UNION (SELECT 'F1világ' AS source, 'f1vilag.hu' AS domain, id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id = 219 ORDER BY datetime DESC LIMIT 8)
		";
	}

	$res_cached_feeds = $db->QueryCache($l_query, "feeds".$env->u[1].".cache", 500);

	foreach($res_cached_feeds as $key => $value) {
		$res_cached_feeds[$key]['category_alias'] = $env->l['content']['category_alias_'.$value['category_id']];
		$res_cached_feeds[$key]['date'] = date('H:i', strtotime($value['datetime']));
	}
	//p($res_cached_feeds);die;
	$smarty->assign('feeds', $res_cached_feeds);


}
elseif($env->u[2] == $env->l['content']['url_latest']) { // összes friss

	// banner MASTER kód
	$smarty->assign('ad', array('category' => $env->u[1], 'page' => 'nyito'));


	$p = new Pagination((int)@$_GET['page'], 30);

	$res_s = $db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, t.link, t.description, CONCAT(t.id,'-',t.alias) AS alias, t.datetime, c.comments FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id WHERE t.category_id = ".$row_cat['id']." AND t.datetime < NOW() AND (t.content_type IS NULL OR t.content_type <> 7)
	ORDER BY t.datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);

	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);
	$p->results_num = $rows[0];
	$smarty->assign('p', $p->getPaging());

	while($row_s = $db->fetchArray($res_s)) {
		$row_s['orighost'] = $env->getHost($row_s['link']);
		$row_s['category_alias'] = $row_cat['alias'];
		$row_s['date'] = $env->dateToString($row_s['datetime']);
		$row_s['description'] = $env->getExcerpt($row_s['description']);
		$list[] = $row_s;
	}

	if(!$list) $env->setHeader(404);
	$smarty->assign('list', $list);

	$smarty->assign('titleplus', ' - 24 óra friss hírei');

}
elseif($env->u[1] == 'tag') { // új címke oldal ("/tag/krasznai+tünde")

	// banner MASTER kód
	$smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));


	$env->u[2] = strtr($env->u[2], "+", " ");

// címke oldalak összevonása
	$redir = array(
		'a szerk' => 'a+szerk.',
		'szíjjártó' => 'szijjártó+péter',
		'szijjártó' => 'szijjártó+péter',
		'szíjjártó péter' => 'szijjártó+péter',
		'szijjárto péter' => 'szijjártó+péter',
		'orban' => 'orbán+viktor',
		'orbán' => 'orbán+viktor',
		'orban viktor' => 'orbán+viktor',
		'' => '',
		'' => ''
	);

	if(isset($redir[$env->u[2]])) {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$env->base.'/tag/'.$redir[$env->u[2]]);
		exit;
	}


	$p = new Pagination((int)@$_GET['page'], 30);
/*
	$res_s = $db->Query("#CIMKE: ".$db->escape($env->u[2])."
	SELECT SQL_CALC_FOUND_ROWS c.id, category_id, title, link, description, CONCAT(c.id,'-',alias) AS alias, datetime, comments FROM "._DBPREF."content c
	LEFT JOIN "._DBPREF."content_tags_conn conn ON c.id = conn.content_id
	LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE tag COLLATE utf8_general_ci = '".$db->escape($env->u[2])."' ORDER BY datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);
// kivettem, mert eltűnnek így a régi témák:
// c.id > ".(_SPD_CONTENT_ID-700000)." AND
*/





	$res_tid = $db->Query("#CIMKE_TID: ".$db->escape($env->u[2])."
	SELECT id FROM "._DBPREF."content_tags WHERE tag = '".mb_strtolower(mysql_real_escape_string($env->u[2]), 'utf-8')."' LIMIT 1");
	$row_tid = $db->fetchArray($res_tid);

/*
	$res_s = $db->Query("#CIMKE: ".$db->escape($env->u[2])."
	SELECT SQL_CALC_FOUND_ROWS c.id, category_id, title, link, description, CONCAT(c.id,'-',alias) AS alias, datetime, comments FROM "._DBPREF."content c
	LEFT JOIN "._DBPREF."content_tags_conn conn ON c.id = conn.content_id
	WHERE conn.tag_id = ".(int)$row_tid['id']." ORDER BY datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);
*/


	$res_s = $db->Query("#CIMKE: ".$db->escape($env->u[2])."
	SELECT SQL_CALC_FOUND_ROWS c.id, category_id, title, link, description, CONCAT(c.id,'-',alias) AS alias, datetime, comments FROM "._DBPREF."content_tags_conn conn
	INNER JOIN "._DBPREF."content c ON (c.id = conn.content_id)
	WHERE conn.tag_id = '".$row_tid['id']."' AND c.datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC
	LIMIT ".$p->results_from.", ".$p->results_limit);




	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);
	$p->results_num = $rows[0];
	$smarty->assign('p', $p->getPaging());

	if(!$p->results_num) $env->setHeader(404);

	while($row_s = $db->fetchAssoc($res_s)) {
		$row_s['orighost'] = $env->getHost($row_s['link']);
		$row_s['category_alias'] = $env->l['content']['category_alias_'.$row_s['category_id']];
		$row_s['date'] = $env->dateToString($row_s['datetime']);
		$row_s['description'] = $env->getExcerpt($row_s['description']);

		$res_t = $db->Query("SELECT tag FROM "._DBPREF."content_tags_conn conn
		INNER JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
		WHERE conn.content_id = ".$row_s['id']." LIMIT 5");
		while($row_t = $db->fetchAssoc($res_t)) {
			$row_s['tags'][] = $row_t['tag'];
		}

		$list[] = $row_s;
	}

	$smarty->assign('list', $list);



	$smarty->assign('tagtitle', $env->u[2]);
	$smarty->assign('titleplus', $env->u[2]);

}
elseif($env->u[1] == 'szerzo') { // szerzői oldal ("/szerzo/Kovacs+Bela")
	// banner MASTER kód
	$smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));


	$env->u[2] = strtr($env->u[2], "+", " ");



	$p = new Pagination((int)@$_GET['page'], 30);

	$res_a = $db->Query("#AUTHOR_AID: ".$db->escape($env->u[2])."
	SELECT * FROM "._DBPREF."authors WHERE alias = '".mb_strtolower(mysql_real_escape_string($env->u[2]), 'utf-8')."' LIMIT 1");
	$row_a = $db->fetchArray($res_a);


	$res_s = $db->Query("#AUTHOR: ".$db->escape($env->u[2])."
	SELECT SQL_CALC_FOUND_ROWS c.id, category_id, title, link, description, CONCAT(c.id,'-',alias) AS alias, datetime, comments FROM "._DBPREF."content_authors_conn conn
	INNER JOIN "._DBPREF."content c ON (c.id = conn.content_id)
	WHERE conn.author_id = '".$row_a['id']."' AND c.datetime < NOW() ORDER BY datetime DESC
	LIMIT ".$p->results_from.", ".$p->results_limit);


	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);
	$p->results_num = $rows[0];
	$smarty->assign('p', $p->getPaging());

	if(!$p->results_num) $env->setHeader(404);

	while($row_s = $db->fetchAssoc($res_s)) {
		$row_s['orighost'] = $env->getHost($row_s['link']);
		$row_s['category_alias'] = $env->l['content']['category_alias_'.$row_s['category_id']];
		$row_s['date'] = $env->dateToString($row_s['datetime']);
		$row_s['description'] = $env->getExcerpt($row_s['description']);

		$res_t = $db->Query("SELECT tag FROM "._DBPREF."content_tags_conn conn
		INNER JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
		WHERE conn.content_id = ".$row_s['id']." LIMIT 5");
		while($row_t = $db->fetchAssoc($res_t)) {
			$row_s['tags'][] = $row_t['tag'];
		}

		$list[] = $row_s;
	}

	$smarty->assign('list', $list);

	$smarty->assign('author', $row_a);

	$smarty->assign('tagtitle', $row_a['name'].' cikkei');
	$smarty->assign('titleplus', $row_a['name'].' cikkei');

}
else { // régi címke oldal

	$ur = htmlspecialchars(urlencode($env->u[2]), ENT_QUOTES, 'UTF-8');

	header('HTTP/1.1 301 Moved Permanently');
	header('Location: '.$env->base.'/tag/'.str_replace(' ', '+', $ur));
	exit;

/*

	$p = new Pagination((int)@$_GET['page'], 30);

	$res_s = $db->Query("#CIMKEOLDAL:".$db->escape($env->u[2])."
	SELECT SQL_CALC_FOUND_ROWS c.id, category_id, title, link, description, CONCAT(c.id,'-',alias) AS alias, datetime, comments FROM "._DBPREF."content c
	LEFT JOIN "._DBPREF."content_tags_conn conn ON c.id = conn.content_id
	LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE c.id > ".(_SPD_CONTENT_ID-500000)." AND category_id = ".$row_cat['id']." AND tag COLLATE utf8_general_ci = '".$db->escape($env->u[2])."' ORDER BY datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);
// kivettem, mert eltűnnek így a régi témák:
// c.id > ".(_SPD_CONTENT_ID-500000)." AND

	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);
	$p->results_num = $rows[0];
	$smarty->assign('p', $p->getPaging());

	if(!$p->results_num) $env->setHeader(404);

	while($row_s = $db->fetchAssoc($res_s)) {
		$row_s['orighost'] = $env->getHost($row_s['link']);
		$row_s['category_alias'] = $row_cat['alias'];
		$row_s['date'] = $env->dateToString($row_s['datetime']);
		$row_s['description'] = $row_s['description'];

		$res_t = $db->Query("SELECT tag FROM "._DBPREF."content_tags_conn conn
		LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
		WHERE conn.content_id = ".$row_s['id']." LIMIT 5");
		while($row_t = $db->fetchAssoc($res_t)) {
			$row_s['tags'][] = $row_t['tag'];
		}

		$list[] = $row_s;
	}

	$smarty->assign('list', $list);


	$smarty->assign('tagtitle', $env->u[2]);
	$smarty->assign('titleplus', ' - '.$env->u[2]);
*/
}


$smarty->assign('activities', $env->getActivities());
$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('content/category.tpl');

?>
