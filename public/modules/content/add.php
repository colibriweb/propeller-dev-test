<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

$auth->isAuthRequired(true);

if(isset($_POST['action']) && $_POST['action'] == 'add') { // hozzáadás
	if(!$content->doContent($_SESSION['user']['id'], $_POST['content'], 'add')) { // van hibaüzenet
		$smarty->assign('form', $content->getContentFormValues());
		$smarty->assign('error', $content->error);
	}
	else { // sikerült
		$res = $db->Query("SELECT category_id, CONCAT(id,'-',alias) AS alias, content_type FROM "._DBPREF."content WHERE id = '".$content->contentID."' LIMIT 1");
		$row = $db->fetchArray($res);
		$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];

		/*if((bool)$env->c['content']['trackback']) { // kell trackbackelni is
			$trackbackParams = array(
			'title' => $_POST['content']['title'],
			'url' => $env->base.'/'.$row['category_alias'].'/'.$row['alias'],
			'excerpt' => $env->getSafeText($_POST['content']['description'], 'UTF-8', true),
			'blog_name' => $env->l['title']
			);

			$ec = $content->sendTrackback($_POST['content']['link'], $trackbackParams);
		}*/

	$env->setOverlayDialog('<div style="text-align:left;"><b>Kedves Hír Beküldő!</b><br><br>
A Propeller pörög tovább, de a cikk feltöltése után eltelhet egy kis idő, amíg megjelenik.<br><br>
Az utóbbi évben sok hamis, fake news hírt töltöttek fel egyes regisztrált Propeller.hu tagok, illetve egy-egy témában, akár két-három példányban is felkerültek ugyanazok a cikkek az oldalra. Ezért egy új ellenőrző modult indítottunk el, ami némi idő után élesíti a feltöltők cikkeit.<br><br>
Megértésüket köszönjük,<br>
Propeller.hu csapat</div>');

		if ($row['content_type'] == '7') { // pending beküldés, címlapra irányítjuk
			//$env->setOverlayNotification('Köszönjük, a beküldött hírt hamarosan ellenőrzi egy moderátor.');
			header('Location: /');
			exit;
		} else {
			//$env->setOverlayNotification('A beküldött hír megjelent.');
			header('Location: /'.$row['category_alias'].'/'.$row['alias']);
			exit;
		}
	}
}




if($_SESSION['user']['status'] == 1)
$smarty->assign('error', 'error_status1add');

// dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// bal és középső hasábok, fejléc kiemelések
$smarty->assign('blocks', $content->q_blocks());

$smarty->assign('action', 'add');


    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));

$res_cat = $db->Query("SELECT id, category FROM "._DBPREF."content_categories ORDER BY ordinal");
while($row_cat = $db->fetchArray($res_cat)) {
	$categories[] = $row_cat;
}
$smarty->assign('categories', $categories);


$res_u = $db->Query("SELECT count(id) AS num FROM "._DBPREF."content_temp WHERE user_id = '".$_SESSION['user']['id']."' AND datetime >= '".date('Y-m-d H:00:00')."'");
$row_u = $db->fetchArray($res_u);
$res_c = $db->Query("SELECT content_num FROM "._DBPREF."users WHERE id = '".$_SESSION['user']['id']."' LIMIT 1");
$row_c = $db->fetchArray($res_c);

$smarty->assign('content_num', ($row_c['content_num']-$row_u['num']));


$smarty->assign('activities', $env->getActivities());
$smarty->display('content/add.tpl');

?>
