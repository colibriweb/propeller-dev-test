<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

/* ***************************************************************************** */

switch($env->u[1]) {
case 'check': // címlapi most beszedtema és júzer aktivitsok frissítése
	if(!$env->isAjax()) { die; }

	header('Content-type: application/json');

	$arr = array();
	if(!empty($_POST['commented_since'])) {
		require('Content.class.php');
		$content = new Content();
		$content->env = $env;

		$rows = $content->q_commented();

		if($rows[0]['datetime'] != $_POST['commented_since'])
		$arr['comments'] = $rows;
	}

	if(isset($_SESSION['user'])) {

		$arr['activities'] = $env->getActivities();

		/*
		0-19s -ig csak a páros user_id -jűeknek fut le,
		30-39s -ig a páratlan user_id -jűeknek
		40-59 -ig senkinek
		*/
		if( floor(date('s')/20) == ($_SESSION['user']['id'] % 2) ) {
			$db->Query("UPDATE "._DBPREF."users SET modtime = NOW() WHERE id = '".$_SESSION['user']['id']."' LIMIT 1");
		}

	}

	echo json_encode($arr);
	die;

break;
/* ***************************************************************************** */
case 'kovetkezo-hir': // következő hír random

	$next_post = array();
	if(isset($_COOKIE['next_post'])) {
		$next_post = explode('|', $_COOKIE['next_post']);
	}

	$res_cached = (array)$env->db->QueryCache("SELECT t.id, t.category_id, CONCAT(t.id,'-',t.alias) AS alias, t.datetime, c.counter,
	(c.counter-1) / POW(((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(t.datetime))/3600) / 2, 2.6) as score
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id

	WHERE t.user_id = 1758 AND c.counter > 20
	AND t.datetime > '".date('Y-m-d H:i:s', strtotime('-24 hours'))."'
	AND t.datetime < NOW()
	ORDER BY score DESC LIMIT 0, 40", "next_posts", 300);
	//p($res_cached);
	foreach($res_cached as $key => $value) {
		if(! in_array($value['id'], $next_post)) {
			header('Location: /'.$env->l['content']['category_alias_'.$value['category_id']].'/'.$value['alias'], true, 302);
			exit;
		}
	}

	$env->setOverlayNotification('Gratulálunk, minden fontos hírt elolvasott!', true, 302);
	header('Location: /');
	exit;

break;
/* ***************************************************************************** */
case 'captcha_comment': // ellenőrző captcha a hozzászólásoknál

	define('CAPTCHA_SESSION_ID', 'captcha_comment');
	require('../../system/PhpCaptcha.class.php');

break;

/* ***************************************************************************** */
case 'ajax_get_feedtitle': // cím kikeresése az RSS feedből
	if(!$env->isAjax()) { die; }

	if(!preg_match("/^(http|https):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i", $_POST['value'])) {
		$env->setHeader(403);
		die;
	}

	error_reporting(0);
	set_time_limit(5);

	$bc = $env->getFileContent($_POST['value']);

	preg_match("'".$_POST['attr']."=(.*?)[\'\"]'si", $bc, $encoding); // encoding|charset

	if(!preg_match("/<title>([^<]*?)<\/title>?/i", $bc, $title)) {
		$env->setHeader(403);
		die;
	}

	$title[1] = strip_tags(preg_replace('/[\s\r\n\t]+/', ' ', $title[1]));
	$title[1] = iconv($encoding[1], 'UTF-8', $title[1]);
	echo (strlen($title[1]) > 4) ? trim(html_entity_decode($title[1])) : '';

break;
/* ***************************************************************************** */
case 'suggest': // e-mail címes ajánlás
	if(!$env->isAjax()) { die; }

	if(!preg_match("/^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$/i", $_GET['to'])) {
		$env->setHeader(403);
		echo 'Nem megfelelő e-mail cím formátum, ellenőrizd';
		die;
	}

	$res = $db->Query("SELECT category_id, title, CONCAT(id,'-',alias) AS alias FROM "._DBPREF."content WHERE id = '".(int)$_GET['content_id']."' LIMIT 1");
	if(!$db->numRows($res)) { // nincs ilyen tartalom
		$env->setHeader(403);
		die;
	}
	$row = $db->fetchArray($res);
	$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];

	/** Rendszerosztály: megjelenítés */
	require('../../system/libraries/smarty/Smarty.class.php');
	$smarty = new Smarty();
	$smarty->template_dir = '../../templates/'.$env->template;
	$smarty->compile_dir = '../../system/cache/smarty_templates_c_'.$env->template;
	$smarty->cache_dir = '../../system/cache/smarty_cache_'.$env->template;


	/** Rendszerosztály: e-mail küldés */
	require('../../system/libraries/phpmailer/class.phpmailer.php');
	$phpMailer = new PHPMailer();

	// levélküldő osztály feltöltése és levélküldés
	$phpMailer->CharSet = 'UTF-8';
	$phpMailer->From = $env->l['mail']['from'];
	$phpMailer->FromName = $env->l['mail']['fromname'];
	$phpMailer->Subject = $row['title'];
	$phpMailer->AddAddress($_GET['to']);
	$phpMailer->IsHTML(true);

	// mi is kapjunk róla
	//$phpMailer->AddBCC('info@propeller.hu');

	$name_maybe = explode('@', $_GET['to']);

	$smarty->assign('subject', $env->l['content']['mail_suggestsubject']);
	$smarty->assign('name', $name_maybe[0]);
	$smarty->assign('body', htmlspecialchars($_GET['from'], ENT_QUOTES).' nevű felhasználónk egy témát szeretne a figyelmedbe ajánlani, amely úgy gondolja, érdekelhet:<br /><br />
	<a href="'.$env->base.'/'.$row['category_alias'].'/'.$row['alias'].'?utm_source=suggest&utm_medium=mail&utm_campaign=nrp" target="_blank">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a>');

	$phpMailer->Body = $smarty->fetch('mail.tpl');
	$phpMailer->Send();


	$u = ($_SESSION['user']) ? "'".$_SESSION['user']['id']."'" : "NULL";
	$res = $db->Query("INSERT INTO "._DBPREF."content_suggestion (content_id, user_id, datetime, ip, mail) VALUES ('".(int)$_GET['content_id']."', ".$u.", NOW(), '".$env->getIpAddress()."', '".$db->escape($_GET['to'])."')");
	echo 'Rendben, elküldtük az ajánlót a megadott e-mail címre.';


break;

/* ***************************************************************************** */
case 'get_title': // cím kikeresése az oldal URL-jéből
	if(!$env->isAjax()) { die; }

	$url = trim($_GET['title']);
	header('Content-type: text/json');
	//header('Content-type: application/json');

	if(!preg_match("/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i", $url)) {
		echo '{'."\n";
		echo '"title": "", ';
		echo '"nolink": 1';
		echo "\n".'}';
		die;
	}

	require('../../system/OpenGraph.class.php');

	$graph = OpenGraph::fetch($url);
	if($graph) { // vannak og adatok
		if($graph->title != '') {
			$title = cleanText($graph->title);
		}
		if($graph->description != '') {
			$description = cleanText($graph->description);
		}
		if($graph->url != '') {
			$url = cleanUrl($graph->url);
		}
		if($graph->tag != '') {
			$tags = implode(', ', $graph->tag);
			trim($tags);
		}
	}

	// megnézzük volt-e már beküldve
	$www = str_replace('://', '://www.', $url);
	$nowww = str_replace('://www.', '://', $url);

	$res = $db->Query("SELECT title, CONCAT(id,'-',alias) AS alias, category_id
	FROM "._DBPREF."content
	WHERE link = '".$db->escape($url)."' OR link = '".$db->escape($www)."' OR link = '".$db->escape($nowww)."' LIMIT 1");
	if($db->numRows($res)) { // ez a tartalom már szerepel
		$row = $db->fetchArray($res);
		echo '{"duplicatedtitle": "'.htmlspecialchars($row['title'], ENT_QUOTES).'", ';
		echo '"duplicatedlink": "'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['alias'].'"}';
		die;
	}

	if(!isset($title)) {
		echo '404';
		die;
	}
	else {
		echo json_encode(array('title' => $title, 'description' => (string)@$description, 'link' => $url, 'tags' => (string)@$tags));
	}

	die;


	error_reporting(0);
	set_time_limit(5); // 8 másodperc

	$bc = $env->getFileContent($url);

	if(!$bc) {
		echo '{"title": ""}'; die;
	}

	preg_match("/charset=[\'\"]?(.*?)[\'\"]/si", $bc, $encoding);


	$c_title = '';
	$c_desc = '';

	if(preg_match("/og:title[\'\"] content=[\'\"]?([^<]*?)[\'\"]/i", $bc, $title)) {
		$c_title = $title[1];
	}
	elseif(preg_match("/<title>([^<]*?)<\/title>?/i", $bc, $title)) {
		$c_title = $title[1];
	}

	$c_title = strip_tags(preg_replace('/[\s\r\n\t]+/', ' ', $c_title));
	$c_title = trim(html_entity_decode(iconv($encoding[1], 'UTF-8', $c_title)));
	if(mb_strlen($c_title, 'utf-8') < 12) $c_title = '';

	if($c_title != '') {

		$c_title_array = explode('{#}', str_replace(array(' - ', ' – ', ' / ', ' | '), '{#}', $c_title));

		function sortval($a, $b) {
			return mb_strlen($b, 'utf-8')-mb_strlen($a, 'utf-8');
		}

		usort($c_title_array, 'sortval');
		$c_title = $c_title_array[0];

		$c_title = trim($c_title, '-:–/| ');
		if(mb_strlen($c_title, 'utf-8') < 12) $c_title = '';

/*		$t = str_replace(array(
		'Családi Net',
		'Csellengők.Net',
		'HírMax',
		'CKM',
		'JOY',
		'koos.hu',
		'www.vizpiac.hu',
		'comment:com',
		'Antivírus blog',
		'HVG.hu',
		'Ingatlan.net Magazin',
		'BKV-figyelő',
		'isite.hu',
		'Elosztó.hu'
		), '', $c_title);*/


		if(preg_match("/og:description[\'\"] content=[\'\"]?([^<]*?)[\'\"]/i", $bc, $desc)) {
			$c_desc = $desc[1];
		}
		/*elseif(preg_match("/description[\'\"] content=[\'\"]?(.+)[\'\"]/i", $bc, $desc)) {
			$c_desc = $desc[1];
		}*/

		$c_desc = strip_tags(preg_replace('/[\s\r\n\t]+/', ' ', $c_desc));
		$c_desc = trim(html_entity_decode(iconv($encoding[1], 'UTF-8', $c_desc)));
		if(mb_strlen($c_desc, 'utf-8') < 15) $c_desc = '';
	}

	echo json_encode(array('title' => $c_title, 'description' => $c_desc));

break;

/* ***************************************************************************** */
case 'tags_autocomplete': // tagek autocomplete
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT tag FROM "._DBPREF."content_tags WHERE tag LIKE '".$db->escape($_GET['q'])."%' ORDER BY tag ASC");
	while($row = $db->fetchArray($res)) {
		echo htmlspecialchars($row['tag'], ENT_QUOTES)."\n";
	}


break;

/* ***************************************************************************** */
/*case 'ajax_quiz': // kvízjáték válasz
	if(!$env->isAjax()) { die; }
die;
	$quiz = 'peking2008';

	if(!$_SESSION['user']) {
		echo 'Hiba: Ehhez a funkcióhoz be kell jelentkezned, ha még nem <a href="/register">regisztráltál</a>, itt az ideje';
		die;
		exit;
	}

	$res_u = $db->Query("SELECT question_id FROM "._DBPREF."quiz_answers WHERE question_id = '".(int)$_POST['question_id']."' AND user_id = '".$_SESSION['user']['id']."' LIMIT 1");
	if($db->numRows($res_u)) {
		echo('Hiba: Erre a kérdésre már válaszoltál egyszer');
		die;
		exit;
	}

	$res_ok = $db->Query("INSERT INTO "._DBPREF."quiz_answers (question_id, user_id, answer)
		VALUES ('".(int)$_POST['question_id']."', '".$_SESSION['user']['id']."', '".(int)$_POST['answer']."')");

	if($res_ok) {
		echo 'Rendben, rögzítettük a válaszod. Ne felejts el rendszeresen visszalátogatni, hiszen hamarosan jön a következő kérdés!';
		die;
		exit;
	}
*/
break;

/* ***************************************************************************** */
case 'delete_content': // tartalom törlése
	if(!$env->isAjax() || !$_SESSION['user']) { die; }

	// tiltott juzer ne törölgethessen
	if($env->isDenied($_SESSION['user']['id'])) {
		die;
	}


	// átmozgatjuk a lomtárba a tartalmat
	$res = $db->Query("INSERT INTO "._DBPREF."content_del SELECT * FROM "._DBPREF."content WHERE id = '".(int)$_GET['content_id']."' AND user_id = '".$_SESSION['user']['id']."' LIMIT 1");

	if(!$db->affectedRows($res)) {
		$env->setHeader(403);
		die;
	}
	else {
		// töröljük az "éles" táblából
		$db->Query("DELETE FROM "._DBPREF."content WHERE id = '".(int)$_GET['content_id']."' LIMIT 1");

		// töröljük a temp táblából
		$db->Query("DELETE FROM "._DBPREF."content_temp WHERE id = '".(int)$_GET['content_id']."' LIMIT 1");


		$db->Query("DELETE FROM "._DBPREF."content_tags_conn WHERE content_id = '".(int)$_GET['content_id']."'");

		// átmozgatjuk a lomtárba a kommenteket
		$db->Query("INSERT INTO "._DBPREF."content_comments_del SELECT * FROM "._DBPREF."content_comments WHERE content_id = '".(int)$_GET['content_id']."'");

		$db->Query("DELETE FROM "._DBPREF."content_comments WHERE content_id = '".(int)$_GET['content_id']."'");

		$db->Query("DELETE FROM "._DBPREF."users_activities WHERE content_id = '".(int)$_GET['content_id']."'");

		$db->Query("DELETE FROM "._DBPREF."content_comments_report WHERE content_id = '".(int)$_GET['content_id']."'");

		// logban is töröltre állítjuk, hogy a komment gyorsító kverikbe ne kerüljön bele
		$db->Query("UPDATE "._DBPREF."content_comments_log SET removed = '1' WHERE content_id = '".$_GET['content_id']."'");

		$db->DropQueryCache('users.cache');
		$db->DropQueryCache('dropdown.cache');

		echo 'OK';
	}

break;

/* ***************************************************************************** */
case 'like_comment': // komment lájkolása
	if(!$env->isAjax() || !$_SESSION['user']) { die; }

		// tiltott juzer
	if($env->isDenied($_SESSION['user']['id'])) {
		die;
	}

	$res = $db->Query("SELECT comment_id FROM "._DBPREF."content_comments_likes WHERE comment_id = '".(int)$_GET['comment_id']."' AND pwid = 'pr".$_SESSION['user']['id']."' LIMIT 1");
	if(!$db->numRows($res)) {

		$res = $db->Query("UPDATE "._DBPREF."content_comments SET likes = likes+1 WHERE id = '".(int)$_GET['comment_id']."' LIMIT 1");
		$res = $db->Query("INSERT INTO "._DBPREF."content_comments_likes (datetime, comment_id, pwid) VALUES (NOW(), '".(int)$_GET['comment_id']."', 'pr".$_SESSION['user']['id']."')");

		if(isset($_GET['user_id']) && (int)$_GET['user_id'] != 0 && ($_SESSION['user']['id'] != (int)$_GET['user_id'])) { // ha nem a saját kommentjét lájkolta, menjen értesítés
			// aktivitás
			$res_ca = $db->Query("SELECT user_id, category_id, title, alias FROM "._DBPREF."content WHERE id = '".(int)$_GET['content_id']."' LIMIT 1");
			$row_ca = $db->fetchArray($res_ca);
			$row_ca['link'] = $env->l['content']['category_alias_'.$row_ca['category_id']].'/'.(int)$_GET['content_id'].'-'.$row_ca['alias'];


			$activity_mess = 'kedveli egy hozzászólását: <a href="'.$env->base.'/'.$row_ca['link'].'?all#c'.(int)$_GET['comment_id'].'">'.htmlspecialchars($row_ca['title'], ENT_QUOTES).'</a>';

			$res = $db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
			VALUES ('4', '".$_SESSION['user']['id']."', '".$db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', '".(int)$_GET['user_id']."', '".(int)$_GET['content_id']."', '".(int)$_GET['comment_id']."', '".$db->escape($activity_mess)."', NOW())");

		}

		echo 'OK';
	}
	else {
		echo 'VOLTMAR';
	}

break;

/* ***************************************************************************** */
case 'get_embed': // cimlapi video scroll
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT fullembed FROM "._DBPREF."content_videos WHERE id = '".(int)$_GET['video_id']."' LIMIT 1");
	$row = $db->fetchArray($res);
	echo $row['fullembed'];

break;

/* ***************************************************************************** */
case 'act_viewed': // aktivitások olvasottra jelölése a buborékban
	if(!$env->isAjax()) { die; }

	$db->Query("UPDATE "._DBPREF."users_activities SET viewed = 1 WHERE id = '".(int)$_POST['id']."' LIMIT 1");
	die('ok');

break;

/* ***************************************************************************** */
case 'report_comment': // hozzászólás jelentése a moderátornak
	if(!$env->isAjax() || !$_SESSION['user']) { die; }

		// tiltott juzer
	if($env->isDenied($_SESSION['user']['id'])) {
		die;
	}

	$res = $db->Query("INSERT INTO "._DBPREF."content_comments_report (content_id, comment_id, user_id, datetime) VALUES ('".(int)$_GET['content_id']."', '".(int)$_GET['comment_id']."', '".$_SESSION['user']['id']."', NOW())");

	echo 'OK';

break;

/* ***************************************************************************** */
case 'toplist': // népszerű tartalmak lekérése
	if(!$env->isAjax()) { die; }

	if(!$_GET['interval']) { // most népszerű
		/** Modulosztály: feed- és beküldött tartalmak kezelése */
		require('Content.class.php');
		$content = new Content();
		$content->env = $env;

    	$res = $content->q_top((int)$_GET['category_id']);
		foreach($res as $key => $value) {
			echo '<li><a href="'.$value['category_alias'].'/'.$value['alias'].'" title="'.$value['counter'].' olvasás">'.htmlspecialchars($env->strTruncate($value['title'], 54, '...'), ENT_QUOTES).'</a></li>';
		}
	}
	else {
		if(!$_GET['category_id'])
			$q_cat = "";
		else
			$q_cat = " category_id = '".(int)$_GET['category_id']."' AND ";


		$q_date = "'".date('Y-m-d', strtotime((-1*(int)$_GET['interval']).' day'))."' AND date_add('".date('Y-m-d', strtotime((-1*(int)$_GET['interval']).' day'))."', interval 1 day)";

		$res = $db->Query("SELECT category_id, title, CONCAT(id,'-',alias) AS alias, counter, comments FROM "._DBPREF."content
		WHERE id > "._SPD_CONTENT_ID." AND ".$q_cat." datetime BETWEEN ".$q_date." ORDER BY counter DESC LIMIT 6");

		while($row = $db->fetchArray($res)) {
			echo '<li><a href="'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['alias'].'" title="'.$row['counter'].' olvasás">'.htmlspecialchars($env->strTruncate($row['title'], 54, '...'), ENT_QUOTES).'</a></li>';
		}
	}

break;

/* ***************************************************************************** */
case 'get_cities': // települések lekérdezése
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT id, city FROM "._DBPREF."system_cities WHERE city COLLATE utf8_hungarian_ci LIKE '".$db->escape($_GET['q'])."%' ORDER BY city ASC");
	while($row = $db->fetchArray($res)) {
		echo htmlspecialchars($row['city'], ENT_QUOTES).'|'.$row['id']."\n";
	}

break;

/* ***************************************************************************** */
default:

	$env->setHeader(404);

}

?>