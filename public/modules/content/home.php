<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
if ($_SERVER['HTTP_HOST'] == 'localhost') {
	$smarty->clear_cache();
	$smarty->clear_all_cache();
	print $smarty->clear_compiled_tpl();
}
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

if(!empty($_GET['new_policy']) && !empty($_GET['hash'])) {
    if($_GET['hash'] == $env->getNewPolicyHash($_GET['new_policy'])) {
        $new_policy = $db->escape(trim($_GET['new_policy']));

        $res = $db->Query("SELECT mail FROM _adatvedelmi_valaszok WHERE mail = '".$new_policy."' LIMIT 1");
        $row = $db->fetchArray($res);

        if(! $db->numRows($res)) {
            $db->Query("INSERT INTO _adatvedelmi_valaszok (mail, datetime) VALUES ('".$new_policy."', NOW())");
        }

        $env->setOverlayNotification('Köszönjük válaszod!');
        header('Location: /');
    }
}



// dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// toplista
$smarty->assign('top', $content->q_top(0));

// most beszédtéma
$smarty->assign('commented', $content->q_commented());

// bal és középső hasábok, fejléc kiemelések
$smarty->assign('blocks', $content->q_blocks());

// partner témák
$smarty->assign('partners', $content->q_partners());


    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'cimlap', 'page' => ''));

$smarty->assign('activities', $env->getActivities());
$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('content/home.tpl');

?>