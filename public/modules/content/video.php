<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

/** Rendszerosztály: munkamenetkezelés */
	require('../../system/Session.class.php');

/** Rendszerosztály: környezeti tulajdonságok beállítása */
	require('../../system/Environment.class.php');
	$env = new Environment('content');
	$env->db = $db;
	$env->m = array('pages', 'content', 'users');

	$env->setLangArray();
	$env->setConfigArray();

/** Rendszerosztály: felhasználókezelés és azonosítás */
	require('../users/Auth.class.php');
	$auth = new Auth($env);


/** Rendszerosztály: megjelenítés */
	require('../../system/libraries/smarty/Smarty.class.php');
	$smarty = new Smarty();
	$smarty->template_dir = '../../templates/'.$env->template;


	$smarty->assign('env', $env);

/* ***************************************************************************** */

	/** Modulosztály: feed- és beküldött tartalmak kezelése */
	require('Content.class.php');
	$content = new Content();
	$content->env = $env;


$smarty->assign('content', array('category_alias' => $env->u[1])); // rovat alias


$smarty->assign('misc', $content->q_header_misc());

$smarty->assign('topstories', $content->q_header_topstories());




$smarty->assign('top', $content->q_box_top(0));




$limit = (int)$env->c['content']['category_lastco_perpage'];
$current = (int)$db->escape($_GET['page']);

if(($_GET['page'] * $limit) == 0) { $from = 0; }
else { $from = $current * $limit - $limit; }

if(empty($env->u[2])) { // most beszédtéma
	$smarty->assign('category_title', $env->l['content']['video']); // rovat elnevezése
	$smarty->assign('latest', $content->q_box_latest(0));

	if($row_cat['id'] != 0)
	$co_query = " category_id = '".$row_cat['id']."' AND ";

	// most beszédtéma a kategóriában
	$res_co = $db->Query("SELECT SQL_CALC_FOUND_ROWS category_id, title, description, CONCAT(c.id,'-',c.alias) AS alias, co.datetime, comments
	FROM "._DBPREF."content_comments co
	LEFT JOIN "._DBPREF."content c ON co.content_id = c.id
	WHERE ".$co_query." co.datetime = (SELECT max(datetime) FROM "._DBPREF."content_comments co2 WHERE co.content_id = co2.content_id)
	AND c.datetime < NOW()
	ORDER BY co.datetime DESC LIMIT ".$from.", ".$limit);


	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);

	while($row_co = $db->fetchArray($res_co)) {
		$row_co['category_alias'] = $env->l['content']['category_alias_'.$row_co['category_id']];
		$row_co['date'] = $env->dateToString($row_co['datetime']);
		$commented[] = $row_co;
	}

	$smarty->assign('commented', $commented);

	/** Rendszerosztály: lapozásokhoz szükséges oldalszámok kiszámítása */
		require_once('../../system/Page.class.php');

		$page = new Page($rows[0], $limit, $db->escape($_GET['page']));
		$smarty->assign('page', $page);

}
elseif($env->u[2] == $env->l['content']['url_all']) { // minden videó
	$smarty->assign('latest', $content->q_box_latest_users(0));
	$smarty->assign('latest_title', $env->l['content']['box_latest_users']);


	$smarty->assign('category_title', $env->l['content']['video'].' > '.$env->l['content']['allvideo']);


	$res_co = $db->Query("SELECT SQL_CALC_FOUND_ROWS category_id, title, description, CONCAT(id,'-',alias) AS alias, datetime, comments
	FROM "._DBPREF."content c
	WHERE content_type = '1' ORDER BY datetime DESC LIMIT ".$from.", ".$limit);

	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);

	while($row_co = $db->fetchArray($res_co)) {
		$row_co['category_alias'] = $env->l['content']['category_alias_'.$row_co['category_id']];
		$row_co['date'] = $env->dateToString($row_co['datetime']);
		$commented[] = $row_co;
	}

	$smarty->assign('commented', $commented);

	/** Rendszerosztály: lapozásokhoz szükséges oldalszámok kiszámítása */
		require_once('../../system/Page.class.php');

		$page = new Page($rows[0], $limit, $db->escape($_GET['page']));
		$smarty->assign('page', $page);

	$smarty->assign('page_url', $env->l['content']['url_all']);
}





//	$smarty->clear_assign('content');



$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('content/category_'.$env->u[1].'.tpl');

?>
