<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

if(isset($_POST['action']) && $_POST['action'] == 'comment') { // kommentelés POST

    $userID = isset($_SESSION['user']) ? $_SESSION['user']['id'] : false;

    if(!$content->doComment($_POST['id'], $_POST['comment'], $userID)) { // van hibaüzenet
        $smarty->assign('form', $content->getCommentFormValues());
        $smarty->assign('error', $content->error);
    }
    else { // sikeres kommentelés
        $cts = array(
            'itthon' => 1,
            'nagyvilag' => 2,
            'technika' => 4,
            'szorakozas' => 6,
            'sport' => 7
        );

        // "most beszédtéma" doboz cache-ének eldobása
        $db->DropQueryCache('commented_'.$cts[$env->u[1]].'.cache');

        $env->setOverlayNotification('Hozzászólás elküldve.');
        header('Location: /'.$env->u[1].'/'.$env->u[2].'-'.$env->u[3].'#v'.$content->comment['id']);
        exit;
    }
}

if (in_array($env->u[2], array('3341438'))) { // valami pereskedés van ezzel a cikkel, kikapcsoltam
    $env->setHeader(404);
}


// tartalom lekérése
$res = $db->Query("SELECT c.id, feed_id, user_id, c.category_id, c.title, c.link, description,
    CONCAT(c.id,'-',c.alias) AS alias, c.datetime, counter, comments, content_type, related, excerpt,
    u.name AS user_name, u.alias AS user_alias, avatar, f.site
    FROM "._DBPREF."content c
    LEFT JOIN "._DBPREF."users u ON c.user_id = u.id
    LEFT JOIN "._DBPREF."content_feeds f ON c.feed_id = f.id
    WHERE c.id = ".(int)$env->u[2]."
    AND (content_type IS NULL OR content_type <> 7)
    LIMIT 1");
$row = $db->fetchAssoc($res);

/*if(!$row || (!isset($_SESSION['user']) && (strtotime($row['datetime']) > time()))) { // nincs ilyen tartalom, vagy guest próbál vázlatot megnézni
    // 404 helyett dobjuk a címlapra, hogy a SEO-erő megmaradjon
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /');
    exit;
}*/

$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
// ha nem jó a rovat url (pl. nagybetűvel van írva), elirányítjuk 301-el a megfelelőre, hogy a seo-nak egységes legyen
if((mb_strtolower($env->u[2].'-'.$env->u[3], 'UTF-8') != $row['alias']) || (mb_strtolower($env->u[1]) != $row['category_alias'])) {
    /*header('HTTP/1.1 301 Moved Permanently');
    header('Location: /'.$row['category_alias'].'/'.$row['alias']);
    exit;*/
}

$row['category'] = $env->l['content']['category_'.$row['category_id']];
$row['orighost'] = $env->getHost($row['link']);
$row['date'] = $env->dateToString($row['datetime']);
$row['date_c'] = date('c', strtotime($row['datetime'])); // microformats számára

// ez egy feed-tartalom, 120 nap elteltével nem jött rá 10 kattintás sem: töröljük ki
if(!empty($row['feed_id']) && ($row['counter'] < 10) && ((strtotime($row['datetime']) + 86400*120) < time())) {
    $db->Query("DELETE FROM "._DBPREF."content WHERE id = '".$row['id']."' LIMIT 1");

    $db->Query("DELETE FROM "._DBPREF."content_comments WHERE content_id = '".$row['id']."'");
    $db->Query("DELETE FROM "._DBPREF."content_videos WHERE content_id = '".$row['id']."'");
    $db->Query("DELETE FROM "._DBPREF."users_activities WHERE content_id = '".$row['id']."'");

    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /');
    exit;
}

if(!empty($row['user_id'])) { // beküldés vagy saját cikk
    // címkék kikeresése
    $res_tags = $db->Query("SELECT tags.id, tags.tag FROM "._DBPREF."content_tags_conn conn
    LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
    WHERE conn.content_id = ".$row['id']." ORDER BY tags.tag LIMIT 5");
    $tags_query = "";
    while($row_tags = $db->fetchAssoc($res_tags)) {
        $tags_query .= "tag_id = ".$row_tags['id']." OR ";
        $row['tags'][] = $row_tags['tag'];
    }

    if($row['user_id'] == '1758') { // saját cikk, keressünk hozzá szerzőt (ha van)
        $res_authors = $db->Query("SELECT authors.* FROM "._DBPREF."content_authors_conn conn
        LEFT JOIN "._DBPREF."authors authors ON conn.author_id = authors.id
        WHERE conn.content_id = ".$row['id']." LIMIT 3");
        while($row_authors = $db->fetchAssoc($res_authors)) {
            $row['authors'][] = $row_authors;
        }
    }
}

// most jön az, hogy milyen típusú tartalom ez aszerint lesznek még extra teendők vele

if($row['content_type'] == '1') { // videós rss-ből érkező videós tartalom vagy videót tartalmazó saját cikk

    // videó embed kód hozzákeresése
    $res_video = $db->Query("SELECT fullembed FROM "._DBPREF."content_videos WHERE content_id = '".$row['id']."' LIMIT 1");
    $row = array_merge($row, (array)$db->fetchArray($res_video));

    // ha youtube-os kód volt, szedjük ki a borítóképét, ez kell majd az og:image meta-tag számára
    if(@eregi('youtube.com/embed', $row['fullembed'])) {
        preg_match_all("|www.youtube.com/embed/(.+)|si", $row['fullembed'], $vid);

        preg_match("|src=[\'\"](.*?)[\'\"]|si", $row['fullembed'], $src);

        if(@eregi('?', $src[1]))
        $row['fullembed'] = str_replace($src[1], $src[1].'&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent', $row['fullembed']); // &amp;wmode=transparent
        else
        $row['fullembed'] = str_replace($src[1], $src[1].'?rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent', $row['fullembed']); //&amp;wmode=transparent

        $smarty->assign('share_image', 'http://img.youtube.com/vi/'.substr($vid[1][0], 0, 11).'/hqdefault.jpg');
    }
}
elseif($row['content_type'] == '6') { // nagyképes fotósorozat
    $row['related'] = 'N'; // ennek a tartalomtípusnak nem kell kapcsolódó cikkeket keresni/megjeleníteni

    $smarty->assign('bigpicture', true); // az og:image -nek

    if(!strstr($row['description'], '<p')) $row['description'] = '<p>'.$row['description'].'</p>';

    // képek kiszedése
    $assoc = array();
    $res_b = $db->Query("SELECT title, taken, author, filepath FROM "._DBPREF."bigpicture WHERE content_id = '".$row['id']."' ORDER BY ordinal");
    while($row_b = $db->fetchAssoc($res_b)) {
        $assoc[] = $row_b;
    }
    $smarty->assign('img', $assoc);

    // kapcsolódó nagyképes-tartalmak keresése
    $assoc_sug = array();
    $res_bs = $db->Query("SELECT c.title, category_id, CONCAT(c.id,'-',c.alias) AS alias, filepath FROM "._DBPREF."bigpicture b LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
    WHERE content_id <> '".$row['id']."' AND DATE_SUB('".$row['datetime']."', INTERVAL 120 DAY) < datetime GROUP BY content_id ORDER BY rand() LIMIT 4");
    while($row_bs = $db->fetchAssoc($res_bs)) {
        $row_bs['category_alias'] = $env->l['content']['category_alias_'.$row_bs['category_id']];
        $row_bs['filepath'] = str_replace('_normal.', '_thumb.', $row_bs['filepath']);
        $assoc_sug[] = $row_bs;
    }
    $smarty->assign('bigpic_sug', $assoc_sug);

}

// valaki egy youtube.com-os linket küldött be, generáljunk hozzá embed kódot
if(@eregi('youtube.com/watch', $row['link'])) {
    $parsed_url = parse_url($row['link']);
    parse_str($parsed_url['query'], $parsed_query);

    $row['fullembed'] = '
<iframe width="590" height="332" frameborder="0" allowfullscreen="true" src="http://www.youtube.com/embed/'.$parsed_query['v'].'?rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent"></iframe>
    ';
    $smarty->assign('share_image', 'http://img.youtube.com/vi/'.$parsed_query['v'].'/hqdefault.jpg');
}

// lead utáni banner kódja
$cikkoldal_1 = '
    <div id="banner-lead" class="banner">'
    .$env->getBanner('slave', $row['category_alias'], 'cikkoldal', 'roadblock_felso').'
    </div>
';



if($row['link']) { // ha van linkje a tartalomnak, akkor ez vagy egy rss-ből érkező hír, vagy user beküldés
    $row['description'] = '<p>'.strip_tags($row['description']).'</p>';
    $row['description'] = $env->textCorrect($row['description']);
    $row['description'] = str_replace('<p></p>', '', $row['description']);
    $row['description'] .= "\n".$cikkoldal_1;
}
else { // ha nincs link, akkor az csak egy szerkesztő által készített saját hír tartalom lehet
    $row['description'] = $env->nofollowOutgoing($row['description']);

    // ha van excerpt, tegyük a szöveg elé
    if(!empty($row['excerpt'])) {
        $row['description'] = '<p>'.$row['excerpt'].'</p>'.$row['description'];
    }

    // banner-kód hozzáfűzése a szöveghez
    // megpróbáljuk az első bekezdés megtalálni, és mögétesszük
    if(preg_match("/<br \/><br \/>/", $row['description'])) { // sortöréssel van (régi cikkekben van ilyen)
        $row['description'] = preg_replace("/<br \/><br \/>/", "</p>\n".$cikkoldal_1."\n<p>", $row['description'], 1);
    }
    elseif(preg_match("/<\/p>/", $row['description'])) { // bekezdésekkel van
        $row['description'] = preg_replace("/<\/p>/", "</p>\n".$cikkoldal_1."\n", $row['description'], 1);
    }
    if(!preg_match("/class=\"banner/", $row['description'])) { // nem találtunk bekezdéseket a szövegben, tegyük a szöveg végére a bannert
        $row['description'] .= "\n".$cikkoldal_1."\n";
    }

    // ha vannak képek a szövegben, a html-kódjuk "alt" címkéjét töltsük fel a címmel (SEO miatt)
    $row['description'] = str_replace("<img ", '<img alt="'.htmlentities($row['title'], ENT_QUOTES).'" ', $row['description']);

    // maradt még sortörés (régi cikkeknél), cseréljük le <p> -re
    $row['description'] = preg_replace("/<br \/>\s?<br \/>/", "</p>\n<p>", $row['description']);

    // keressünk képet a szövegben az og:image meta-tag számára
    if(preg_match("/src=[\'\"]http:\/\/static\.propeller\.hu\/images(.*?)[\'\"]/i", $row['description'], $src)) {
        // régi cikkeknél a static.propeller.hu -n vannak a képek
        $smarty->assign('share_image', 'http://static.propeller.hu/images'.$src[1]);
    }
    elseif(preg_match("/src=[\'\"]http:\/\/propeller\.hu\/images\/cikk(.*?)[\'\"]/i", $row['description'], $src)) {
        // újabb cikkkenél már a propeller.hu/images alá kerülnek a cikk-képek
        $smarty->assign('share_image', $env->base.'/images/cikk'.$src[1]);
    }

    // van a szövegben youtube videó embed-kód is, kicsit tegyük rendbe kicsit
/*    if(@eregi('youtube.com/embed', $row['description'])) {
        preg_match_all("|www.youtube.com/embed/(.+)|si", $row['description'], $vid);

        preg_match("|src=[\'\"](.*?)[\'\"]|si", $row['description'], $src);

        if(@eregi('?', $src[1]))
        $row['description'] = str_replace($src[1], $src[1].'&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent', $row['description']); // &amp;wmode=transparent
        else
        $row['description'] = str_replace($src[1], $src[1].'?rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent', $row['description']); //&amp;wmode=transparent
    }
    // kiszedtem, mert az src cseérlős rész elbasz mást is, nem csak youtube src-ket
*/
}

$row['meta_desc'] = strip_tags(preg_replace('#<script(.*?)>(.*?)</script>#is', '', $row['description']));

// vége, készen van a tartalommal kapcsolatos minden teendő


// most pedig kapcsolódó releváns tartalmak keresése. csak a cikk első betöltődésekor keresünk,
// ekkor elmentjük a táblába a kapcsolódó cikkek ID-jét ("related" mezőben vesszővel felsorolva),
// a későbbi oldalletöltésekkor pedig már csak innen olvassuk ki (sebesség miatt)

$counter_update_flag = false;
//$similar_content_debug = ($_SESSION["user"]['id'] == 16889) ? true : false;
$similar_content_debug = false;
if(empty($row['related']) || $similar_content_debug) { // még nincsenek kapcsolódó tartalmak
    $similarIds = array();

    if(isset($tags_query) && ($tags_query != '')) { // vannak címkék, keressünk először azzal
        $tags_query = rtrim($tags_query, ' OR ');

        $res_by_tags = $db->Query("SELECT DISTINCT c.id, title FROM "._DBPREF."content_tags_conn conn
        INNER JOIN "._DBPREF."content c ON (c.id = conn.content_id)
        WHERE c.id <> '".$row['id']."' AND (".$tags_query.") ORDER BY datetime DESC LIMIT 0, 4");
        while($row_by_tags = $db->fetchArray($res_by_tags)) {
            $similarIds[] = $row_by_tags['id'];
        }
    }

    // nincsenek címkék (mert pl. nem szerkesztői tartalom, hanem RSS-ből érkezett), vagy a címkék alapján
    // nem sikerült legalább 4 kapcsolódót találni: keressünk hasonló tartalmakat a cikk szavai alpaján
    if(!isset($tags_query) || (count($similarIds) < 4)) {

        $rel_title = preg_replace("/(:|\!|\?|\–|\"|\,|\;|\.|\'|\+)/ie", "", $row['title']);

        function remove_stopwords($str) {
            $stop_wrods = array(', ', 'az?','[\d]+','és','is','de','mi','én','ha','fotó','videó','miatt','ellen','volt','jobb','új','erre','íme','ezt?','jó','szerint',
                'éves?','találtak','bréking','hivatalos','egy','két','ilyen','meg','így','fel','be','ki','le','el','át','rá','ide','oda','szét','összes','vissza','ne','dr','van','ott','lesz',
                'eddig','nem','igen','ami','alig','kell','már','csak','hogy','még','előtt','után','kicsit','jöhet','mutatjuk','minden','lett','indul','újabb');
            foreach($stop_wrods as $w) {
                $str = preg_replace("/^|\s".$w."\s|$/isu", " ", $str); // ha kicsi, ha nagy, ezek nem kellenek
            }

           $str = preg_replace("/^|\s[a-záéíóöőúüű]{2}\s|$/su", " ", $str); // ez csak ha kicsi, akkor nem kell
           $str = preg_replace("/^|\s[a-záéíóöőúüű]{3}\s|$/su", " ", $str); // ez csak ha kicsi, akkor nem kell

            $str = preg_replace("/(\-)(et|at|ot|t|es|ra|re|nak|nek|tól|től|ban|ben|ból|ből|ról|ről|val|vel|hoz|hez|höz|nál|nél)/ieu", "", $str);

            $str = preg_replace("/(\s\-\s)/ie", "' '", $str);
            return trim($str);
        }

        $rel_title = remove_stopwords($rel_title);

        $tparts = explode(' ', trim($rel_title));
        if($similar_content_debug) {
            echo 'cimben talált:';
            p($tparts);
        }
        if(count($tparts) < 5) { // nem találtunk értelmes szavakat a címben
            /// bevezetőből is kiszedünk szavakat
            $todesc = $env->strTruncate($row['description'], 300, ' ');
            preg_match_all('/(([A-ZŰÁÉÍÚŐÓÜÖ][-a-zA-Z0-9ŰÁÉÍÚŐÓÜÖöüóőúéáäűí]+\s?)+\s?)/u', $todesc, $matches);
            if(count($matches[1]) > 0) {
                $matches[1] = array_map('trim', $matches[1]);

                $new_rel_desc = array();
                foreach($matches[1] as $key => $value) {
                    $value = remove_stopwords($value);
                    if(!empty($value)) $new_rel_desc[] = $value;
                }
                $tparts = array_merge($tparts, $new_rel_desc);
            }
        }
        sort($tparts);
        if($similar_content_debug) {
            echo 'szövegben talált:';
            p($new_rel_desc);
            echo 'összes:';
            p($tparts);
        }

        // most már vannak talán értelmes szavak, keressünk ezek alapján hasonló tartalmakat
        foreach($tparts AS $key => $value) {
            $value = trim($value);
            $value = $db->escape($value);
            if(!empty($value)) {
                $where_arr[] =  " instr(title COLLATE utf8_hungarian_ci, ' ".$value." ') ";
                $order_arr[] =  " if(instr(title COLLATE utf8_hungarian_ci, ' ".$value." '),1,0)  ";
            }
        }

        $res_rel = $db->Query("SELECT * FROM "._DBPREF."content_temp
        WHERE id <> '".$row['id']."'
            AND category_id = '".$row['category_id']."'
            AND (".implode(' OR ', $where_arr).")
             AND datetime < NOW()
             AND (content_type IS NULL OR content_type <> 7)
        ORDER BY ".implode(' + ', $order_arr)." DESC, datetime DESC LIMIT 0, 4");
        while($row_rel = $db->fetchArray($res_rel)) {
            $similarIds[] = $row_rel['id'];
            if($similar_content_debug) { echo $row_rel['title'].'<br>'; }
        }


    }

    $similarIds = array_unique($similarIds);
    $similarIds = array_slice($similarIds, 0, 4);

    $row['related'] = implode(',', $similarIds);
    if($row['related'] == '') $row['related'] = 'N';

    $db->Query("UPDATE "._DBPREF."content SET counter = counter+1, related = '".$row['related']."' WHERE id = '".$row['id']."' LIMIT 1");
    $counter_update_flag = true;
}

if($row['related'] != 'N') {
    $res_r = $db->Query("SELECT id, category_id, title, description, CONCAT(id,'-',alias) AS alias, related, comments FROM "._DBPREF."content
    WHERE id IN (".$row['related'].") ORDER BY FIND_IN_SET(id, '".$row['related']."') LIMIT 5");

    $ri = 1;
    while($row_r = $db->fetchArray($res_r)) {
        $row_r['category_alias'] = $env->l['content']['category_alias_'.$row_r['category_id']];

        $related[] = $row_r;
    }
    $smarty->assign('related', $related);
}
//p($related);


// ha vanank ilyen kényes szavak a cikk szövegben, akkor az IS_ADULT -ot tegyük true-ra,
// mert akkor nem szabad reklámzónát kitenni majd a template-ben
if(preg_match("/(playboy|\+18|18\+|dugják|[^b]anális|\sorál|szexvideó|szexvideo|gyerm?ekporn|porn[o|ó])/ieu", $row['title'].' '.strip_tags($row['description']), $isadult)) {
    $smarty->assign('is_adult', true);
}

// cikk olvasottság-számlálójának növelése: temp táblába bedobjuk, onnan lesz majd időnként összeszedve, és növelve
// a "content" tábla "counter" mezője (sebesség miatt)
if(!$env->isRobot() && !$counter_update_flag) {
    $db->Query("INSERT INTO "._DBPREF."content_counter_temp (content_id) VALUES (".$row['id'].")");
}



// jobb oldalon a "következő cikk" gomb
// cookieban gyűjtjük, hogy melyik saját cikkeket olvasta már a látogató, hogy
// utána ha a gombra kattint, olyat tudjunk neki adni, amelyiken még nem járt

$next_post = array();
if(! isset($_POST['action']) && empty($row['link'])) { // sima cikkoldal töltés (nem komment post) egy saját cikknél

    $next_post = [];
    if(isset($_COOKIE['next_post'])) {
        $next_post = explode('|', $_COOKIE['next_post']);
    }

    $next_post[] = $row['id'];
    $next_post = array_unique($next_post);

    //print_r($next_post);
    setrawcookie('next_post', implode('|', $next_post), strtotime('+1 day'), '/', HOST, false, true);
}
$smarty->assign('next_post_counter', count($next_post));


// átadjuk az egész tartalmat a templatenek
$smarty->assign('content', $row);


if (!empty($row['fullembed'])) {
    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'video', 'page' => 'cikkoldal'));
} else {
    // banner MASTER kód
    $smarty->assign('ad', array('category' => $row['category_alias'], 'page' => 'cikkoldal'));
}



// fejléc dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// toplista
$smarty->assign('top', $content->q_top($row['category_id']));

// most beszédtéma
$smarty->assign('commented', $content->q_commented());

// bal és középső hasábok, fejléc kiemelések
$blocks = $content->q_blocks();
// kiszedjük a nem-képes dobozokat
foreach($blocks['left'] AS $key => $value) {
    if(empty($value['image'])) unset($blocks['left'][$key]);
    unset($blocks['left'][$key]['sublink']);
    unset($blocks['left'][$key]['description']);
}
$blocks['left'] = array_values($blocks['left']);
foreach($blocks['right'] AS $key => $value) {
    if(empty($value['image'])) unset($blocks['right'][$key]);
    unset($blocks['right'][$key]['sublink']);
    unset($blocks['right'][$key]['description']);
}
$blocks['right'] = array_values($blocks['right']);

$smarty->assign('blocks', $blocks);



$topstories_content = array_merge(array_slice($blocks['topstories'], 0, 3), array_slice($blocks['right'], 0, 3));
$topstories_content = array_merge($topstories_content, array_slice($blocks['left'], 3, 5));
shuffle($topstories_content);

foreach ($topstories_content as $key => $value) {
    if ($value['content_id'] == $row['id'] ||
        !eregi('propeller.', $value['link'])) {
        unset($topstories_content[$key]);
    }
}

$smarty->assign('topstories_content', array_slice(array_values($topstories_content), 0, 3));




// most pedig a tartalom hozzászólásainak lekérdezése


$comments = array();
$best = array(); // legjobb komment

if($row['comments'] > 0) {
    $cTurn = 6; // ha ennyit elér a kommentek száma, akkor csak az utolsó $cTurn -t muatssa
    if($row['comments'] <= $cTurn || isset($_GET['all'])) { // még nem érte el, vagy teljes lista kell
        $climit = " LIMIT 0, ".$row['comments'];

        if(isset($_GET['all']) && ($row['comments'] > $cTurn))
        $smarty->assign('cturn', 2);
    }
    else {
        $climit = " LIMIT ".($row['comments']-$cTurn).", 1000";
        $smarty->assign('cturn', 1);
    }

    $res_c = $db->Query("SELECT c.id, u.name, alias, user_id, comment, c.datetime, likes, avatar
    FROM "._DBPREF."content_comments c
    INNER JOIN "._DBPREF."users u ON c.user_id = u.id
    WHERE content_id = ".$row['id']." ORDER BY c.datetime".$climit);
    while($row_c = $db->fetchAssoc($res_c)) {

        $row_c['date'] = $env->dateToString($row_c['datetime']);
        $row_c['comment'] = $env->stringToLink($row_c['comment']);
        $row_c['comment'] = $env->stringToUserlink($row_c['comment']);
        $row_c['avatarsrc'] = !empty($row_c['avatar']) ? STTC.'/avatar/'.floor($row_c['user_id']/1000).'/'.$row_c['user_id'].'/'.$row_c['user_id'].'_thumb.'.$row_c['avatar'].'.jpg' : STTC.'/avatar/default_thumb.jpg';

        $comments[] = $row_c;

        // legjobb komment kiszedése
        if(($row['comments'] <= $cTurn) && ($row['comments'] >= 5)) { // kevés komment van, ezekből ki lehet keresni
            if(($row_c['likes'] >= 3) && (!isset($best['likes']) || ($row_c['likes'] >= $best['likes']))) {
                $best = $row_c;
            }
        }

        if(isset($best['id']) && in_array($best['id'], array($comments[0]['id'], @$comments[1]['id']))) { // az elején van a legjobb komment, ne emeljük ki
            //unset($best);
        }

    }
//p($comments);
    // sok komment van
    if($row['comments'] > $cTurn) { // több komment van, mint a max, az összesből kell nézni a legjobbat

        $res_bc = $db->Query("SELECT c.id, u.name, alias, user_id, comment, c.datetime, likes, avatar
        FROM "._DBPREF."content_comments c
        INNER JOIN "._DBPREF."users u ON c.user_id = u.id
        WHERE content_id = ".$row['id']." AND likes >= 3 ORDER BY likes DESC LIMIT 0, 1");
        while($row_bc = $db->fetchArray($res_bc)) {
            $row_bc['date'] = $env->dateToString($row_bc['datetime']);
            $row_bc['comment'] = $env->stringToLink($row_bc['comment']);
            $row_bc['comment'] = $env->stringToUserlink($row_bc['comment']);
            $row_bc['avatarsrc'] = !empty($row_bc['avatar']) ? STTC.'/avatar/'.floor($row_bc['user_id']/1000).'/'.$row_bc['user_id'].'/'.$row_bc['user_id'].'_thumb.jpg?'.$row_bc['avatar'] : STTC.'/avatar/default_thumb.jpg';

            $best = $row_bc;
        }
    }
}
$smarty->assign('comments', $comments);
$smarty->assign('best', @$best);
//p($best);







// opengraph képek kiszedése, ha kiemelt cikkről van szó, és van hozzá fotó
// (csak az 5 napnál nem régebbi cikkeknél, hogy ne mindnél túráztassuk)

if(strtotime($row['datetime']) > strtotime('-5 days')) {
    foreach($blocks['left'] as $value) {
        if(($row['id'] == $value['content_id']) && (trim($value['image']) != '')) $share_img = $value['image'];
    }
    foreach($blocks['right'] as $value) {
        if(($row['id'] == $value['content_id']) && (trim($value['image']) != '')) $share_img = $value['image'];
    }
    foreach($blocks['topstories'] as $value) {
        if(($row['id'] == $value['content_id']) && (trim($value['image']) != '')) $share_img = $value['image'];
    }

    if(isset($share_img)) {
        $smarty->assign('share_image', $share_img);
    }
}



// "most népszerű a neten" hírkereső.hu partnerdoboz
require_once('../../system/Feed.class.php');
$feed = new Feed('http://rss.hirkereso.hu/rss.fc/topoldal/84.xml?partner=propeller');
// jó nagyra tesszük, így innen biztosan nem fog lefrissíteni,
// hanem a sytslem/cron/600.php frissíti ezt az rss-t
$feed->cacheTime = 60000000;
$feed->maxItems = 8;
$feed->getItems = 8;
$feed->schema = 'rss';
if($items = $feed->getFetched()) {
    $items_clean = array();
    foreach($items as $value) {
        // adsense miatt
        if(!preg_match("/(playboy|\+18|18\+|dugják|orgazmus|anális|\sorál|szex|porn)/ieu", $value['title'])) {
            $items_clean[] = $value;
        }
    }
    $smarty->assign('hirkereso', $items_clean);
}


$smarty->assign('activities', $env->getActivities());
$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('content/content.tpl');
?>