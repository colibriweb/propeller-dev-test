<?php
/**
* Feed- és beküldött tartalmak kezelése
* @version 1.0
* @copyright Copyright {@link &copy;} 2008. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
class Content {

	/**
	* Környezeti tulajdonságok
	* @var Environment
	*/
	var $env;

	/**
	* Tartalom tulajdonságai
	* @var array
	*/
	var $content = array();

	/**
	* Hozzászólás tulajdonságai
	* @var array
	*/
	var $comment = array();

	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Content() {

	}

	/**
	* Új link hozzáadása, vagy meglévő módosítása
	* @param integer $userID felhasználó azonosítója
	* @param array $content Tartalom tulajdonságai
	* @param string $action Művelet típusa
	* @return boolean Sikeres művelet esetén true-val, egyébként false-sal tér vissza
	*/
	function doContent($userID, $content, $action = 'add') {

		$content['link'] = ($content['link'] == 'http://') ? '' : $content['link'];
		$this->content = $content;

		$this->content['description'] = $this->env->strTruncate($this->content['description'], 1000, '...');
		$this->content['description'] = $this->env->getSafeText($this->content['description'], 'UTF-8', true);
		$this->content['title'] = str_replace(array('- Antivirus.blog'), '', $this->content['title']);
		$this->content['title'] = trim($this->content['title']);

		if($_SESSION['user']['status'] == 1) {
			$this->error = 'error_status1add';
			return false;
		}

		if($this->env->isDenied($userID)) {
			$this->error = 'error_notallowed';
			return false;
		}

		if(mb_strlen($this->content['title'], 'UTF-8') < 8 || (mb_strtoupper($string, 'UTF-8') == $this->content['title'])) { // rövid vagy csupa nagybetű
			$this->error = 'error_wrongtitle';
			return false;
		}

		if(preg_match("/[A-ZÁÉÍÓÖŐÚÜŰ]{8,}/", $this->content['title'])) { // nagybetűs szó van a címben
			$this->error = 'error_capitalletters';
			return false;
		}

		if(mb_strlen($this->content['description'], 'UTF-8') < 200 || ($this->content['title'] == $this->content['description'])) {
			$this->error = 'error_wrongdescription';
			return false;
		}

		if(empty($this->content['category_id'])) {
			$this->error = 'error_nocategory';
			return false;
		}

		if(!$this->checkUrlFormat($this->content['link'])) {
			$this->error = 'error_badurlformat';
			return false;
		}

        $pu = parse_url($this->content['link']);
		if((!isset($pu['path']) || $pu['path'] == '/') && !isset($pu['query'])) {
			$this->error = 'error_badurlformat';
			return false;
		}

        foreach (array(
			'gepnarancs.hu',
			'privatkopo.hu',
			'pestiriport.hu',
			'fullank.com',
			'kolozsvaros.com',
			'goo.gl',
			'bit.ly',
			'bitly.com',
			'sztarklikk.hu',
			'24per7.info',
			'hirsumma.info'
		) as $bw) {
			if(strpos(' '.$content['link'], $bw)) {
				$this->error = 'error_baddomain';
				return false;
				break;
			}
		}

		$this->content = $this->env->db->escape($this->content);

		$res_link_temp = $this->env->db->Query("SELECT id FROM "._DBPREF."content_temp WHERE link = '".$this->content['link']."' OR title = '".$this->content['title']."' LIMIT 1");
		if($this->env->db->numRows($res_link_temp)) { // van már ilyen link
			$this->error = 'error_duplicatedlink';
			return false;
		}

		$res_link = $this->env->db->Query("SELECT id FROM "._DBPREF."content WHERE link = '".$this->content['link']."' LIMIT 1");
		if($this->env->db->numRows($res_link)) { // van már ilyen link
			$this->error = 'error_duplicatedlink';
			return false;
		}

		$res_u = $this->env->db->Query("SELECT count(id) AS num FROM "._DBPREF."content_temp WHERE user_id = '".$userID."' AND datetime >= '".date('Y-m-d H:00:00')."'");
		$row_u = $this->env->db->fetchArray($res_u);


		$res_c = $this->env->db->Query("SELECT content_num FROM "._DBPREF."users WHERE id = '".$userID."' LIMIT 1");
		$row_c = $this->env->db->fetchArray($res_c);

		if($row_u['num'] >= $row_c['content_num']) { // elérte a limitjét
			$this->error = 'error_contentlimit';
			return false;
		}

		/*
		**********************************************************************************
		*/

		if(empty($this->error)) { // nincs hiba, beilleszthető az elem
			$alias = $this->env->getSefUrl($this->content['title']);

			$q_content_type = "NULL";
			$misc = $this->q_misc();
			$blacklist = explode(PHP_EOL, $misc['user_content_blacklist']);
			foreach ($blacklist as $value) {
				if (!empty($value)) {
					if (preg_match('/'.trim($value).'/ui', $this->content['title'])) { // tiltott szó a címben
						$q_content_type = "'7'"; // 7 = pending beküldött tartalom
						break;
					}
				}
			}

			$res = $this->env->db->Query("INSERT
			INTO "._DBPREF."content (user_id, category_id, title, link, description, alias, datetime, content_type)
			VALUES ('".$userID."', '".$this->content['category_id']."', '".$this->content['title']."', '".$this->content['link']."', '".$this->content['description']."', '".$alias."', NOW(), ".$q_content_type.")");

			if(!$res) {
				return false;
			}

			$this->contentID = $this->env->db->lastInsertID($res, 'id', _DBPREF."content");

			// temp tábla írás
			$this->env->db->Query("INSERT INTO "._DBPREF."content_temp
			(id, user_id, category_id, title, link, description, alias, datetime, content_type) VALUES
			('".$this->contentID."', '".$userID."', '".$this->content['category_id']."', '".$this->content['title']."', '".$this->content['link']."', '".$this->content['description']."', '".$alias."', NOW(), ".$q_content_type.")");

			$this->setTags($this->content['tags']);

			// aktivitás
/*			$res_ca = $this->env->db->Query("SELECT user_id, category_id, title, alias FROM "._DBPREF."content WHERE id = '".$this->contentID."' LIMIT 1");
			$row_ca = $this->env->db->fetchArray($res_ca);
			$row_ca['link'] = $this->env->l['content']['category_alias_'.$row_ca['category_id']].'/'.$this->contentID.'-'.$row_ca['alias'];

			$activity_mess = 'beküldött egy témát: <a href="'.$this->env->base.'/'.$row_ca['link'].'">'.htmlspecialchars($row_ca['title'], ENT_QUOTES).'</a>';

			$res = $this->env->db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
			VALUES ('7', '".$_SESSION['user']['id']."', '".$this->env->db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', NULL, '".$this->contentID."', NULL, '".$this->env->db->escape($activity_mess)."', NOW())");
*/

			return true;
		}
		return false;

	}

	/**
	* Visszaadja a megjelenítés számára a form mezők értékeit asszociatív tömbben
	* @return array A form mezőket tartalmazó tömbbel tér vissza
	*/
	function getContentFormValues() {

		$values = $this->content;
		return $values;

	}

	/**
	* Címkék beszúrása vagy módosítása
	* @param array $tags Címkék stringben
	* @return array Sikeres művelet esetén true-val, egyébként false-sal
	*/
	function setTags($tags) {

		// címkék feldolgozása tömbbe
		$tags = $this->env->getTagArray($tags, ",", true, (int)$this->env->c['content']['tagsnum']);

		// meglévők törlése, ha voltak (edit esetén)
		$res = $this->env->db->Query("DELETE FROM "._DBPREF."content_tags_conn WHERE content_id = '".$this->contentID."'");

		if(is_array($tags)) { // vannak címkék megadva
			foreach($tags as $i => $value) {
				$query .= "tag = '".$this->env->db->escape($value)."' OR ";
			}
			$query = rtrim($query, "OR ");
			$res_old_i = $this->env->db->Query("SELECT id, tag FROM "._DBPREF."content_tags WHERE ".$query);

			if($this->env->db->numRows($res_old_i)) { // voltak már korábban használt címkék
				while($row_i = $this->env->db->fetchArray($res_old_i)) {
					$res_tags_events = $this->env->db->Query("INSERT INTO "._DBPREF."content_tags_conn
					(content_id, tag_id) VALUES ('".$this->contentID."', '".$row_i['id']."')");

					foreach($tags as $i => $value) {
						//if($value == $this->env->db->escape($row_i['tag'])) {
						if($value == $row_i['tag']) {
							unset($tags[$i]);
						}
					}
				}
			}

			if(count($tags) == 0) { // elfogytak, nincs más
				return true;
			}

			// meglévők beírása és összekapcsolás a tartalommal
			foreach($tags as $i => $value) {
				$res_tags = $this->env->db->Query("INSERT
				INTO "._DBPREF."content_tags (tag) VALUES ('".$this->env->db->escape($value)."')");

				$last = $this->env->db->lastInsertID($res_tags, 'id', _DBPREF."content_tags");

				$res_tags_content = $this->env->db->Query("INSERT
				INTO "._DBPREF."content_tags_conn (content_id, tag_id) VALUES ('".$this->contentID."', '".$last."')");

				unset($tags[$i]);
			}
		}
		// nem használt címkék törlése a _content_tags -ból, lásd: /cron/daily.php
		return true;

	}

	/**
	* Trackback pinget küld egy megadott url alapján
	* @param string $string A szöveg
	* @param integer $length Megengedett karakterhossz
	* @param str $etc Folytatást jelző string
	* @return string Sikeres művelet esetén true-val, egyébként hibaüzenettel tér vissza
	*/
	function sendTrackback($url, $trackbackParams) {

		$trackbackParams['excerpt'] = $this->env->strTruncate($trackbackParams['excerpt']);
		$trackbackParams = array_map('urlencode', $trackbackParams);

		$bc = $this->env->getFileContent($url);

		if($trackback_uri = $this->getTrackback($bc)) { // sikerült trackback url-találni
			$params['http']['method'] = 'POST';
			$params['http']['header'] = 'Content-Type: application/x-www-form-urlencoded';
			$params['http']['content'] = 'title='.$trackbackParams['title'].'&url='.$trackbackParams['url'].'&blog_name='.$trackbackParams['blog_name'].'&excerpt='.$trackbackParams['excerpt'];

			$ch = curl_init();

			curl_setopt($ch,CURLOPT_URL, $trackback_uri);
			curl_setopt($ch,CURLOPT_POST, 4);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $params['http']['content']);

			ob_start();

			$result = curl_exec($ch);
			curl_close($ch);
			$response = ob_get_contents();

			ob_end_clean();

/*			$context = stream_context_create($params);

			$fp = @fopen($trackback_uri, 'rb', false, $context);
			$response = @stream_get_contents($fp);
			fclose($fp);
	*/
			if(stripos($response, '<error>0</error>')) { // nincs hiba a trackback során
				return 'Trackback was sent successfully.';
			}
			else { // hibaüzenet
			    $start_resp = stripos($response, '<message>');
			    $end_resp = stripos($response, '</message>');

			    $outcome = substr($response, $start_resp, $end_resp-$start_resp-1);

			    $outcome = str_replace('<message>', '', $outcome);
			    $outcome = str_replace('</message>', '', $outcome);
			    return 'Error: '.$outcome;
			}
		}

		return 'No trackback uri.';

	}

	/**
	* Kikeresi egy htmlkód darabból a trackback url címet
	* @param string $bc A html kód
	* @return string Sikeres művelet esetén a trackback url-lel tér vissza
	*/
	function getTrackback($bc) {

		if(preg_match('/trackback:ping="([^"]+)"/i', $bc, $matches) || preg_match('/trackback:ping +rdf:resource="([^>]+)"/i', $bc, $matches) || preg_match('/<trackback:ping>([^<>]+)/i', $bc, $matches)) {
			$matches[1] = str_replace('DELETEME', '', $matches[1]); // blog.hu hack
			return trim($matches[1]);
		}
		elseif (preg_match('/<a[^>]+rel="trackback"[^>]*>/i', $bc, $matches)) {
			if (preg_match('/href="([^"]+)"/i', $matches[0], $matches2)) {
				return trim($matches2[1]);
			}
		}
		elseif(preg_match('/<a[^>]+href=[^>]+>trackback<\/a>/i', $bc, $matches)) {
			if(preg_match('/href="([^"]+)"/i', $matches[0], $matches2)) {
				return trim($matches2[1]);
			}
		}
		else {
			return false;
		}

	}


	/**
	* Tartalomhoz írt hozzászólás beszúrása
	* @param integer $contentID Tartalom azonosítója
	* @param array $comment A hozzászólás tulajdonságai
	* @param mixed $userID A felhasználó azonosítója vagy false
	* @return boolean Sikeres művelet esetén true-val, egyébként false-sal tér vissza
	*/
	function doComment($contentID, $comment, $userID = false) {

		$this->contentID = (int)$contentID;
		$this->comment = $comment;

        $this->comment['comment'] = $this->env->strTruncate($this->comment['comment'], 2010, '...');
		$this->comment['comment'] = $this->env->getSafeText($this->comment['comment'], 'UTF-8', true);


		if(strlen($this->comment['comment']) < 2) {
			$this->error = 'error_nocomment';
			return false;
		}

		if(!$userID) { // vendég juzer
			$this->error = 'error_needlogin';
			return false;
		}

		if($this->env->isDenied($userID)) {
			$this->error = 'error_notallowed';
			return false;
		}

		$res_ca = $this->env->db->Query("SELECT user_id, category_id, title, alias FROM "._DBPREF."content WHERE id = '".$this->contentID."' LIMIT 1");
		if(!$this->env->db->numRows($res_ca)) {
			$this->env->setHeader(404);
		}

		$this->content = $this->env->db->fetchArray($res_ca);
		$this->content['link'] = $this->env->l['content']['category_alias_'.$this->content['category_id']].'/'.$this->contentID.'-'.$this->content['alias'];




		$this->comment = $this->env->db->escape($this->comment);


		/*
		**********************************************************************************
		*/

		if(empty($this->error)) { // nincs hiba, beilleszthető a hozzászólás
			$res = $this->env->db->Query("INSERT
			INTO "._DBPREF."content_comments (content_id, user_id, comment, datetime)
			VALUES ('".$this->contentID."', '".$userID."', '".$this->comment['comment']."', NOW())");

			if(!$res) {
				return false;
			}

			$this->comment['id'] = $this->env->db->lastInsertID($res, 'id', _DBPREF."content_comments");


			$res = $this->env->db->Query("UPDATE "._DBPREF."content SET comments = comments + 1 WHERE id = '".$this->contentID."' LIMIT 1");

			$res = $this->env->db->Query("INSERT INTO "._DBPREF."content_comments_log (comment_id, content_id, user_id, datetime, ip, host, useragent)
			VALUES ('".$this->comment['id']."', '".$this->contentID."', '".$userID."', NOW(), '".$this->env->getIpAddress()."', '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."', '".$this->env->db->escape($_SERVER['HTTP_USER_AGENT'])."')");

			// aktivitás
			if($userID) { // nem anonim vagy külső csatlakozós komment
				preg_match_all("/@([a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ\-_\'\.\"\s]{2,32}):/ie", $this->comment['comment'], $uname);

				/*

	1 hozzászólt egy témához:
	2 hozzászólt egy beküldésedhez: (aff)
	3 válaszolt egy hozzászólásodra:

	4 értékelt egy hozzászólásod itt: (aff)

	5 felvett a barátai közé(aff)
					6 elfogadta, hogy ismerősöd legyen (aff) -- nem kell

	7 beküldött egy témát

	8 üzenetet küldött

WHERE (affected = én vagyok) OR userid IN (barátaim)
				*/

				if($uname[1][0]) { // válaszolt valakinek ("@user name: comment") [3]
					$res_re = $this->env->db->Query("SELECT id FROM "._DBPREF."users WHERE name = '".$this->env->db->escape($uname[1][0])."' LIMIT 1");
					if($this->env->db->numRows($res_re)) {
						$row_re = $this->env->db->fetchArray($res_re);
						$activity_mess = 'válaszolt egy hozzászólására: <a href="'.$this->env->base.'/'.$this->content['link'].'?all#c'.$this->comment['id'].'">'.htmlspecialchars($this->content['title'], ENT_QUOTES).'</a>';

						$res = $this->env->db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
						VALUES ('3', '".$userID."', '".$this->env->db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', '".$row_re['id']."', '".$this->contentID."', '".$this->comment['id']."', '".$this->env->db->escape($activity_mess)."', NOW())");
					}
				} // vkinek a témájához szól hozzá [2]
				elseif(isset($this->content['user_id']) && ($this->content['user_id'] != $userID) && ($this->content['user_id'] != 1758)) {
						$activity_mess = 'hozzászólt egy beküldéséhez: <a href="'.$this->env->base.'/'.$this->content['link'].'?all#c'.$this->comment['id'].'">'.htmlspecialchars($this->content['title'], ENT_QUOTES).'</a>';

						$res = $this->env->db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
						VALUES ('2', '".$userID."', '".$this->env->db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', '".$this->content['user_id']."', '".$this->contentID."', '".$this->comment['id']."', '".$this->env->db->escape($activity_mess)."', NOW())");
				}
				else { // egyéb máshova kommentel [1]
						$activity_mess = 'hozzászólt egy hírhez: <a href="'.$this->env->base.'/'.$this->content['link'].'?all#c'.$this->comment['id'].'">'.htmlspecialchars($this->content['title'], ENT_QUOTES).'</a>';

						$res = $this->env->db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
						VALUES ('1', '".$userID."', '".$this->env->db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', NULL, '".$this->contentID."', '".$this->comment['id']."', '".$this->env->db->escape($activity_mess)."', NOW())");
				}
			}

			// cache eldobása
			$this->env->db->DropQueryCache('commented_0.cache');

			return true;
		}
		return false;

	}

	/**
	* Visszaadja a megjelenítés számára a form mezők értékeit asszociatív tömbben
	* @return array A form mezőket tartalmazó tömbbel tér vissza
	*/
	function getCommentFormValues() {

		$values = $this->comment;
		return $values;

	}

	/**
	* E-mail cím formai megfelelőségének ellenőrzése
	* @param string $mail E-mail cím
	* @return boolean Helyes e-mail formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkEmailFormat($mail) {

		if(!preg_match("/^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$/", $mail)) {
			return false;
		}
		return true;

	}

	/**
	* Webcím formai megfelelőségének ellenőrzése
	* @param string $url blogoldal URL-je
	* @return boolean Helyes formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkUrlFormat($url) {

		if(!preg_match("/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i", $url)) {
			return false;
		}
		return true;

	}





	function q_misc() { // fejléc menüpontok, breaking

		if(!$misc = $this->env->getFileCache('misc.cache', 86400)) {
			$res = $this->env->db->Query("SELECT * FROM "._DBPREF."misc LIMIT 1");
			$misc = $this->env->db->fetchAssoc($res);
			$misc['navigation_links'] = json_decode($misc['navigation_links']);

			$this->env->putFileCache('misc.cache', $misc);
		}
		return $misc;

	}

	function q_latest() { // legfrissebb hírek

		$res_cached_latest = (array)$this->env->db->QueryCache("
		(SELECT 1 as a, category_id, title, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE category_id = 1 AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC LIMIT 12)
		UNION
		(SELECT 2 as a, category_id, title, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE category_id = 2 AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC LIMIT 12)
		UNION
		(SELECT 3 as a, category_id, title, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE category_id = 6 AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC LIMIT 12)
		UNION
		(SELECT 4 as a, category_id, title, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE category_id = 4 AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC LIMIT 12)
		UNION
		(SELECT 5 as a, category_id, title, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE category_id = 7 AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC LIMIT 12)
		UNION
		(SELECT 6 as a, category_id, title, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY datetime DESC LIMIT 12)
		", "latest.cache", 90);
		foreach($res_cached_latest as $key => $value) {
			$res_cached_latest[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
			$res_cached_latest[$key]['date'] = date('H:i', strtotime($value['datetime']));
		}
		//p($res_cached_latest);die;
		return $res_cached_latest;

	}

	function q_blocks() {

		$ttl = (ENV == 'dev') ? 1 : 1800;

		if(!$blocks = $this->env->getFileCache('blocks.cache', $ttl)) {
			$res_b = $this->env->db->Query("
			(SELECT 'left' AS col, block_id, b.content_id, b.title, b.link, b.description, b.modtime, image, imagesource #, c.comments
			FROM "._DBPREF."content_blocks b
			# LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
			WHERE block_column = 1 AND b.datetime < NOW() ORDER BY block_position DESC LIMIT 0, 30)
			UNION
			(SELECT 'right' AS col, block_id, b.content_id, b.title, b.link, b.description, b.modtime, image, imagesource #, c.comments
			FROM "._DBPREF."content_blocks b
			# LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
			WHERE block_column = 2 AND b.datetime < NOW() ORDER BY block_position DESC LIMIT 0, 30)
			UNION
			(SELECT 'topstories' AS col, block_id, b.content_id, b.title, b.link, b.description, b.modtime, image, imagesource #, c.comments
			FROM "._DBPREF."content_blocks b
			# LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
			WHERE block_column = 3 AND b.datetime < NOW() ORDER BY block_position DESC LIMIT 0, 30)");
			while($row_b = $this->env->db->fetchAssoc($res_b)) {
				$row_b['sublink'] = array();
				if($row_b['col'] != 'topstories') {
					$res_b_link = $this->env->db->Query("SELECT content_id, title, link FROM "._DBPREF."content_blocks WHERE block_id = '".$row_b['block_id']."' AND ordinal > 0 ORDER BY ordinal");
					while($row_b_link = $this->env->db->fetchAssoc($res_b_link)) {
						$row_b['sublink'][] = $row_b_link;
					}
				}
				$blocks[$row_b['col']][] = $row_b;
			}
			$this->env->putFileCache('blocks.cache', $blocks);
		}
		return $blocks;

	}

	function q_partner_box() {

//        $memcache = new \Memcached();

//        if ($memcache->addServers([['127.0.0.1', 11211]])) {
//	        if (! $blocks = $memcache->get('propeller_scored_content')) {

				$res_b = $this->env->db->Query("
				SELECT block_column, b.content_id, b.title, b.link, image, counter, c.datetime, c.description,
				(counter) / POW(((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(c.datetime))/3600) / 2, 1.8) as score
				FROM "._DBPREF."content_blocks b
				LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
				WHERE b.datetime > '".date('Y-m-d 00:00:00', strtotime('-36 hours'))."'
					AND block_column IS NOT NULL
					#AND image IS NOT NULL
					AND b.datetime < NOW()
				ORDER BY score DESC LIMIT 0, 10");
				while($row_b = $this->env->db->fetchAssoc($res_b)) {

					if(empty($row_b['image']) && preg_match("/src=[\'\"]http:\/\/propeller\.hu\/images\/cikk(.*?)[\'\"]/i", $row_b['description'], $src)) {
						$row_b['image'] = $this->env->base.'/images/cikk'.$src[1];
					}


					$blocks[] = $row_b;
				}


//	            $memcache->set('propeller_scored_content', $blocks, 120);
//	        }
	        return $blocks;
//        }
	}

	function q_blocks_category($category_id) {

		$blocknum = 18;
		$ttl = (ENV == 'dev') ? 1 : 600;

		if(!$blocks = $this->env->getFileCache('blocks_'.$category_id.'.cache', $ttl)) {
			$blocks = array();

			$res_b = $this->env->db->Query("SELECT block_id, ifnull(block_position, 99999999) block_position, b.content_id, b.title, b.link, b.description, b.modtime, image, imagesource, query, c.link AS origlink, c.comments
			FROM "._DBPREF."content_blocks b
			LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
			WHERE cat_".$category_id." = 1 AND
			b.datetime < NOW() AND b.modtime > '".date('Y-m-d 00:00:00', strtotime('-48 hours'))."' ORDER BY block_position DESC, b.datetime DESC LIMIT 0, ".$blocknum."
			");

			$content_ids = array(_SPD_CONTENT_ID);
			while($row_b = $this->env->db->fetchAssoc($res_b)) {
				$content_ids[] = $row_b['content_id'];

				$row_b['sublink'] = array();
				$res_b_link = $this->env->db->Query("SELECT content_id, title, link FROM "._DBPREF."content_blocks
				WHERE block_id = '".$row_b['block_id']."' AND ordinal > 0 ORDER BY ordinal");
				while($row_b_link = $this->env->db->fetchAssoc($res_b_link)) {
					$row_b['sublink'][] = $row_b_link;
				}
				$blocks[] = $row_b;
			}

			// nincs elég friss kiemelés, top témákat keresünk hozzá
			if((count($blocks) < $blocknum) && ($category_id <> 0)) {
				/*$res_ba = $this->env->db->Query("SELECT '0' AS block_id, c.id AS content_id, category_id, c.title, CONCAT(c.id,'-',c.alias) AS alias, c.description, c.link AS origlink, c.comments
				FROM "._DBPREF."content c
				WHERE id > "._SPD_CONTENT_ID." AND category_id = ".$category_id." AND datetime > '".date('Y-m-d 00:00:00', strtotime('-18 hours'))."' AND id NOT IN (".implode(',',$content_ids).") ORDER BY counter DESC LIMIT 0, ".($blocknum-count($blocks))."
				");*/
				$res_ba = $this->env->db->Query("SELECT '0' AS block_id, c.id AS content_id, category_id, c.title, CONCAT(c.id,'-',c.alias) AS alias, c.description, c.link AS origlink, c.comments
				FROM "._DBPREF."content c
				WHERE id > "._SPD_CONTENT_ID." AND category_id = ".$category_id." AND datetime > '".date('Y-m-d 00:00:00', strtotime('-48 hours'))."' AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) AND id NOT IN (".implode(',',$content_ids).") ORDER BY counter DESC LIMIT 0, ".($blocknum-count($blocks))."
				");
				while($row_ba = $this->env->db->fetchAssoc($res_ba)) {
					$row_ba['link'] = $this->env->base.'/'.$this->env->l['content']['category_alias_'.$row_ba['category_id']].'/'.$row_ba['alias'];
					$row_ba['description'] = $this->env->getExcerpt($row_ba['description']);
					$blocks[] = $row_ba;
				}
			}

			foreach($blocks AS $key => $value) {
				if(!empty($value['image'])) {
					unset($blocks[$key]);
					array_unshift($blocks, $value);
					break;
				}
			}
			$this->env->putFileCache('blocks_'.$category_id.'.cache', $blocks);
		}
		//if($_SESSION['user']['id'] == 16889) { p($blocks); }
		//p($blocks);
		return $blocks;

	}

	function q_commented($category_id = 0) { // hozzászólások
/*
		$res_cached = (array)$this->env->db->QueryCache("SELECT category_id, title, CONCAT(c.id,'-',c.alias) AS alias, comments, co.user_id, co.datetime, content_type,
		(SELECT CONCAT(name,'|',alias,'|',id,'|',avatar) FROM "._DBPREF."users WHERE id = co.user_id LIMIT 1) name_user_alias
		FROM "._DBPREF."content_comments co
		LEFT JOIN "._DBPREF."content c ON co.content_id = c.id
		WHERE co.id = (SELECT id FROM "._DBPREF."content_comments WHERE content_id = c.id ORDER BY datetime DESC LIMIT 0, 1)
		ORDER BY co.datetime DESC LIMIT 0, 12", "commented.cache", 86400); //86400
*/

		$query = ($category_id == 0) ? " " : " category_id = '".$category_id."' AND ";

		$res_cached = (array)$this->env->db->QueryCache("SELECT co.id, category_id, title, CONCAT(c.id,'-',c.alias) AS alias, comments, co.datetime
		FROM "._DBPREF."content_comments co
		LEFT JOIN "._DBPREF."content c ON co.content_id = c.id
		WHERE ".$query." co.id > "._SPD_COMMENT_ID." AND co.id = (SELECT id FROM "._DBPREF."content_comments WHERE content_id = c.id ORDER BY datetime DESC LIMIT 0, 1)
		ORDER BY co.datetime DESC LIMIT 0, 16", "commented_".$category_id.".cache", 86400); //86400

		$cNum = array();
		foreach($res_cached as $key => $value) {
			$cNum[$key] = $value['comments'];
			$res_cached[$key]['title'] = $this->env->strTruncate($value['title'], 80, '...');
            $res_cached[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
			$res_cached[$key]['link'] = $res_cached[$key]['category_alias'].'/'.$value['alias'];
			$res_cached[$key]['date'] = $this->env->dateToPast($value['datetime']);
			unset($res_cached[$key]['category_id']);
			unset($res_cached[$key]['alias']);
		}

		//p($res_cached); die;
		return $res_cached;

	}

	function q_top($category_id = 0) { // aktuális napi toplista

		$ttl = (ENV == 'dev' || ENV == 'local') ? 9999999999 : 3600;

		$query = "";
		if($category_id != 0) $query = " category_id = '".$category_id."' AND ";
		$res_cached = (array)$this->env->db->QueryCache("
		(SELECT 1 as t, category_id, title, CONCAT(id,'-',alias) AS alias, counter FROM "._DBPREF."content
		WHERE id > "._SPD_CONTENT_ID." AND ".$query." datetime > '".date('Y-m-d 00:00:00', strtotime('-4 day'))."' AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY counter DESC LIMIT 12)
		", "box_".$category_id."_top.cache", $ttl);
/*
		$res_cached = (array)$this->env->db->QueryCache("
		(SELECT 1 as t, category_id, title, CONCAT(id,'-',alias) AS alias, counter FROM "._DBPREF."content
		WHERE id > "._SPD_CONTENT_ID." AND ".$query." datetime > '".date('Y-m-d H:i:s', strtotime('-1 day'))."' ORDER BY counter DESC LIMIT 12)
		UNION
		(SELECT 2 as t, category_id, title, CONCAT(id,'-',alias) AS alias, counter FROM "._DBPREF."content
		WHERE id > "._SPD_CONTENT_ID." AND ".$query." datetime > '".date('Y-m-d H:i:s', strtotime('-7 days'))."' ORDER BY counter DESC LIMIT 12)
		", "box_".$category_id."_top.cache", 1800);
*/
		foreach($res_cached as $key => $value) {
			$res_cached[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
			//$res_cached[$key]['title'] = str_replace(': ', ':\n', $res_cached[$key]['title']);

		}
		return $res_cached;

	}


	function q_box_vote_by_id($content_id) { // egy bizonyos szavazás lekérése

		if($content_id == 0) return false;

		$res_cached = (array)$this->env->db->QueryCache("SELECT c.id AS content_id, category_id, title, description, CONCAT(c.id,'-',c.alias) AS alias, comments, v.id, answer, vote
		FROM "._DBPREF."content_vote v
		LEFT JOIN "._DBPREF."content c ON v.content_id = c.id
		WHERE content_id = '".(int)$content_id."' ORDER BY ordinal", "vote_".(int)$content_id.".cache", 864000);
		$res_cached[0]['allvote'] = 0;
		$res_cached[0]['max'] = 0;
		foreach($res_cached as $key => $value) {
			$res_cached[0]['allvote'] += $value['vote'];
			if($value['vote'] > $res_cached[0]['max']) $res_cached[0]['max'] = $value['vote'];
			$res_cached[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
		}
		$res_cached[0]['ratio'] = 304/$res_cached[0]['max'];

		return $res_cached;

	}


	function q_newsletter_top24() {

		$query = "";
		$res_cached = (array)$this->env->db->QueryCache("
		(SELECT 1 as t, category_id, title, CONCAT(id,'-',alias) AS alias, counter FROM "._DBPREF."content
		WHERE id > "._SPD_CONTENT_ID." AND datetime > '".date('Y-m-d H:i:s', strtotime('-1 day'))."' AND datetime < NOW() AND (content_type IS NULL OR content_type <> 7) ORDER BY counter DESC LIMIT 12)
		", "newsletter_top.cache", 3600);

		foreach($res_cached as $key => $value) {
			$res_cached[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
			//$res_cached[$key]['title'] = str_replace(': ', ':\n', $res_cached[$key]['title']);

		}
		return $res_cached;

	}


	function q_tagcloud($category_id = 0) { // címkefelhő

		$query = "";
		if($category_id != 0) $query = " category_id = '".$category_id."' AND ";

		if(!$tags = $this->env->getFileCache('tagcloud/'.$category_id.'.cache', 86400)) { // 864000 az eredeti

			$res_tags = $this->env->db->Query("SELECT tag, count(tag_id) num
			FROM "._DBPREF."content c
			LEFT JOIN "._DBPREF."content_tags_conn conn ON c.id = conn.content_id
			LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
			WHERE c.id > ".(_SPD_CONTENT_ID)." AND ".$query." tag <> '' AND user_id IS NOT NULL
			AND datetime > '".date('Y-m-d H:i:s', strtotime('-60 day'))."'
			GROUP BY tag
			ORDER BY num DESC LIMIT 0, 30");

			$frequency = 0;
			$max_num = 0;
			$min_num = 99999999;
			$tags = array();
			while($row_tags = $this->env->db->fetchAssoc($res_tags)) {
				$tags[] = $row_tags;
				$max_num = ($row_tags['num'] > $max_num) ? $row_tags['num'] : $max_num;
				$min_num = ($row_tags['num'] < $min_num) ? $row_tags['num'] : $min_num;
			}
			$num_unit = (22 - 12) / ($max_num - $min_num +1);
			$i = 0;
			foreach($tags as $key) {
				$tags[$i]['size'] = round($tags[$i]['num'] * $num_unit + (14 - $num_unit));
				$i++;
			}
			sort($tags);

			$this->env->putFileCache('tagcloud/'.$category_id.'.cache', $tags);
		}
		return $tags;

	}

	function q_users($category_id = 0, $ttl = 1800) { // beküldések -- 1800!!

		$ttl = (ENV == 'dev') ? 9999999999 : $ttl;

		$query = "";
		if($category_id != 0) $query = " category_id = '".$category_id."' AND ";

		$res_cached = (array)$this->env->db->QueryCache("
		SELECT c.id AS content_id, user_id, category_id, title, CONCAT(c.id,'-',c.alias) AS alias, c.datetime, name
		FROM "._DBPREF."content_temp c
		LEFT JOIN "._DBPREF."users u ON c.user_id = u.id
		WHERE ".$query." user_id IS NOT NULL AND link IS NOT NULL
		AND c.id = (SELECT max(id) FROM "._DBPREF."content_temp t WHERE t.user_id = c.user_id)
		ORDER BY c.datetime DESC LIMIT 0, 8", "users_".$category_id.".cache", $ttl); // 1800
		foreach($res_cached as $key => $value) {
			$res_cached[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
			//$res_cached[$key]['date'] = date('H:i', strtotime($value['datetime']));

		}
		//p($res_cached); die;
		return $res_cached;
	}

	function q_partners($ttl = 3600000) { // partner hírek | 600.php -be kell inkább tenni, onnan fusson

		$res_cached = (array)$this->env->db->QueryCache("
		# 168 óra
		(SELECT id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (349, 355) ORDER BY datetime DESC LIMIT 10)

		#hvg
		UNION
		(SELECT id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (16,139,302) ORDER BY datetime DESC LIMIT 10)

		# napidoktor
		UNION
		(SELECT id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (359) ORDER BY datetime DESC LIMIT 5)

		# ízes élet
		UNION
		(SELECT id, category_id, title, link, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp WHERE feed_id IN (358) ORDER BY datetime DESC LIMIT 5)

		", "partners.cache", $ttl);

		foreach($res_cached as $key => $value) {
			$res_cached[$key]['category_alias'] = $this->env->l['content']['category_alias_'.$value['category_id']];
		}
		//p($res_cached); die;


		//if($_SESSION['user']['id'] == 16889) { p($res_cached); }

		return $res_cached;
	}







}

?>