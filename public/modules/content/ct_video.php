<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

require(ROOT.'/system/Pagination.class.php');

// video


$row_cat = array(
'alias' => 'video',
'title' => 'Videók'
);

$smarty->assign('category', $row_cat);


    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'video', 'page' => 'nyito'));



// kiemelt kulcsszavak
//$keywords = array_map('trim', explode(',', $row_cat['keywords']));
//$smarty->assign('category_keywords', $keywords);

// dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// toplista
$smarty->assign('top', $content->q_top(0));

// bal és középső hasábok, fejléc kiemelések
$smarty->assign('blocks', $content->q_blocks());



/*
$limit = (int)$env->c['content']['perpage'];
$current = (int)$_GET['page'];

if ($current > 100) $env->setHeader(404);

if(($_GET['page'] * $limit) == 0) { $from = 0; }
else { $from = $current * $limit - $limit; }*/

$p = new Pagination((int)@$_GET['page'], 30);
/*
$res_s = $db->Query("#VIDEOROVAT
	SELECT SQL_CALC_FOUND_ROWS c.id, category_id, title, link, description, CONCAT(c.id,'-',c.alias) AS alias, datetime, comments, fullembed FROM "._DBPREF."content c
LEFT JOIN "._DBPREF."content_videos v ON c.id = v.content_id WHERE c.id > ".(_SPD_CONTENT_ID-200000)." AND content_type = 1 ORDER BY datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);
*/
$res_s = $db->Query("#VIDEOROVAT
	SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, t.link, t.description, CONCAT(t.id,'-',t.alias) AS alias, t.datetime, comments FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE (t.content_type = 1 OR t.link LIKE '%youtube.com/%') AND t.datetime < NOW()
	AND (t.content_type IS NULL OR t.content_type <> 7)
	ORDER BY t.datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);

	$res_num = $db->Query("SELECT found_rows()");
	$rows = $db->fetchArray($res_num);
	$p->results_num = $rows[0];
	$smarty->assign('p', $p->getPaging());
$list = array();
while($row_s = $db->fetchArray($res_s)) {
	$row_s['orighost'] = $env->getHost($row_s['link']);
	$row_s['category_alias'] = $env->l['content']['category_alias_'.$row_s['category_id']];
	$row_s['date'] = $env->dateToString($row_s['datetime']);
	$row_s['description'] = $env->getExcerpt($row_s['description']);
	$list[] = $row_s;
}

if(!$list) $env->setHeader(404);
$smarty->assign('list', $list);


/** Rendszerosztály: lapozásokhoz szükséges oldalszámok kiszámítása */
/*require_once('../../system/Page.class.php');

    $rows[0] = ($rows[0] > $limit*100) ? $limit*100 : $rows[0];

$page = new Page($rows[0], $limit, $db->escape($_GET['page']));
$smarty->assign('page', $page);*/


//$smarty->assign('titleplus', ' - Összes friss');




$smarty->assign('activities', $env->getActivities());
$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('content/ct_video.tpl');

?>
