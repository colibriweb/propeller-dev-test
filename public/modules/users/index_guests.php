<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */



// dolgok
$smarty->assign('misc', $content->q_misc());

    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));

switch($env->u[1]) {
/* ***************************************************************************** */
case $env->l['users']['url_login']:
	$auth->isAuthRequired(false);

	/** Modulosztály: be- és kijelentkezés kezelése */
	require(ROOT.'/modules/users/Login.class.php');
	$login = new Login();
	$login->env = $env;

	if(isset($_POST['action']) && $_POST['action'] == 'do') { // van elküldött adat
		if(!$login->doLogin($_POST['user'], @$_POST['note'], @$_POST['store'])) { // van hibaüzenet
			$smarty->assign('form', $login->getLoginFormValues());
			$smarty->assign('error', $login->error);
		}
		else {
			if(!empty($_POST['redirect']) && (strpos(urldecode($_POST['redirect']), '/') === 0)) { // van redirect oldal kiválasztva
				header('Location: '.urldecode($_POST['redirect']));
				exit;
			}
			header('Location: /'.$env->l['users']['url_profile'].'/'.$_SESSION['user']['alias']);
//			header('Location: /'.$env->l['users']['url_messages']);
			exit;
		}
	}

			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());

	if(!empty($_GET['redirect'])) {
		$smarty->assign('redirect', urlencode($_GET['redirect']));
		$smarty->assign('error', 'error_onlyforusers');
	}
$smarty->display('users/login.tpl');
break;

/* ***************************************************************************** */
case $env->l['users']['url_registration']:
	$auth->isAuthRequired(false);

	if(!(bool)$env->c['users']['registration']) { // nincs engedélyezve a nyílt regisztráció
		header('Location: /'.$env->l['users']['url_login']);
		exit;
	}

	/** Modulosztály: felhasználói regisztráció kezelése */
	require(ROOT.'/modules/users/Registration.class.php');
	$registration = new Registration();
	$registration->env = $env;

	if(isset($_POST['action']) && $_POST['action'] == 'do') { // van elküldött adat
		if(!$registration->doRegistration($_POST['user'], $_POST['g-recaptcha-response'], $_POST['agree'])) { // van hibaüzenet
			$smarty->assign('form', $registration->getRegistrationFormValues());
			$smarty->assign('error', $registration->error);
		}
		else { // sikeres regisztráció
			if((bool)$env->c['users']['mailvalidation']) { // érvényesítő levél küldése
				/** Rendszerosztály: e-mail küldés */
				require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
				$phpMailer = new PHPMailer();
				$phpMailer = $phpMailer;

				// levélküldő osztály feltöltése és levélküldés
				$phpMailer->CharSet = 'UTF-8';
				$phpMailer->From = $env->l['mail']['from'];
				$phpMailer->FromName = $env->l['mail']['fromname'];
				$phpMailer->Subject = $env->l['users']['mail_validationsubject'];
				$phpMailer->AddAddress($registration->user['mail']);
				$phpMailer->IsHTML(true);

				$smarty->assign('subject', $phpMailer->Subject);
				$smarty->assign('name', $registration->user['name']);
				$smarty->assign('body', $env->l['users']['mail_validationbody'].'<br /><br />
				<a href="'.$env->base.'/'.$env->l['users']['url_validation'].'/'.$registration->randomKey.'?utm_source=validation&utm_medium=email&utm_campaign=noreply" target="_blank">'.$env->base.'/'.$env->l['users']['url_validation'].'/'.$registration->randomKey.'</a>');

				$phpMailer->Body = $smarty->fetch('mail.tpl');

				if(!$phpMailer->Send()) { // sikertelen levélküldés, átmeneti user törlése
					$registration->clearFailedRegistration($registration->user['id']);
					$smarty->assign('error', 'error_failedreg');
				}
				else { // sikeres levélküldés
					// eltesszük a címét a belépést segítő linkhez
					$_SESSION['registration_user_id'] = $registration->user['id'];

					// rögtön be is léptetjük
					//$auth->setUserSession($registration->user['id']);

					header('Location: /'.$env->l['users']['url_registration'].'?ok');
					exit;
				}
			}
			else { // nincs érvényesítés, azonnal regisztrálttá vált
				if((bool)$env->c['users']['noticeadminreg']) { // értesítő levél küldése az adminisztrátornak

				}
				header('Location: /'.$env->l['users']['url_registration'].'?ok');
				exit;
			}
		}
	}


			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());
$smarty->display('users/registration.tpl');
break;

/* ***************************************************************************** */
case $env->l['users']['url_resend']:
	$auth->isAuthRequired(false);

	if(!(bool)$env->c['users']['registration']) { // nincs engedélyezve a nyílt regisztráció
		header('Location: /'.$env->l['users']['url_login']);
		exit;
	}

	if((bool)$env->c['users']['mailvalidation'] && isset($_SESSION['registration_user_id'])) { // érvényesítő levél küldése
		$res = $db->Query("SELECT name, mail, randomkey FROM "._DBPREF."users_registration r
		LEFT JOIN "._DBPREF."users u ON r.user_id = u.id
		WHERE user_id = '".$_SESSION['registration_user_id']."' LIMIT 1");
		if(!$db->numRows($res)) { // nincs már ilyen key, aktivált közben
			header('Location: /'.$env->l['users']['url_registration'].'?ok');
			exit;
		}
		$row = $db->fetchArray($res);

		/** Rendszerosztály: e-mail küldés */
		require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
		$phpMailer = new PHPMailer();
		$phpMailer = $phpMailer;

		// levélküldő osztály feltöltése és levélküldés
		$phpMailer->CharSet = 'UTF-8';
		$phpMailer->From = $env->l['mail']['from'];
		$phpMailer->FromName = $env->l['mail']['fromname'];
		$phpMailer->Subject = $env->l['users']['mail_validationsubject'];
		$phpMailer->AddAddress($row['mail']);
		$phpMailer->IsHTML(true);

		$smarty->assign('subject', $phpMailer->Subject);
		$smarty->assign('name', $row['name']);
		$smarty->assign('body', $env->l['users']['mail_validationbody'].'<br /><br />
		<a href="'.$env->base.'/'.$env->l['users']['url_validation'].'/'.$row['randomkey'].'?utm_source=validation_by_resend&utm_medium=email&utm_campaign=noreply" target="_blank">'.$env->base.'/'.$env->l['users']['url_validation'].'/'.$row['randomkey'].'</a>');

		$phpMailer->Body = $smarty->fetch('mail.tpl');

		if(!$phpMailer->Send()) { // sikertelen levélküldés, átmeneti user törlése
			$registration->clearFailedRegistration($_SESSION['registration_user_id']);
			$smarty->assign('error', 'error_failedreg');
		}
		else { // sikeres levélküldés
			header('Location: /'.$env->l['users']['url_registration'].'?ok');
			exit;
		}
	}
	else {
		header('Location: /'.$env->l['users']['url_login']);
		exit;
	}
//$smarty->assign('activities', $env->getActivities());
break;

/* ***************************************************************************** */
case $env->l['users']['url_validation']:
	$auth->isAuthRequired(false);

	if(!(bool)$env->c['users']['registration'] OR !(boolean)$env->c['users']['mailvalidation']) { // nincs engedélyezve a nyílt regisztráció
		header('Location: /'.$env->l['users']['url_login']);
		exit;
	}

	/** Modulosztály: felhasználói regisztráció kezelése */
	require(ROOT.'/modules/users/Registration.class.php');
	$validation = new Registration();
	$validation->env = $env;

	if(!$validation->doValidation($env->u[2])) { // van hibaüzenet, nem megfelelő kód
		$smarty->assign('error', $validation->error);
	}
	else { // sikeres érvényesítés
		if((bool)$env->c['users']['noticeadminreg']) { // értesítő levél küldése az adminisztrátornak

		}
		// eltesszük a címét a belépést segítő linkhez
		//$_SESSION['validation_mail'] = $validation->user['mail'];

		// rögtön be is léptetjük
		$auth->setUserSession($validation->user['id']);

		header('Location: /'.$env->l['users']['url_profile'].'/'.$_SESSION['user']['alias'].'?welcome');
		exit;
	}

			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());
//$smarty->assign('activities', $env->getActivities());
$smarty->display('users/validation.tpl');
break;

/* ***************************************************************************** */
case $env->l['users']['url_lostpass']:
	$auth->isAuthRequired(false);

	/** Modulosztály: jelszó újraküldés és új jelszó érvényesítés */
	require(ROOT.'/modules/users/Lostpass.class.php');
	$lostpass = new Lostpass();
	$lostpass->env = $env;

	if(isset($_POST['action']) && $_POST['action'] == 'do') { // van elküldött adat
		if(!$lostpass->doLostpass($_POST['user'])) { // van hibaüzenet
			$smarty->assign('form', $lostpass->getLostpassFormValues());
			$smarty->assign('error', $lostpass->error);
		}
		else { // sikeres az új jelszó generálás és adatbázisba írás
			/** Rendszerosztály: e-mail küldés */
			require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
			$phpMailer = new PHPMailer();

			// levélküldő osztály feltöltése és levélküldés
			$phpMailer->CharSet = 'UTF-8';
			$phpMailer->From = $env->l['mail']['from'];
			$phpMailer->FromName = $env->l['mail']['fromname'];
			$phpMailer->Subject = $env->l['users']['mail_lostpasssubject'];
			$phpMailer->AddAddress($lostpass->user['mail']);
			$phpMailer->IsHTML(true);

			$smarty->assign('subject', $phpMailer->Subject);
			$smarty->assign('name', $lostpass->user['name']);
			$smarty->assign('body', $env->l['users']['mail_newpass'].'<br /><br />
			'.$lostpass->newPass.'<br /><br />
			'.$env->l['users']['mail_lostpassbody'].'<br /><br />
			<a href="'.$env->base.'/'.$env->l['users']['url_newpass'].'/'.$lostpass->randomKey.'?utm_source=lostpass&utm_medium=email&utm_campaign=noreply" target="_blank">'.$env->base.'/'.$env->l['users']['url_newpass'].'/'.$lostpass->randomKey.'</a>');

			$phpMailer->Body = $smarty->fetch('mail.tpl');
			if(!$phpMailer->Send()) { //
				die('hiba');
			}

			// eltesszük a címét a belépést segítő linkhez
		//	$_SESSION['lostpass_mail'] = $lostpass->user['mail'];

			header('Location: /'.$env->l['users']['url_lostpass'].'?ok');
			exit;
		}
	}
/*
	// postafiók szolgáltató felajánlása
	if(isset($_SESSION['lostpass_mail']) && $mailService = $auth->getMailService($_SESSION['lostpass_mail'])) {
		$smarty->assign('mailservice', $mailService);
	}*/

			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());
//$smarty->assign('activities', $env->getActivities());
$smarty->display('users/lostpass.tpl');
//unset($_SESSION['lostpass_mail']);
break;

/* ***************************************************************************** */
case $env->l['users']['url_newpass']:
	$auth->isAuthRequired(false);

	/** Modulosztály: jelszó újraküldés és új jelszó érvényesítés */
	require(ROOT.'/modules/users/Lostpass.class.php');
	$newpass = new Lostpass();
	$newpass->env = $env;

	if(!$newpass->doNewpass($env->u[2])) { // van hibaüzenet
		$smarty->assign('error', $newpass->error);
	}
	else { // sikeres érvényesítés
		// eltesszük a címét a belépést segítő linkhez
	//	$_SESSION['newpass_mail'] = $newpass->user['mail'];

		header('Location: /'.$env->l['users']['url_newpass'].'?ok');
		exit;
	}

			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());
//$smarty->assign('activities', $env->getActivities());
$smarty->display('users/newpass.tpl');
break;

/* ***************************************************************************** */
default:

	$env->setHeader(404);

}

?>
