<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

/* ***************************************************************************** */

switch($env->u[1]) {
/* ***************************************************************************** */
case 'captcha_registration': // ellenőrző captcha a regisztrációnál

	define('CAPTCHA_SESSION_ID', 'captcha_registration');
	require(ROOT.'/system/PhpCaptcha.class.php');

break;

/* ***************************************************************************** */
case 'get_regions': // régiók lekérdezése
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT id, region FROM "._DBPREF."system_regions WHERE country_id = '".(int)$_POST['parent']."'
	ORDER BY region COLLATE utf8_hungarian_ci ASC");
	echo '{items:[';
	$str = ($db->numRows($res)) ? '["","- Nincs megadva -"],' : '';
	while($row = $db->fetchArray($res)) {
		$str .= '['.$row['id'].',"'.htmlspecialchars($row['region'], ENT_QUOTES).'"],';
	}
	echo rtrim($str, ',');
	echo ']}';

break;

/* ***************************************************************************** */
case 'get_cities': // települések lekérdezése
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT id, city FROM "._DBPREF."system_cities WHERE region_id = '".(int)$_POST['parent']."'
	ORDER BY city COLLATE utf8_hungarian_ci ASC");
	echo '{items:[';
	$str = ($db->numRows($res)) ? '["","- Nincs megadva -"],' : '';
	while($row = $db->fetchArray($res)) {
		$str .= '['.$row['id'].',"'.htmlspecialchars($row['city'], ENT_QUOTES).'"],';
	}
	echo rtrim($str, ',');
	echo ']}';

break;

/* ***************************************************************************** */
case 'add_friend': // barátnak jelöl vagy elfogadja a meghívást
	if(!$env->isAjax() || !isset($_SESSION['user'])) { die; }

	$res_user = $db->Query("SELECT id, name, mail, alias, notify FROM "._DBPREF."users WHERE id = '".(int)$_POST['user_id']."' AND status = '2' LIMIT 1");
	if(!$db->numRows($res_user)) { // aktiválatlna juzert akar jelölni
		echo 'Sajnáljuk, ez a felhasználó még nem aktiválta a regisztrációját, így nem tudod bejelölni';
		die;
	}


	$res_fs = $db->Query("SELECT user_id FROM "._DBPREF."users_friendship WHERE user_id = '".$_SESSION['user']['id']."' AND partner_id = '".(int)$_POST['user_id']."' LIMIT 1");
	if(!$db->numRows($res_fs)) {

		$db->Query("INSERT INTO "._DBPREF."users_friendship (user_id, partner_id, datetime) VALUES ('".$_SESSION['user']['id']."', '".(int)$_POST['user_id']."', NOW())");

		$res_pd = $db->Query("SELECT user_id FROM "._DBPREF."users_friendship WHERE user_id = '".(int)$_POST['user_id']."' AND partner_id = '".$_SESSION['user']['id']."' LIMIT 1");
		if($db->numRows($res_fs)) { // ő már bejelölt engem korábban, tehát ez egy visszakövetés
			$mail_subject = $_SESSION['user']['name'].' szintén hozzáadott a barátaihoz';
			$mail_body = $_SESSION['user']['name']." szintén hozzáadott a barátaihoz, így mostantól ő is követi milyen témákat küldesz be, mihez szólsz hozzá:";
		}
		else {
			$mail_subject = $_SESSION['user']['name'].' hozzáadott a barátaihoz';
			$mail_body = $_SESSION['user']['name']." hozzáadott a barátaihoz. Ha érdekesnek találod őt, jelöld vissza, így követheted milyen témákat küld be, mihez szól hozzá:";
		}

		$activity_mess = 'hozzáadott a barátaihoz';
		$res = $db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
		VALUES ('5', '".$_SESSION['user']['id']."', '".$db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', '".(int)$_POST['user_id']."', NULL, NULL, '".$db->escape($activity_mess)."', NOW())");

		$row_m = $db->fetchArray($res_user);
		if($row_m['notify'] == 1) { // van e-mail küldés
			/** Rendszerosztály: megjelenítés */
			require('../../system/libraries/smarty/Smarty.class.php');
			$smarty = new Smarty();
			$smarty->template_dir = '../../templates/'.$env->template;
			$smarty->compile_dir = '../../system/cache/smarty_templates_c_'.$env->template;
			$smarty->cache_dir = '../../system/cache/smarty_cache_'.$env->template;

			/** Rendszerosztály: e-mail küldés */
			require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
			$phpMailer = new PHPMailer();

			// levélküldő osztály feltöltése és levélküldés
			$phpMailer->CharSet = 'UTF-8';
			$phpMailer->From = $env->l['mail']['from'];
			$phpMailer->FromName = $env->l['mail']['fromname'];
			$phpMailer->Subject = $mail_subject;
			$phpMailer->AddAddress($row_m['mail']);
			$phpMailer->IsHTML(true);

			$smarty->assign('subject', $mail_subject);
			$smarty->assign('name', $row_m['name']);
			$smarty->assign('body',  $mail_body.'<br /><br />
			<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$_SESSION['user']['alias'].'?utm_source=friend&utm_medium=mail&utm_campaign=nrp" target="_blank">'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$_SESSION['user']['alias'].'</a>');

			$phpMailer->Body = $smarty->fetch('mail.tpl');
			$phpMailer->Send();

		}
		echo 'Rendben, a felhasználót hozzáadtad a barátaidhoz.';

	}


break;

/* ***************************************************************************** */
case 'delete_friend': // töröl egy kedvenc felhasználót
	if(!$env->isAjax() || !isset($_SESSION['user'])) { die; }

	// nem aktiválatlan akit jelölni akar, és ő maga aktivált felhasználó
/*	$res = $db->Query("SELECT id FROM "._DBPREF."users WHERE (id = '".(int)$_POST['user_id']."' AND status <> 1) OR (id = '".$_SESSION['user']['id']."' AND status = '2') LIMIT 2");
	if($db->numRows($res) != 2) { // valamelyik feltétel nem teljesül
		$env->setHeader(403);
	}
*/

	$db->Query("DELETE FROM "._DBPREF."users_friendship WHERE user_id = '".$_SESSION['user']['id']."' AND partner_id = '".(int)$_POST['user_id']."' LIMIT 1");

	echo 'Rendben, a felhasználó törlésre került a barátaid közül.';


	// ő jelölt engem, most visszautasítom
/*	$res_remove = $db->Query("UPDATE "._DBPREF."users_friends SET approved = '0' WHERE user_id_1 = '".(int)$_POST['user_id']."' AND user_id_2 = '".$_SESSION['user']['id']."' AND approved = '1' LIMIT 1");

	if(!$db->affectedRows($res_remove)) { // nincs ilyen sor, akkor én jelöltem előbb
		$res_del = $db->Query("DELETE FROM "._DBPREF."users_friends WHERE user_id_1 = '".$_SESSION['user']['id']."' AND user_id_2 = '".(int)$_POST['user_id']."' AND approved = '0' LIMIT 1");

		if(!$db->affectedRows($res_del)) { // nincs ilyen sor, akkor én jelöltem előbb, de visszajelölt
			$res_del2 = $db->Query("DELETE FROM "._DBPREF."users_friends WHERE user_id_1 = '".$_SESSION['user']['id']."' AND user_id_2 = '".(int)$_POST['user_id']."' AND approved = '1' LIMIT 1");

			if($db->affectedRows($res_del2)) { // van ilyen sor, az ő jelölését vissza kell tenni
			$res_ins = $db->Query("INSERT INTO "._DBPREF."users_friends (user_id_1, user_id_2, approved) VALUES ('".(int)$_POST['user_id']."', '".$_SESSION['user']['id']."', '0')");
			}
		}
	}

	if($db->affectedRows($res_remove) || $res_del || $res_del2 || $res_ins) { // sikeres törlés
		// profil értesítés
//		$db->Query("INSERT INTO "._DBPREF."users_activities (u1, u2, act, datetime) VALUES ('".$_SESSION['user']['id']."', '".(int)$_POST['user_id']."', '4', NOW())");

		echo 'Rendben, a felhasználó törlésre került a barátaid közül.';
	}*/

break;

/* ***************************************************************************** */
case 'delete_message': // üzenetek törlése
	if(!$env->isAjax() || !isset($_SESSION['user'])) { die; }

//	$res = $db->Query("SELECT sender_id, recipient_id, sender_del, recipient_del FROM "._DBPREF."users_messages WHERE id = '".(int)$_GET['message_id']."' LIMIT 1");
//	$row = $db->fetchArray($res);

	$db->Query("DELETE FROM "._DBPREF."users_messages WHERE id = '".(int)$_GET['message_id']."' AND user_id_inbox = '".(int)$_SESSION['user']['id']."' LIMIT 1");
	echo 'OK';


/*
	if(($row['sender_del'] == 0) && ($row['recipient_del'] == 0)) { // egyik sem törölte még, update
        $field = ($row['sender_id'] == $_SESSION['user']['id']) ? 'sender_del' : 'recipient_del';

		$db->Query("UPDATE "._DBPREF."users_messages SET ".$field." = '1' WHERE id = '".(int)$_GET['message_id']."' LIMIT 1");
		echo 'OK';
	}
	else { // valamelyik törölte már

        if($row['sender_id'] == $_SESSION['user']['id']) { // feladó töröl
			if($row['recipient_del'] == 1) {
				$db->Query("DELETE FROM "._DBPREF."users_messages WHERE id = '".(int)$_GET['message_id']."' LIMIT 1");
			}
			else {
				$db->Query("UPDATE "._DBPREF."users_messages SET sender_del = '1' WHERE id = '".(int)$_GET['message_id']."' LIMIT 1");
			}
			echo 'OK';
		}


        if($row['recipient_id'] == $_SESSION['user']['id']) { // címzett töröl
			if($row['sender_del'] == 1) {
				$db->Query("DELETE FROM "._DBPREF."users_messages WHERE id = '".(int)$_GET['message_id']."' LIMIT 1");
			}
			else {
				$db->Query("UPDATE "._DBPREF."users_messages SET recipient_del = '1' WHERE id = '".(int)$_GET['message_id']."' LIMIT 1");
			}
			echo 'OK';
		}

	}
*/

break;

/* ***************************************************************************** */
case 'ajax_remove_activities': // értesítés törlése
/*	if(!$env->isAjax() || !isset($_SESSION['user'])) { die; }

	$res_del = $db->Query("DELETE FROM "._DBPREF."users_activities WHERE id = '".(int)$_GET['user_id']."' AND u2 = '".$_SESSION['user']['id']."' LIMIT 1");

	if($res_del)
	echo 'OK';
*/
break;

/* ***************************************************************************** */
case 'recipient_autocomplete': // címzett kiválasztása az üzenetküldőnél
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT id, name FROM "._DBPREF."users WHERE name COLLATE utf8_hungarian_ci LIKE '".$db->escape($_GET['q'])."%' AND status = '2' AND id <> '".$_SESSION['user']['id']."' ORDER BY name ASC LIMIT 50");
	while($row = $db->fetchArray($res)) {
		echo htmlspecialchars($row['name'], ENT_QUOTES).'|'.$row['id']."\n";
	}

break;

/* ***************************************************************************** */
default:

	$env->setHeader(404);

}

?>
