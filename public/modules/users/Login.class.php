<?php
/**
* Be- és kijelentkezés kezelése
* @package SWEN
* @subpackage users
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/

/** False attempt azonosító: bejelentkezés */
define('LOGIN', 1);

/**
* Modulosztály: be- és kijelentkezés kezelése
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
class Login extends Auth {

	/**
	* Beléptetés megjegyzése
	* @var boolean
	*/
	var $note;

	/**
	* E-mail cím megőrzése
	* @var boolean
	*/
	var $store;

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Login() {

	}

	/**
	* Felhasználói bejelentkezés ellenőrzése, sikeres azonosítás után a munkamenet beállítása
	* @param array $user Felhasználó tulajdonságai
	* @param boolean $note Beléptetés megjegyzése
	* @param boolean $store E-mail cím megjegyzése
	* @return boolean Sikeres beléptetés esetén true-val, egyébként false-sal tér vissza
	*/
	function doLogin($user, $note = 0, $store = 0) {

		$this->user = $user;
		$this->note = $note;
		$this->store = $store;

		if(!$this->checkErrorLog($this)) { // tömeges űrlapküldések lekérdezése
			$this->error = 'error_toomanybad';
			return false;
		}

		if(!$this->checkEmailFormat($this->user['mail'])) { // nem megfelelő e-mail cím formátum
			$this->error = 'error_wrongmail';

			$this->setErrorLog($this);
			return false;
		}

		if(!$this->checkPassFormat($this->user['pass'], (int)$this->env->c['users']['passlength'])) { // nem megfelelő jelszó formátum
			$this->error = 'error_wrongpass';

			$this->setErrorLog($this);
			return false;
		}

		// hashkódolt jelszóforma előállítása
		$passHash = $this->getHashmark($this->user['pass']);

		$mail = $this->env->db->escape($this->user['mail']);

		// belépési adatok lekérdezése az adatbázisból
		$res = $this->env->db->Query("SELECT id, mail, status, name, alias FROM "._DBPREF."users WHERE mail COLLATE utf8_general_ci = '".$mail."' AND pass = '".$passHash."' LIMIT 1");

		if($this->env->db->numRows($res) != 1) { // nem megfelelő e-mail cím vagy jelszó
			$this->error = 'error_wrongacc';

			$this->setErrorLog($this);
			return false;
		}

		$row = $this->env->db->fetchArray($res);

		if($row['status'] == 1) { // nincs aktiválva
			$this->error = 'error_activate';

			$this->setErrorLog($this);
			return false;
		}

		if($this->env->isDenied() || ($row['status'] == 0)) { //if($row['status'] == 0) {
			$this->error = 'error_notallowed';

			$this->setErrorLog($this);
			return false;
		}

		/*
		**********************************************************************************
		*/

		if(empty($this->error)) { // nincs hiba, megtörténhet a beléptetés, munkamenet beállítása
			$cookie = ($this->note == 1) ? true : false; // megjegyzés cookieval
			$this->setLogged($row, $cookie);

			if($this->store == 1) { // e-mail cím megőrzése az input mező számára
				setcookie('mail', $row['mail'], time()+(int)$this->env->c['users']['mailcookietime'], '/', false);
			}
			if(isset($_COOKIE['mail']) && $this->store == 0) { // volt megőrzött e-mail cím, de ezúttal már nem szükséges
				setcookie('mail', NULL, 0, '/', false);
			}
			return true;
		}
		return false;

	}

	/**
	* Visszaadja a megjelenítés számára a form mezők értékeit asszociatív tömbben
	* @return array A form mezőket tartalmazó tömbbel tér vissza
	*/
	function getLoginFormValues() {

		return array(
		'mail' => $this->user['mail'],
		'note' => $this->note,
		'store' => $this->store
		);

	}

	/**
	* Felhasználói kiléptetés elvégzése, a munkamenet, valamint ha van, a cookie törlése
	* @return void
	*/
	function doLogout() {

		$_SESSION['user'] = array();
		unset($_SESSION['user']);

		if(isset($_COOKIE['user_id'])) { // fel volt jegyezve cookie-val is
			setcookie('user_id', '', 0, '/', $this->env->domainBase, false);
			setcookie('sc2', '', 0, '/', $this->env->domainBase, false);
		}

	}

}

?>
