<?php
/**
* Jelszó újraküldés és új jelszó érvényesítés
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
class Lostpass extends Auth {

	/**
	* Új véletlen generált jelszó
	* @var string
	*/
	var $newPass;

	/**
	* Érvényesítő kulcs
	* @var string
	*/
	var $randomKey;

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Lostpass() {

	}

	/**
	* Új jelszó generálásához szükséges érvényesítés elkészítése
	* @param array $user Felhasználó tulajdonságai
	* @return boolean Sikeres műveéet esetén true-val, egyébként false-sal tér vissza
	*/
	function doLostpass($user) {

		$this->user = $user;

		if(!$this->checkErrorLog($this)) { // tömeges űrlapküldések lekérdezése
			$this->error = 'error_toomanybad';
			return false;
		}

		if(!$this->checkEmailFormat($this->user['mail'])) { // nem megfelelő e-mail cím formátum
			$this->error = 'error_wrongmail';

			$this->setErrorLog($this);
			return false;
		}

		$mail = $this->env->db->escape($this->user['mail']);

		// felhasználói adatok lekérdezése, létezik-e ilyen felhasználó, és nem tiltott
		$res_user = $this->env->db->Query("SELECT id, mail, name FROM "._DBPREF."users WHERE mail = '".$mail."' AND status <> '0' LIMIT 1");
		$row_user = $this->env->db->fetchArray($res_user);

		if($this->env->db->numRows($res_user) != 1) { // nem megfelelő e-mail cím vagy név
			$this->error = 'error_wronglostacc';

			$this->setErrorLog($this);
			return false;
		}

		$this->user = $row_user;

		/*
		**********************************************************************************
		*/

		if(empty($this->error)) { // nincs hiba, megtörténhet az új jelszó felvétele az ideiglenes táblába
			$this->newPass = $this->getRandomPass((int)$this->env->c['users']['passlength']);
			$this->randomKey = $this->getRandomKey((int)$this->env->c['users']['keylength']);

			// új hashkódolt jelszóforma generálása
			$newPassHash = $this->getHashmark($this->newPass);

			$res_another = $this->env->db->Query("SELECT user_id FROM "._DBPREF."users_lostpass WHERE user_id = '".$this->user['id']."' LIMIT 1");
			if($this->env->db->numRows($res_another) == 1) { // van már folyamatban lévő jelszó újraküldés, azt kell felülírni
				// felhasználó felülírása a lostpass táblában
				$res_temp = $this->env->db->Query("UPDATE "._DBPREF."users_lostpass
				SET randomkey = '".$this->randomKey."',
				newpass = '".$newPassHash."',
				datetime = NOW()
				WHERE user_id = '".$this->user['id']."' LIMIT 1");
			}
			else { // új jelszó újraküldés, be kell szúrni az érvényesítő sort
				// felhasználó beírása a lostpass táblába
				$res_temp = $this->env->db->Query("INSERT
				INTO "._DBPREF."users_lostpass (user_id, randomkey, newpass, datetime)
				VALUES ('".$this->user['id']."', '".$this->randomKey."', '".$newPassHash."', NOW())");
			}

			if(!$res_temp) { // sikertelen volt a beírás a temp táblába
				$this->error = 'error_failedlostpass';
				return false;
			}
			return true;
		}

	}

	/**
	* Visszaadja a megjelenítés számára a form mezők értékeit asszociatív tömbben
	* @return array A form mezőket tartalmazó tömbbel tér vissza
	*/
	function getLostpassFormValues() {

		return $this->user;

	}

	/**
	* Érvényesítő levélben kapott kód ellenőrzése, és a jelszó újraküldés befejezése
	* @param string $randomKey E-mail érvényesítő kód
	* @return boolean Sikeres érvényesítés esetén true-val, egyébként false-sal tér vissza
	*/
	function doNewpass($randomKey) {

		$this->randomKey = $randomKey;

		if(!$this->checkErrorLog($this)) { // tömeges űrlapküldések lekérdezése
			$this->error = 'error_toomanybad';
			return false;
		}

		if(!$this->checkRandomKeyFormat($this->randomKey, (int)$this->env->c['users']['keylength'])) { // nem megfelelő kulcs formátum
			$this->error = 'error_wrongkeyformat';

			$this->setErrorLog($this);
			return false;
		}

		// escapelt stringforma elkészítése
		$randomKey = $this->env->db->escape($this->randomKey);

		// érvényesítőkód lekérdezése az adatbázisból
		$res = $this->env->db->Query("SELECT user_id, newpass FROM "._DBPREF."users_lostpass WHERE randomkey = '".$randomKey."' LIMIT 1");
		$row = $this->env->db->fetchArray($res);

		if($this->env->db->numRows($res) != 1) { // nem megfelelő érvényesítőkód
			$this->error = 'error_wrongkeyformat';

			$this->setErrorLog($this);
			return false;
		}
		else { // helyes érvényesítő kód
			$res_upd = $this->env->db->Query("UPDATE "._DBPREF."users SET pass = '".$row['newpass']."'
			WHERE id = '".$row['user_id']."' LIMIT 1");

			if(!$res_upd) { // sikertelen volt a felülírás
				$this->setErrorLog($this);
				return false;
			}
			else { // sikeres felülírás, lostpass táblából törlés
				$res_del = $this->env->db->Query("DELETE FROM "._DBPREF."users_lostpass WHERE user_id = '".$row['user_id']."' LIMIT 1");

				$res_mail = $this->env->db->Query("SELECT mail FROM "._DBPREF."users WHERE id = '".$row['user_id']."' LIMIT 1");
				$this->user = $this->env->db->fetchArray($res_mail);

				return true;
			}
			return false;
		}

	}

}

?>
