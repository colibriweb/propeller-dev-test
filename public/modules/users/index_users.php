<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */

require(ROOT.'/system/Pagination.class.php');



// dolgok
$smarty->assign('misc', $content->q_misc());


    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));

switch($env->u[1]) {
/* ***************************************************************************** */
case $env->l['users']['url_settings']:
	$auth->isAuthRequired(true);

	/** Modulosztály: felhasználói beállítások kezelése */
	require(ROOT.'/modules/users/Settings.class.php');
	$settings = new Settings();
	$settings->env = $env;

	if(isset($_POST['action']) && $_POST['action'] == 'do') { // van elküldött adat
		if(!$settings->doSettings($_POST['user'], $_SESSION['user']['id'])) { // van hibaüzenet
			$smarty->assign('user', $settings->getSettingsFormValues());
			$smarty->assign('error', $settings->error);
		}
		else {
			if(!empty($_FILES['avatar']['name'])) {
				/** Rendszerosztály: képkezelés és feltöltés */
				require(ROOT.'/system/Image.class.php');
				$image = new Image($_FILES['avatar']);
				$image->imgBase = ROOT.'_static/avatar/';
				$image->fileID = $_SESSION['user']['id'];

				if(!$image->saveForAvatar()) {
					$smarty->assign('user', $settings->getSettingsFormValues());
					$smarty->assign('error', $image->error);
				}
				else {
					$env->setOverlayNotification('Beállítások és arckép fotó elmentve.');
					$db->Query("UPDATE "._DBPREF."users SET avatar = '".time()."' WHERE id = '".$_SESSION['user']['id']."' LIMIT 1");
					header('Location: /'.$env->l['users']['url_profile'].'/'.$_SESSION['user']['alias']);
					exit;
				}
			}
			else {
				$env->setOverlayNotification('Beállítások elmentve.');
				header('Location: /'.$env->l['users']['url_profile'].'/'.$_SESSION['user']['alias']);
				exit;
			}
		}
	}
	else { // nincs küldött adat
		$res = $db->Query("SELECT u.id, mail, notify, notify_act, name, alias, firstname, lastname,
			datetime, modtime, pubmail, birthdate, profession, country_id, city_id, website, motto,
			t_like, t_dislike, topics, politics, city, avatar
		FROM "._DBPREF."users u
		LEFT JOIN "._DBPREF."system_cities c ON u.city_id = c.id WHERE u.id = '".$_SESSION['user']['id']."' LIMIT 1");
		$row = $db->fetchArray($res);

		$row['birthyear'] = substr($row['birthdate'], 0, 4);
		$row['birthmonth'] = (int)substr($row['birthdate'], 5, 2);
		$row['birthday'] = (int)substr($row['birthdate'], 8, 2);
		$row['avatarsrc'] = !empty($row['avatar']) ? STTC.'/avatar/'.floor($row['id']/1000).'/'.$row['id'].'/'.$row['id'].'_thumb.jpg?'.$row['avatar'] : STTC.'/avatar/default_thumb.jpg';

		$smarty->assign('user', $row);
	}

	// legfrissebb
	$smarty->assign('latest', $content->q_latest());

	// bal és középső hasábok, fejléc kiemelések
	$smarty->assign('blocks', $content->q_blocks());

	$smarty->assign('year', range(date('Y')-100, date('Y')-1));
	$smarty->assign('month', range(1, 12));
	$smarty->assign('day', range(1, 31));
	$smarty->assign('activities', $env->getActivities());
	$smarty->display('users/settings.tpl');
break;

/* ***************************************************************************** */
case $env->l['users']['url_logout']:

	if(isset($_GET['ok'])) {
			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());

		$smarty->assign('activities', $env->getActivities());
		$smarty->display('users/logout.tpl');
	}
	else {
		$auth->isAuthRequired(true);

		/** Modulosztály: be- és kijelentkezés kezelése */
		require(ROOT.'/modules/users/Login.class.php');
		$login = new Login();
		$login->env = $env;

		$login->doLogout();

//		header('Location: /'.$env->l['users']['url_logout'].'?ok');
		header('Location: /');
		exit;
	}
break;

/* ***************************************************************************** */
case $env->l['users']['url_profile']: // profil oldal

 	// amíg a keresőből ki nem pusztulnak a régi címek
	if($env->u[1] == $env->l['users']['url_profile']) {
		if($env->u[2] == 'predikator') $new_al = 'dr_predikator';
		elseif($env->u[2] == 'Bojti_Banyus') $new_al = 'Rezangyal';
		elseif($env->u[2] == 'luigilongo60') $new_al = 'Dr_luigilongo60';
		elseif($env->u[2] == 'brumi58') $new_al = 'Dr_Akula';

		if(isset($new_al)) {
			header('HTTP/1.1 301 Moved Permanently');
			if(!empty($env->u[3]))
			header('Location: /'.$env->l['users']['url_profile'].'/'.$new_al.'/'.$env->u[3]);
			else
			header('Location: /'.$env->l['users']['url_profile'].'/'.$new_al);
			exit;
		}
	}


	if(isset($_GET['show']) && $_GET['show'] == 'b') {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /'.$env->l['users']['url_profile'].'/'.$env->u[2]);
			exit;
	}
	elseif(isset($_GET['show']) && $_GET['show'] == 'o') {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /'.$env->l['users']['url_profile'].'/'.$env->u[2].'/bekuldesek');
			exit;
	}
	elseif(isset($_GET['show']) && $_GET['show'] == 'c') {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /'.$env->l['users']['url_profile'].'/'.$env->u[2]);
			exit;
	}
	elseif(isset($env->u[3]) && $env->u[3] == 'hozzaszolasok') {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /'.$env->l['users']['url_profile'].'/'.$env->u[2]);
			exit;
	}



	if($env->u[1] == $env->l['users']['url_profile']) {
		$user_alias = $db->escape($env->u[2]);
	}
	elseif($env->u[1] == $env->l['users']['url_messages']) {
		$user_alias = $_SESSION['user']['alias'];
	}

	// alap adatok
	$res = $db->Query("SELECT u.id, mail, status, name, alias, firstname, lastname, datetime, modtime, pubmail, floor(DATEDIFF(now(), birthdate)/365.2425) AS age,
		profession, city_id, website, motto, t_like, t_dislike, topics, politics, usertype, avatar, city
	FROM "._DBPREF."users u
	LEFT JOIN "._DBPREF."system_cities c ON u.city_id = c.id
	WHERE alias COLLATE utf8_general_ci = '".$user_alias."' AND (status = '0' OR status = '1' OR status = '2') LIMIT 1"); //AND (status = '1' OR status = '2'), mindenkinek látszik a profilja, tiltottnak is (12/06/07)
	if(!$db->numRows($res)) $env->setHeader(404);
	$row = $db->fetchArray($res);

	//$s = strtotime($row['datetime']);
	//$row['datetime'] = date('Y. ', $s).$env->l['months']['month'.date('m', $s)].' '.date('j', $s).'.'; // regisztráció ideje

	if(empty($row['politics']))
	$row['pol'] = "";
	elseif(in_array($row['politics'], array('1','2','3','4','5','6','7')))
	$row['pol'] = $env->l['users']['pol'.$row['politics']];
	else
	$row['pol'] = $row['politics'];

	$row['avatarsrc'] = !empty($row['avatar']) ? STTC.'/avatar/'.floor($row['id']/1000).'/'.$row['id'].'/'.$row['id'].'_thumb.jpg?'.$row['avatar'] : STTC.'/avatar/default_thumb.jpg';

	$smarty->assign('user', $row);


	if(empty($env->u[3])) { // XY hozzászólásai

		$smarty->assign('titleplus', 'Hozzászólások');

		$p = new Pagination((int)@$_GET['page'], 30);

		$res_s = $db->Query("#PROFIL_HOSSZASZOLASOK
		SELECT SQL_CALC_FOUND_ROWS c.id, c.category_id, c.title, c.link, CONCAT(c.id,'-',c.alias,'?all#c',co.id) AS alias, c.comments, co.id AS comment_id, co.datetime, co.comment AS description, co.likes
		FROM "._DBPREF."content_comments co
		LEFT JOIN "._DBPREF."content c ON co.content_id = c.id
		WHERE co.id > ".(_SPD_COMMENT_ID)." AND co.user_id = ".$row['id']."
		ORDER BY co.datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);

		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);
		$p->results_num = $rows[0];
		$smarty->assign('p', $p->getPaging());

		$list = array();
		while($row_s = $db->fetchArray($res_s)) {
			$row_s['orighost'] = $env->getHost($row_s['link']);
			$row_s['category_alias'] = $env->l['content']['category_alias_'.$row_s['category_id']];
			$row_s['date'] = $env->dateToString($row_s['datetime']);
			$row_s['description'] = $env->stringToLink($row_s['description']);
			$row_s['description'] = $env->stringToUserlink($row_s['description']);
			$row_s['description'] = $row_s['description'];
			$list[] = $row_s;
		}

		if(!$list && ($p->results_from != 0)) $env->setHeader(404);
		$smarty->assign('list', $list);


	}
	elseif($env->u[3] == 'bekuldott') {

		$smarty->assign('titleplus', 'Beküldött hírek');

		$p = new Pagination((int)@$_GET['page'], 30);

		$res_s = $db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, t.link, t.description, CONCAT(t.id,'-',t.alias) AS alias, t.datetime, c.comments FROM "._DBPREF."content t
		LEFT JOIN "._DBPREF."content c ON t.id = c.id WHERE t.user_id = ".$row['id']."
		AND (t.content_type IS NULL OR t.content_type <> 7)
		ORDER BY t.datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);

		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);
		$p->results_num = $rows[0];
		$smarty->assign('p', $p->getPaging());

		while($row_s = $db->fetchArray($res_s)) {
			$row_s['orighost'] = $env->getHost($row_s['link']);
			$row_s['category_alias'] = $env->l['content']['category_alias_'.$row_s['category_id']];
			$row_s['date'] = $env->dateToString($row_s['datetime']);
			$row_s['description'] = $env->getExcerpt($row_s['description']);

			/*$res_t = $db->Query("SELECT tag FROM "._DBPREF."content_tags_conn conn
			INNER JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
			WHERE conn.content_id = ".$row_s['id']." LIMIT 5");
			while($row_t = $db->fetchAssoc($res_t)) {
				$row_s['tags'][] = $row_t['tag'];
			}*/

			$list[] = $row_s;
		}

		//if(!$list) $env->setHeader(404);
		$smarty->assign('list', $list);

	}
	elseif($env->u[3] == 'informaciok') {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: /'.$env->l['users']['url_profile'].'/'.$env->u[2].'/adatlap');
		exit;
	}
	elseif($env->u[3] == 'bekuldesek') {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: /'.$env->l['users']['url_profile'].'/'.$env->u[2].'/bekuldott');
		exit;
	}
	elseif($env->u[3] == 'adatlap') {

	}
	else {
		$env->setHeader(404);
	}



if(!isset($activities_del)) $smarty->assign('activities', $env->getActivities());

			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());

$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('users/profile.tpl');
break;

/* ***************************************************************************** */

case $env->l['users']['url_messages']: // üzenetek
	$auth->isAuthRequired(true);

	// alap adatok
	$res = $db->Query("SELECT u.id, mail, status, name, alias, firstname, lastname, datetime, modtime, pubmail, floor(DATEDIFF(now(), birthdate)/365.2425) AS age,
		profession, city_id, website, motto, t_like, t_dislike, topics, politics, usertype, avatar
	FROM "._DBPREF."users u
	WHERE id = '".$_SESSION['user']['id']."' LIMIT 1"); //AND (status = '1' OR status = '2'), mindenkinek látszik a profilja, tiltottnak is (12/06/07)
	$row = $db->fetchArray($res);
	$smarty->assign('user', $row);


	/** Modulosztály: felhasználói- és rendszerüzenetek kezelése */
	require_once('Messages.class.php');
	$messages = new Messages();
	$messages->env = $env;

	if(empty($env->u[2])) { // bejövő üzenetek

		$smarty->assign('titleplus', 'Üzeneteim');

		$p = new Pagination((int)@$_GET['page'], 20);

      $message_list = array();

		$res = $db->Query("SELECT SQL_CALC_FOUND_ROWS
		m.id AS msg_id, user_id_inbox, user_id_partner, user_id_sender, message, datetime, viewed,
		CONCAT(user_id_inbox,'-',user_id_partner) AS gfield
		FROM "._DBPREF."users_messages m
		WHERE user_id_inbox = '".$_SESSION['user']['id']."'
		AND id = (SELECT id FROM "._DBPREF."users_messages
							WHERE user_id_inbox = '".$_SESSION['user']['id']."' AND user_id_partner = m.user_id_partner
							ORDER BY datetime DESC LIMIT 0, 1)
		GROUP BY gfield ORDER BY datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);

		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);
		$p->results_num = $rows[0];
		$smarty->assign('p', $p->getPaging());

		if($rows[0]) { // vannak bejövői
			while($row = $db->fetchArray($res)) {
				$row['date'] = $env->dateToString($row['datetime']);

		/*		$ex = explode('|', $row['message']);
	            $row['last_sender_id'] = $ex[0];
	            $row['message'] = $ex[1];*/

				$res_u = $db->Query("SELECT name, alias, avatar FROM "._DBPREF."users WHERE id = '".$row['user_id_partner']."' LIMIT 1");
				$row = array_merge($row, $db->fetchArray($res_u));

				//$row['avatar'] = ($row['avatar'] == 1) ? floor($row['user_id_partner']/1000).'/'.$row['user_id_partner'].'/'.$row['user_id_partner'].'_normal.jpg' : 'default_normal.jpg';

				$message_list[] = $row;
			}
		}
//p($message_list);
		$smarty->assign('message_list', $message_list);

	}
	elseif($env->u[2] == $env->l['users']['url_messages_compose']) { // üzenet írás
		$smarty->assign('titleplus', 'Új személyes üzenet');


		if(isset($_GET['to_id'])) {
			$res_toid = $db->Query("SELECT name FROM "._DBPREF."users WHERE id = '".(int)$_GET['to_id']."' LIMIT 1");
			$row_toid = $db->fetchArray($res_toid);
			$smarty->assign('form', array('recipient' => $row_toid['name']));

		}

		if(isset($_POST['message'])) { // válasz küldés

			// címzett ID-jének lekérdezése az adatbázisból
			$res_recp = $db->Query("SELECT alias FROM "._DBPREF."users WHERE name = '".$db->escape($_POST['message']['recipient'])."' LIMIT 1");
			$row_recp = $db->fetchAssoc($res_recp);


			if(!$messages->sendMessage($_SESSION['user']['id'], $_POST['message'], $row_recp['alias'])) { // van hibaüzenet
				$smarty->assign('form', $messages->getMessagesFormValues());
				$smarty->assign('error', $messages->error);
			}
			else {

				$activity_mess = htmlspecialchars($_SESSION['user']['name'], ENT_QUOTES).' üzenetet küldött: <a href="'.$env->base.'/'.$env->l['users']['url_messages'].'/'.$_SESSION['user']['alias'].'">'.htmlspecialchars($env->strTruncate($_POST['message']['message'], 60, '...'), ENT_QUOTES).'</a>';
				$res = $db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, user_id_affected, content_id, comment_id, message, datetime)
				VALUES ('8', '".$_SESSION['user']['id']."', '".$messages->message['recipientID']."', NULL, NULL, '".$db->escape($activity_mess)."', NOW())");


				/* ezt itt át kell dolgozni, hogy mjd az aktivitásokból küldje a mailt   ---- miért is? */


				$notify = $messages->getNotification($messages->message['recipientID']);

				if($notify['notify']) { // kért e-mail értesítést
					/** Rendszerosztály: e-mail küldés */
					require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
					$phpMailer = new PHPMailer();

					// levélküldő osztály feltöltése és levélküldés
					$phpMailer->CharSet = 'UTF-8';
					$phpMailer->From = $env->l['mail']['from'];
					$phpMailer->FromName = $env->l['mail']['fromname'];
					$phpMailer->Subject = $_SESSION['user']['name'].' '.$env->l['users']['mail_newmessage'];
					$phpMailer->AddAddress($notify['mail']);
					$phpMailer->IsHTML(true);

					$smarty->assign('subject', $phpMailer->Subject);
					$smarty->assign('name', $notify['name']);
					$smarty->assign('body', $env->l['users']['mail_newmessagebody'].'<br /><br />
					<a href="'.$env->base.'/'.$env->l['users']['url_messages'].'/'.$_SESSION['user']['alias'].'?utm_source=messages&utm_medium=email&utm_campaign=noreply" target="_blank">'.$env->base.'/'.$env->l['users']['url_messages'].'/'.$_SESSION['user']['alias'].'</a>');

					$phpMailer->Body = $smarty->fetch('mail.tpl');
					$phpMailer->Send();
				}

				//header('Location: /'.$env->l['users']['url_messages'].'/'.$messages->message['recipient'].'#m'.$messages->message['id']);

				$env->setOverlayNotification('Üzenet elküldeve.');
				header('Location: /'.$env->l['users']['url_messages'].'/'.$messages->message['recipient']);
				exit;
			}
		}

	}
	elseif(!empty($env->u[2])) { // -------------------------- egy üzenet szál
		if(isset($_POST['message'])) { // válasz küldés
			if(!$messages->sendMessage($_SESSION['user']['id'], $_POST['message'], $env->u[2])) { // van hibaüzenet
				$smarty->assign('form', $messages->getMessagesFormValues());
				$smarty->assign('error', $messages->error);
			}
			else {
				$activity_mess = 'üzenetet küldött: <a href="'.$env->base.'/'.$env->l['users']['url_messages'].'/'.$_SESSION['user']['alias'].'">'.htmlspecialchars($env->strTruncate($_POST['message']['message'], 60, '...'), ENT_QUOTES).'</a>';
				$res = $db->Query("INSERT INTO "._DBPREF."users_activities (action, user_id, name, alias, user_id_affected, content_id, comment_id, message, datetime)
				VALUES ('8', '".$_SESSION['user']['id']."', '".$db->escape($_SESSION['user']['name'])."', '".$_SESSION['user']['alias']."', '".$messages->message['recipientID']."', NULL, NULL, '".$db->escape($activity_mess)."', NOW())");


				/* ezt itt át kell dolgozni, hogy mjd az aktivitásokból küldje a mailt  --- miért is nem jo így? */


				$notify = $messages->getNotification($messages->message['recipientID']);

				if($notify['notify']) { // kért e-mail értesítést
					/** Rendszerosztály: e-mail küldés */
					require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
					$phpMailer = new PHPMailer();

					// levélküldő osztály feltöltése és levélküldés
					$phpMailer->CharSet = 'UTF-8';
					$phpMailer->From = $env->l['mail']['from'];
					$phpMailer->FromName = $env->l['mail']['fromname'];
					$phpMailer->Subject = $_SESSION['user']['name'].' '.$env->l['users']['mail_newmessage'];
					$phpMailer->AddAddress($notify['mail']);
					$phpMailer->IsHTML(true);

					$smarty->assign('subject', $phpMailer->Subject);
					$smarty->assign('name', $notify['name']);
					$smarty->assign('body',  $env->l['users']['mail_newmessagebody'].'<br /><br />
					<a href="'.$env->base.'/'.$env->l['users']['url_messages'].'/'.$_SESSION['user']['alias'].'?utm_source=messages&utm_medium=email&utm_campaign=noreply" target="_blank">'.$env->base.'/'.$env->l['users']['url_messages'].'/'.$_SESSION['user']['alias'].'</a>');

					$phpMailer->Body = $smarty->fetch('mail.tpl');
					$phpMailer->Send();
				}

				//header('Location: /'.$env->l['users']['url_messages'].'/'.$messages->message['recipient'].'#m'.$messages->message['id']);

				$env->setOverlayNotification('Üzenet elküldeve.');
				header('Location: /'.$env->l['users']['url_messages'].'/'.$messages->message['recipient']);
				exit;
			}
		}

		$res = $db->Query("SELECT id, name, alias, avatar FROM "._DBPREF."users WHERE alias COLLATE utf8_general_ci = '".$db->escape($env->u[2])."' LIMIT 1");
		if(!$db->numRows($res) == 1) $env->setHeader(404);
		$row = $db->fetchArray($res);
		if($row['id'] == $_SESSION['user']['id']) $env->setHeader(404);
		$smarty->assign('message_partner', $row);

		$smarty->assign('titleplus', $row['name'].' üzenetei');

		// értesítések eltűntetése
		//$db->Query("UPDATE "._DBPREF."users_activities SET viewed = 1 WHERE action = 8 AND user_id = '".$row['id']."' AND user_id_affected = '".$_SESSION['user']['id']."' AND viewed = 0");
		//$activities_del = true;


		$p = new Pagination((int)@$_GET['page'], 10);

		/*
		kétszer van letárolva minden üzenet, egy-egy példányban mindkét félnek (user_id_inbox)
		a partner a címzett ID, a sender a feladó. törlésnél a megfelelő sort töröljük csak
		*/

		$res = $db->Query("SELECT SQL_CALC_FOUND_ROWS m.id AS msg_id, user_id_inbox, user_id_partner, user_id_sender, message, m.datetime, viewed, name, alias, avatar
		FROM "._DBPREF."users_messages m
		LEFT JOIN "._DBPREF."users u ON m.user_id_sender = u.id
		WHERE user_id_inbox = '".$_SESSION['user']['id']."' AND user_id_partner = '".$row['id']."'
		ORDER BY m.datetime DESC LIMIT ".$p->results_from.", ".$p->results_limit);


		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);
		$p->results_num = $rows[0];
		$smarty->assign('p', $p->getPaging());

	    $message_thread = array();
		$is_unreaded = false;
		while($row_b = $db->fetchAssoc($res)) {
			$row_b['date'] = $env->dateToPast($row_b['datetime']);
			$row_b['avatar'] = ($row_b['avatar'] == 1) ? floor($row_b['user_id_sender']/1000).'/'.$row_b['user_id_sender'].'/'.$row_b['user_id_sender'].'_normal.jpg' : 'default_normal.jpg';
			$message_thread[] = $row_b;


			// olvasottak beállítása
			if($row_b['viewed'] == 0) {
				$db->Query("UPDATE "._DBPREF."users_messages SET viewed = 1 WHERE id = '".$row_b['msg_id']."' LIMIT 1");
			}
		}

		$smarty->assign('message_thread', $message_thread);

	}


			// legfrissebb
			$smarty->assign('latest', $content->q_latest());

			// bal és középső hasábok, fejléc kiemelések
			$smarty->assign('blocks', $content->q_blocks());

$smarty->assign('activities', $env->getActivities());
$env->l['pg'] = number_format((microtime(true) - $time_start), 4);
$smarty->display('users/messages.tpl');

break;





/* ***************************************************************************** */
default:
	$auth->isAuthRequired(true);

	$env->setHeader(404);

}

?>