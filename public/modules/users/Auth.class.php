<?php
class Auth {

	/**
	* Környezeti tulajdonságok
	* @var Environment
	*/
	var $env;

	/**
	* Felhasználó tulajdonságai
	* @var array
	*/
	var $user = array();

	/**
	* Olvasatlan üzenetek száma
	* @var integer
	*/
	var $unreaded = 0;

	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat, és beállítja a bejelentkezés státuszát a
	* session és/vagy a cookie paraméterei alapján
	*/
	function Auth($env) {

		$this->env = $env;

		if(!isset($_SESSION['user'])) { // nincs érvényes beléptetett session...
			if(isset($_COOKIE['user_id']) && isset($_COOKIE['sc2'])) { // ...viszont van cookie
				$res = $this->env->db->Query("SELECT id, mail, status, name, alias FROM "._DBPREF."users WHERE id = '".(int)$_COOKIE['user_id']."' LIMIT 1");
				$row = $this->env->db->fetchAssoc($res);

				// van ilyen felhasználó és érvényes a visszaellenőrző SecureCookie értéke
				if(($this->env->db->numRows($res) == 1) && ($_COOKIE['sc2'] == $this->getSecureCookie($row['mail']))) {
					if($row['status'] < 2) {
						header('Location: /'.$env->l['users']['url_logout']);
						exit;
					}
					$this->setLogged($row, false);
					//if($row['status'] == 1) $this->env->message = $this->env->l['users']['message_verify'];
				}
			}
		}
		else {
			//if($_SESSION['user']['status'] == 1) $this->env->message = $this->env->l['users']['message_verify'];
		}

	}

	/**
	* Sikeres beléptetés után a felhasználói státusz rögzítése sessionben, cookieban
	* @param array $userArr Felhasználó tulajdonságai
	* @param boolean $note Feljegyzés cookie-val is (false = nem, true = igen)
	* @return boolean Sikeres belépés feljegyzés esetén true-val, egyébként false-sal tér vissza
	*/
	function setLogged($userArr, $note = false) {

		$_SESSION['user'] = $userArr;

		//$res = $this->env->db->Query("UPDATE "._DBPREF."users SET modtime = NOW() WHERE id = '".$userArr['id']."' LIMIT 1");

		if($note) { // belépés megjegyzése cookie-val is
			$secCookie = $this->getSecureCookie($userArr['mail']);

			setcookie('user_id', $userArr['id'], time()+(int)$this->env->c['users']['logincookietime'], '/', $this->env->domainBase, false);
			setcookie('sc2', $secCookie, time()+(int)$this->env->c['users']['logincookietime'], '/', $this->env->domainBase, false);
		}
		return true;

	}

	/**
	* Beállítja, vagy újragenerálja az adatbázis alapján a belépett felhasználó tulajdonságait tartalmazó SESSION tömböt
	* @param integer $userID Felhasználó azonosítója
	* @return void
	*/
	function setUserSession($userID) {

		$res = $this->env->db->Query("SELECT id, mail, status, name, alias FROM "._DBPREF."users WHERE id = '".(int)$userID."' LIMIT 1");
		$_SESSION['user'] = $this->env->db->fetchAssoc($res);

	}

	/**
	* Jelszavak hashkódolású formájának előállítása
	* @param string $pass Kódolandó jelszó
	* @return string Sikeres művelet esetén a jelszóhash-sel, egyébként false-sal tér vissza
	*/
	function getHashmark($pass) {

		return sha1('dM1kp0'.md5($pass.'I2ds4')); // chr(40)

	}

	/**
	* Ellenőrző string készítése a cookie-k számára
	* @param string $str Konstans szövegdarab
	* @return string Sikeres művelet esetén a cookiehash-sel, egyébként false-sal tér vissza
	*/
	function getSecureCookie($str) {

		return sha1('W8uMzd'.md5($str.'f3ms9'));

	}

	/**
	* Feljegyzett hibás események, rosszul kitöltött űrlapok számának lekérdezése
	* @param obj $object Eseményt kiváltó objektum
	* @return boolean A megengedettet meghaladó hibás események esetén false-sal, egyébként true-val tér vissza
	*/
	function checkErrorLog($object) {

		// elévült események törlése
		$logTime = (int)$this->env->c['users']['logtime'];
		$res = $this->env->db->Query("DELETE FROM "._DBPREF."users_errorlog WHERE datetime < NOW() - INTERVAL ".$logTime." MINUTE");

		$userAgent = $this->env->db->escape($_SERVER['HTTP_USER_AGENT']);

		// hibás események számának lekérése
		$res_num = $this->env->db->Query("SELECT count(id) AS num FROM "._DBPREF."users_errorlog
		WHERE classname = '".get_class($object)."'
		AND session_id = '".session_id()."'
		AND ip = '".$this->env->getIpAddress()."'
		AND useragent = '".$userAgent."'");
		$row = $this->env->db->fetchArray($res_num);

		if($row['num'] > (int)$this->env->c['users']['errorlimit']) {
			return false;
		}
		return true;

	}

	/**
	* Hibás események, rosszul kitöltött űrlapok feljegyzése, ha a konfiguráció szerint szükséges
	* @param obj $object Eseményt kiváltó objektum
	* @return void
	*/
	function setErrorLog($object, $str) {

		if((boolean)$this->env->c['users']['errorlog']) { // hibás kísérlet feljegyzése
			$userAgent = $this->env->db->escape($_SERVER['HTTP_USER_AGENT']);

			$res = $this->env->db->Query("INSERT
			INTO "._DBPREF."users_errorlog (classname, datetime, session_id, ip, useragent, errorstr)
			VALUES ('".get_class($object)."', NOW(), '".session_id()."', '".$this->env->getIpAddress()."', '".$userAgent."', '".$str."')");
		}

	}

	/**
	* Felhasználói bejelentkezés szükségességétől függő elirányítása
	* @param boolean $required Szükséges az azonosítás (false = nem, true = igen)
	* @return void
	*/
	function isAuthRequired($required) {

		if(!$required && isset($_SESSION['user'])) { // már be van jelentkezve
			header('Location: /'.$this->env->l['users']['url_profile'].'/'.$_SESSION['user']['alias']);
			exit;
		}

		if($required && !$_SESSION['user']) { // ehhez szükséges bejelentkezés
			header('Location: /'.$this->env->l['users']['url_login'].'?redirect='.$_SERVER['REQUEST_URI']);
			exit;
		}

	}

	/**
	* Felhasználói státusz lekérdezése, tiltott-e
	* @param integer $userID Felhasználó azonosítója
	* @return boolean Tiltott felhasználó esetén true-val, egyébként false-sal tér vissza
	*/
	function isBannedUser($userID) {

		$res = $this->env->db->Query("SELECT status FROM "._DBPREF."users WHERE id = '".$userID."' LIMIT 1");
		$row = $this->env->db->fetchArray($res);
		return ($row['status'] == 0) ? true : false;

	}

	/**
	* Felhasználói tulajdonságd egyediségének lekérdezése
	* @param string $field Mező neve a _users táblában
	* @param string $value Lekérdezendő érték
	* @return boolean Már létező rekord esetén true-val, egyébként false-sal tér vissza
	*/
	function isReserved($field, $value) {

		$field = $this->env->db->escape($field);
		$value = $this->env->db->escape($value);

		// regisztrációs adatok lekérdezése a felhasználók adattáblájából
		$res = $this->env->db->Query("SELECT ".$field." FROM "._DBPREF."users WHERE ".$field." COLLATE utf8_general_ci = '".$value."' LIMIT 1");

		if($this->env->db->numRows($res) != 0) { // már van ilyen rekord a $field mezőben
			return true;
		}
		return false;

	}

	/**
	* Két captcha ellenőrző kód összehasonlítása, ha a konfigurációs fájl szerint erre szükség van
	* @param integer $captcha Ellenőrző kód
	* @param integer $recaptcha Ellenőrző kód újra
	* @return boolean Nem egyező kódok esetén false-sal, egyébként true-val tér vissza
	*/
	function checkCaptcha($captcha, $recaptcha) {

		if((boolean)$this->env->c['users']['usecaptcha'] && (mb_strtolower($captcha, 'UTF-8') != mb_strtolower($recaptcha, 'UTF-8'))) { // nem megfelelő
			return false;
		}
		return true;

	}

	/**
	* E-mail cím formai megfelelőségének ellenőrzése
	* @param string $mail E-mail cím
	* @return boolean Helyes e-mail formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkEmailFormat($mail) {

		if(!preg_match("/^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$/i", $mail)) {
			return false;
		}

		$temp = array('drdrb.com', 'rmqkr.net', '0815.ru0clickemail.com', '0wnd.net', '0wnd.org', '10minutemail.com', '20minutemail.com', '2prong.com', '3d-painting.com', '4warding.com', '4warding.net', '4warding.org',
'9ox.net', 'a-bc.net', 'amilegit.com', 'anonbox.net', 'anonymbox.com', 'antichef.com', 'antichef.net', 'antispam.de', 'baxomale.ht.cx', 'beefmilk.com', 'binkmail.com',
'bio-muesli.net', 'bobmail.info', 'bodhi.lawlita.com', 'bofthew.com', 'brefmail.com', 'bsnow.net', 'bugmenot.com', 'bumpymail.com', 'casualdx.com', 'chogmail.com',
'cool.fr.nf', 'correo.blogos.net', 'cosmorph.com', 'courriel.fr.nf', 'courrieltemporaire.com', 'curryworld.de', 'cust.in', 'dacoolest.com', 'dandikmail.com',
'deadaddress.com', 'despam.it', 'devnullmail.com', 'dfgh.net', 'digitalsanctuary.com', 'discardmail.com', 'discardmail.de', 'disposableaddress.com',
'disposemail.com', 'dispostable.com', 'dm.w3internet.co.uk example.com', 'dodgeit.com', 'dodgit.com', 'dodgit.org', 'dontreg.com', 'dontsendmespam.de',
'dump-email.info', 'dumpyemail.com', 'e4ward.com', 'email60.com', 'emailias.com', 'emailinfive.com', 'emailmiser.com', 'emailtemporario.com.br', 'emailwarden.com',
'ephemail.net', 'explodemail.com', 'fakeinbox.com', 'fakeinformation.com', 'fastacura.com', 'filzmail.com', 'fizmail.com', 'frapmail.com', 'garliclife.com', 'get1mail.com',
'getonemail.com', 'getonemail.net', 'girlsundertheinfluence.com', 'gishpuppy.com', 'great-host.in', 'gsrv.co.uk', 'guerillamail.biz', 'guerillamail.com', 'guerillamail.net',
'guerillamail.org', 'guerrillamail.com', 'guerrillamailblock.com', 'haltospam.com', 'hotpop.com', 'ieatspam.eu', 'ieatspam.info', 'ihateyoualot.info', 'imails.info',
'inboxclean.com', 'inboxclean.org', 'incognitomail.com', 'incognitomail.net', 'ipoo.org', 'irish2me.com', 'jetable.com', 'jetable.fr.nf', 'jetable.net', 'jetable.org',
'junk1e.com', 'kaspop.com', 'kulturbetrieb.info', 'kurzepost.de', 'lifebyfood.com', 'link2mail.net', 'litedrop.com', 'lookugly.com', 'lopl.co.cc', 'lr78.com', 'maboard.com',
'mail.by', 'mail.mezimages.net', 'mail4trash.com', 'mailbidon.com', 'mailcatch.com', 'maileater.com', 'mailexpire.com', 'mailin8r.com', 'mailinator.com', 'mailinator.net',
'mailinator2.com', 'mailincubator.com', 'mailme.lv', 'mailnator.com', 'mailnull.com', 'mailzilla.org', 'mbx.cc', 'mega.zik.dj', 'meltmail.com', 'mierdamail.com', 'mintemail.com',
'moncourrier.fr.nf', 'monemail.fr.nf', 'monmail.fr.nf', 'mt2009.com', 'mx0.wwwnew.eu', 'mycleaninbox.net', 'mytrashmail.com', 'neverbox.com', 'nobulk.com', 'noclickemail.com',
'nogmailspam.info', 'nomail.xl.cx', 'nomail2me.com', 'no-spam.ws', 'nospam.ze.tc', 'nospam4.us', 'nospamfor.us', 'nowmymail.com', 'objectmail.com', 'obobbo.com',
'onewaymail.com', 'ordinaryamerican.net', 'owlpic.com', 'pookmail.com', 'proxymail.eu', 'punkass.com', 'putthisinyourspamdatabase.com', 'quickinbox.com', 'rcpt.at', 'recode.me',
'recursor.net', 'regbypass.comsafe-mail.net', 'safetymail.info', 'sandelf.de', 'saynotospams.com', 'selfdestructingmail.com', 'sendspamhere.com', 'shiftmail.com',
'skeefmail.com', 'slopsbox.com', 'smellfear.com', 'snakemail.com', 'sneakemail.com', 'sofort-mail.de', 'sogetthis.com', 'soodonims.com', 'spam.la', 'spamavert.com',
'spambob.net', 'spambob.org', 'spambog.com', 'spambog.de', 'spambog.ru', 'spambox.info', 'spambox.us', 'spamcannon.com', 'spamcannon.net', 'spamcero.com',
'spamcorptastic.com', 'spamcowboy.com', 'spamcowboy.net', 'spamcowboy.org', 'spamday.com', 'spamex.com', 'spamfree24.com', 'spamfree24.de', 'spamfree24.eu',
'spamfree24.info', 'spamfree24.net', 'spamfree24.org', 'spamgourmet.com', 'spamgourmet.net', 'spamgourmet.org', 'spamherelots.com', 'spamhereplease.com', 'spamhole.com',
'spamify.com', 'spaminator.de', 'spamkill.info', 'spaml.com', 'spaml.de', 'spammotel.com', 'spamobox.com', 'spamspot.com', 'spamthis.co.uk', 'spamthisplease.com',
'speed.1s.fr', 'suremail.info', 'tempalias.com', 'tempemail.biz', 'tempemail.com', 'tempe-mail.com', 'tempemail.net', 'tempinbox.co.uk', 'tempinbox.com', 'tempomail.fr',
'temporaryemail.net', 'temporaryinbox.com', 'thankyou2010.com', 'thisisnotmyrealemail.com', 'throwawayemailaddress.com', 'tilien.com', 'tmailinator.com', 'tradermail.info',
'trash2009.com', 'trash-amil.com', 'trashmail.at', 'trash-mail.at', 'trashmail.com', 'trash-mail.com', 'trash-mail.de', 'trashmail.me', 'trashmail.net', 'trashymail.com',
'trashymail.net', 'tyldd.com', 'uggsrock.com', 'wegwerfmail.de', 'wegwerfmail.net', 'wegwerfmail.org', 'wh4f.org', 'whyspam.me', 'willselfdestruct.com', 'winemaven.info',
'wronghead.com', 'wuzupmail.net', 'xoxy.net', 'yogamaven.com', 'yopmail.com', 'yopmail.fr', 'yopmail.net', 'yuurok.com', 'zippymail.info', 'jnxjn.com', 'trashmailer.com',
'klzlk.com', 'nospamforus','kurzepost.de', 'objectmail.com', 'proxymail.eu', 'rcpt.at', 'trash-mail.at', 'trashmail.at', 'trashmail.me', 'trashmail.net', 'wegwerfmail.de',
'wegwerfmail.net', 'wegwerfmail.org', 'jetable', 'link2mail', 'meltmail', 'anonymbox', 'courrieltemporaire', 'sofimail', '0-mail.com', 'moburl.com', 'get2mail', 'yopmail',
'10minutemail', 'mailinator', 'dispostable', 'spambog', 'mail-temporaire','filzmail','sharklasers.com', 'guerrillamailblock.com', 'guerrillamail.com', 'guerrillamail.net',
'guerrillamail.biz', 'guerrillamail.org', 'guerrillamail.de','mailmetrash.com', 'thankyou2010.com', 'trash2009.com', 'mt2009.com', 'trashymail.com', 'mytrashmail.com',
'mailcatch.com','trillianpro.com','junk.','joliekemulder','lifebeginsatconception','beerolympics','smaakt.naar.gravel','q00.','dispostable','spamavert','mintemail',
'tempemail','spamfree24','spammotel','spam','mailnull','e4ward','spamgourmet','mytempemail','incognitomail','spamobox','mailinator.com', 'trashymail.com', 'mailexpire.com',
'temporaryinbox.com', 'MailEater.com', 'spambox.us', 'spamhole.com', 'spamhole.com', 'jetable.org', 'guerrillamail.com', 'uggsrock.com', '10minutemail.com', 'dontreg.com',
'tempomail.fr', 'TempEMail.net', 'spamfree24.org', 'spamfree24.de', 'spamfree24.info', 'spamfree24.com', 'spamfree.eu', 'kasmail.com', 'spammotel.com', 'greensloth.com',
'spamspot.com', 'spam.la', 'mjukglass.nu', 'slushmail.com', 'trash2009.com', 'mytrashmail.com', 'mailnull.com', 'jetable.org','10minutemail.com', '20minutemail.com',
'anonymbox.com', 'beefmilk.com', 'bsnow.net', 'bugmenot.com', 'deadaddress.com', 'despam.it', 'disposeamail.com', 'dodgeit.com', 'dodgit.com', 'dontreg.com', 'e4ward.com',
'emailias.com', 'emailwarden.com', 'enterto.com', 'gishpuppy.com', 'goemailgo.com', 'greensloth.com', 'guerrillamail.com', 'guerrillamailblock.com', 'hidzz.com',
'incognitomail.net ', 'jetable.org', 'kasmail.com', 'lifebyfood.com', 'lookugly.com', 'mailcatch.com', 'maileater.com', 'mailexpire.com', 'mailin8r.com', 'mailinator.com',
'mailinator.net', 'mailinator2.com', 'mailmoat.com', 'mailnull.com', 'meltmail.com', 'mintemail.com', 'mt2009.com', 'myspamless.com', 'mytempemail.com', 'mytrashmail.com',
'netmails.net', 'odaymail.com', 'pookmail.com', 'shieldedmail.com', 'smellfear.com', 'sneakemail.com', 'sogetthis.com', 'soodonims.com', 'spam.la', 'spamavert.com',
'spambox.us', 'spamcero.com', 'spamex.com', 'spamfree24.com', 'spamfree24.de', 'spamfree24.eu', 'spamfree24.info', 'spamfree24.net', 'spamfree24.org', 'spamgourmet.com',
'spamherelots.com', 'spamhole.com', 'spaml.com', 'spammotel.com', 'spamobox.com', 'spamspot.com', 'tempemail.net', 'tempinbox.com', 'tempomail.fr', 'temporaryinbox.com',
'tempymail.com', 'thisisnotmyrealemail.com', 'trash2009.com', 'trashmail.net', 'trashymail.com', 'tyldd.com', 'yopmail.com', 'zoemail.com','deadaddress','soodo','tempmail',
'uroid','spamevader','gishpuppy','privymail.de','trashmailer.com','fansworldwide.de','onewaymail.com', 'mobi.web.id', 'ag.us.to', 'gelitik.in', 'fixmail.tk', 'uyhip.com');

		$ex = explode('@', $mail);
		$domain = mb_strtolower($ex[1], 'UTF-8');

		foreach($temp as $k=>$v) {
			if(strstr($domain, $v)){
				return false;
			}
		}

		return true;

	}

	/**
	* Jelszó formai megfelelőségének ellenőrzése
	* @param string $pass Jelszó
	* @param integer $passLength Jelszó minimális hossza
	* @return boolean Helyes jelszó formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkPassFormat($pass, $passLength) {
/*
		if((strlen($pass) < $passLength) || preg_match("/^[A-Za-z]+$|^[0-9]+$/", $pass)) {
			return false;
		}
*/
		if(strlen($pass) < $passLength) {
			return false;
		}
		return true;

	}

	/**
	* Felhasználónév formai megfelelőségének ellenőrzése
	* @param string $name Felhasználónév
	* @return boolean Helyes felhasználónév formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkNameFormat($name) {

		if(!preg_match("/^[a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ\-_\'\.\"\s]{2,32}$/", $name)) {
			return false;
		}
		return true;

	}

	/**
	* Felhasználónév URL alias-ának előállítása
	* @param string $name Felhasználónév
	* @return boolean Helyes felhasználónév formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function getNameAlias($name) {

		$string = preg_quote($name);
		$accents = array("[á]","[ä]","[é]","[í]","[ó]","[ö]","[ő]","[ú]","[ü]","[ű]", "[Á]","[É]","[Í]","[Ó]","[Ö]","[Ő]","[Ú]","[Ü]","[Ű]");
		$replaces = array("a","a","e","i","o","o","o","u","u","u","A","E","I","O","O","O","U","U","U");
		$string = preg_replace($accents, $replaces, $string);
		$string = preg_replace('([^a-zA-Z0-9-_]+)', '_', $string);
		return trim($string, '_');

	}

	/**
	* Véletlen generált kulcs készítése a regisztrációs és jelszóújraküldő adatok érvényesítéséhez
	* melynek hossza a konfigurációs fájlban beállított értéktől függ
	* @param integer $keyLength Generált kulcs hossza
	* @return string Sikeres művelet esetén a generált kóddal, egyébként false-sal tér vissza
	*/
	function getRandomKey($keyLength) {

		$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$key = "";
		for($i=0; $i < $keyLength; $i++) {
			$key .= substr($chars, rand(0, strlen($chars)-1), 1);
		}
		return $key;

	}

	/**
	* Véletlen generált kulcs formai megfelelőségének ellenőrzése
	* @param string $key Véletlen generált kulcs
	* @param integer $keyLegth Kulcs kötelező hossza
	* @return boolean Helyes kulcs formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkRandomKeyFormat($key, $keyLength) {

		if(!preg_match("/^[0-9a-zA-Z]{".$keyLength."}$/", $key)) {
			return false;
		}
		return true;

	}

	/**
	* Véletlen generált jelszó készítése a jelszó újraküldés számára, mely biztosítottan
	* megfelelő hosszúságú és nehézségi szintű
	* @param integer $passLength Jelszó minimális hossza
	* @return string Sikeres művelet esetén a generált kóddal, egyébként false-sal tér vissza
	*/
	function getRandomPass($passLength) {

		$chars = "23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
		$key = "";
		for($i = 0; $i < $passLength-2; $i++) {
			$key .= substr($chars, rand(0, strlen($chars)-1), 1);
		}
		// hozzáad még, hogy biztosan legyen benne szám és betű
		$key .= chr(rand(50, 57)); // szám
		$key .= chr(rand(74, 90)); // betű

		return str_shuffle($key);

	}

	/**
	* Megpróbálja kitalálni az e-mail címből az e-mail postafiók szolgáltatóját
	* @param string $mail Felhasználó e-mail címe
	* @return array Sikeres művelet esetén a szolgáltató tulajdonságaival, egyébként false-sal tér vissza
	*/
	function getMailService($mail) {

		$mail = strtolower($mail);
		$domain = substr(stristr($mail, '@'), 1);

		if($domain == 'gmail.com') {
			return array('name' => 'Gmail', 'url' => 'http://mail.google.com/', 'img' => 'gmail');
		}
		elseif($domain == 'yahoo.com') {
			return array('name' => 'Yahoo! Mail', 'url' => 'http://mail.yahoo.com/', 'img' => 'yahoo');
		}
		elseif($domain == 'hotmail.com' || $domain == 'msn.com') {
			return array('name' => 'Yahoo! Mail', 'url' => 'http://hotmail.com/', 'img' => 'hotmail');
		}
		elseif($domain == 'freemail.hu') {
			return array('name' => '[freemail]', 'url' => 'http://freemail.hu/', 'img' => 'freemail');
		}
		elseif($domain == 'citromail.hu') {
			return array('name' => 'CitroMail.hu', 'url' => 'http://citromail.hu/', 'img' => 'citromail');
		}
		elseif($domain == 'vipmail.hu') {
			return array('name' => 'Vipmail', 'url' => 'http://vipmail.hu/', 'img' => 'vipmail');
		}
		elseif($domain == 'mailbox.hu') {
			return array('name' => 'MailBox', 'url' => 'http://mailbox.hu/', 'img' => 'mailbox');
		}
		else {
			return false;
		}

	}

	/**
	* Visszaadja a fődomaint a cookie-elhelyezés számára
	* @return string
	*/
	function getCookieDomain() {

		$ua = explode('.', str_replace('http://', '', $this->env->base));
		return '.'.$ua[count($ua)-2].'.'.$ua[count($ua)-1];

	}

}

?>
