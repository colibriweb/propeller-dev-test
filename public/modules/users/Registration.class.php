<?php
/**
* Felhasználói regisztráció kezelése
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
class Registration extends Auth {

	/**
	* Captcha ellenőrző kód
	* @var string
	*/
	var $recaptcha;

	/**
	* Feltételek elfogadása
	* @var boolean
	*/
	var $agree;

	/**
	* Érvényesítő kulcs
	* @var string
	*/
	var $randomKey;

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Registration() {

	}

	/**
	* Felhasználói regisztráció ellenőrzése, a felhasználói adatok beírása az adattáblába,
	* illetve a temorary táblába, ha a konfigurációs fájl szerint e-mail-es aktiválás is szükséges
	* @param array $user Felhasználó tulajdonságai
	* @param integer $recaptcha Captcha ellenőrző kód
	* @param boolean $agree Feltételek elfogadása
	* @return boolean Sikeres beléptetés esetén true-val, egyébként false-sal tér vissza
	*/
	function doRegistration($user, $recaptcha = 0, $agree = 0) {

		$this->user = $user;
		$this->recaptcha = $recaptcha;
		$this->agree = $agree;

//        $this->error = 'error_noregistration'; return false;

        if($this->env->isDenied()) { //if($row['status'] == 0) {
            $this->error = 'error_noregistration';

            $this->setErrorLog($this);
            return false;
        }

		if(!$this->checkErrorLog($this)) { // tömeges űrlapküldések lekérdezése
			$this->error = 'error_toomanybad';
			return false;
		}

		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lf1a3wUAAAAALgqwmLXRN2K7WILMPgtoR49mqH8&response=".$this->recaptcha), true); /*."&remoteip=".$_SERVER['REMOTE_ADDR']*/
		if($response['success'] == false) { // nem megfelelő captcha ellenőrző kód
			$this->error = 'error_wrongcaptcha';

			$this->setErrorLog($this);
			return false;
		}

		if(!$this->checkEmailFormat($this->user['mail'])) { // nem megfelelő e-mail cím formátum
			$this->error = 'error_wrongmail';

			$this->setErrorLog($this);
			return false;
		}

		if(!$this->checkPassFormat($this->user['pass'], (int)$this->env->c['users']['passlength'])) { // nem megfelelő jelszó formátum
			$this->error = 'error_easypass';

			$this->setErrorLog($this);
			return false;
		}

		if($this->user['pass'] != $this->user['repass']) { // nem egyezik a két jelszó
			$this->error = 'error_notsamepass';

			$this->setErrorLog($this);
			return false;
		}

		if(!$this->checkNameFormat($this->user['name'])) { // nem megfelelő felhasználónév formátum
			$this->error = 'error_wrongname';

			$this->setErrorLog($this);
			return false;
		}

		if($this->agree == 0) { // nem fogadta el a feltételeket
			$this->error = 'error_disagree';

			$this->setErrorLog($this);
			return false;
		}

		if($this->isReserved('mail', $this->user['mail'])) { // már foglalt e-mail cím
			$this->error = 'error_reservedmail';

			$this->setErrorLog($this);
			return false;
		}

		if($this->isReserved('name', $this->user['name'])) { // már foglalt felhasználónév
			$this->error = 'error_reservedname';

			$this->setErrorLog($this);
			return false;
		}

		$this->user['alias'] = $this->getNameAlias($this->user['name']);
		if($this->isReserved('alias', $this->user['alias'])) { // már foglalt alias
			$this->error = 'error_reservedname';

			$this->setErrorLog($this);
			return false;
		}

		if($this->env->isDenied()) { //if($row['status'] == 0) {
			$this->error = 'error_notallowed';

			$this->setErrorLog($this);
			return false;
		}

		/*
		**********************************************************************************
		*/

		if(empty($this->error)) { // nincs hiba, regisztrálhatók az adatok
			// hashkódolt jelszóforma generálása
			$pass = $this->getHashmark($this->user['pass']);

			$this->user = $this->env->db->escape($this->user);

			$q_nl = ($this->user['newsletter'] == 0) ? 0 : 1;
			$q_status = ((boolean)$this->env->c['users']['mailvalidation']) ? 1 : 2;

			// felhasználó beírása a users táblába
			$res_reg = $this->env->db->Query("INSERT
			INTO "._DBPREF."users (mail, pass, status, newsletter, name, alias, datetime, pubmail, country_id)
			VALUES ('".$this->user['mail']."', '".$pass."', '".$q_status."', '".$q_nl."', '".trim($this->user['name'])."', '".$this->user['alias']."', NOW(), '1', '36')");

			$this->user['id'] = $this->env->db->lastInsertID($res_reg, 'id', _DBPREF."users");

			if((boolean)$this->env->c['users']['mailvalidation']) { // e-mailes érvényesítéssel válhat csak véglegessé
				$this->randomKey = $this->getRandomKey((int)$this->env->c['users']['keylength']);

				$res_temp = $this->env->db->Query("INSERT INTO "._DBPREF."users_registration (user_id, randomkey)
				VALUES ('".$this->user['id']."', '".$this->randomKey."')");

				if(!$res_temp) { // sikertelen volt a beírás az árványesítő táblába
					$this->error = 'error_failedreg';

					return false;
				}
				return true;
			}
			return true;

		}
		return false;

	}

	/**
	* Visszaadja a megjelenítés számára a form mezők értékeit asszociatív tömbben
	* @return array A form mezőket tartalmazó tömbbel tér vissza
	*/
	function getRegistrationFormValues() {

		return array_merge($this->user, array(
			'agree' => $this->agree
		));

	}

	/**
	* Sikertelen regisztrációs e-mail kiküldés esetén visszavonja (törli) a regisztrációt
	* @param integer $userID Felhasználó azonosítója
	* @return void
	*/
	function clearFailedRegistration($userID) {

		$res = $this->env->db->Query("DELETE FROM "._DBPREF."users WHERE id = '".(int)$userID."' LIMIT 1");
		$res_reg = $this->env->db->Query("DELETE FROM "._DBPREF."users_registration WHERE user_id = '".(int)$userID."' LIMIT 1");

	}

	/**
	* Érvényesítő levélben kapott kód ellenőrzése, és a regisztráció befejezése
	* @param string $randomKey E-mail érvényesítő kód
	* @return boolean Sikeres érvényesítés esetén true-val, egyébként false-sal tér vissza
	*/
	function doValidation($randomKey) {

		$this->randomKey = $randomKey;

		if(!$this->checkErrorLog($this)) { // tömeges űrlapküldések lekérdezése
			$this->error = 'error_toomanybad';
			return false;
		}

		if(!$this->checkRandomKeyFormat($this->randomKey, (int)$this->env->c['users']['keylength'])) { // nem megfelelő kulcs formátum
			$this->error = 'error_wrongkeyformat';

			$this->setErrorLog($this);
			return false;
		}

		$randomKey = $this->env->db->escape($this->randomKey);

		// érvényesítőkód lekérdezése az adatbázisból
		$res = $this->env->db->Query("SELECT user_id FROM "._DBPREF."users_registration WHERE randomkey = '".$randomKey."' LIMIT 1");
		$row = $this->env->db->fetchArray($res);

		if($this->env->db->numRows($res) != 1) { // nem megfelelő érvényesítőkód
			$this->error = 'error_wrongkeyformat';

			$this->setErrorLog($this);
			return false;
		}
		else { // helyes érvényesítő kód
			$this->user['id'] = $row['user_id'];

			// beállítjuk a júzert 2-es státuszúra (aktivált)
			$res_reg = $this->env->db->Query("UPDATE "._DBPREF."users SET status = '2' WHERE id = '".$this->user['id']."' LIMIT 1");

			if($res_reg) { // töröljük a regisztrációs táblából, kikeressük a nevét az admin értesítőhöz
				$res_del = $this->env->db->Query("DELETE FROM "._DBPREF."users_registration WHERE user_id = '".$this->user['id']."' LIMIT 1");

				$res_name = $this->env->db->Query("SELECT name, mail, newsletter FROM "._DBPREF."users WHERE id = '".$this->user['id']."' LIMIT 1");
				$row_name = $this->env->db->fetchArray($res_name);
				$this->user['name'] = $row_name['name'];

				if($row_name['newsletter'] == 1) { // kért hírlevelet
					/*$res_temp = $this->env->db->Query("INSERT INTO "._DBPREF."newsletter_list (name, mail)
					VALUES ('".$this->env->db->escape($row_name['name'])."', '".$row_name['mail']."')
					ON DUPLICATE KEY UPDATE name = '".$this->env->db->escape($row_name['name'])."'");*/

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'http://propeller.hu/hirlevel/subscribe');
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['api' => 1, 'mail' => $row_name['mail'], 'mail_name' => $row_name['name']]));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$response = curl_exec($ch);
					curl_close($ch);
					if($response == 'OK') {
						//echo $response;
					}
				}

				// újrageneráljuk a sessiontömböt
				//$this->setUserSession($this->user['id']);

				return true;
			}
			return false;
		}

	}

}

?>
