<?php
/**
* Felhasználói- és rendszerüzenetek kezelése
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/
class Messages {

	/**
	* Üzenet tulajdonságai
	* @var array
	*/
	var $message;

	/**
	* Hibaüzenetek feljegyzése
	* @var string
	*/
	var $error = '';

	/**
	* Konstruktor, feltölti az objektumváltozókat
	*/
	function Messages() {

	}

	/**
	* Felhasználó bejövő üzeneteinek lekérdezése
	* @param integer $userID Felhasználó azonosítója
	* @return boolean Sikeres művelet esetén true-val, egyébként false-sal tér vissza
	*/
	function getIncomingByUserID($userID, $from, $limit) {

		$userID = (int)$userID;

		$res = $this->env->db->Query("SELECT SQL_CALC_FOUND_ROWS
			m.id AS msg_id,
			if(sender_id = '".$userID."', recipient_id, sender_id) AS user_id,

			if(sender_id > recipient_id, CONCAT(sender_id,'-',recipient_id), CONCAT(recipient_id,'-',sender_id)) AS gfield,

			(SELECT concat(sender_id,'|',message) FROM "._DBPREF."users_messages WHERE(sender_id = m.sender_id AND recipient_id = m.recipient_id) OR (recipient_id = m.sender_id AND sender_id = m.recipient_id)
				ORDER BY datetime DESC LIMIT 1
			) message,

			(SELECT datetime FROM "._DBPREF."users_messages WHERE(sender_id = m.sender_id AND recipient_id = m.recipient_id) OR (recipient_id = m.sender_id AND sender_id = m.recipient_id)
				ORDER BY datetime DESC LIMIT 1
			) datetime

			FROM "._DBPREF."users_messages m
			WHERE (sender_id = '".$userID."' OR recipient_id = '".$userID."')


			GROUP BY gfield ORDER BY datetime DESC LIMIT ".$from.", ".$limit);

		if($this->env->db->numRows($res)) { // vannak bejövői
			while($row = $this->env->db->fetchArray($res)) {
			$row['date'] = $this->env->dateToPast($row['datetime']);

			$ex = explode('|', $row['message']);
            $row['last_sender_id'] = $ex[0];
            $row['message'] = $ex[1];

			$res_u = $this->env->db->Query("SELECT name, alias, avatar FROM "._DBPREF."users WHERE id = '".$row['user_id']."' LIMIT 1");
			$row = array_merge($row, $this->env->db->fetchArray($res_u));

			$row['avatar'] = ($row['avatar'] == 1) ? floor($row['user_id']/1000).'/'.$row['user_id'].'/'.$row['user_id'].'_normal.jpg' : 'default_normal.jpg';



			$assoc[] = $row;
			}
//p($assoc);
			return $assoc;
		}
		return false;

	}

	/**
	* Felhasználó elküldött üzeneteinek lekérdezése
	* @param integer $userID Felhasználó azonosítója
	* @return boolean Sikeres művelet esetén true-val, egyébként false-sal tér vissza
	*/
	function getSentByUserID($userID, $from, $limit) {

		$userID = $this->env->db->escape($userID);
		$res = $this->env->db->Query("SELECT SQL_CALC_FOUND_ROWS m.id AS msg_id, sender_id, recipient_id, subject, message, m.datetime AS datetime, sender_viewed AS viewed, u.id AS userid, avatar, name, alias
			FROM "._DBPREF."users_messages m
			LEFT JOIN "._DBPREF."users u ON m.recipient_id = u.id
			WHERE sender_id = '".$userID."' AND sender_del = '0' ORDER BY datetime DESC LIMIT ".$from.", ".$limit);

		if($this->env->db->numRows($res)) { // vannak bejövői
			while($row = $this->env->db->fetchArray($res)) {
			$row['date'] = $this->env->dateToPast($row['datetime']);
			if($row['avatar'] != NULL)
			$row['avatarsrc'] = ($row['avatar'] == 1) ? floor($row['userid']/1000).'/'.$row['userid'].'/'.$row['userid'].'_small.jpg' : 'default_small.jpg';

			$assoc[] = $row;
			}
			return $assoc;
		}
		return false;

	}

	/**
	* Egy bizonyos bejövő üzeneteinek részleteinek lekérdezése
	* @param integer $userID Felhasználó azonosítója
	* @param integer $msgID Üzenet azonosítója
	* @return boolean Sikeres művelet esetén true-val, egyébként false-sal tér vissza
	*/
	function getMessageByID($userID, $msgID) {

		$userID = $this->env->db->escape($userID);
		$msgID = $this->env->db->escape($msgID);
		$res = $this->env->db->Query("SELECT m.id AS msg_id, sender_id, recipient_id, subject, message, m.datetime AS datetime,
		u.id AS userid, name, u.alias AS user_alias, avatar
			FROM "._DBPREF."users_messages m
			LEFT JOIN "._DBPREF."users u ON m.sender_id = u.id
			WHERE m.id = '".$msgID."' AND (
			(recipient_id = '".$userID."' AND recipient_del = '0') OR
			(sender_id = '".$userID."' AND sender_del = '0')
			)");

		if($this->env->db->numRows($res)) { // vannak bejövői

			$row = $this->env->db->fetchArray($res);
			$row['date'] = $this->env->dateToString($row['datetime']);
			if($row['avatar'] != NULL)
			$row['avatarsrc'] = ($row['avatar'] == 1) ? floor($row['sender_id']/1000).'/'.$row['sender_id'].'/'.$row['sender_id'].'_small.jpg' : 'default_small.jpg';

			return $row;
		}
		return false;

	}

	/**
	* Olvasott státusz beállíátsa egy üzeneten, attól függően, hogy a címzett vagy a feladó olvasta
	* @param integer $msgID Üzenet azonosítója
	* @param string $whoIs Ki olvasta ('sender', 'recipient')
	* @return void
	*/
	function setViewed($msgID, $whoIs) {

		$msgID = $this->env->db->escape($msgID);
		$res = $this->env->db->Query("UPDATE "._DBPREF."users_messages SET ".$whoIs."_viewed = '1' WHERE id = '".$msgID."'");

	}

	/**
	* Felhasználói üzenet küldése
	* @param integer $senderID A feladó azonosítója
	* @param array $message Az üzenet részletei
	* @param string $type Az üzenet típusa ('normal', 'reply')
	* @param integer $replyTo Reply esetén az eredeti üzenet azonosítója
	* @return boolean Sikeres művelet esetén true-val, egyébként false-sal tér vissza
	*/
	function sendMessage($senderID, $message, $recipient = null) {

		global $groups; // azbesztmegbaszta, csoportmeghívó

		$this->message = $message;
		$this->message['senderID'] = $senderID;
		$this->message['recipient'] = $recipient;

		if($_SESSION['user']['status'] == 1) {
			$this->error = 'error_activate';
			return false;
		}


		$recipient = $this->env->db->escape($this->message['recipient']);

		// címzett ID-jének lekérdezése az adatbázisból
		$res = $this->env->db->Query("SELECT id, status FROM "._DBPREF."users WHERE alias = '".$this->message['recipient']."' LIMIT 1");
		if(!$this->env->db->numRows($res)) {
			$this->error = 'error_norecipient';
			return false;
		}

		$row = $this->env->db->fetchArray($res);
		$this->message['recipientID'] = $row['id'];

		if($row['status'] != 2) {
			$this->error = 'error_messagestatus';
			return false;
		}


		if(empty($this->message['message'])) {
			$this->error = 'error_nomessage';
			return false;
		}

		if($this->message['recipientID'] == $this->message['senderID']) {
			$this->error = 'error_noselfmessage';
			return false;
		}

        $this->message['message'] = $this->env->strTruncate($this->message['message'], 1010, '...');
		$this->message['message'] = $this->env->getSafeText($this->message['message'], 'UTF-8', true);



		/*
		**********************************************************************************
		*/

		if(empty($this->error)) { // nincs hiba, beírható az üzenet
			$this->message = $this->env->db->escape($this->message);

			// partnernek
			$res_p = $this->env->db->Query("INSERT INTO "._DBPREF."users_messages (user_id_inbox, user_id_partner, user_id_sender, message, datetime)
			VALUES ('".$this->message['recipientID']."', '".$this->message['senderID']."', '".$this->message['senderID']."', '".$this->message['message']."', NOW())");

			// feladónak
			$res_s = $this->env->db->Query("INSERT INTO "._DBPREF."users_messages (user_id_inbox, user_id_partner, user_id_sender, message, datetime)
			VALUES ('".$this->message['senderID']."', '".$this->message['recipientID']."', '".$this->message['senderID']."', '".$this->message['message']."', NOW())");

			$this->message['id'] = $this->env->db->lastInsertID($res_s, 'id', _DBPREF."users_messages");

			if(!$res) {
				return false;
			}
			return true;
		}
		return false;

	}

	/**
	* Visszaadja a megjelenítés számára a form mezők értékeit asszociatív tömbben
	* @return array A form mezőket tartalmazó tömbbel tér vissza
	*/
	function getMessagesFormValues() {

		return $this->message;

	}

	/**
	* Szükséges-e e-mail értesítőt küldeni a címzettnek
	* @param integer $recipientID A címzett felhasználó azonosítója
	* @return boolean E-mail értesítő szükségessége esetén true-val, egyébként false-sal tér vissza
	*/
	function getNotification($recipientID) {

		$res = $this->env->db->Query("SELECT mail, name, notify FROM "._DBPREF."users WHERE id = '".$recipientID."' LIMIT 1");
		return $this->env->db->fetchArray($res);

	}

}

?>
