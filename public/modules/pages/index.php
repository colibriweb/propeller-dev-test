<?php
require('../../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

require(ROOT.'/system/libraries/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ROOT.'/templates/'.$env->template;
$smarty->compile_dir = ROOT.'/system/cache/smarty_templates_c_'.$env->template;
$smarty->cache_dir = ROOT.'/system/cache/smarty_cache_'.$env->template;
//$smarty->compile_check = true;

$smarty->assign('env', $env);

require(ROOT.'/modules/content/Content.class.php');
$content = new Content();
$content->env = $env;

/* ***************************************************************************** */


$alias = $db->escape($env->u[2]);
$res = $db->Query("SELECT * FROM "._DBPREF."pages WHERE alias = '".$alias."' AND public = '1' LIMIT 1");
if(!$db->numRows($res)) {
	$env->setHeader(404);
}

$row = $db->fetchArray($res);
$smarty->assign('pages', $row);


if($alias == '404') header("HTTP/1.0 404 Not Found");


    // banner MASTER kód
    $smarty->assign('ad', array('category' => 'tag', 'page' => 'nyito'));

// dolgok
$smarty->assign('misc', $content->q_misc());

// legfrissebb
$smarty->assign('latest', $content->q_latest());

// bal és középső hasábok, fejléc kiemelések
$smarty->assign('blocks', $content->q_blocks());

$smarty->assign('activities', $env->getActivities());
$smarty->display('pages/index.tpl');


?>