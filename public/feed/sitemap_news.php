<?php
require('../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

/* ***************************************************************************** */

header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
<?php


$res = $db->Query("SELECT id, category_id, CONCAT(id,'-',alias) AS alias, title, link, datetime FROM "._DBPREF."content_temp
	WHERE (content_type IN (2,4,6) OR user_id = 1758 OR link IS NULL)
	AND datetime > '".date('Y-m-d 00:00:00', strtotime('-2 days'))."'
	AND datetime < NOW()
	ORDER BY datetime DESC LIMIT 0, 300");
while($row = $db->fetchArray($res)) {
	$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
	//$row['category'] = $env->l['content']['category_'.$row['category_id']];

	$res_tags = $db->Query("SELECT tags.id, tags.tag FROM "._DBPREF."content_tags_conn conn LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE conn.content_id = ".$row['id']." ORDER BY tags.tag LIMIT 5");
	$tags_query = "";
	while($row_tags = $db->fetchAssoc($res_tags)) {
		$row['tags'][] = $row_tags['tag'];
	}

echo '<url>';
echo '<loc>http://propeller.hu/'.$row['category_alias'].'/'.$row['alias'].'</loc>';



echo '<news:news>';
	echo '<news:publication>';
		echo '<news:name>Propeller</news:name>';
		echo '<news:language>hu</news:language>';
	echo '</news:publication>';
//	echo '<news:access>Subscription</news:access>';
//	echo '<news:genres>PressRelease, Blog</news:genres>';
	echo '<news:publication_date>'.date('c', strtotime($row['datetime'])).'</news:publication_date>';
	echo '<news:title>'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</news:title>';
	echo '<news:keywords>'.implode(', ', $row['tags']).'</news:keywords>';
echo '</news:news>';



echo '</url>'."\n";

}
?>
</urlset>