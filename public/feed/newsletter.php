<?php
// RSS feed: tartalmak

/** RSS generálást végző osztály */
	require('../system/libraries/feedcreator/FeedCreator.class.php');

	$rss = new UniversalFeedCreator();
	$rss->useCached('RSS2.0', '../system/cache/feedcreator/newsletter.xml', 5);

require('../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

/* ***************************************************************************** */

	$rss->link = $env->base;

	$rss->title = $env->l['title'].' - Hírlevél';

	$res = $db->Query("SELECT c.id, c.category_id, c.title, c.description, CONCAT(c.id,'-',c.alias) AS alias, c.datetime, b.image AS block_img, block_column,
	(counter) / POW(((NOW() - UNIX_TIMESTAMP(c.datetime))/3600) / 2, 1.2) as score #newsletter.php

	 FROM "._DBPREF."content c
	LEFT JOIN "._DBPREF."content_blocks b ON c.id = b.content_id
	WHERE c.id > "._SPD_CONTENT_ID."
		AND (c.content_type IN (2,4,6) OR c.user_id = 1758 OR c.link IS NULL)
		AND c.datetime > '".date('Y-m-d 00:00:00')."' # csak a mai cikkek
		# AND c.datetime < NOW()
		GROUP BY c.id ORDER BY score DESC LIMIT 16");
	$assoc = array();
	while($row = $db->fetchAssoc($res)) {
		$row['link'] = $env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['alias'];
		$s = strtotime($row['datetime']);
		$assoc[] = $row;
	}
	//rsort($assoc);

	foreach($assoc as $key => $row) {
		$item = new FeedItem();
		$item->descriptionHtmlSyndicated = true;
		$item->title = $row['title'];
		$item->link = $row['link'];

		$item->description = $env->getExcerpt($row['description']);

		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = $row['id'];

		if(!empty($row['block_img'])) {
			$item->enclosure->url = $row['block_img'];
			$item->enclosure->length = 100; //filesize($row['block_img']);
			$item->enclosure->type = 'image/jpeg';
		}
		elseif(preg_match("/src=[\'\"]http:\/\/static\.propeller\.hu\/images(.*?)[\'\"]/i", $row['description'], $src)) {
			$item->enclosure->url = STTC.'/images'.$src[1];
			$item->enclosure->length = filesize(ROOT.'_static/images'.$src[1]);
			$item->enclosure->type = 'image/jpeg';
		}
		elseif(preg_match("/src=[\'\"]http:\/\/propeller\.hu\/images\/cikk(.*?)[\'\"]/i", $row['description'], $src)) {
			$item->enclosure->url = $env->base.'/images/cikk'.$src[1];
			$item->enclosure->length = filesize(ROOT.'/images/cikk'.$src[1]);
			$item->enclosure->type = 'image/jpeg';
		}


		$rss->addItem($item);
	}




	$rss->saveFeed('RSS2.0', '../system/cache/feedcreator/newsletter.xml');

?>