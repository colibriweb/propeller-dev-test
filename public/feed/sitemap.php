<?php
require('../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

/* ***************************************************************************** */

header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php


$res = $db->Query("SELECT id, category_id, CONCAT(id,'-',alias) AS alias, link, datetime
	FROM "._DBPREF."content_temp
	WHERE datetime < NOW()
	AND (content_type IS NULL OR content_type <> 7)
	ORDER BY datetime DESC LIMIT 0, 300");
while($row = $db->fetchArray($res)) {
	$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
	//$row['category'] = $env->l['content']['category_'.$row['category_id']];

echo '<url>
	<loc>http://propeller.hu/'.$row['category_alias'].'/'.$row['alias'].'</loc>
	<lastmod>'.date('c', strtotime($row['datetime'])).'</lastmod>';
if(!$row['link']) echo '
	<priority>0.8</priority>';
echo '
</url>'."\n";

}

$voltmar = array();
$res_tags = $db->Query("SELECT tag, c.datetime, (SELECT count(content_id) FROM "._DBPREF."content_tags_conn WHERE tag_id = conn.tag_id) num FROM "._DBPREF."content_temp c
JOIN "._DBPREF."content_tags_conn conn ON c.id = conn.content_id
JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
WHERE tag <> '' AND user_id IS NOT NULL AND c.datetime < NOW()
ORDER BY c.datetime DESC LIMIT 0, 100");
while($row_tags = $db->fetchArray($res_tags)) {
	if(in_array($row_tags['tag'], $voltmar) || ($row_tags['num'] < 2)) continue;
	$voltmar[] = $row_tags['tag'];
echo '<url>
	<loc>http://propeller.hu/tag/'.str_replace(' ', '+', htmlspecialchars($row_tags['tag'], ENT_QUOTES)).'</loc>
	<lastmod>'.date('c', strtotime($row_tags['datetime'])).'</lastmod>
	<changefreq>daily</changefreq>
	<priority>0.8</priority>
</url>'."\n";
}

?>
</urlset>