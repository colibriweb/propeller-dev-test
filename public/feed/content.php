<?php
// RSS feed: tartalmak
date_default_timezone_set('Europe/Budapest');
/** RSS generálást végző osztály */
	require('../system/libraries/feedcreator/FeedCreator.class.php');

	$q = !empty($_REQUEST['qsa']) ? ltrim($_REQUEST['qsa'], '-') : 'mind';

	$rss = new UniversalFeedCreator();
	$rss->useCached('RSS2.0', '../system/cache/feedcreator/content-'.$q.'.xml', 1);

require('../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

		function encodeURIComponent($str) {
		    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
		    return strtr(rawurlencode($str), $revert);
		}
/* ***************************************************************************** */

	$rss->link = $env->base;
	$rss->description = $env->l['description'];
/*
	$image = new FeedImage();
	$image->title = "Propeller";
	$image->url = "http://static.propeller.hu/images/default/feed_logo.gif";
	$image->link = "http://propeller.hu/";

	$rss->image = $image;
*/

if($q == 'video') { // videók
	$rss->title = $env->l['title'].' - Videó';

	$res = $db->Query("SELECT c.id, title, description, CONCAT(c.id,'-',c.alias) AS alias, cc.alias AS category_alias, cc.category, datetime
	FROM "._DBPREF."content_temp c
	LEFT JOIN "._DBPREF."content_categories cc ON c.category_id = cc.id
	WHERE (content_type = 1 OR link LIKE '%youtube.com/%') AND c.datetime < NOW() AND (c.content_type IS NULL OR c.content_type <> 7) ORDER BY c.datetime DESC LIMIT 0, 10");
	while($row = $db->fetchArray($res)) {
		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'];
		$item->description = $env->strTruncate(strip_tags($row['description']), 400, '...');
		$item->descriptionHtmlSyndicated = true;
		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = 'propeller@'.$row['id'];
		$item->category = $row['category'];

		$rss->addItem($item);
	}

}
if($q == 'nagykep') { // nagyképek
	$rss->title = $env->l['title'].' - Nagykép';

	$res = $db->Query("SELECT c.id, title, description, CONCAT(c.id,'-',c.alias) AS alias, cc.alias AS category_alias, cc.category, datetime FROM "._DBPREF."content_temp c
	LEFT JOIN "._DBPREF."content_categories cc ON c.category_id = cc.id
	WHERE content_type = '6' AND c.datetime < NOW() ORDER BY c.datetime DESC LIMIT 0, 10");
	while($row = $db->fetchArray($res)) {
		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'];
		$item->description = '<p>'.$row['description'].'</p>';

		$res_b = $db->Query("SELECT filepath FROM "._DBPREF."bigpicture WHERE content_id = '".$row['id']."' ORDER BY ordinal LIMIT 1");
		$row_b = $db->fetchArray($res_b);

		$item->description .= '<p><img src="'.STTC.'/images/bigpicture/'.str_replace('_normal.', '_thumb.', $row_b['filepath']).'" alt="" /></p>';

		$item->descriptionHtmlSyndicated = true;
		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = $row['id'];
		$item->category = $row['category'];

		$item->enclosure->url = STTC.'/images/bigpicture/'.str_replace('_normal.', '_thumb.', $row_b['filepath']);
		$item->enclosure->length = filesize(ROOT.'_static/images/bigpicture/'.str_replace('_normal.', '_thumb.', $row_b['filepath']));
		$item->enclosure->type = 'image/jpeg';

		$rss->addItem($item);
	}

}
elseif($q == 'sajat') { // saját tartalmak, minden
	$rss->title = $env->l['title'].' - Saját anyagok';

	$res = $db->Query("SELECT id, category_id, title, description, CONCAT(id,'-',alias) AS alias, datetime, content_type FROM "._DBPREF."content_temp
	WHERE (content_type IN (2,4,6) OR user_id = 1758 OR link IS NULL) AND datetime < NOW()
	ORDER BY datetime DESC LIMIT 0, 20");
	while($row = $db->fetchArray($res)) {
		$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
		$row['category'] = $env->l['content']['category_'.$row['category_id']];

		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'];
		//$item->description = htmlspecialchars($row['description'], ENT_QUOTES, 'UTF-8');
		$item->description = $env->strTruncate(strip_tags($row['description']), 400, '...');
		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = 'propeller@'.$row['id'];
		$item->category = $row['category'];

		if($row['content_type'] == 6) {
			$item->description = $row['description'];
			$res_b = $db->Query("SELECT filepath FROM "._DBPREF."bigpicture WHERE content_id = '".$row['id']."' ORDER BY ordinal LIMIT 1");
			$row_b = $db->fetchArray($res_b);
			$item->description .= '<p><img src="'.STTC.'/images/bigpicture/'.str_replace('_normal.', '_thumb.', $row_b['filepath']).'" alt="" /></p>';

			$item->enclosure->url = STTC.'/images/bigpicture/'.str_replace('_normal.', '_thumb.', $row_b['filepath']);
			$item->enclosure->length = filesize(ROOT.'_static/images/bigpicture/'.str_replace('_normal.', '_thumb.', $row_b['filepath']));
			$item->enclosure->type = 'image/jpeg';
		}


		$rss->addItem($item);
	}

}
elseif($q == 'hirkereso') { // hírkereősnek a saját tartalmak
	$rss->title = $env->l['title'].' - Hírkereső';

	$res = $db->Query("SELECT id, category_id, title, description, CONCAT(id,'-',alias) AS alias, datetime, content_type FROM "._DBPREF."content_temp
	WHERE (content_type IN (2,4,6) OR user_id = 1758 OR link IS NULL) AND datetime < NOW()
	ORDER BY datetime DESC LIMIT 0, 20");
	while($row = $db->fetchArray($res)) {
		$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
		$row['category'] = $env->l['content']['category_'.$row['category_id']];

		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'].'?utm_source=hirkereso.hu&utm_medium=referral';

		$item->description = $env->strTruncate(strip_tags($row['description']), 800, '...');

		// valami karakter basz néha elrontja az xml-t
		$item->description = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', '', $item->description);

		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = $row['id'];
		$item->category = $row['category'];

		$rss->addItem($item);
	}

}
elseif($q == 'hirstart') { // hírkereősnek a saját tartalmak
	$rss->title = $env->l['title'].' - Hírstart';

	$res = $db->Query("SELECT id, category_id, title, description, CONCAT(id,'-',alias) AS alias, datetime, content_type FROM "._DBPREF."content_temp
	WHERE (content_type IN (2,4,6) OR user_id = 1758 OR link IS NULL) AND datetime < NOW()
	ORDER BY datetime DESC LIMIT 0, 20");
	while($row = $db->fetchArray($res)) {
		$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
		$row['category'] = $env->l['content']['category_'.$row['category_id']];

		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'].'?utm_source=hirstart.hu&utm_medium=referral';
		//$item->description = htmlspecialchars($row['description'], ENT_QUOTES, 'UTF-8');
		$item->description = $env->strTruncate(strip_tags($row['description']), 800, '...');
		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = $row['id'];

	    // címkék kikeresése
	    $res_tags = $db->Query("SELECT tags.id, tags.tag FROM "._DBPREF."content_tags_conn conn
	    LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	    WHERE conn.content_id = ".$row['id']." ORDER BY tags.tag LIMIT 5");
	    $tags = array();
	    while($row_tags = $db->fetchAssoc($res_tags)) {
	        $tags[] = $row_tags['tag'];
	    }

	    if (in_array('baleset', $tags) || in_array('bűnügy', $tags)) {
			$item->category = 'Baleset-bűnügy';
	    } else {
	    	$item->category = $row['category'];
	    }

		$rss->addItem($item);
	}

}
elseif($q == 'mind') { // minden friss
	$rss->title = $env->l['title'].' - '.$env->l['content']['box_latest'];

	$res = $db->Query("SELECT id, category_id, title, description, CONCAT(id,'-',alias) AS alias, datetime FROM "._DBPREF."content_temp
	WHERE datetime < NOW() AND (content_type IS NULL OR content_type <> 7)
	ORDER BY datetime DESC LIMIT 0, 20");
	while($row = $db->fetchArray($res)) {
		$row['category_alias'] = $env->l['content']['category_alias_'.$row['category_id']];
		$row['category'] = $env->l['content']['category_'.$row['category_id']];

		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'];
		//$item->description = htmlspecialchars($row['description'], ENT_QUOTES, 'UTF-8');
		$item->description = $env->strTruncate(strip_tags($row['description']), 400, '...');
		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = 'propeller@'.$row['id'];
		$item->category = $row['category'];
		$rss->addItem($item);
	}
}
elseif(!empty($q)) { // adott rovat

	$res = $db->Query("SELECT c.id, title, description, CONCAT(c.id,'-',c.alias) AS alias, cc.alias AS category_alias, cc.category, datetime FROM "._DBPREF."content_temp c
	LEFT JOIN "._DBPREF."content_categories cc ON c.category_id = cc.id WHERE cc.alias = '".$db->escape($q)."' AND c.datetime < NOW() AND (c.content_type IS NULL OR c.content_type <> 7) ORDER BY c.datetime DESC LIMIT 0, 20");
	while($row = $db->fetchArray($res)) {
		$item = new FeedItem();
		$item->title = $row['title'];
		$item->link = $env->base.'/'.$row['category_alias'].'/'.$row['alias'];
		//$item->description = htmlspecialchars($row['description'], ENT_QUOTES, 'UTF-8');
		$item->description = $env->strTruncate(strip_tags($row['description']), 400, '...');
		$item->date = date('r', strtotime($row['datetime']));
		$item->guid = 'propeller@'.$row['id'];
		$item->category = $row['category'];
		$rss->title = $env->l['title'].' - '.$row['category'];
		$rss->addItem($item);
	}
}
else {

	die();

}

	$rss->saveFeed('RSS2.0', '../system/cache/feedcreator/content-'.$q.'.xml');

?>