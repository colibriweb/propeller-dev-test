<?php
defined('ENV') or die;

if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_EDITOR)) {

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$content = $_POST['content'];

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$content = $db->escape($content);

			$nav_arr = array();
			foreach($_POST['nav_label'] as $key => $value) {
				if(!empty($value) && !empty($_POST['nav_value'][$key])) {
					$nav_arr[$value] = $_POST['nav_value'][$key];
				}
			}
			//p($nav_arr);die;
			$nav_arr = json_encode($nav_arr, JSON_UNESCAPED_UNICODE);

			$res = $db->Query("UPDATE "._DBPREF."misc SET
			is_breaking = ".($content['is_breaking'] == '1' ? "'1'" : "'0'").",
			is_breaking_subtitle = '".$content['is_breaking_subtitle']."',
			navigation_links = '".$nav_arr."',
			user_content_blacklist = '".$content['user_content_blacklist']."'
			WHERE id = '1' LIMIT 1");

			// cache eldobása
			$env->dropFileCache('misc.cache');
			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=home&mode=edit');
				exit;
			}
		}
	}

// már meglévő elem esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if($_GET['mode'] == 'edit') { // módosító űrlap
		$action = 'update';

		$res = $db->Query("SELECT * FROM "._DBPREF."misc WHERE id = '1' LIMIT 1");
		$content = $db->fetchArray($res);

		$nav_label = array();
		$nav_value = array();
		$content['navigation_links'] = (array)json_decode($content['navigation_links']);
		foreach($content['navigation_links'] as $key => $value) {
			$nav_label[] = $key;
			$nav_value[] = $value;
		}

	}

?>

	<h1>Site beállítások</h1>
	<h2>Add meg a módosítandó dolgokat, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">
		<input type="hidden" name="action" id="action" value="<?= $action; ?>" />

		<fieldset>
			<label for="is_breaking">Breaking van:</label>
			<input type="checkbox" id="is_breaking" class="text" name="content[is_breaking]" value="1"<?php if($content['is_breaking']) echo ' checked="checked"'; ?> /><br class="clear" />
			<label>&nbsp;</label><i>Pipáld be, ha a címlap első (képes) doboza egy nagyon fontos, rendkívüli hír!</i><br class="clear" />

			<label for="is_breaking_subtitle">Breaking felirat:</label>
			<input type="text" id="is_breaking_subtitle" class="text" name="content[is_breaking_subtitle]" value="<?= htmlspecialchars($content['is_breaking_subtitle'], ENT_QUOTES); ?>" /><br />
			<label>&nbsp;</label><i>Beírhatsz egy figyelemfelkeltő címet, ami a breaking doboz fotójára kerül (pl. "Rendkívüli hír!")</i><br class="clear" />

<br />

			<label for="tag_itthon">Menüpontok:</label><br class="clear" />

		<?php for($i = 0; $i <= 12; $i++) { ?>
<label for="tag_<?= $i ?>_label" style="height:auto;padding:0;"><input type="text" id="tag_label_<?= $i ?>" class="text" style="width:auto;" name="nav_label[<?= $i ?>]" value="<?= htmlspecialchars($nav_label[$i], ENT_QUOTES); ?>" /></label>
<input type="text" id="tag_value_<?= $i ?>" class="text" name="nav_value[<?= $i ?>]" value="<?= htmlspecialchars($nav_value[$i], ENT_QUOTES); ?>" /><br />
		<?php } ?>

<br />


			<label for="user_content_blacklist">Tiltott szavak a beküldéseknél:</label>
			<textarea id="user_content_blacklist" rows="8" class="mceNoEditor" style="height:100px" name="content[user_content_blacklist]" /><?= htmlspecialchars($content['user_content_blacklist'], ENT_QUOTES); ?></textarea><br />
			<label>&nbsp;</label><i>Egy sorba egy szót írj (kisbetű-nagybetű nem számít)!</i><br class="clear" />


		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés"/>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>
<?php
}

?>
