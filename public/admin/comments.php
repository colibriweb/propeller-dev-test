<?php
defined('ENV') or die;

if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_MODERATOR)) {

	if(!isset($_GET['id'])) {
		header('Location: '.BACKEND.'/?op=comments&mode=show&category_id='.$_GET['category_id'].'&order='.$_SESSION['_comments_order']);
		exit;
	}

	if($_POST['action'] == 'insert') { // űrlapelküldés, kiértékelés új beszúráshoz

	}

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$comment = $_POST['comment'];
		//$comment['comment'] = $env->getSafeText($comment['comment'], 'UTF-8', true);

		if(empty($comment['content_id'])) { $error = 'Add meg a tartalom azonosítóját'; }
		if(strlen($comment['comment']) < 2) { $error = 'Add meg a hozzászólás szövegét'; }
		if(empty($comment['name']) && empty($comment['user_name'])) { $error = 'Add meg a felhasználó nevét'; }
		/*if($comment['type'] == '2' && empty($comment['mail'])) { $error = 'Írd be az e-mail címet'; }*/

		$comment['website'] = ($comment['website'] == 'http://') ? '' : $comment['website'];

		$comment = $db->escape($comment);

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			// kikeressük a felhasználó ID-jét a neve alapján
			$res_u = $db->Query("SELECT id FROM "._DBPREF."users WHERE name = '".$comment['name']."' LIMIT 1");
			$row_u = $db->fetchArray($res_u);
			$q_user_id = "'".$row_u['id']."'";

			$res = $db->Query("UPDATE "._DBPREF."content_comments SET
			content_id = '".$comment['content_id']."',
			user_id = '".$row_u['id']."',
			comment = '".$comment['comment']."'
			WHERE id = '".(int)$_GET['id']."' LIMIT 1");

			$db->Query("DELETE FROM "._DBPREF."content_comments_report WHERE comment_id = '".$_GET['id']."'");

			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=comments&mode=show&category_id='.$_GET['category_id'].'&order='.$_SESSION['_comments_order'].'&page='.$_SESSION['_comments_page']);
				exit;
			}
		}
	}

// már meglévő elem esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$res = $db->Query("SELECT content_id, user_id, comment, u.name
		FROM "._DBPREF."content_comments co
		LEFT JOIN "._DBPREF."users u ON co.user_id = u.id
		WHERE co.id = '".(int)$_GET['id']."' LIMIT 1");
		$comment = $db->fetchArray($res);

	}
	else { // új oldal
		$action = 'insert';
		$comment = array();
	}

?>

	<h1><?= ($action == 'insert') ? 'Új hozzászólás hozzáadása' : 'Hozzászólás szerkesztése'; ?></h1>
	<h2>Add meg a hozzászólás részleteit, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">
		<input type="hidden" name="action" id="action" value="<?= $action; ?>" />

		<fieldset>

			<label for="cid">Tartalom ID:</label>
			<input name="comment[content_id]" type="text" class="text required" value="<?= $comment['content_id']; ?>" title="Add meg a tartalom azonosítóját" /><br style="clear:both;" />

			<label for="comment">Hozzászólás:<br />
			<a href="javascript:;" onclick="$('comment').value = '<i>Ez a hozzászólás törlésre került. Többszöri törlés a felhasználó kitiltását vonja maga után.</i>';">Szöveg kitakarása</a></label>
			<textarea name="comment[comment]" rows="8" class="mceNoEditor required" style="height: 100px;" id="comment" title="Add meg a hozzászólás szövegét"><?= htmlspecialchars($comment['comment'], ENT_QUOTES); ?></textarea><br />

<div id="userid-wrapper">
			<label for="uid">Felhasználónév:</label>
			<input name="comment[name]" type="text" id="name" class="text required" value="<?= $comment['name']; ?>" title="Add meg a felhasználó nevét" /><br style="clear:both;" />
			<div class="auto_complete" id="name_autocomplete"></div>
			<script type="text/javascript">
			// <![CDATA[
				new Ajax.Autocompleter('name', 'name_autocomplete', 'requests.php?what=get_usernames');
			// ]]>
			</script>
</div>


		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés"/>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="redirDel(\'comments\', '.$_GET['id'].');" />' : ''; ?>
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');

		function checkIsChecked(n) {
			if(n.value == '1') {
				$('userid-wrapper').setStyle({display: 'block'});
				$('name-wrapper').setStyle({display: 'none'});
			}
			else {
				$('name-wrapper').setStyle({display: 'block'});
				$('userid-wrapper').setStyle({display: 'none'});
			}
		}

	// ]]>
	</script>
<?php
}

if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_MODERATOR)) {
?>
	<h1>Hozzászólások szerkesztése</h1>
	<h2>Válaszd ki a módosítandó hozzászólást az alábbi listából.</h2>

	<br class="clear" />


<?php
$order = "";
if(!empty($_GET['q'])) {
	$words = explode(" ", $_GET['q']);
	while(list($k, $v) = each($words)){
		$v = $db->escape($v);
		if(!$j) {
			$order .= "(co.comment LIKE '%".$v."%' OR u.name LIKE '%".$v."%' OR l.host LIKE '%".$v."%' OR l.ip LIKE '%".$v."%')";
			$j = 1;
		}
		else {
			$order .= " AND (co.comment LIKE '%".$v."%' OR u.name LIKE '%".$v."%' OR l.host LIKE '%".$v."%' OR l.ip LIKE '%".$v."%')";
		}
	}
	$order .= " AND ";
}


if($_GET['category_id'] == 0) {
	$order .= "c.category_id IN (".implode(',', $myCategories).") ";
}
else {
	$order .= "c.category_id = '".(int)$_GET['category_id']."' ";
}


if($_GET['order'] == 'removed') {
	$table = _DBPREF."content_comments_del";
	$flag = 1;
	$_SESSION['_comments_order'] = 'removed';
}
elseif($_GET['order'] == 'duplicated') {
	$flag = 2;
	$_SESSION['_comments_order'] = 'duplicated';
}
elseif($_GET['order'] == 'likes') {
	$table = _DBPREF."content_comments";
	$flag = 3;
	$_SESSION['_comments_order'] = 'likes';

	$order .= " AND co.datetime > '".date("Y-m-d H:i:s", strtotime('-3 days'))."' ";
}
else {
	$table = _DBPREF."content_comments";
	$flag = 0;
	$_SESSION['_comments_order'] = '';
}


?>
	<form id="backend-search" action="" method="get">
		<input type="hidden" name="op" value="comments" />
		<input type="hidden" name="mode" value="show" />
		<input type="hidden" name="category_id" value="<?= (int)$_GET['category_id']; ?>" />
		<input type="text" name="q" class="text" value="<?= htmlspecialchars($_GET['q'], ENT_QUOTES); ?>" /> <input type="submit" class="button" value="Keresés" />
	</form>


	<p id="lead">
		<?= ($flag == 0) ? 'legfrissebbek' : '<a href="?op=comments&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'">legfrissebbek</a>'; ?> |
		<?= ($flag == 3) ? 'legjobbak' : '<a href="?op=comments&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=likes">legjobbak</a>'; ?> |
		<?= ($flag == 1) ? 'töröltek' : '<a href="?op=comments&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=removed">töröltek</a>'; ?> |
		<?= ($flag == 2) ? 'dupla nevek' : '<a href="?op=comments&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=duplicated">dupla nevek</a>'; ?>
	</p>


<?php
// lapozások kiszámítása, ennyi legyen egy oldalon:
	$perpage = 30;

	if(empty($_GET['page'])) { // első oldalon vagyunk
		$listfrom = 0;
		$pnow = 1;
		$_SESSION['_comments_page'] = 1;
	}
	else { // next pages
		$listfrom = $_GET['page'] * $perpage - $perpage;
		$pnow = $_GET['page'];
		$_SESSION['_comments_page'] = (int)$_GET['page'];
	}

	if(($_GET['order'] == 'removed') || ($_GET['order'] == 'likes') || empty($_GET['order'])) {

		if($_GET['order'] == 'likes') {
			$ord = " likes DESC ";
		}
		else {
			$ord = " co.datetime DESC ";
		}

		$res = $env->db->Query("
		SELECT SQL_CALC_FOUND_ROWS category_id, title, CONCAT(c.id,'-',c.alias) AS alias, u.name AS user_name, u.alias AS user_alias, co.id, co.user_id, comment, co.datetime,
		l.ip, l.host, l.useragent, likes, r.comment_id AS reported
		FROM ".$table." co
		LEFT JOIN "._DBPREF."content c ON co.content_id = c.id
		LEFT JOIN "._DBPREF."users u ON co.user_id = u.id
		LEFT JOIN "._DBPREF."content_comments_log l ON co.id = l.comment_id
		LEFT JOIN "._DBPREF."content_comments_report r ON co.id = r.comment_id
		WHERE co.id > "._SPD_COMMENT_ID." AND ".$order." ORDER BY ".$ord." LIMIT ".$listfrom.", ".$perpage);

		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);


		if(!$rows[0]) {
			echo '<p>Nincsenek a feltételnek megfelelő sorok...</p>';
		}
		else {
		$pgs = ceil($rows[0] / $perpage); // oldalak száma

		$i = $listfrom + 1; // lista sorszáma
		echo '<table cellspacing="0" cellpadding="1" width="100%">';
			echo '
			<tr class="head">
				<td valign="top">&nbsp;</td>
				<td valign="top"><b>Hozzászólás</b></td>
				<td valign="top"><b>Név</b></td>
				<td valign="top"><b>Tetszik</b></td>
				<td valign="top"><b>Dátum</b></td>
				<td></td>
			</tr>
			';
		$diffcolor = '';
			while($row = $env->db->fetchArray($res)) {
				$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';

				if($row['reported']) { $diffcolor = '#FFF998'; }

				$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));

		//htmlspecialchars(mb_substr($row['comment'], 0, 100, 'UTF-8'), ENT_QUOTES)
				echo '
				<tr style="background-color: '.$diffcolor.'">
					<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
					<td valign="top">'.htmlspecialchars($row['comment'], ENT_QUOTES).' <a href="?op=comments&amp;mode=edit&amp;id='.$row['id'].'&amp;category_id='.$row['category_id'].'">[...]</a>
					<div id="more-'.$row['id'].'" style="display:none;">

		<br /><b>IP:</b> '.$row['ip'].' ('.$row['host'].') - <a href="http://myip.ms/info/whois/'.$row['ip'].'" onclick="window.open(this.href, \'_blank\'); return false;">whois</a><br />
		<b>User-agent</b>: '.$row['useragent'].'</div></td>

				<td valign="top" nowrap="nowrap">';
				echo ($row['user_id'] != NULL) ? '<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['user_alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlspecialchars($row['user_name'], ENT_QUOTES).'</a>' : '<a href="mailto:'.$row['mail'].'"><i>'.$row['user_name'].'</i></a>';
				echo '</td>

				<td valign="top" nowrap="nowrap" align="center">'.(int)$row['likes'].'</td>
				<td valign="top" nowrap="nowrap">'.$admin->dateFormat($row['datetime']).'</td>

				<td valign="top" align="right" nowrap="nowrap">
					<a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['alias'].'#c'.$row['id'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
					<a href="javascript:;" onclick="Element.toggle(\'more-'.$row['id'].'\');" title="Hozzászólás adatai"><img src="images/icon/search.gif" alt="" /></a>
					<a href="javascript:redirDel(\'comments\', '.$row['id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
				</td>
			</tr>
			';
		$i++;
		}
		echo '</table>';


		echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
		$turn = ($pnow <= 15) ? 1 : ($pnow - 15);
		$tto = ($pnow <= 15) ? (($pgs < 30) ? $pgs : 30): ($pnow+15);
			for($j = $turn; $j <= $tto; $j++) { // következő, előző oldalak számainak kiírása
				if($pnow == $j) { // ez az oldal, itt állunk most
					echo ' <b>'.$j.'</b>';
				}
				else { // további oldalak linkkel
					if(!empty($_GET['q']))
						echo ' <a href="?op=comments&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;q='.htmlentities($_GET['q'], ENT_QUOTES).'&amp;page='.$j.'">'.$j.'</a> ';
					else
						echo ' <a href="?op=comments&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order='.$_GET['order'].(($_GET['order'] == 'category') ? '&amp;cid='.$_GET['cid'] : '').'&amp;page='.$j.'">'.$j.'</a> ';
				}
			}
		echo '</p>';

		}
	}
	elseif($_GET['order'] == 'duplicated') { // troll juzerek kikeresése


		$res = $env->db->Query("
		SELECT COUNT(DISTINCT user_id) AS num, ip, host
		FROM "._DBPREF."content_comments_log
		WHERE removed = '0' AND user_id IS NOT NULL
		GROUP BY ip
		ORDER BY num DESC, INET_ATON(ip) ASC LIMIT 0, 50");


		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);


		$i = $listfrom + 1; // lista sorszáma
		echo '<table cellspacing="0" cellpadding="1" width="100%">';
			echo '
			<tr class="head">
				<td valign="top">&nbsp;</td>
				<td valign="top"><b>Szum</b></td>
				<td valign="top"><b>IP-cím</b></td>

				<td valign="top"><b>Név</b></td>
			</tr>
			';
		$diffcolor = '';
			while($row = $env->db->fetchArray($res)) {
				$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';

				if($row['reported']) { $diffcolor = '#FFF998'; }


		//htmlspecialchars(mb_substr($row['comment'], 0, 100, 'UTF-8'), ENT_QUOTES)
				echo '
				<tr style="background-color: '.$diffcolor.'">
					<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
					<td valign="top">'.$row['num'].'</td>
					<td valign="top" nowrap="nowrap"><b>'.$row['ip'].'</b> - <a href="http://myip.ms/info/whois/'.$row['ip'].'" onclick="window.open(this.href, \'_blank\'); return false;">whois</a><br />
					<small>'.$row['host'].'</small></td>


					<td valign="top">';

						$res_dup = $env->db->Query("
						SELECT u.id, u.name AS user_name, u.alias AS user_alias, useragent
						FROM "._DBPREF."content_comments_log l
						RIGHT JOIN "._DBPREF."content_comments co ON l.comment_id = co.id
						LEFT JOIN "._DBPREF."users u ON co.user_id = u.id
						WHERE ip = '".$row['ip']."'
						GROUP BY user_name
						ORDER BY co.datetime DESC");
						echo '<table>';
						while($row_dup = $env->db->fetchArray($res_dup)) {
							echo '<tr>';
							echo '<td style="width:110px;"><a href="?op=users&amp;mode=edit&amp;id='.$row_dup['id'].'">'.htmlspecialchars($row_dup['user_name'], ENT_QUOTES).'</a></td>';
							echo '<td><small>'.$row_dup['useragent'].'</small></td>';
							echo '</tr>';
						}
						echo '</table>';
//							<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row_dup['user_alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">profil</a><br >';

					echo '</td>





			</tr>
			';
		$i++;
		}
		echo '</table>';



	}


}


if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_MODERATOR)) {

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés
		$res_co = $db->Query("SELECT content_id FROM "._DBPREF."content_comments WHERE id = '".$_GET['id']."' LIMIT 1");
		$row_co = $db->fetchArray($res_co);

		// átmozgatjuk a lomtárba a kommentet
		$res = $db->Query("INSERT INTO "._DBPREF."content_comments_del SELECT * FROM "._DBPREF."content_comments WHERE id = '".$_GET['id']."' LIMIT 1");

		// logban is töröltre állítjuk, hogy a komment gyorsító kverikbe ne kerüljön bele
		$db->Query("UPDATE "._DBPREF."content_comments_log SET removed = '1' WHERE comment_id = '".$_GET['id']."' LIMIT 1");

		if($res) {
			$db->Query("DELETE FROM "._DBPREF."content_comments WHERE id = '".$_GET['id']."' LIMIT 1");

			$db->Query("UPDATE "._DBPREF."content SET comments = comments - 1 WHERE id = '".$row_co['content_id']."' LIMIT 1");
		}

		$db->Query("DELETE FROM "._DBPREF."users_activities WHERE content_id = '".$row_co['content_id']."'");
		$db->Query("DELETE FROM "._DBPREF."users_activities WHERE comment_id = '".$_GET['id']."'");

		$db->Query("DELETE FROM "._DBPREF."content_comments_report WHERE comment_id = '".$_GET['id']."'");

		// cache eldobása
		$db->DropQueryCache('commented_0.cache');

		header('Location: '.BACKEND.'/?op=comments&mode=show&category_id='.$_GET['category_id'].'&order='.$_SESSION['_comments_order'].'&page='.$_SESSION['_comments_page']);

		?>
		<h1>Hozzászólás törlése</h1>
		<h2>A hozzászólás törlése megtörtént.</h2>
		<?php
	}
}

?>
