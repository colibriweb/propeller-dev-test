<?php
defined('ENV') or die;

function removeEmoji($text) {
    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}

if(($_GET['mode'] == 'edit')) {

	/**
	* Címkék beszúrása vagy módosítása
	* @param array $tags Címkék stringben
	* @return array Sikeres művelet esetén true-val, egyébként false-sal
	*/
	function setTags($tags, $id) {

		global $env, $db;

		// címkék feldolgozása tömbbe
		$tags = $env->getTagArray($tags, ",", true, (int)$env->c['content']['tagsnum']);

		// meglévők törlése, ha voltak (edit esetén)
		$res = $db->Query("DELETE FROM "._DBPREF."content_tags_conn WHERE content_id = '".$id."'");

		if(is_array($tags)) { // vannak címkék megadva
			foreach($tags as $i => $value) {
				$query .= "tag = '".$db->escape($value)."' OR ";
			}
			$query = rtrim($query, "OR ");
			$res_old_i = $db->Query("SELECT id, tag FROM "._DBPREF."content_tags WHERE ".$query);

			if($db->numRows($res_old_i)) { // voltak már korábban használt címkék
				while($row_i = $db->fetchArray($res_old_i)) {
					$res_tags_events = $db->Query("INSERT
					INTO "._DBPREF."content_tags_conn (content_id, tag_id) VALUES ('".$id."', '".$row_i['id']."')");

					foreach($tags as $i => $value) {
						if($value == $row_i['tag']) {
							unset($tags[$i]);
						}
					}
				}
			}

			if(count($tags) == 0) { // elfogytak, nincs más
				return true;
			}

			// meglévők beírása és összekapcsolás a tartalommal
			foreach($tags as $i => $value) {
				$res_tags = $db->Query("INSERT
				INTO "._DBPREF."content_tags (tag) VALUES ('".$db->escape($value)."')");

				$last = $db->lastInsertID($res_tags, 'id', _DBPREF."content_tags");

				$res_tags_content = $db->Query("INSERT
				INTO "._DBPREF."content_tags_conn (content_id, tag_id) VALUES ('".$id."', '".$last."')");

				unset($tags[$i]);
			}
		}
		// nem használt címkék törlése a _content_tags -ból, lásd: /cron/daily.php
		return true;

	}

	if($_POST['action'] == 'insert' && $admin->hasRight(ROLE_EDITOR)) { // űrlapelküldés, kiértékelés új beszúráshoz
		$content = $_POST['content'];
		$content['link'] = ($content['link'] == 'http://') ? '' : $content['link'];
		$related_ids = (array)$_POST['related']['content_id'];

		if(!empty($content['link'])) $content['description'] = strip_tags($content['description']);

		$content['description'] = removeEmoji($content['description']);
		$content['description'] = preg_replace('/<p>\p{Z}*<\/p>/u', '',$content['description']);
		$content['description'] = preg_replace('/<br \/>$/', '', $content['description']);
		$content['description'] = str_ireplace('<p> </p>', "", $content['description']);
		$content['description'] = str_ireplace(array('<strong>', '</strong>'), array('<b>', '</b>'), $content['description']);
		$content['description'] = str_ireplace('<br /><br />', "</p>\n<p>", $content['description']);
		$content['description'] = preg_replace("/<br \/>\s?<br \/>/", "</p>\n<p>", $content['description']);
		$content['description'] = str_ireplace('<br /></b><br />', '</b><br /><br />', $content['description']);
		$content['description'] = $env->dashReplace($content['description']);
		$content['title'] = $env->dashReplace($content['title']);
		$content['fullembed'] = trim(removeEmoji($content['fullembed']));

		if(empty($content['title'])) { $error = 'Írd be a tartalom címét'; }
		//if(in_array($content['content_type'], array('1')) && empty($content['link'])) { $error = 'Add meg a tartalom URL-jét'; }
		if(empty($content['category_id'])) { $error = 'Válaszd ki a tartalom kategóriáját'; }
		if(empty($content['datetime'])) { $error = 'Add meg a tartalom élesedésének dátumát'; }
		//if(!is_numeric($content['counter'])) { $error = 'Add meg a tartalom számláló értékét'; }
		//if($content['content_type'] == '1' && empty($content['fullembed'])) { $error = 'Add meg a beillesztőkódot'; }
		if($content['content_type'] == '6' && count($_FILES['files']['name']) < 2) { $error = 'Ilyen kevés képpel nem készíthető sorozat'; }

		$content = $db->escape($content);

		$content['datetime'] = date('Y-m-d H:i:s', strtotime($content['datetime']));

		if(in_array($content['content_type'], array('0', '1')) && !empty($content['link'])) {
			$res_link = $db->Query("SELECT id FROM "._DBPREF."content WHERE link = '".$content['link']."' LIMIT 1");
			if($db->numRows($res_link)) { // van már ilyen link
				$error = 'Ezt a linket már beküldte valaki';
			}
		}

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			if(!empty($content['fullembed'])) { // van videó, 1-es content_tpye legyen
				$q_content_type = "'1'";
				$content['content_type'] = 1;
			}

			$alias = $env->getSefUrl($content['title']);
			$q_feed_id = !empty($content['feed_id']) ? "'".$content['feed_id']."'" : "NULL";
			$q_link = !empty($content['link']) ? "'".$content['link']."'" : "NULL";
			$q_content_type = ($content['content_type'] == '0') ? "NULL" : "'".(int)$content['content_type']."'";
			$q_excerpt = empty($content['excerpt']) ? "NULL" : "'".$content['excerpt']."'";

			$rel_arr = array();
			foreach($related_ids as $key => $value) {
				if(is_numeric($value)) { // van content_id
					$rel_arr[] = $value;
				}
			}

			if($content['name']) { // van név megadva
				if(empty($content['link'])) {
					$q_user_id = "'1758'";

					if(!empty($content['name']) && ($content['name'] != 'Propeller')) { // saját szerzős írás lesz
						// kikeressük a szerző ID-jét a neve alapján
						$res_a = $db->Query("SELECT id FROM "._DBPREF."authors WHERE name COLLATE utf8_hungarian_ci = '".$content['name']."' LIMIT 1");
						$row_a = $db->fetchArray($res_a);
						if(!$row_a['id']) {
							$res_pa = $db->Query("INSERT INTO "._DBPREF."authors (name, alias) VALUES
							('".$content['name']."', '".$env->getSefUrl($content['name'])."')");
							$author_id = $db->lastInsertID($res, 'id', _DBPREF."authors");

						}
						else {
							$author_id = $row_a['id'];
						}
					}
				}
				else {
					// kikeressük a felhasználó ID-jét a neve alapján
					$res_u = $db->Query("SELECT id FROM "._DBPREF."users WHERE name = '".$content['name']."' LIMIT 1");
					$row_u = $db->fetchArray($res_u);
					$q_user_id = "'".$row_u['id']."'";
				}
			}
			else {
				$q_user_id = "NULL";
			}

			$res = $db->Query("INSERT INTO "._DBPREF."content (user_id, category_id, title, link, description, alias, datetime, counter, content_type, related, excerpt) VALUES (
			".$q_user_id.", '".$content['category_id']."', '".$content['title']."', ".$q_link.", '".$content['description']."', '".$alias."', '".$content['datetime']."', '0', ".$q_content_type.", '".implode(',',$rel_arr)."', ".$q_excerpt.")");

			if($res) { // nincs hiba
				$content_id = $db->lastInsertID($res, 'id', _DBPREF."content");

				// temp tábla írás
				$db->Query("INSERT INTO "._DBPREF."content_temp
				(id, user_id, category_id, title, link, description, alias, datetime, content_type) VALUES
				('".$content_id."', ".$q_user_id.", '".$content['category_id']."', '".$content['title']."', ".$q_link.", '".$content['description']."', '".$alias."', '".$content['datetime']."', ".$q_content_type.")");

				setTags($content['tags'], $content_id);

				// szerző, ha volt
				if(isset($author_id)) {
					$db->Query("INSERT INTO "._DBPREF."content_authors_conn (content_id, author_id) VALUES (
					'".$content_id."', '".$author_id."')");
				}

/******/		if($content['content_type'] == '1') { // videó tartalom
					$db->Query("INSERT INTO "._DBPREF."content_videos (content_id, fullembed) VALUES ('".$content_id."', '".$content['fullembed']."')");
				}
				elseif($content['content_type'] == '6') { // nagykép sorozat
					/** Rendszerosztály: képkezelés és feltöltés */
					require_once('../system/Image.class.php');

					$_POST['image']['title'] = $db->escape($_POST['image']['title']);
					$_POST['image']['taken'] = $db->escape($_POST['image']['taken']);
					$_POST['image']['author'] = $db->escape($_POST['image']['author']);

					foreach($_FILES['files']['name'] AS $key => $value) {
						if(!empty($value)) { // kép feltöltése
							$tmp = array('name' => $_FILES['files']['name'][$key], 'type' => $_FILES['files']['type'][$key], 'tmp_name' => $_FILES['files']['tmp_name'][$key], 'error' => $_FILES['files']['error'][$key], 'size' => $_FILES['files']['size'][$key]);

							$image = new Image($tmp);
							$image->imgBase = '../../public_static/images/bigpicture/';

							$image->fileID = $env->getSefUrl(substr($tmp['name'], 0, strrpos($tmp['name'], ".")));
							$image->fileID = str_replace('-', '_', $image->fileID);

							$image->imagePath = $image->imgBase.'/'.floor($content_id/10000).'/'.$content_id;

							if(!$image->saveForBigpicture()) {
								die($image->error);
							}
							else {
								$path = floor($content_id/10000).'/'.$content_id.'/'.$image->toStore;

								$q_ititle = !empty($_POST['image']['title'][$key]) ? "'".$_POST['image']['title'][$key]."'" : "NULL";
								$q_itaken = !empty($_POST['image']['taken'][$key]) ? "'".$_POST['image']['title'][$key]."'" : "NULL";
								$q_iauthor = !empty($_POST['image']['author'][$key]) ? "'".$_POST['image']['author'][$key]."'" : "NULL";

								$db->Query("INSERT INTO "._DBPREF."bigpicture (content_id, title, taken, author, filepath, ordinal, cover)
								VALUES ('".$content_id."', ".$q_ititle.", ".$q_itaken.", ".$q_iauthor.", '".$path."', '".(int)$key."', NULL)");
							}
						}
					}
				}

				$db->DropQueryCache('latest.cache');

		if (ENV == 'prod') {
			/** Rendszerosztály: e-mail küldés */
			require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
			$phpMailer = new PHPMailer();

			// levélküldő osztály feltöltése és levélküldés
			$phpMailer->CharSet = 'UTF-8';
			$phpMailer->Subject = 'Új hír: '.$content['title'];
			$phpMailer->FromName = 'Propeller Szerk. – '.$_SESSION['user']['name'];
			$phpMailer->From = 'szerk@propeller.hu';

			//foreach($admin->getStaffMailsForNotifications() as $value) {
				$phpMailer->AddAddress('szerk-ertesites@propeller.hu');
			//}

			$cat_alias = $env->l['content']['category_alias_'.$content['category_id']];
			$phpMailer->Body =
				"Megnyitás: http://".HOST."/".$cat_alias."/".$content_id."-".$alias.
				"\n\nSzerkesztés: http://".HOST."/admin/?op=content&mode=edit&id=".$content_id."&category_id=".$content['category_id'];

			$phpMailer->Send();
		}

		$db->Query("INSERT INTO "._DBPREF."users_backend_editorlog (user_id, content_id, datetime, action)
			VALUES ('".$_SESSION['user']['id']."', '".$content_id."', NOW(), 'insert')");



				if($_POST['redir'] == 'edit')
					header('Location: '.BACKEND.'/?op=content&mode=edit&id='.$content_id.'&category_id='.$content['category_id']);
				else
					header('Location: '.BACKEND.'/?op=content&mode=show&category_id='.$content['category_id'].'&order='.$_SESSION['_content_order'].'&page='.$_SESSION['_content_page']);
				exit;
			}
		}
	}

	if($_POST['action'] == 'update' && $admin->hasRight(ROLE_EDITOR)) { // űrlapelküldés, kiértékelés meglévő mentéséhez

		$content = $_POST['content'];
		$content['link'] = ($content['link'] == 'http://') ? '' : $content['link'];
		$related_ids = (array)$_POST['related']['content_id'];

		if(!empty($content['link'])) $content['description'] = strip_tags($content['description']);

		if($_SESSION['user']['id'] == 1) {
			//p($content['description']);
			//die;
		}

		$content['description'] = removeEmoji($content['description']);
		$content['description'] = preg_replace('/<p>\p{Z}*<\/p>/u', '',$content['description']);
		$content['description'] = preg_replace('/<br \/>$/', '', $content['description']);
		$content['description'] = str_ireplace('<p> </p>', "", $content['description']);
		$content['description'] = str_ireplace(array('<strong>', '</strong>'), array('<b>', '</b>'), $content['description']);
		$content['description'] = str_ireplace('<br /><br />', "</p>\n<p>", $content['description']);
		$content['description'] = preg_replace("/<br \/>\s?<br \/>/", "</p>\n<p>", $content['description']);
		$content['description'] = str_ireplace('<br /></b><br />', '</b><br /><br />', $content['description']);
		$content['description'] = $env->dashReplace($content['description']);
		$content['title'] = $env->dashReplace($content['title']);
		$content['fullembed'] = trim(removeEmoji($content['fullembed']));


		if(empty($content['title'])) { $error = 'Írd be a tartalom címét'; }
		//if(in_array($content['content_type'], array('1')) && empty($content['link'])) { $error = 'Add meg a tartalom URL-jét'; }
		if(empty($content['category_id'])) { $error = 'Válaszd ki a tartalom kategóriáját'; }
		if(empty($content['datetime'])) { $error = 'Add meg a tartalom élesedésének dátumát'; }
		//if(!is_numeric($content['counter'])) { $error = 'Add meg a tartalom számláló értékét'; }
		//if($content['content_type'] == '1' && empty($content['fullembed'])) { $error = 'Add meg a beillesztőkódot'; }
		if($content['content_type'] == '6' && count($_POST['image']['title']) < 2) { $error = 'Ilyen kevés képpel nem készíthető sorozat'; }

		$content = $db->escape($content);

		if(in_array($content['content_type'], array('0', '1')) && !empty($content['link'])) {
			$res_link = $db->Query("SELECT id FROM "._DBPREF."content WHERE link = '".$content['link']."' AND id <> '".(int)$_GET['id']."' LIMIT 1");
			if($db->numRows($res_link)) { // van már ilyen link
				$error = 'Ezt a linket már beküldte valaki';
			}
		}

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			//$alias = $env->getSefUrl($content['alias']);

			if(!empty($content['fullembed'])) { // van videó, 1-es content_tpye legyen
				$q_content_type = "'1'";
				$content['content_type'] = 1;
			}
			if(empty($content['fullembed']) && ($content['content_type'] == 1)) { // eddig video volt, de már nem az
				$content['content_type'] = 0;
				$need_del_video = true;
			}

			$q_feed_id = !empty($content['feed_id']) ? "'".$content['feed_id']."'" : "NULL";
			$q_link = !empty($content['link']) ? "'".$content['link']."'" : "NULL";
			$q_content_type = ($content['content_type'] == '0') ? "NULL" : "'".(int)$content['content_type']."'";

			$q_datetime = date('Y-m-d H:i:s', strtotime($content['datetime']));
			$q_excerpt = empty($content['excerpt']) ? "NULL" : "'".$content['excerpt']."'";

			$rel_arr = array();
			foreach($related_ids as $key => $value) {
				if(is_numeric($value)) { // van content_id
					$rel_arr[] = $value;
				}
			}


			if(!empty($content['name'])) { // van név megadva

				if(empty($content['link'])) {
					$q_user_id = "'1758'";
					$row_u['id'] = 1758;

					if(!empty($content['name']) && ($content['name'] != 'Propeller')) { // saját szerzős írás lesz

						// kikeressük a szerző ID-jét a neve alapján
						$res_a = $db->Query("SELECT id FROM "._DBPREF."authors WHERE name COLLATE utf8_hungarian_ci = '".$content['name']."' LIMIT 1");
						$row_a = $db->fetchArray($res_a);
						if(!$row_a['id']) {
							$res_pa = $db->Query("INSERT INTO "._DBPREF."authors (name, alias) VALUES
							('".$content['name']."', '".$env->getSefUrl($content['name'])."')");
							$author_id = $db->lastInsertID($res, 'id', _DBPREF."authors");

						}
						else {
							$author_id = $row_a['id'];
						}
					}
				}
				else {
					// kikeressük a felhasználó ID-jét a neve alapján
					$res_u = $db->Query("SELECT id FROM "._DBPREF."users WHERE name = '".$content['name']."' LIMIT 1");
					$row_u = $db->fetchArray($res_u);
					$q_user_id = "'".$row_u['id']."'";
				}

				// megnézzük, hogy most kapta-e a nevet (előtte beküldött volt-e), mert akkor kell a $needmail a szerkesztőknek
				$res_ne = $db->Query("SELECT user_id, alias, comments FROM "._DBPREF."content WHERE id = '".(int)$_GET['id']."' LIMIT 1");
				$row_ne = $db->fetchArray($res_ne);
				if(($row_u['id'] != $row_ne['user_id']) && ($row_u['id'] == 1758)) {
					$needmail = true;
				}

			}
			else {
				$q_user_id = "NULL";
			}

			$res = $db->Query("UPDATE "._DBPREF."content SET
			user_id = ".$q_user_id.",
			feed_id = ".$q_feed_id.",
			category_id = '".$content['category_id']."',
			title = '".$content['title']."',
			link = ".$q_link.",
			description = '".$content['description']."',
			#alias = '".$alias."',
			datetime = '".$q_datetime."',
			content_type = ".$q_content_type.",
			related = '".implode(',',$rel_arr)."',
			excerpt = ".$q_excerpt."
			WHERE id = '".(int)$_GET['id']."' LIMIT 1");

			if($res) { // nincs hiba

				// temp táblában módosítás (ha még ott van)
				$res = $db->Query("UPDATE "._DBPREF."content_temp SET
				user_id = ".$q_user_id.",
				feed_id = ".$q_feed_id.",
				category_id = '".$content['category_id']."',
				title = '".$content['title']."',
				link = ".$q_link.",
				description = '".$content['description']."',
				#alias = '".$alias."',
				datetime = '".$q_datetime."',
				content_type = ".$q_content_type."
				WHERE id = '".(int)$_GET['id']."' LIMIT 1");

				setTags($content['tags'], (int)$_GET['id']);


				// szerző, ha volt
				if(isset($author_id)) {
					$db->Query("DELETE FROM "._DBPREF."content_authors_conn WHERE content_id = ".(int)$_GET['id']);

					$db->Query("INSERT INTO "._DBPREF."content_authors_conn (content_id, author_id) VALUES (
					'".(int)$_GET['id']."', '".$author_id."')");
				}



				if($need_del_video) {
					$db->Query("DELETE FROM "._DBPREF."content_videos WHERE content_id = '".(int)$_GET['id']."' LIMIT 1");
				}

				if($content['content_type'] == '1') { // videó tartalom
					$res_upv = $db->Query("UPDATE "._DBPREF."content_videos SET fullembed = '".$content['fullembed']."' WHERE content_id = '".(int)$_GET['id']."' LIMIT 1");
					if(!$db->affectedRows($res_upv)) {
						$db->Query("INSERT INTO "._DBPREF."content_videos (content_id, fullembed) VALUES ('".(int)$_GET['id']."', '".$content['fullembed']."')");
					}
				}
				elseif($content['content_type'] == '6') { // nagykép sorozat
					/** Rendszerosztály: képkezelés és feltöltés */
					require_once('../system/Image.class.php');

					$res = $db->Query("DELETE FROM "._DBPREF."bigpicture WHERE content_id = '".(int)$_GET['id']."'");


					$_POST['image']['title'] = $db->escape($_POST['image']['title']);
					$_POST['image']['author'] = $db->escape($_POST['image']['author']);

					foreach($_POST['image']['title'] AS $key => $value) {
//						if(!empty($value)) { // kép feltöltése

						$q_ititle = !empty($_POST['image']['title'][$key]) ? "'".$_POST['image']['title'][$key]."'" : "NULL";
						$q_itaken = !empty($_POST['image']['taken'][$key]) ? "'".$_POST['image']['taken'][$key]."'" : "NULL";
						$q_iauthor = !empty($_POST['image']['author'][$key]) ? "'".$_POST['image']['author'][$key]."'" : "NULL";



if(!empty($_FILES['files']['name'][$key])) { // új feltöltés lesz
	$tmp = array('name' => $_FILES['files']['name'][$key], 'type' => $_FILES['files']['type'][$key], 'tmp_name' => $_FILES['files']['tmp_name'][$key], 'error' => $_FILES['files']['error'][$key], 'size' => $_FILES['files']['size'][$key]);

	$image = new Image($tmp);
	$image->imgBase = '../../public_static/images/bigpicture/';

	$image->fileID = $env->getSefUrl(substr($tmp['name'], 0, strrpos($tmp['name'], ".")));
	$image->fileID = str_replace('-', '_', $image->fileID);

	$image->imagePath = $image->imgBase.'/'.floor((int)$_GET['id']/10000).'/'.(int)$_GET['id'];

	if(!$image->saveForBigpicture()) {
		die($image->error);
	}
	else {
		$path = floor((int)$_GET['id']/10000).'/'.(int)$_GET['id'].'/'.$image->toStore;

		$db->Query("INSERT INTO "._DBPREF."bigpicture (content_id, title, taken, author, filepath, ordinal, cover)
		VALUES ('".(int)$_GET['id']."', ".$q_ititle.", ".$q_itaken.", ".$q_iauthor.", '".$path."', '".(int)$key."', NULL)");
	}
}
else { // ez egy meglévő kép már, csak filepath-ot kell beírni
	if(!empty($_POST['image']['filepath'][$key])) {
		$db->Query("INSERT INTO "._DBPREF."bigpicture (content_id, title, taken, author, filepath, ordinal, cover)
		VALUES ('".(int)$_GET['id']."', ".$q_ititle.", ".$q_itaken.", ".$q_iauthor.", '".$_POST['image']['filepath'][$key]."', '".(int)$key."', NULL)");
	}
}




					} // foreach

				} // elseif contenty_type == 6

				$db->DropQueryCache('latest.cache');

				if($needmail && (ENV == 'prod')) { // beküldött tartalomból lett saját, menjen az átírásról mail
					/** Rendszerosztály: e-mail küldés */
					require(ROOT.'/system/libraries/phpmailer/class.phpmailer.php');
					$phpMailer = new PHPMailer();

					// levélküldő osztály feltöltése és levélküldés
					$phpMailer->CharSet = 'UTF-8';
					$phpMailer->Subject = 'Átírt hír: '.$content['title'];
					$phpMailer->FromName = 'Propeller Szerk. – '.$_SESSION['user']['name'];
					$phpMailer->From = 'szerk@propeller.hu';

					//foreach($admin->getStaffMailsForNotifications() as $value) {
						$phpMailer->AddAddress('szerk-ertesites@propeller.hu');
					//}

					$cat_alias = $env->l['content']['category_alias_'.$content['category_id']];
					$phpMailer->Body =
					$content['title']."\n\n".
					"Megnyitás: http://".HOST."/".$cat_alias."/".(int)$_GET['id']."-".$row_ne['alias'].
					"\n\nSzerkesztés: http://".HOST."/admin/?op=content&mode=edit&id=".(int)$_GET['id']."&category_id=".$content['category_id'];

					$phpMailer->Send();

					$db->Query("INSERT INTO "._DBPREF."users_backend_editorlog (user_id, content_id, datetime, action)
					VALUES ('".$_SESSION['user']['id']."', '".(int)$_GET['id']."', NOW(), 'update')");
				} // needmail


				if($_POST['redir'] == 'edit')
					header('Location: '.BACKEND.'/?op=content&mode=edit&id='.(int)$_GET['id'].'&category_id='.$content['category_id']);
				else
					header('Location: '.BACKEND.'/?op=content&mode=show&category_id='.$content['category_id'].'&order='.$_SESSION['_content_order'].'&page='.$_SESSION['_content_page']);
				exit;
			} // if $res
		}
	}

// már meglévő elem esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$res = $db->Query("SELECT c.*, u.name, v.fullembed FROM "._DBPREF."content c
		LEFT JOIN "._DBPREF."users u ON c.user_id = u.id
		LEFT JOIN "._DBPREF."content_videos v ON c.id = v.content_id
		WHERE c.id = '".(int)$_GET['id']."' AND c.category_id = '".(int)$_GET['category_id']."' LIMIT 1");
		$content = $db->fetchArray($res);
		$content['datetime'] = date('Y-m-d H:i', strtotime($content['datetime']));

		if(!$admin->hasRight(ROLE_EDITOR)) die;

		if($content['user_id'] == 1758) {
			$res_authors = $db->Query("SELECT name FROM "._DBPREF."content_authors_conn conn
				LEFT JOIN "._DBPREF."authors authors ON conn.author_id = authors.id
				WHERE conn.content_id = '".$content['id']."' LIMIT 1");

			$row_authors = $db->fetchArray($res_authors);
			$content['name'] = !empty($row_authors['name']) ? $row_authors['name'] : 'Propeller';


		}

		if($content['user_id']) { // címkék
			$res_tags = $db->Query("SELECT tags.id, tags.tag FROM "._DBPREF."content_tags_conn conn
				LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
				WHERE conn.content_id = '".$content['id']."' ORDER BY tags.tag");
			$s_query = "";
			$assoc = array();
			while($row_tags = $db->fetchArray($res_tags)) {
				$s_query .= "tag_id = '".$row_tags['id']."' OR ";
				$assoc[] = $row_tags['tag'];
			}
			$content['tags'] = implode($assoc, ', ');
		}

		if($content['content_type'] == '6') { // nagykép
			$imgs = array();
			$res = $env->db->Query("SELECT * FROM "._DBPREF."bigpicture WHERE content_id = '".$content['id']."' ORDER BY ordinal ASC");
			while($row = $env->db->fetchArray($res)) {
				$imgs[] = $row;
			}
		}

		// kapcsolódók
		$related = array();
		if($content['related'] != 'N' && !empty($content['related'])) {
			$res_rel = $env->db->Query("SELECT id AS content_id, title, CONCAT(id,'-',alias) AS alias, category_id FROM "._DBPREF."content WHERE id IN (".$content['related'].") ORDER BY FIND_IN_SET(id, '".$content['related']."')");
			while($row_rel = $env->db->fetchArray($res_rel)) {
					$row_rel['link'] = $env->base.'/'.$env->l['content']['category_alias_'.$row_rel['category_id']].'/'.$row_rel['alias'];
					$related[] = $row_rel;
			}
		}
	}
	elseif($admin->hasRight(ROLE_EDITOR)) { // új oldal
		$action = 'insert';
		$_POST['content']['datetime'] = date('Y-m-d H:i');
		$_POST['content']['category_id'] = (int)$_GET['category_id'];
		$_POST['content']['counter'] = '0';
		$_POST['content']['name'] = 'Propeller'; //$_SESSION['user']['name'];
		$_POST['content']['author'] = 'Propeller'; //$_SESSION['user']['name'];
		$_POST['content']['content_type'] = $_GET['content_type'];
		$content = $_POST['content'];

		if($content['content_type'] == '6') {
			$content['name'] = 'Propeller';
			//$photo_copy = 'Fotó: Propeller / Ajpek Orsi';
		}

	}

?>

	<h1><?= ($action == 'insert') ? 'Új tartalom hozzáadása' : 'Tartalom szerkesztése'; ?></h1>
	<h2>Add meg a tartalom részleteit, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>


<?php
	if($action == 'insert' && !isset($content['content_type'])) {
?>
	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="get">
		<input type="hidden" name="op" value="content" />
		<input type="hidden" name="mode" value="edit" />
		<input type="hidden" name="category_id" id="action" value="<?= (int)$content['category_id']; ?>" />

		<fieldset>

			<label for="category">Tartalom típus:</label>
<!--			<select name="content_type" class="required" title="Válaszd ki a tartalom típusát">
			<option label="" value="0">Általános tartalom (külső hír linkje, felhasználóhoz vagy feed-hez kapcsolva)</option>
			<option label="" value="1">Videó tartalom (külső videó linkje, videós feed-hez kapcsolva)</option>
			<option label="" value="2">Szavazás (saját szavazás)</option>
			<option label="" value="3">Infografika (saját rajz, térkép, PHP kód)</option>
			</select><br />
-->

<input type="radio" name="content_type" value="0" checked="checked" /> <b>Hír tartalom</b> - Beküldött téma, szerkesztőségi hír vagy feed tartalom<br class="clear" />
<label>&nbsp;</label><input type="radio" name="content_type" value="4" /> <b>Jegyzet</b> - Külső szerző által írt jegyzet<br class="clear" />
<label>&nbsp;</label><input type="radio" name="content_type" value="6" /> <b>Nagykép</b> - Fotósorozat feltöltése<br class="clear" />
<label>&nbsp;</label><input type="radio" name="content_type" value="5" /> <b>Saját videó</b> - Előzőleg feltöltött saját videó publikálása<br class="clear" />
<label>&nbsp;</label><input type="radio" name="content_type" value="1" /> <b>Beérkezett videó</b> - Más site videójának beillesztése embed kóddal<br class="clear" />
<label>&nbsp;</label><input type="radio" name="content_type" value="2" /> <b>Szavazás</b> - Heti szavazás tartalomban<br class="clear" />
<label>&nbsp;</label><input type="radio" name="content_type" value="3" /> <b>Egyéni script</b> - PHP script becsatolása tartalomként<br class="clear" />
		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Tovább" />
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>

<?php
	}
	else {
?>
	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post"<?php if($content['content_type'] == '6') { echo ' enctype="multipart/form-data"'; } ?>>
		<input type="hidden" name="action" id="action" value="<?= $action; ?>" />
		<input type="hidden" name="content[content_type]" value="<?= (int)$content['content_type']; ?>" />

		<fieldset>
<?php if(empty($content['content_type']) || ($content['content_type'] == 1) || ($content['content_type'] == 7)) { ?>
			<label for="link">Külső link:</label>
			<input name="content[link]" type="text" id="link" placeholder="http://" onkeyup="toggleEditor(this, 'description');" class="text" value="<?= empty($content['link']) ? '' : $content['link']; ?>" title="Add meg a tartalom URL-jét" /><br />
			<label>&nbsp;</label><i>Hagyd üresen, ha szerkesztőségi hír vagy videó!</i><br class="clear" />
<?php } ?>
			<label for="title">Tartalom címe:</label>
			<input type="text" id="title" class="text required" name="content[title]" value="<?= htmlspecialchars($content['title'], ENT_QUOTES); ?>" title="Írd be a tartalom címét" /><br />
<!--<?= ($action == 'insert') ? 'onkeyup="getSefURL(this, $(\'alias\'), \'\', \'\');" ' : ''; ?>-->
			<!--label for="title">URL alias:</label>
			<input type="text" id="alias" class="text required" name="content[alias]" value="<?= htmlspecialchars($content['alias'], ENT_QUOTES); ?>" title="Írd be a tartalom URL aliasát" /><br /-->

			<label for="description">Szöveg:</label>
			<textarea name="content[description]" rows="8"<?= (!empty($content['link']) ? 'class="mceNoEditor"': ''); ?> style="height: 100px;" id="description" title="Add meg a tartalom szövegét"><?= ($content['content_type'] == '4') ? $content['description'] : htmlspecialchars($content['description'], ENT_QUOTES); ?></textarea><br />

		<div id="excerpt-wrapper"<?= (!empty($content['link'])) ? ' style="display:none;"' : ''; ?>>
			<label for="excerpt">Rövid kivonat:</label>
			<textarea name="content[excerpt]" rows="8" class="mceNoEditor" style="height:40px;" id="excerpt" title="Add meg a címlapi kivonatot"><?= htmlspecialchars($content['excerpt'], ENT_QUOTES); ?></textarea><br />
		</div>

<?php if($content['content_type'] != '6') { // nem nagykép ?>
			<label for="fullembed">Videó embed:</label>
			<textarea name="content[fullembed]" rows="8" class="mceNoEditor" style="height:40px;font-family:'Courier New', monospace;" id="fullembed" title="Add meg a beillesztőkódot"><?= htmlspecialchars($content['fullembed'], ENT_QUOTES); ?></textarea><br />

<?php } ?>

			<label for="category">Rovat:</label>
			<?php
			foreach($admin->categories AS $category) {
				if($category['id'] != 0 && $category['id'] != 9) {
				echo '<input type="radio" name="content[category_id]" label="'.$category['category'].'" value="'.$category['id'].'"';
				echo ($content['category_id'] == $category['id'] || (empty($content['category_id']) && $category['id'] == 1)) ? ' checked="checked"' : '';
				echo '> '.$category['category'].' '."\n";
				}
			}
			?>
			<br class="clear" />

			<!--select name="content[category_id]" class="required" title="Válaszd ki a tartalom kategóriáját">
			<option label="" value="">Nincs megadva</option>
			<?php
			foreach($admin->categories AS $category) {
				if($category['id'] != 0 && $category['id'] != 9) {
				echo '<option label="'.$category['category'].'" value="'.$category['id'].'"';
				echo ($content['category_id'] == $category['id']) ? ' selected="selected"' : '';
				echo '>'.$category['category'].'</option>'."\n";
				}
			}
			?>
			</select><br /-->
<?php /*if(empty($content['content_type']) || in_array($content['content_type'], array('1'))) { ?>
			<label for="category">Feed:</label>
			<select name="content[feed_id]" title="Válaszd ki a feedet" onchange="checkIsFeed(this);">
			<option label="" id="feed_is_null" value="">Nem feed tartalom</option>
			<?php
			if($content['content_type'] == '1')
				$res_c = $db->Query("SELECT id, title FROM "._DBPREF."content_feeds WHERE video_embed IS NOT NULL ORDER BY title");
			else
				$res_c = $db->Query("SELECT id, title FROM "._DBPREF."content_feeds ORDER BY title");

			while($row_c = $db->fetchArray($res_c)) {
			echo '<option label="'.htmlspecialchars($row_c['title'], ENT_QUOTES).'" value="'.$row_c['id'].'"';
			echo ($content['feed_id'] == $row_c['id']) ? ' selected="selected"' : '';
			echo '>'.htmlspecialchars($row_c['title'], ENT_QUOTES).'</option>'."\n";
			}
			?>
			</select><br />
<?php } */?>
<?php //if($content['content_type'] != '1') { // általános tartalom ?>
<div id="userid-wrapper"<?= ((!empty($content['feed_id']) && $action == 'update')) ? ' style="display:none;"' : ''; ?>>

			<label for="tags">Címkék (vesszővel):</label>
			<input type="text" id="tags" class="text" name="content[tags]" value="<?= $content['tags']; ?>" />
			<div class="auto_complete" id="tags_autocomplete"></div>
			<script type="text/javascript">
			// <![CDATA[
				new Ajax.Autocompleter('tags', 'tags_autocomplete', 'requests.php?what=tags_autocomplete', {tokens: ','});
			// ]]>
			</script><br style="clear:both;" />


			<label for="uid" id="name-label"><?= empty($content['link']) ? 'Szerző:' : 'Felhasználó:' ?></label>
			<input name="content[name]" type="text" rel="<?= empty($content['link']) ? 'authors' : 'usernames' ?>" id="name" class="text required" style="width: 194px;" value="<?= $content['name']; ?>" title="Add meg a felhasználó nevét" /><?php if($content['content_type'] != '6') { // nem nagykép  ?> <a href="javascript:;" onclick="addRelated();">Kapcsolódó tartalom hozzáadása</a><?php } ?><br style="clear:both;" />
			<div class="auto_complete" id="name_autocomplete"></div>
			<script type="text/javascript">
			// <![CDATA[
				new Ajax.Autocompleter('name', 'name_autocomplete', 'requests.php?what=get_usernames', {
			        callback: function(element, entry) {
			            return entry + '&type=' + $('name').readAttribute('rel');
			        }
			      }
        		);
			// ]]>
			</script>

		<span id="datetime-wrapper"<?= (!empty($content['link'])) ? ' style="display:none;"' : ''; ?>>
			<label for="datetime">Megjelenik:</label>
			<input name="content[datetime]" type="datetime-local" id="datetime" class="text required" style="width: 194px;" value="<?= strftime('%Y-%m-%dT%H:%M:%S', strtotime($content['datetime'])); ?>" title="Add meg a tartalom élesedésének dátumát" /><br style="clear:both;" />
		</span>

</div>
<?php// } ?>

			<!--label for="datetime">Számláló:</label>
			<input name="content[counter]" type="text" id="counter" class="text required" style="width: 200px;" value="<?= $content['counter']; ?>" title="Add meg a tartalom számláló-értékét" /-->

<?php if($content['content_type'] != '6') { // nem nagykép  ?>


		<div id="cloneable" class="cloneable" style="display: none;" rel="">

			<input name="related[content_id][]" type="hidden" id="content_id" value=""/>

			<label for="link">Kapcsolódó linkje:</label>
			<input name="related[link][]" type="text" id="link" onblur="getTitleByUrlCloned(this.parentNode.rel);" placeholder="http://" class="text required" style="color:#aaa;width:578px;" value="" title="Add meg a tartalom URL-jét" />
			<a href="javascript:;" onclick="$(this.parentNode).remove();" title="Törlés"><img src="images/icon/x.gif" alt="" /></a><br style="clear:both;" />

			<label for="cid">Kapcsolódó címe:</label>
			<input type="text" id="title" class="text required" style="width:578px;" onfocus="initAutocomp(this.parentNode.rel);" name="related[title][]" value="" title="Írd be a tartalom címét" />
			<div class="auto_complete" id="autocomplete" style="z-index: 9999;"></div>

		</div>
<div id="related">
		<?php
		if($action == 'update') {
			$rel = 100;
			foreach($related as $value) {
				echo '<div class="cloneable" id="cloned'.$rel.'">

				<input name="related[content_id][]" type="hidden" id="content_id-'.$rel.'" value="'.$value['content_id'].'"/>

				<label for="link">Kapcsolódó linkje:</label>
				<input name="related[link][]" type="text" id="link-'.$rel.'" onblur="getTitleByUrlCloned('.$rel.');" placeholder="http://" class="text required" style="color:#aaa;width:578px;" value="'.$value['link'].'" title="Add meg a tartalom URL-jét" />
				<a href="javascript:;" onclick="$(this.parentNode).remove();" title="Törlés"><img src="images/icon/x.gif" alt="" /></a><br style="clear:both;" />

				<label for="cid">Kapcsolódó címe:</label>
				<input type="text" id="title-'.$rel.'" class="text required" style="width:578px;" onfocus="initAutocomp('.$rel.');" name="related[title][]" value="'.htmlspecialchars($value['title'], ENT_QUOTES).'" title="Írd be a tartalom címét" />
				<div class="auto_complete" id="autocomplete-'.$rel.'" style="z-index: 9999;"></div>


				</div>';
				$rel++;
			}
		}
		?>
		<script type="text/javascript">
		var curr_e = null;
		function initAutocomp(e) {
			curr_e = e;
			return new Ajax.Autocompleter('title-'+e, 'autocomplete-'+e, 'requests.php?what=get_related&not=<?= (int)$content['id']; ?>', {minChars: 2, afterUpdateElement : getSelectionId});

		}

		function getSelectionId(text, li) {
			$('content_id-'+curr_e).value = li.id;
			$('link-'+curr_e).value = li.readAttribute('rel');
		}

		</script>

		<span id="clone-here"></span>
</div>
<?php } ?>



















<?php if($content['content_type'] == '6') { // nagyképes
?>

		<div id="cloneable" class="cloneable" style="display:none;" rel="">
			<label for="file" id="img-count"></label>
			<input type="hidden" name="image[filepath][]" value="" />
			<input type="file" id="file" name="files[]" size="73" accept="image/gif, image/png, image/jpeg, image/jpg" /><br style="clear:both;" />

			<!--label for="taken">Helyszín, dátum:</label>
			<input type="text" id="taken" class="text" name="image[taken][]" value="" title="Írd be a kép készítésének helyét, dátumát" maxlength="200" /-->


			<label for="title">Képaláírás:</label>
			<input type="text" id="title" class="text" name="image[title][]" value="" title="Írd be a kép címét vagy leírását" maxlength="200" />
			<a href="javascript:;" onclick="$(this.parentNode).remove();" title="Törlés"><img src="images/icon/x.gif" alt="" /></a><br style="clear:both;" />


			<label for="author">Szerző, forrás:</label>
			<input type="text" id="link" class="text" name="image[author][]" value="<?= $photo_copy; ?>" title="Add meg a kép szerzőjét, copyright szövegét" maxlength="200" /><br style="clear:both;" />
<!--
			<label for="cover">Ez a kiemelt fotó?</label>
			<input type="radio" id="cover" class="radio" name="image[cover][]" value="1" /><br style="clear:both;" />
-->
		</div>
		<script type="text/javascript">var lastImg = 0;</script>

<div id="imgs">
		<?php
		if($action == 'update') {
			$rel = 100;
			$last_item = end($imgs);

			foreach($imgs as $value) {
				echo '

		<div class="cloneable" id="cloned'.$rel.'">
			<label for="file" id="img-count-'.($rel-99).'">'.($rel-99).'. fájl:</label>
			<input type="hidden" name="image[filepath][]" value="'.$value['filepath'].'" />
			<input style="display:none;" type="file" id="file-'.$rel.'" name="files[]" size="73" accept="image/gif, image/png, image/jpeg, image/jpg" />

<a href="'.STTC.'/images/bigpicture/'.$value['filepath'].'" onclick="window.open(this.href, \'_blank\'); return false;"><img src="'.STTC.'/images/bigpicture/'.str_replace('_normal.', '_thumb.', $value['filepath']).'" alt="" height="50" /></a><br style="clear:both;" />

			<!--label for="taken">Helyszín, dátum:</label>
			<input type="text" id="taken-'.$rel.'" class="text" name="image[taken][]" value="'.htmlspecialchars($value['taken'], ENT_QUOTES).'" title="Írd be a kép készítésének helyét, dátumát" maxlength="200" /-->


			<label for="title">Képaláírás:</label>
			<input type="text" id="title-'.$rel.'" class="text" name="image[title][]" value="'.htmlspecialchars($value['title'], ENT_QUOTES).'" title="Írd be a kép címét vagy leírását" maxlength="200" />

			<a href="javascript:;" onclick="$(this.parentNode).remove();" title="Törlés"><img src="images/icon/x.gif" alt="" /></a><br style="clear:both;" />

			<label for="author">Szerző, forrás:</label>
			<input type="text" id="link-'.$rel.'" class="text" name="image[author][]" value="'.htmlspecialchars($value['author'], ENT_QUOTES).'" title="Add meg a kép szerzőjét, copyright szövegét" maxlength="200" /><br style="clear:both;" />
<!--
			<label for="cover">Ez a kiemelt fotó?</label>
			<input type="radio" id="cover" class="radio" name="image[cover][]" value="1"'.(($value['cover'] == 1) ? ' checked="checked"' : '').' /><br style="clear:both;" />
-->
		</div>';

				if($value== $last_item)
				echo '<script type="text/javascript">var lastImg = '.($rel-99).';</script>';

				$rel++;
			}
		}
		?>
		<span id="clone-here"></span>
</div>


<?php
} ?>

		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés" />
		<button type="submit" class="button" name="redir" value="edit">Mentés és szerkesztés</button>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="redirDel(\'content\', '.$_GET['id'].');" />' : ''; ?>
<?php if($content['content_type'] == '6') { echo ' <input type="button" id="addimgbtn" class="button" value="Kép hozzáadása" onclick="addImg();" />'; } ?>
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');

		Sortable.create("related", {tag: "DIV"});

		var checkIsFeed = function(n) {
			if(!$('userid-wrapper')) return false;
			if(n.value != "") {
				$('userid-wrapper').setStyle({display: 'none'});
                $('name').setAttribute('name', '');
               // $('tags').setAttribute('name', '');
			}
			else {
				$('userid-wrapper').setStyle({display: 'block'});
                $('name').setAttribute('name', 'content[name]');
                //$('tags').setAttribute('name', 'content[tags]');
                $('name').setAttribute('value', 'Propeller');
				$('name-label').innerHTML = 'Szerző:';
			}
		}


		<?php if($content['content_type'] == 6) { echo 'Sortable.create("imgs", {tag: "DIV", hoverclass: "add-sortable-hover"});'; } ?>

	// ]]>
	</script>
<?php
	}
}

if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_EDITOR)) {
?>
	<h1>Tartalmak szerkesztése</h1>
	<h2>Válaszd ki a módosítandó tartalmat az alábbi listából.</h2>

	<br class="clear" />

<?php
$where = "";
$order = "";

// lapozások kiszámítása, ennyi legyen egy oldalon:
$perpage = 20;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
	$_SESSION['_content_page'] = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
	$_SESSION['_content_page'] = (int)$_GET['page'];
}



if(!empty($_GET['q'])) {
	$words = explode(" ", $_GET['q']);
	while(list($k, $v) = each($words)){
		$v = $db->escape($v);
		if(!$j) {
			$where .= "AND (t.title COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR t.description COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR t.link COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR t.title COLLATE utf8_hungarian_ci LIKE '".$v."%' OR t.title COLLATE utf8_hungarian_ci LIKE '%".$v."' OR t.description COLLATE utf8_hungarian_ci LIKE '".$v."%' OR t.description COLLATE utf8_hungarian_ci LIKE '%".$v."')";
			$j = 1;
		}
		else {
			$where .= " AND (t.title COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR t.description COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR t.link COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR t.title COLLATE utf8_hungarian_ci LIKE '".$v."%' OR t.title COLLATE utf8_hungarian_ci LIKE '%".$v."' OR t.description COLLATE utf8_hungarian_ci LIKE '".$v."%' OR t.description COLLATE utf8_hungarian_ci LIKE '%".$v."')";
		}
	}
}



if($_GET['category_id'] == 0) {
//die('ez most nem műxik.');
	if(count($myCategories) < 9)
	$where .= " AND t.category_id IN (".implode(',', $myCategories).") ";
}
else {
	$where .= " AND t.category_id = '".(int)$_GET['category_id']."' ";
}



/*if($_GET['order'] == 'title') { // rendezések
	$order .= "ORDER BY replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
	co.title,'á','azz'),'í','izz'),'cs','czz'),'ly','lzz'),'é','ezz'),'ó','oxx'),'ö','oyy'),'ő','ozz'),'ty','tzz'),'gy','gzz'),'ú','uxx'),'ü','uyy'),'ű','uzz'),'ny','nzz'),'zs','zzz')
 ASC";
	$flag = 1;
	$_SESSION['_content_order'] = 'modtime';
}
else*/
if($_GET['order'] == 'hourly') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, counter, comments, if(feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content t
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND t.datetime > NOW() - INTERVAL 3 HOUR
	ORDER BY counter DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 2;
	$_SESSION['_content_order'] = 'hourly';
}
elseif($_GET['order'] == 'weekly') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, counter, comments, if(feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content t
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND t.datetime > NOW() - INTERVAL 168 HOUR
	ORDER BY counter DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 3;
	$_SESSION['_content_order'] = 'weekly';
}
elseif($_GET['order'] == 'user_id') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND t.user_id IS NOT NULL AND t.user_id <> 1758
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 5;
	$_SESSION['_content_order'] = 'user_id';
}

elseif($_GET['order'] == 'content_type_1') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND (t.content_type = '1' OR t.link LIKE '%youtube.com/%')
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 1;
	$_SESSION['_content_order'] = 'content_type_1';
}
/*elseif($_GET['order'] == 'content_type_2') {
	$table = _DBPREF."content_temp";
	$where .= "AND content_type = '2' ";
	$order .= "datetime DESC";
	$flag = 8;
	$_SESSION['_content_order'] = 'content_type_2';
}*/
elseif($_GET['order'] == 'own') { // minden saját
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND (t.content_type IN (2,4,6) OR t.user_id = 1758 OR t.link IS NULL)
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 12;
	$_SESSION['_content_order'] = 'own';
}
elseif($_GET['order'] == 'content_type_4') { // jegyzet
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND t.content_type = '4'
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 9;
	$_SESSION['_content_order'] = 'content_type_4';
}
/*elseif($_GET['order'] == 'content_type_5') {
	$table = _DBPREF."content_temp";
	$where .= "AND content_type = '5' ";
	$order .= "datetime DESC";
	$flag = 10;
	$_SESSION['_content_order'] = 'content_type_5';
}*/
elseif($_GET['order'] == 'content_type_6') { // nagykép
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND t.content_type = '6'
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 11;
	$_SESSION['_content_order'] = 'content_type_6';
}

elseif($_GET['order'] == 'content_type_7') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where." AND t.content_type = '7'
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 7;
	$_SESSION['_content_order'] = 'content_type_7';
}
elseif($_GET['order'] == 'removed') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, t.counter, t.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_del t
	WHERE t.id > "._SPD_CONTENT_ID." ".$where."
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 4;
	$_SESSION['_content_order'] = 'removed';
}
else { // mind
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS t.id, t.category_id, t.title, CONCAT(t.id,'-',t.alias) AS content_alias, t.datetime, t.user_id, c.counter, c.comments, if(t.feed_id IS NOT NULL,
		(SELECT title FROM "._DBPREF."content_feeds WHERE id = t.feed_id LIMIT 1),
		(SELECT name FROM "._DBPREF."users WHERE id = t.user_id LIMIT 1)) source
	FROM "._DBPREF."content_temp t
	LEFT JOIN "._DBPREF."content c ON t.id = c.id
	WHERE t.id > "._SPD_CONTENT_ID." ".$where."
	ORDER BY t.datetime DESC
	LIMIT ".$listfrom.", ".$perpage);

	$flag = 0;
	$_SESSION['_content_order'] = '';
}

?>
	<form id="backend-search" action="" method="get">
		<input type="hidden" name="op" value="content" />
		<input type="hidden" name="mode" value="show" />
		<input type="hidden" name="category_id" value="<?= (int)$_GET['category_id']; ?>" />
		<input type="text" name="q" class="text" value="<?= htmlspecialchars($_GET['q'], ENT_QUOTES); ?>" /> <input type="submit" class="button" value="Keresés" />
	</form>

	<p id="lead">
		<?= ($flag == 5) ? 'beküldés' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=user_id">beküldés</a>'; ?> |
		<?= ($flag == 7) ? 'várakozó beküldés' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=content_type_7">várakozó beküldés</a>'; ?> |
		<?= ($flag == 12) ? 'saját' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=own">saját</a>'; ?> |
		<?= ($flag == 0) ? 'mind' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'">mind</a>'; ?> |


		<?= ($flag == 1) ? 'videó' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=content_type_1">videó</a>'; ?> |
		<!--<?= ($flag == 9) ? 'jegyzet' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=content_type_4">jegyzet</a>'; ?> |-->
		<?= ($flag == 11) ? 'nagykép' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=content_type_6">nagykép</a>'; ?> |


		<!--<?= ($flag == 8) ? 'szavazás' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=content_type_2">szavazás</a>'; ?> |-->
		<!--<?= ($flag == 12) ? 'infografika' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=content_type_3">infografika</a>'; ?> |-->
		<?= ($flag == 2) ? '3 óra top' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=hourly">3 óra top</a>'; ?> |
		<?= ($flag == 3) ? '7 nap top' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=weekly">7 nap top</a>'; ?> |
		<?= ($flag == 4) ? 'törölt' : '<a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order=removed">törölt</a>'; ?>
	</p>


<?php

//SQL_CALC_FOUND_ROWS
/*

$res = $env->db->Query("SELECT id, category_id, title, CONCAT(id,'-',alias) AS content_alias, datetime, counter, comments,
if(feed_id IS NOT NULL,
	(SELECT title FROM "._DBPREF."content_feeds WHERE id = feed_id LIMIT 1),
	(SELECT name FROM "._DBPREF."users WHERE id = user_id LIMIT 1)) source, user_id
FROM ".$table."
".$query." LIMIT ".$listfrom.", ".$perpage);*/

$res_num = $db->Query("SELECT found_rows()");
$rows = $db->fetchArray($res_num);
//$rows[0] = 500;

if(!$rows[0]) {
	echo '<p>Nincsenek a feltételnek megfelelő sorok...</p>';
}
else {
$pgs = ceil($rows[0] / $perpage); // oldalak száma

$i = $listfrom + 1; // lista sorszáma
echo '<table cellspacing="0" cellpadding="1" width="100%">';
	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Cím</b></td>
		<td valign="top"><b>Rovat</b></td>
		<td valign="top"><b>Forrás</b></td>
		<td valign="top"><b>Szerző</b></td>
		<td valign="top"><b>Sz.</b></td>
		<td valign="top"><b>H.</b></td>
		<td valign="top"><b>Megjelenik</b></td>
		<td>&nbsp;</td>
	</tr>
	';
	while($row = $env->db->fetchArray($res)) {
		$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
//		$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));

		$isArchive = '';
		if (strtotime($row['datetime']) > time()) {
			$isArchive = ' class="archive"';
		}

	$row_szerk = false;
	if($row['user_id'] == 1758) {

		$res_szerk = $env->db->Query("SELECT u.name, u.alias
		FROM "._DBPREF."content c
		LEFT JOIN "._DBPREF."users_backend_editorlog sz ON c.id = sz.content_id
		LEFT JOIN "._DBPREF."users u ON sz.user_id = u.id
		WHERE c.id = ".$row['id']." LIMIT 1");
		$row_szerk = $env->db->fetchArray($res_szerk);
		//p($row_szerk);
	}


		echo '
		<tr'.$isArchive.' style="background-color: '.$diffcolor.'">
			<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
			<td valign="top"><a href="?op=content&amp;mode=edit&amp;id='.$row['id'].'&amp;category_id='.$row['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a>'.($isArchive ? ' <b style="color:#000;">– IDŐZÍTVE</b> ' : '').'</td>
			<td valign="top"><a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'" onclick="window.open(this.href, \'_blank\'); return false;">'.$env->l['content']['category_'.$row['category_id']].'</a></td>
			<td valign="top" nowrap="nowrap">';
//			echo ($row['user_id'] != NULL) ? '<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['source'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlentities($row['source'], ENT_QUOTES).'</a>' : htmlentities($row['source'], ENT_QUOTES);
			echo htmlspecialchars($row['source'], ENT_QUOTES);

			echo '</td>
			<td valign="top">';
			if ($row_szerk) {
				echo '<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row_szerk['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlentities($row_szerk['name'], ENT_QUOTES).'</a>';
			} else {
				echo '-';
			}

			echo '</td>
			<td valign="top">'.$row['counter'].'</td>
			<td valign="top">'.$row['comments'].'</td>
			<td valign="top" nowrap="nowrap">'.$admin->dateFormat($row['datetime']).'</td>
			<td valign="top" align="right" nowrap="nowrap">';
				if ($flag == 7) {
					echo '<a href="?op=content&amp;mode=enable_user_content&amp;id='.$row['id'].'" title="Élesítés"><img src="images/icon/checkmark.gif" alt="" /></a> ';
				} else{
				echo '<a href="?op=blocks&amp;mode=edit&amp;suggest_id='.$row['id'].'&amp;category_id='.$row['category_id'].'" title="Kiemelés"><img src="images/icon/arrow-left.gif" alt="" /></a> ';
					echo '<a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['content_alias'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a> ';
				}


				echo '<a href="javascript:redirDel(\'content\', '.$row['id'].', '.$row['category_id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a> ';
			echo '</td>
		</tr>
		';
	$i++;
	}
	echo '</table>';

	echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
$turn = ($pnow <= 15) ? 1 : ($pnow - 15);
$tto = ($pnow <= 15) ? (($pgs < 30) ? $pgs : 30): ($pnow+15);
		for($j = $turn; $j <= $tto; $j++) { // következő, előző oldalak számainak kiírása
			if($pnow == $j) { // ez az oldal, itt állunk most
				echo ' <b>'.$j.'</b>';
			}
			else { // további oldalak linkkel
				if(!empty($_GET['q']))
					echo ' <a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;q='.htmlentities($_GET['q'], ENT_QUOTES).'&amp;page='.$j.'">'.$j.'</a> ';
				else
					echo ' <a href="?op=content&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;order='.$_GET['order'].(($_GET['order'] == 'category') ? '&amp;cid='.$_GET['cid'] : '').'&amp;page='.$j.'">'.$j.'</a> ';
			}
		}
	echo '</p>';
}
}


if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_EDITOR)) {

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés

		// átmozgatjuk a lomtárba a sort
		$res = $db->Query("INSERT INTO "._DBPREF."content_del SELECT * FROM "._DBPREF."content WHERE id = '".$_GET['id']."' LIMIT 1");

		if($res) {

		$db->Query("DELETE FROM "._DBPREF."content WHERE id = '".$_GET['id']."' LIMIT 1");

		$db->Query("DELETE FROM "._DBPREF."content_temp WHERE id = '".$_GET['id']."' LIMIT 1");
		$db->Query("DELETE FROM "._DBPREF."users_backend_editorlog WHERE content_id = '".$_GET['id']."' LIMIT 1");

		$db->Query("DELETE FROM "._DBPREF."content_tags_conn WHERE content_id = '".$_GET['id']."'");

		// átmozgatjuk a lomtárba a kommenteket
		$db->Query("INSERT INTO "._DBPREF."content_comments_del SELECT * FROM "._DBPREF."content_comments WHERE content_id = '".$_GET['id']."'");
		$db->Query("DELETE FROM "._DBPREF."content_comments WHERE content_id = '".$_GET['id']."'");

		// átmozgatjuk a lomtárba a videókat
		$db->Query("INSERT INTO "._DBPREF."content_videos_del SELECT * FROM "._DBPREF."content_videos WHERE content_id = '".$_GET['id']."'");
		$db->Query("DELETE FROM "._DBPREF."content_videos WHERE content_id = '".$_GET['id']."'");

		$db->Query("DELETE FROM "._DBPREF."users_activities WHERE content_id = '".$_GET['id']."'");

		$db->Query("DELETE FROM "._DBPREF."bigpicture WHERE content_id = '".$_GET['id']."'");

		$db->Query("DELETE FROM "._DBPREF."content_comments_report WHERE content_id = '".$_GET['id']."'");


		// logban is töröltre állítjuk, hogy a komment gyorsító kverikbe ne kerüljön bele
		$db->Query("UPDATE "._DBPREF."content_comments_log SET removed = '1' WHERE content_id = '".$_GET['id']."'");

		// cache eldobása
		$db->DropQueryCache('latest.cache');
		$db->DropQueryCache('commented_0.cache');

		header('Location: '.BACKEND.'/?op=content&mode=show&order='.$_SESSION['_content_order'].'&category_id='.$_GET['category_id'].'&page='.$_SESSION['_content_page']);
		exit;
		?>
		<h1>Tartalom törlése</h1>
		<h2>A tartalom törlése megtörtént.</h2>
		<a href="<?= '?op=content&mode=show&order='.$_SESSION['_content_order'].'&category_id='.$_GET['category_id'].'&page='.$_SESSION['_content_page']; ?>">Tovább</a>

		<?php
		}
	}
}


if(($_GET['mode'] == 'enable_user_content') && $admin->hasRight(ROLE_EDITOR)) { // várakozó beküldött tartalom engedélyezése
	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés

		$db->Query("UPDATE "._DBPREF."content SET
			content_type = NULL,
			datetime = NOW()
			WHERE id = '".$_GET['id']."' LIMIT 1");

		$db->Query("UPDATE "._DBPREF."content_temp SET
			content_type = NULL,
			datetime = NOW()
			WHERE id = '".$_GET['id']."' LIMIT 1");

		$db->DropQueryCache('latest.cache');

		header('Location: '.BACKEND.'/?op=content&mode=show&category_id=0&order=content_type_7');
		exit;
	}
}


?>