<?php
defined('ENV') or die;

function refreshFacebookOpenGraphCache($url)
{
	$ch = curl_init();

	curl_setopt( $ch, CURLOPT_URL, 'https://graph.facebook.com' );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_POST, true );
	$params = array(
	    'id' => $url,
	    'scrape' => true );
	$data = http_build_query( $params );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );

	curl_exec( $ch );
	$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
}

if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_EDITOR)) {
	if($_POST['action'] == 'insert') { // űrlapelküldés, kiértékelés új hozzáadáshoz
		$content = $_POST['content'];
		$content['image'] = $_POST['image'];
		$cats = $content['cat'];
		$related = $_POST['related'];

		if(preg_match("/\/(itthon|nagyvilag|szorakozas|technika|sport)\/(\d+)\-/i", $content['link'], $content_id)) {
			$content['content_id'] = $content_id[2];
		}
		else {
			$content['content_id'] = 0;
		}

		if(empty($content['title'])) { $error = 'Add meg a tartalom címét'; }
		if(empty($content['link'])) { $error = 'Add meg a tartalom URL-jét'; }
		if(empty($content['datetime'])) { $error = 'Add meg a tartalom megjelenésének időpontját'; }
		if(empty($content['block_column'])) { $error = 'Add meg a kiemelés helyét'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$content = $db->escape($content);
			$datetime = date('Y-m-d H:i:s', strtotime($content['datetime']));
			$q_image = !empty($content['image']) ? "'".$content['image']."'" : "NULL";
			$q_description = !empty($content['description']) ? "'".$content['description']."'" : "NULL";

			$res_block = $env->db->Query("SELECT block_id FROM "._DBPREF."content_blocks ORDER BY block_id DESC LIMIT 0, 1");
			$row_block = $env->db->fetchArray($res_block);
			$block_id = (int)$row_block['block_id']+1;

			// kikeressük a legfelső block pozícióját, hogy afölé tehessük ezt az újat
			$q_block_column = "'".(int)$content['block_column']."'";
			$res_block_pos = $db->Query("SELECT block_position FROM "._DBPREF."content_blocks WHERE block_column = '".(int)$content['block_column']."' ORDER BY block_position DESC LIMIT 0, 1");
			$row_block_pos = $db->fetchArray($res_block_pos);
			$block_pos = (int)$row_block_pos['block_position']+1;
			$q_block_position = "'".$block_pos."'";

			if($cats) {
				$q_cids = implode(", ", $cats).', ';
				$v_cids = str_repeat("'1', ", count($cats));
			}

			$res = $db->Query("INSERT
			INTO "._DBPREF."content_blocks (block_id, ".$q_cids." content_id, title, link, description, ordinal, image, block_column, block_position, datetime, user_id, modtime)
			VALUES ('".$block_id."', ".$v_cids." '".$content['content_id']."', '".$content['title']."', '".$content['link']."', ".$q_description.", '0', ".$q_image.", ".$q_block_column.", ".$q_block_position.", '".$datetime."', '".$_SESSION['user']['id']."', NOW())");

			if($res) { // nincs hiba a beszúrásban
				// további (kapcsolódó) tartalmak
				$i=1;
				foreach($related['link'] as $key => $value) {
					if(!empty($value)) {
						if(preg_match("/\/(itthon|nagyvilag|szorakozas|technika|sport)\/(\d+)\-/i", $value, $content_id)) {
							$related_cid = $content_id[2];
						}
						else {
							$related_cid = 0;
						}

						$res = $db->Query("INSERT
						INTO "._DBPREF."content_blocks (block_id, content_id, title, link, ordinal)
						VALUES ('".$block_id."', '".$related_cid."', '".$db->escape($related['title'][$key])."', '".$db->escape($link)."', '".$i."')");
						$i++;
					}
				}

				// ha az első oszlopba tettük, és van éppen breaking bepipálva, akkor rögtön nyomjuk is lejjebb eggyel
				if ($content['block_column'] == 1) {
					$res_misc = $env->db->Query("SELECT is_breaking FROM "._DBPREF."misc LIMIT 1");
					$misc = $env->db->fetchAssoc($res_misc);
					if ($misc['is_breaking'] == 1) {
						$res_prev = $env->db->Query("SELECT block_id FROM "._DBPREF."content_blocks WHERE block_column = '1' AND block_position < ".$block_pos." AND ordinal = '0' ORDER BY block_position DESC LIMIT 1");
						$row_prev = $env->db->fetchArray($res_prev);
						$prev = $row_prev['block_id'];

						$res = $db->Query("UPDATE "._DBPREF."content_blocks SET block_position = '".($block_pos-1)."' WHERE block_id = '".$block_id."' AND ordinal = '0' LIMIT 1");
						$res = $db->Query("UPDATE "._DBPREF."content_blocks SET block_position = '".$block_pos."' WHERE block_id = '".$prev."' AND ordinal = '0' LIMIT 1");
					}
				}

				foreach($cats AS $key => $value) {
					$ex = explode('_', $value);
					$env->dropFileCache('blocks_'.(int)$ex[1].'.cache');
				}
				$env->dropFileCache('blocks.cache');

				if (!empty($content['image'])) {
					refreshFacebookOpenGraphCache($content['link']);
				}

				header('Location: '.BACKEND.'/?op=blocks&mode=show&category_id=0');
				exit;
			}
		}
	}

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$content = $_POST['content'];
		$content['image'] = $_POST['image'];
		$cats = $content['cat'];
		$related = $_POST['related'];

		if(preg_match("/\/(itthon|nagyvilag|szorakozas|technika|sport)\/(\d+)\-/i", $content['link'], $content_id)) {
			$content['content_id'] = $content_id[2];
		}
		else {
			$content['content_id'] = 0;
		}
		if(empty($content['title'])) { $error = 'Add meg a tartalom címét'; }
		if(empty($content['link'])) { $error = 'Add meg a tartalom URL-jét'; }
		if(empty($content['datetime'])) { $error = 'Add meg a tartalom megjelenésének időpontját'; }
		if(empty($content['block_column'])) { $error = 'Add meg a kiemelés helyét'; }

		if(empty($error)) { // nincs hiba, megtörténhet a módosítás
			$content = $db->escape($content);
			$datetime = date('Y-m-d H:i:s', strtotime($content['datetime']));
			$q_image = !empty($content['image']) ? "'".$content['image']."'" : "NULL";
			$q_description = !empty($content['description']) ? "'".$content['description']."'" : "NULL";

		//	if(in_array('cat_0', $cats)) { // címlapi doboz, lehet vezető is, kell neki block_position-t keresni
				//block_position kell neki + block_column
					$q_block_column = " block_column = '".(int)$content['block_column']."', ";

					$res_block_col = $db->Query("SELECT block_column FROM "._DBPREF."content_blocks WHERE block_id = '".(int)$_GET['id']."' AND ordinal = '0' LIMIT 1");
					$row_block_col = $db->fetchArray($res_block_col);

					if(!$content['block_position'] || ($row_block_col['block_column'] != $content['block_column'])) {
						$res_block_pos = $db->Query("SELECT block_position FROM "._DBPREF."content_blocks WHERE block_column = '".(int)$content['block_column']."' ORDER BY block_position DESC LIMIT 0, 1");
						$row_block_pos = $db->fetchArray($res_block_pos);
						$q_block_position = " block_position = '".((int)$row_block_pos['block_position']+1)."', ";
					}
					else {
						$q_block_position = " block_position = '".(int)$content['block_position']."', ";
					}

		/*	}
			else { // nincs a címlapon, nem kell se leader, se block_position
				$q_leader = "'0'";
				$q_block_column = " block_column = NULL, ";
				$q_block_position = " block_position = NULL, ";
			}
*/
			// első (képes) tartalom
			$res = $db->Query("UPDATE "._DBPREF."content_blocks SET
			cat_1 = ".(in_array('cat_1', $cats) ? "'1'": "NULL").",
			cat_2 = ".(in_array('cat_2', $cats) ? "'1'": "NULL").",
			cat_4 = ".(in_array('cat_4', $cats) ? "'1'": "NULL").",
			cat_6 = ".(in_array('cat_6', $cats) ? "'1'": "NULL").",
			cat_7 = ".(in_array('cat_7', $cats) ? "'1'": "NULL").",
			content_id = '".$content['content_id']."',
			title = '".$content['title']."',
			link = '".$content['link']."',
			description = ".$q_description.",
			image = ".$q_image.",
			".$q_block_column."
			".$q_block_position."
			datetime = '".$datetime."',
			user_id = '".$_SESSION['user']['id']."',
			modtime = NOW()
			WHERE block_id = '".(int)$_GET['id']."' AND ordinal = '0' LIMIT 1");

			if($res) { // nincs hiba a módosításban
				$res = $db->Query("DELETE FROM "._DBPREF."content_blocks WHERE block_id = '".(int)$_GET['id']."' AND ordinal <> '0'");

				// további (kapcsolódó) tartalmak
				$i=1;
				foreach($related['link'] as $key => $value) {
					if(!empty($value)) {
						if(preg_match("/\/(itthon|nagyvilag|szorakozas|technika|sport)\/(\d+)\-/i", $value, $content_id)) {
							$related_cid = $content_id[2];
						}
						else {
							$related_cid = 0;
						}

						$res = $db->Query("INSERT
						INTO "._DBPREF."content_blocks (block_id, content_id, title, link, ordinal)
						VALUES ('".(int)$_GET['id']."', '".$related_cid."', '".$db->escape($related['title'][$key])."', '".$db->escape($value)."', '".$i."')");
						$i++;
					}
				}
				foreach($cats AS $key => $value) {
					$ex = explode('_', $value);
					$env->dropFileCache('blocks_'.(int)$ex[1].'.cache');
				}
				$env->dropFileCache('blocks.cache');

				if (!empty($content['image'])) {
					refreshFacebookOpenGraphCache($content['link']);
				}

				header('Location: '.BACKEND.'/?op=blocks&mode=show&category_id=0');
				exit;
			}
		}
	}

// már meglévő elem oldal esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$related = array();
		$res = $env->db->Query("SELECT * FROM "._DBPREF."content_blocks l WHERE block_id = '".(int)$_GET['id']."' ORDER BY ordinal ASC");
		while($row = $env->db->fetchArray($res)) {
			if($row['ordinal'] == '0')
				$content = $row;
			else
				$related[] = $row;
		}
	}
	elseif(isset($_GET['suggest_id']) && is_numeric($_GET['suggest_id'])) { // átajánló űrlap
		$action = 'insert';

		$res = $env->db->Query("SELECT c.id AS content_id, c.title, CONCAT(c.id,'-',c.alias) AS alias, cat.alias AS category_alias, description
		FROM "._DBPREF."content c
		LEFT JOIN "._DBPREF."content_categories cat ON c.category_id = cat.id
		WHERE c.id = '".(int)$_GET['suggest_id']."' LIMIT 1");
		$content = $env->db->fetchArray($res);
		$content['datetime'] = date('Y-m-d H:i:00');
		$content['link'] = $env->base.'/'.$content['category_alias'].'/'.$content['alias'].'"';
		$content['description'] = $env->strTruncate(strip_tags($content['description']), 300, '...');
	}
	else { // új oldal
		$action = 'insert';
		$_POST['content']['datetime'] = date('Y-m-d H:i:00');
		$_POST['content']['category_id'] = (int)$_GET['category_id'];
		$content = $_POST['content'];
	}

?>

	<h1><?= ($action == 'insert') ? 'Új kiemelés készítése' : 'Kiemelések szerkesztése'; ?></h1>
	<h2>Add meg a kiemelés részleteit, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">
	<input type="hidden" name="action" value="<?= $action; ?>" />
	<input type="hidden" name="content[block_position]" value="<?= $content['block_position']; ?>" />

		<fieldset>

			<!--label for="leader">Kiemelt vezető doboz:</label>
			<?php
			echo '<input type="checkbox" class="radio" name="content[leader]" value="1" onchange="checkCatColumn(this);"';
			echo ($content['leader'] == 1) ? ' checked="checked"' : '';
			echo ' />'."\n";
			?>
			<br style="clear:both;" /-->

		<div id="column-wrapper">

				<label for="column">Címlapon:</label>
				<div id="column" style="float: left;">

					<?php
					echo '<input type="radio" class="radio" id="block1" name="content[block_column]" value="1"';
					echo ($action == 'insert' || ($content['block_column'] == 1)) ? ' checked="checked"' : '';
					echo ' /> BAL'."\n";

					echo '<input type="radio" class="radio" id="block2" name="content[block_column]" value="2"';
					echo ($content['block_column'] == 2) ? ' checked="checked"' : '';
					echo ' /> JOBB'."\n";

					echo '<input type="radio" class="radio" id="block3" name="content[block_column]" value="3"';
					echo ($content['block_column'] == 3) ? ' checked="checked"' : '';
					echo ' /> FEJLÉC'."\n";

					?>
				</div>
				<br style="clear:both;" />
		</div>


		<label for="category">Rovatban:</label>
		<div id="category" style="float: left;">
		<?php
		foreach($admin->categories AS $category) {
			if($category['id'] == '0') continue;

		echo '<input type="checkbox" class="radio" name="content[cat][]" value="cat_'.$category['id'].'"';
		echo (($_GET['category_id'] == $category['id']) || ($content['cat_'.$category['id']] == '1')) ? ' checked="checked"' : '';
		echo ' /> '.$category['category'].' '."\n";
		}
/*
echo '<input type="checkbox" class="radio" name="content[cat][]" value="cat_9"';
echo (($_GET['category_id'] == $category['id']) || ($content['cat_'.$category['id']] == '9')) ? ' checked="checked"' : '';
echo ' /> Életmód (címlap)';*/
		?>
		</div><br style="clear:both;" />

		<!--label for="cid">Tartalom ID:</label>
		<input name="content[content_id]" type="text" id="content_id" onblur="getTitleById();" class="text required" value="<?= $content['content_id']; ?>" title="Add meg a tartalom azonosítóját" /><br style="clear:both;" /-->

		<label for="link">Tartalom linkje:</label>
		<input name="content[link]" type="text" id="link" class="text required" onblur="getTitleByUrl();" placeholder="http://" value="<?= $content['link']; ?>" title="Add meg a tartalom URL-jét" /><br style="clear:both;" />

		<label for="title">Tartalom címe:</label>
		<input type="text" id="title" class="text required" name="content[title]" value="<?= htmlspecialchars($content['title'], ENT_QUOTES); ?>" title="Írd be a tartalom címét" /><br style="clear:both;" />

		<label for="description">Ajánlószöveg:</label>
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400&amp;subset=latin,latin-ext">
		<textarea name="content[description]" rows="8" class="mceNoEditor" style="height: 100px;width:310px;font: 400 14px 'Open Sans', Helvetica, Arial, sans-serif; line-height: 1.4;-webkit-font-smoothing: subpixel-antialiased;color:#333;" id="description" title="Add meg a tartalom szövegét"><?= htmlspecialchars($content['description'], ENT_QUOTES); ?></textarea><br />

		<label for="image">Tartalom képe:</label>
		<input type="hidden" name="imgselectorstr" id="imgselectorstr" value="&opendir=<?= $parsedimg[1]; ?>&current=<?= $parsedimg[2]; ?>" />
		<input name="image" type="text" id="image" class="text" style="width: 450px;" value="<?= $content['image']; ?>" title="Add meg a tartalom képének elérési útvonaát" />

		<?php
		$exploded = explode('/', $content['image']);
		if(preg_match("/\/images\/content\/cikkepek\/(.+)\/(.+)/i", $content['image'], $parsedimg)) {
		}

		?>


<a href="/admin/js/filemanager/dialog.php?type=1&sort_by=date&descending=true&field_id=image&fldr=<?= $parsedimg[1]; ?>" class="iframe-btn" data-fancybox-type="iframe">Kép kiválasztása</a>
<script>

jQuery.noConflict();

// Use jQuery via jQuery(...)
jQuery(document).ready(function(){
 	jQuery(".iframe-btn").fancybox({

		width		: '90%',
		height		: '100%',
		autoSize	: false
	});
});

function responsive_filemanager_callback(field_id){
	if(field_id=='image_link'){
    console.log(field_id);
    var url=jQuery('#'+field_id).val();
    alert('update '+field_id+" with "+url);
    $('#image_preview').attr('src',url);
    //your code
  }
}

</script>
		<!--a href="javascript:;" id="imgselector" onclick="openImagePopup('block'+$F('imgselectorstr'));">Kép kiválasztása (600x312)</a--><br style="clear:both;" />


		<iframe id="imgframe" src="" width="610" height="300" frameborder="0" scrolling="no" style="display: none; margin-left: 152px;"></iframe>


			<!--label for="imagesource">Kép forrása:</label>
			<input type="text" id="imagesource" class="text" style="width: 294px;" name="content[imagesource]" value="<?= htmlspecialchars($content['imagesource'], ENT_QUOTES); ?>" title="Írd be a kép forrását" /><br style="clear:both;" /-->

			<label for="datetime">Megjelenik:</label>
			<input name="content[datetime]" type="datetime-local" id="datetime" class="text required" style="width: 194px;" value="<?= strftime('%Y-%m-%dT%H:%M:%S', strtotime($content['datetime'])); ?>" title="Add meg a megjelenés dátumát" /> <a href="javascript:;" onclick="addRelated();">Kapcsolódó tartalom hozzáadása</a><br style="clear:both;" />


		<div id="cloneable" class="cloneable" style="display: none;" rel="">


			<label for="link">Tartalom linkje:</label>
			<input name="related[link][]" type="text" id="link" onblur="getTitleByUrlCloned(this.parentNode.rel);" placeholder="http://" class="text required" value="" title="Add meg a tartalom URL-jét" />
			<a href="javascript:;" onclick="$(this.parentNode).remove();" title="Törlés"><img src="images/icon/x.gif" alt="" /></a><br style="clear:both;" />

			<label for="cid">Tartalom címe:</label>
			<input type="text" id="title" class="text required" onfocus="initAutocomp(this.parentNode.rel);" name="related[title][]" value="" title="Írd be a tartalom címét" />
			<div class="auto_complete" id="autocomplete" style="z-index: 9999;"></div>


		</div>
<div id="related">
		<?php
		if($action == 'update') {
			$rel = 100;
			foreach($related as $value) {
				echo '<div class="cloneable" id="cloned'.$rel.'">
				<label for="link">Tartalom linkje:</label>
				<input name="related[link][]" type="text" id="link-'.$rel.'" onblur="getTitleByUrlCloned('.$rel.');" placeholder="http://" class="text required" value="'.$value['link'].'" title="Add meg a tartalom URL-jét" />
				<a href="javascript:;" onclick="$(this.parentNode).remove();" title="Törlés"><img src="images/icon/x.gif" alt="" /></a><br style="clear:both;" />

				<label for="cid">Tartalom címe:</label>
				<input type="text" id="title-'.$rel.'" class="text required" onfocus="initAutocomp('.$rel.');" name="related[title][]" value="'.htmlspecialchars($value['title'], ENT_QUOTES).'" title="Írd be a tartalom címét" />
				<div class="auto_complete" id="autocomplete-'.$rel.'" style="z-index: 9999 !important;"></div>


				</div>';
				$rel++;
			}
		}
		?>
		<span id="clone-here"></span>
</div>
		</fieldset>

		<label>&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés" />
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="redirDel(\'blocks\', '.$_GET['id'].');" />' : ''; ?>
		<!--input type="button" id="preview" class="button" value="Villámnézet" onclick="showBlockPreview();" /-->
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var curr_e = null;
		function initAutocomp(e) {
			curr_e = e;
			return new Ajax.Autocompleter('title-'+e, 'autocomplete-'+e, 'requests.php?what=get_related&not=0', {minChars: 2, afterUpdateElement : getSelectionId});

		}

		function getSelectionId(text, li) {
			$('link-'+curr_e).value = li.readAttribute('rel');
		}

		var valid = new Validation('form');

		Sortable.create("related", {tag: "DIV"});


	// ]]>
	</script>


<?php
}

if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_EDITOR)) {
?>
	<h1>Kiemelések szerkesztése</h1>
	<h2>Válaszd ki egy módosítandó kiemelést az alábbi listából.</h2>

<br class="clear" />

<?php

$order = "b.cat_".(int)$_GET['category_id']." = '1'";

if((int)$_GET['category_id'] == 0) {
	$q_cat_0 = " AND ordinal = '0' ";
}





if((int)$_GET['category_id'] != 0) { // rovatok szerkesztése

	// lapozások kiszámítása, ennyi legyen egy oldalon:
	$perpage = 12;
	if(empty($_GET['page'])) { // első oldalon vagyunk
		$listfrom = 0;
		$pnow = 1;
	}
	else { // next pages
		$listfrom = $_GET['page'] * $perpage - $perpage;
		$pnow = $_GET['page'];
	}

	$res_all = $env->db->Query("SELECT * FROM "._DBPREF."content_blocks b WHERE ".$order." ".$q_cat_0." GROUP BY b.block_id");
	$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

	$i = $listfrom + 1; // lista sorszáma
	echo '<table cellspacing="0" cellpadding="1" width="100%">';
	$res = $env->db->Query("SELECT b.block_id, b.title, b.link, b.description, image, b.datetime, u.name, u.alias, b.modtime, c.counter
		FROM "._DBPREF."content_blocks b
		LEFT JOIN "._DBPREF."users u ON b.user_id = u.id
		LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
		WHERE (block_column = 1 OR block_column = 2) AND ".$order." ".$q_cat_0." GROUP BY b.block_id ORDER BY b.modtime DESC LIMIT ".$listfrom.", ".$perpage);
		echo '
		<tr class="head">
			<td align="center"><b>Kép</b></td>
			<td valign="top"><b>Tartalom címe</b></td>
			<!--td valign="top"><b>Sz.</b></td-->
			<td valign="top"><b>Módosítva</b></td>
			<td></td>
		</tr>
	<tbody>
		';
	$ts = 0;
	while($row = $env->db->fetchArray($res)) {
		$isArchiveTag = false;
		$queue = $admin->dateFormat($row['datetime']);

		if($_GET['category_id'] == '0') {
			if(strtotime($row['datetime']) <= time() && $ts < 1) {
				$ts++;
				$isArchive = ' class="noarchive"';
			}
			else {
				$isArchive = ' class="archive"';

				if(strtotime($row['datetime']) > time()) {
				$isArchiveTag = true;
				$i--;
				}
			}
		}
		else {

			if(strtotime($row['datetime']) <= time()) {
				//$isArchive = ' style="background-color: '.((($i + 1 ) % 2 == 0) ? '' : '#F8F9FA').'"';
				$isArchiveTag = false;
				$isArchive = ' class="noarchive"';
			}
			else {
				$isArchive = ' class="archive"';
				$isArchiveTag = true;
				$i--;
			}

		}



	/*
			$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
	//		$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));

			echo '
			<tr style="background-color: '.$diffcolor.'">
	*/

		echo '
		<tr'.$isArchive.'>
			<td valign="top" align="center">';

		if($row['image']) {
			echo '<img src="'.$row['image'].'" width="48" alt="" />';
		}
		else {
			echo '<img src="http://dummyimage.com/48x30/dddddd/fffffff&text=NINCS" width="48" height="30" alt="" />';
		}

		echo '</td>
			<td valign="top" width="80%"><a href="?op=blocks&amp;mode=edit&amp;id='.$row['block_id'].'&amp;category_id='.(int)$_GET['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a>'.($isArchiveTag ? ' <b style="color:#000;">– IDŐZÍTVE</b> ' : '').'</td>
			<!--td valign="top" nowrap="nowrap">';

/*	$counter = array();
	$res_counter = $env->db->Query("SELECT counter FROM "._DBPREF."content_blocks l
	LEFT JOIN "._DBPREF."content c ON l.content_id = c.id WHERE l.block_id = '".$row['block_id']."' ORDER BY ordinal");
	while($row_counter = $env->db->fetchArray($res_counter)) { $counter[] = $row_counter['counter']; }
	echo implode(', ', $counter); */

			echo '</td-->
			<td valign="top" nowrap>'.$admin->dateFormatQueue($row['modtime']).'<br><a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlspecialchars($row['name'], ENT_QUOTES).'</a></td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$row['link'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<a href="javascript:redirDel(\'blocks\', '.$row['block_id'].', '.(int)$_GET['category_id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
			</td>
		</tr>
		';

	$i++;
	}
	echo '</tbody>';
	echo '</table>';

	echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
		for($j = 1; $j <= $pgs; $j++) { // következő, előző oldalak számainak kiírása
			if($pnow == $j) { // ez az oldal, itt állunk most
				echo ' <b>'.$j.'</b>';
			}
			else { // további oldalak linkkel
				echo ' <a href="?op=blocks&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;page='.$j.'">'.$j.'</a> ';
			}
		}
	echo '</p>';



}
else { // címlap szerkesztése --------------------------------------------------------------------------------------------------------


	/*
	---------- FEJLÉC
	*/


	// lapozások kiszámítása, ennyi legyen egy oldalon:
	$perpage = 3;
	if(empty($_GET['page'])) { // első oldalon vagyunk
		$listfrom = 0;
		$pnow = 1;
	}
	else { // next pages
		$listfrom = $_GET['page'] * $perpage - $perpage;
		$pnow = $_GET['page'];
	}

	$res_all = $env->db->Query("SELECT * FROM "._DBPREF."content_blocks b WHERE ordinal = '0' AND block_column = 3 GROUP BY b.block_id");
	$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

	$i = $listfrom + 1; // lista sorszáma
	$li = $listfrom + 1;
	echo '<table cellspacing="0" cellpadding="1" width="100%">';
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS b.block_id, b.title, b.link, b.description, image, block_position, b.datetime, u.name, u.alias, b.modtime, c.counter
		FROM "._DBPREF."content_blocks b
		LEFT JOIN "._DBPREF."users u ON b.user_id = u.id
		LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
		WHERE ordinal = '0' AND block_column = '3' GROUP BY b.block_id ORDER BY block_position DESC LIMIT ".$listfrom.", ".$perpage);
		$res_num = $db->Query("SELECT found_rows()");
		$rows = $db->fetchArray($res_num);
		$num = $rows[0];
		echo '
		<tr class="head">
			<!--td valign="top">&nbsp;</td-->
			<td align="center"><b>Kép</b></td>
			<td valign="top"><b>Tartalom címe</b></td>
			<!--td valign="top"><b>Sz.</b></td-->
			<td valign="top"><b>Módosítva</b></td>
			<td valign="top" align="center"><b>Le</b></td>
			<td valign="top" align="center"><b>Fel</b></td>
			<td></td>
		</tr>
	<tbody>
		';
	$ts = 0;
	while($row = $env->db->fetchArray($res)) {
		$isArchiveTag = false;
		//$queue = $admin->dateFormat($row['datetime']);
		$queue = ($ts < 3) ? $admin->dateFormatQueue($row['datetime']) : '-';

		if($_GET['category_id'] == '0') {
			if(strtotime($row['datetime']) <= time() && $ts < 3) {
				$ts++;
				$isArchive = ' class="noarchive"';
			}
			else {
				$isArchive = ' class="archive"';

				if(strtotime($row['datetime']) > time()) {
				$isArchiveTag = true;
				$i--;
				}
			}
		}
		else {

			if(strtotime($row['datetime']) <= time()) {
				$isArchive = ' style="background-color: '.((($i + 1 ) % 2 == 0) ? '' : '#F8F9FA').'"';
				$isArchiveTag = false;
			}
			else {
				$isArchive = ' class="archive"';
				$isArchiveTag = true;
				$i--;
			}

		}

	/*
			$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
	//		$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));

			echo '
			<tr style="background-color: '.$diffcolor.'">
	*/

		echo '
		<tr'.$isArchive.'>
			<!--td valign="top"><span style="color: #aaa;"><b>'.(($isArchiveTag) ? "&nbsp;" : "$i.").'</b></span></td-->
			<td valign="top" align="center">';

		if($row['image']) {
			echo '<img src="'.$row['image'].'" width="48" alt="" />';
		}
		else {
			echo '<img src="http://dummyimage.com/48x30/dddddd/fffffff&text=NINCS" width="48" height="30" alt="" />';
		}

		echo '</td>
			<td valign="top" width="80%"><a href="?op=blocks&amp;mode=edit&amp;id='.$row['block_id'].'&amp;category_id='.(int)$_GET['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a>'.($isArchiveTag ? ' <b style="color:#000;">– IDŐZÍTVE</b> ' : '').'</td>
			<!--td valign="top" align="center" nowrap="nowrap">';

			echo $row['counter'];

			echo '</td-->
			<td valign="top" nowrap>'.$admin->dateFormatQueue($row['modtime']).'<br><a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlspecialchars($row['name'], ENT_QUOTES).'</a></td>
			<td valign="top" align="center">';
		if($li < $num)
			echo '<a href="?op=blocks&amp;mode=move&amp;col=3&amp;m=down&amp;block_position='.$row['block_position'].'&amp;id='.$row['block_id'].'"><img src="images/icon/arrow-down.gif" alt="" border="0" /></a>';

			echo '</td>
			<td valign="top" align="center">';

		if($li > 1)
			echo '<a href="?op=blocks&amp;mode=move&amp;col=3&amp;m=up&amp;block_position='.$row['block_position'].'&amp;id='.$row['block_id'].'"><img src="images/icon/arrow-up.gif" alt="" border="0" /></a>';

			echo '</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$row['link'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<a href="javascript:redirDel(\'blocks\', '.$row['block_id'].', '.(int)$_GET['category_id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
			</td>
		</tr>
		';

	$i++;
	$li++;
	}
	echo '</tbody>';
	echo '</table>';

	echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
		for($j = 1; $j <= $pgs; $j++) { // következő, előző oldalak számainak kiírása
			if($pnow == $j) { // ez az oldal, itt állunk most
				echo ' <b>'.$j.'</b>';
			}
			else { // további oldalak linkkel
				echo ' <a href="?op=blocks&amp;mode=show&amp;category_id='.(int)$_GET['category_id'].'&amp;page='.$j.'">'.$j.'</a> ';
			}
		}
	echo '</p><br>';



	/*
	---------- BAL HASÁB
	*/


	$i = $listfrom + 1; // lista sorszáma
	$li = $listfrom + 1;
	echo '<table cellspacing="0" cellpadding="1" width="49%" style="float:left;">';
	$res = $env->db->Query("SELECT b.block_id, b.title, b.link, b.description, image, block_position, b.datetime, u.name, u.alias, b.modtime, c.counter
		FROM "._DBPREF."content_blocks b
		LEFT JOIN "._DBPREF."users u ON b.user_id = u.id
		LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
		WHERE ordinal = '0' AND block_column = '1' GROUP BY b.block_id ORDER BY block_position DESC LIMIT 0, 28");
	$num = $env->db->numRows($res);
		echo '
		<tr class="head">
			<td align="center"><b>Kép</b></td>
			<td valign="top"><b>Tartalom címe</b></td>
			<!--td valign="top"><b>Megjelenés</b></td-->
			<!--td valign="top"><b>Sz.</b></td-->
			<td valign="top"><b>Módosítva</b></td>
			<td valign="top" align="center"><b>Le</b></td>
			<td valign="top" align="center"><b>Fel</b></td>
			<td></td>
		</tr>
	<tbody>
		';
	while($row = $env->db->fetchArray($res)) {
		$isArchiveTag = false;
		$queue = $admin->dateFormat($row['datetime']);

			if(strtotime($row['datetime']) <= time()) {
				//$isArchive = ' style="background-color: '.((($i + 1 ) % 2 == 0) ? '' : '#F8F9FA').'"';
				$isArchive = ' class="noarchive"';
				$isArchiveTag = false;
			}
			else {
				$isArchive = ' class="archive"';
				$isArchiveTag = true;
				$i--;
			}

		echo '
		<tr'.$isArchive.'>
			<td valign="top" align="center">';

		if($row['image']) {
			echo '<img src="'.$row['image'].'" width="48" alt="" />';
		}
		else {
			echo '<img src="http://dummyimage.com/48x30/dddddd/fffffff&text=NINCS" width="48" height="30" alt="" />';
		}

		echo '</td>
			<td valign="top" width="80%"><a href="?op=blocks&amp;mode=edit&amp;id='.$row['block_id'].'&amp;category_id='.(int)$_GET['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a>'.($isArchiveTag ? ' <b style="color:#000;">– IDŐZÍTVE</b>' : '').
			'</td>
			<!--td valign="top">'.$queue.'</td-->
			<!--td valign="top" nowrap="nowrap">';

/*	$counter = array();
	$res_counter = $env->db->Query("SELECT counter FROM "._DBPREF."content_blocks l
	LEFT JOIN "._DBPREF."content c ON l.content_id = c.id WHERE l.block_id = '".$row['block_id']."' ORDER BY ordinal");
	while($row_counter = $env->db->fetchArray($res_counter)) { $counter[] = $row_counter['counter']; }
	echo implode(', ', $counter); */

			echo '</td-->
			<td valign="top" nowrap>'.$admin->dateFormatQueue($row['modtime']).'<br><a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlspecialchars($row['name'], ENT_QUOTES).'</a></td>
			<td valign="top" align="center">';
		if($li < $num)
			echo '<a href="?op=blocks&amp;mode=move&amp;col=1&amp;m=down&amp;block_position='.$row['block_position'].'&amp;id='.$row['block_id'].'"><img src="images/icon/arrow-down.gif" alt="" border="0" /></a>';

			echo '</td>
			<td valign="top" align="center">';

		if($li > 1)
			echo '<a href="?op=blocks&amp;mode=move&amp;col=1&amp;m=up&amp;block_position='.$row['block_position'].'&amp;id='.$row['block_id'].'"><img src="images/icon/arrow-up.gif" alt="" border="0" /></a>';

			echo '</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$row['link'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<a href="javascript:redirDel(\'blocks\', '.$row['block_id'].', '.(int)$_GET['category_id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
			</td>
		</tr>
		';

	$i++;
	$li++;
	}
	echo '</tbody>';
	echo '</table>';


	/*
	---------- KÖZÉPSŐ HASÁB
	*/


	$i = $listfrom + 1; // lista sorszáma
	$li = $listfrom + 1;
	echo '<table cellspacing="0" cellpadding="1" width="49%" style="float:right;">';
	$res = $env->db->Query("SELECT b.block_id, b.title, b.link, b.description, image, block_position, b.datetime, u.name, u.alias, b.modtime, c.counter
		FROM "._DBPREF."content_blocks b
		LEFT JOIN "._DBPREF."users u ON b.user_id = u.id
		LEFT JOIN "._DBPREF."content c ON b.content_id = c.id
		WHERE ordinal = '0' AND block_column = '2' GROUP BY b.block_id ORDER BY block_position DESC LIMIT 0, 28");
	$num = $env->db->numRows($res);
		echo '
		<tr class="head">
			<td align="center"><b>Kép</b></td>
			<td valign="top"><b>Tartalom címe</b></td>
			<!--td valign="top"><b>Megjelenés</b></td-->
			<!--td valign="top"><b>Sz.</b></td-->
			<td valign="top"><b>Módosítva</b></td>
			<td valign="top" align="center"><b>Le</b></td>
			<td valign="top" align="center"><b>Fel</b></td>
			<td></td>
		</tr>
	<tbody>
		';
	while($row = $env->db->fetchArray($res)) {
		$isArchiveTag = false;
		$queue = $admin->dateFormat($row['datetime']);

			if(strtotime($row['datetime']) <= time()) {
				//$isArchive = ' style="background-color: '.((($i + 1 ) % 2 == 0) ? '' : '#F8F9FA').'"';
				$isArchive = ' class="noarchive"';
				$isArchiveTag = false;
			}
			else {
				$isArchive = ' class="archive"';
				$isArchiveTag = true;
				$i--;
			}

		echo '
		<tr'.$isArchive.'>
			<td valign="top" align="center">';

		if($row['image']) {
			echo '<img src="'.$row['image'].'" width="48" alt="" />';
		}
		else {
			echo '<img src="http://dummyimage.com/48x30/dddddd/fffffff&text=NINCS" width="48" height="30" alt="" />';
		}


		echo '</td>
			<td valign="top"><a href="?op=blocks&amp;mode=edit&amp;id='.$row['block_id'].'&amp;category_id='.(int)$_GET['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a>'.($isArchiveTag ? ' <b style="color:#000;">– IDŐZÍTVE</b>' : '').
			'</td>
			<!--td valign="top">'.$queue.'</td-->
			<!--td valign="top" nowrap="nowrap">';

/*	$counter = array();
	$res_counter = $env->db->Query("SELECT counter FROM "._DBPREF."content_blocks l
	LEFT JOIN "._DBPREF."content c ON l.content_id = c.id WHERE l.block_id = '".$row['block_id']."' ORDER BY ordinal");
	while($row_counter = $env->db->fetchArray($res_counter)) { $counter[] = $row_counter['counter']; }
	echo implode(', ', $counter); */

			echo '</td-->
			<td valign="top" nowrap>'.$admin->dateFormatQueue($row['modtime']).'<br><a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlspecialchars($row['name'], ENT_QUOTES).'</a></td>
			<td valign="top" align="center">';
		if($li < $num)
			echo '<a href="?op=blocks&amp;mode=move&amp;col=2&amp;m=down&amp;block_position='.$row['block_position'].'&amp;id='.$row['block_id'].'"><img src="images/icon/arrow-down.gif" alt="" border="0" /></a>';

			echo '</td>
			<td valign="top" align="center">';

		if($li > 1)
			echo '<a href="?op=blocks&amp;mode=move&amp;col=2&amp;m=up&amp;block_position='.$row['block_position'].'&amp;id='.$row['block_id'].'"><img src="images/icon/arrow-up.gif" alt="" border="0" /></a>';

			echo '</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$row['link'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<a href="javascript:redirDel(\'blocks\', '.$row['block_id'].', '.(int)$_GET['category_id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
			</td>
		</tr>
		';

	$i++;
	$li++;
	}
	echo '</tbody>';
	echo '</table>';




} // // címlap szerkesztése eddig







}


if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_EDITOR)) {

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés
		$res = $db->Query("DELETE FROM "._DBPREF."content_blocks WHERE block_id = '".(int)$_GET['id']."'");


		$env->dropFileCache('blocks_'.$_GET['category_id'].'.cache');
		$env->dropFileCache('blocks.cache');


		header('Location: '.BACKEND.'/?op=blocks&mode=show&category_id='.$_GET['category_id']);
		exit;

		?>
		<h1>Kiemelés törlése</h1>
		<h2>A kiválasztott kiemelés doboz törlése megtörtént.</h2>
		<?php
	}
}

if(($_GET['mode'] == 'move') && $admin->hasRight(ROLE_EDITOR)) {

	if(isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['block_position']) && is_numeric($_GET['block_position'])) { // mozgatás

		$col = (int)$_GET['col'];

		if($_GET['m'] == 'up') {
			$res_next = $env->db->Query("SELECT block_id FROM "._DBPREF."content_blocks WHERE block_column = '".$col."' AND block_position > ".(int)$_GET['block_position']." AND ordinal = '0' ORDER BY block_position ASC LIMIT 1");
			$row_next = $env->db->fetchArray($res_next);
			$next = $row_next['block_id'];

			$res = $db->Query("UPDATE "._DBPREF."content_blocks SET block_position = '".($_GET['block_position']+1)."' WHERE block_id = '".$_GET['id']."' AND ordinal = '0' LIMIT 1");
			$res = $db->Query("UPDATE "._DBPREF."content_blocks SET block_position = '".$_GET['block_position']."' WHERE block_id = '".$next."' AND ordinal = '0' LIMIT 1");
		}

		if($_GET['m'] == 'down') {
			$res_prev = $env->db->Query("SELECT block_id FROM "._DBPREF."content_blocks WHERE block_column = '".$col."' AND block_position < ".(int)$_GET['block_position']." AND ordinal = '0' ORDER BY block_position DESC LIMIT 1");
			$row_prev = $env->db->fetchArray($res_prev);
			$prev = $row_prev['block_id'];

			$res = $db->Query("UPDATE "._DBPREF."content_blocks SET block_position = '".($_GET['block_position']-1)."' WHERE block_id = '".$_GET['id']."' AND ordinal = '0' LIMIT 1");
			$res = $db->Query("UPDATE "._DBPREF."content_blocks SET block_position = '".$_GET['block_position']."' WHERE block_id = '".$prev."' AND ordinal = '0' LIMIT 1");
		}

			// címlapi cache eldobása
			$env->dropFileCache('blocks.cache');

		header('Location: '.BACKEND.'/?op=blocks&mode=show&category_id=0');
		exit;
	}
}

?>