
window.name = "swenback";


/*
használat:
onkeyup="getSefURL(this, $('target_field'), '2007', '.html')"
*/

function getSefURL(source, target, pref, ext) {

	var orig = new Array(" ","á","é","í","ó","ö","ő","ú","ü","ű");
	var safe = new Array("-","a","e","i","o","o","o","u","u","u");

	var string = pref; // prefix a leendő fájlnévnek
	var words = 99; // ennyi szóig alakítsa

	var x = $F(source).toLowerCase();
	var spaces = 0;

	for(i = 0; i < x.length; i++) { // lecseréli az ékezeteket
		var y = x.substr(i,1);
		if(y == " ") { spaces++; }
		if(spaces >= words) { break; }

		for(j = 0; j < orig.length; j++) {
			y = y.replace(orig[j], safe[j]);
		}
		string+=y;
	}

	// eltávolít minden más speciális karaktert
	string = string.replace(/([^a-z0-9-]+)/g, "");
	string += ext;

	// vissza lett törölve a "field", "target"-et is nullázzuk
	if(x.length == 0) { string = '' }

	$(target).value = string;

}

/* Tartalom szerkesztésénél, ha kitöröljük a linket, kapcsolgatja az editort */
function toggleEditor(source, target) {
	if($F(source) == '') { // nincs még cím
		//tinyMCE.execCommand('mceAddControl', false, target);
		tinymce.execCommand('mceAddEditor',false, target);

		//$('feed_is_null').setAttribute('selected', 'selected');
		$('userid-wrapper').setStyle({display: 'block'});
		$('datetime-wrapper').setStyle({display: 'block'});
		$('excerpt-wrapper').setStyle({display: 'block'});
		$('name').setAttribute('value', 'Propeller');
		$('name').setAttribute('rel', 'authors');
		$('name-label').innerHTML = 'Szerző:';

	}
	else {
		//tinyMCE.execCommand('mceRemoveControl', false, target);
		tinymce.execCommand('mceRemoveEditor',false, target);
		$('name-label').innerHTML = 'Felhasználó:';
		$('name').setAttribute('rel', 'usernames');
		$('datetime-wrapper').setStyle({display: 'none'});
		$('excerpt-wrapper').setStyle({display: 'none'});
	}
}

Event.observe(window, 'load', function() {

	if($('feed')) {
		var titleReq = false;

		Event.observe($('feed'), 'keydown', function() {
			if($F('feed') == '') { titleReq = false; }
		});

		Event.observe($('link'), 'mousedown', function() {
			if($F('feed') == '') { titleReq = false; }
		});

		Event.observe($('feed'), 'blur', function() { // link felparsolása
			if(!titleReq && ($F('title') == '') && validateLink($F('feed'))) { // nincs még cím
				$('title').addClassName('loading');
				new Ajax.Request('/requests/content/ajax_get_feedtitle', {
					method: 'post',
					parameters: 'value='+escape($F('feed'))+'&attr=encoding',
					onSuccess: function(n) {
						$('title').value = n.responseText;
						titleReq = true;
						$('title').removeClassName('loading');
					}
				});
			}
		});
	}

});


function validateLink(v) {

	return /^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i.test(v);

}

/* Megerősítés az egyes törléseknél */
function redirDel(op, id, category_id) {

	if(confirm("Biztos vagy benne, hogy véglegesen törölni szeretnéd ezt az elemet?")) {
		if(typeof category_id != 'undefined')
		window.location.href = backend+'/?op='+op+'&mode=delete&id='+id+'&category_id='+category_id;
		else
		window.location.href = backend+'/?op='+op+'&mode=delete&id='+id;
	}

}

/* TartalomID alapján lekéri a címet, linket, vezérbe ajánlóhoz a leírást */
/*function getTitleById() {
	if($F('content_id') != '') { // nincs még cím
		$('title').addClassName('loading');
		new Ajax.Request('requests.php?what=get_title_by_id', {
			method: 'post',
			parameters: 'id='+escape($F('content_id')),
			on404: function(req) {
				$('title').removeClassName('loading');
			},
			onSuccess: function(req) {
				var data = eval('('+req.responseText+')');
				$('title').value = data.title;
				$('link').value = data.link;
				if($('description')) {
					$('description').value = data.description;
				}
				$('title').removeClassName('loading');
			}
		});
	}
}*/

function getTitleByUrl() {
	if(!validateLink($F('link'))) return;

	//if($F('title') != '') {
		$('title').addClassName('loading');
		new Ajax.Request('requests.php?what=get_title_by_url', {
			method: 'post',
			parameters: 'url='+escape($F('link')),
			on404: function(req) {
				$('title').removeClassName('loading');
			},
			onSuccess: function(req) {
				var data = eval('('+req.responseText+')');
				$('title').value = data.title;
				$('link').value = data.link;
				$('description').value = data.description;

				if($('image').value == '') $('image').value = data.image;
				$('title').removeClassName('loading');
			}
		});
	//}
}
function getTitleByUrlCloned(cloneId) {
	if(!validateLink($F('link-'+cloneId))) return;

	//if($F('content_id-'+cloneId) != '') {
		$('title-'+cloneId).addClassName('loading');
		new Ajax.Request('requests.php?what=get_title_by_url', {
			method: 'post',
			parameters: 'url='+escape($F('link-'+cloneId)),
			on404: function(req) {
				$('title-'+cloneId).removeClassName('loading');
			},
			onSuccess: function(req) {
				var data = eval('('+req.responseText+')');
				$('title-'+cloneId).value = data.title;
				$('link-'+cloneId).value = data.link;
				$('title-'+cloneId).removeClassName('loading');
			}
		});
	//}

}

/* TartalomID alapján lekéri a címet, embed-kódot a kiemelt videóhoz */
function getFullembedById() {
	if($F('video_content_id') != '' && $F('video_content_id') != '0') { // nincs még cím
		$('video_title').addClassName('loading');
		new Ajax.Request('requests.php?what=get_fullembed_by_id', {
			method: 'post',
			parameters: 'id='+escape($F('video_content_id')),
			on404: function(req) {
				$('title').removeClassName('loading');
			},
			onSuccess: function(req) {
				var data = eval('('+req.responseText+')');
				$('video_title').value = data.title;
				$('video_fullembed').value = data.fullembed;
				$('video_title').removeClassName('loading');
			}
		});
	}
}

/* Kapcsolódó linkek wrapperjének klónozása a vezérnél */
var cloneCount = 1;
function addRelated() {
	if(cloneCount >= 8)
		return false;

	var newFields = $('cloneable').cloneNode(true);
	newFields.id = 'clone-'+cloneCount;
	newFields.rel = cloneCount;
	newFields.style.display = 'block';
	var newField = newFields.childNodes;
	for(var i=0; i<newField.length; i++) {
		var theName = newField[i].name;
		var theId = newField[i].id;
		var theValue = newField[i].value;
		if(theName) {
			newField[i].name = theName;
		}
		if(theId) {
			newField[i].id = newField[i].id+'-'+cloneCount;
		}
	}
	var insertHere = $('clone-here');
	insertHere.parentNode.insertBefore(newFields, insertHere);
	Sortable.create("related", {tag: "DIV", hoverclass: 'add-sortable-hover', onUpdate: function(e) {

}

});
	cloneCount++;
}



/* Fotófeltöltés wrapperjének klónozása a nagyképnél */
var cloneCountImg = 1001;
function addImg() {
	if(cloneCountImg >= 1020)
		return false;

	var newFields = $('cloneable').cloneNode(true);
	newFields.id = 'clone-'+cloneCountImg;
	newFields.rel = cloneCountImg;
	newFields.style.display = 'block';
	var newField = newFields.childNodes;
	for(var i=0; i<newField.length; i++) {
		var theName = newField[i].name;
		var theId = newField[i].id;
		var theValue = newField[i].value;
		if(theName) {
			newField[i].name = theName;
		}
		if(theId) {
			newField[i].id = newField[i].id+'-'+cloneCountImg;

			if(theId == 'img-count') {
				$(newField[i]).innerHTML = parseInt(lastImg+1)+'. fájl:';
				lastImg++;
			}
			if(theId == 'cover') {
				$(newField[i]).value = cloneCountImg;
			}
		}
	}
	var insertHere = $('clone-here');
	insertHere.parentNode.insertBefore(newFields, insertHere);
	Sortable.create("imgs", {tag: "DIV", hoverclass: 'add-sortable-hover'});
	cloneCountImg++;
}

/* TartalomID alapján lekéri a címet, linket a klónozott mezőknél */
/*function getTitleByIdCloned(cloneId) {
	if($F('content_id-'+cloneId) != '') { // nincs még cím
		$('title-'+cloneId).addClassName('loading');
		new Ajax.Request('requests.php?what=get_title_by_id', {
			method: 'post',
			parameters: 'id='+escape($F('content_id-'+cloneId)),
			on404: function(req) {
				$('title-'+cloneId).removeClassName('loading');
			},
			onSuccess: function(req) {
				var data = eval('('+req.responseText+')');
				$('title-'+cloneId).value = data.title;
				$('link-'+cloneId).value = data.link;
				$('title-'+cloneId).removeClassName('loading');
			}
		});
	}
}*/

/* megnyitja a képfeltöltő ablakot */
function openImagePopup(parentDir) {
//	window.open(backend+'/image_popup.php?parent='+parentDir, '_blank', 'width=640px,height=350px,noresizable=no,status=no');
	if($('imgframe').style.display == 'none') {
		$('imgframe').src = backend+'/image_popup.php?parent='+parentDir;
		$('imgframe').style.display = 'block';
	}
	else {
		$('imgframe').style.display = 'none';
	}
	return false;
}

function changeImageList(parentDir, dir) {
	if(dir == '')
		return false;

	$('imagelist').src = backend+'/image_popup.php?parent='+parentDir+'&action=dirbrowser&dir='+dir;
}
function pasteImageLocation(img) {

//	window.opener.document.form.image.value = img;
	parent.document.form.image.value = img;

	var exploded = img.split("/");
	parent.document.form.imgselectorstr.value = '&opendir='+exploded[0]+'&current='+exploded[1];

	parent.document.getElementById('imgframe').style.display = 'none';


//	window.close();
}




// block szerkesztőben
function popforBlock() { //
	imgdr = 'b';

	openImagePopup(imgdr+$F('imgselectorstr'));
}

var checkCatColumn = function(n) {
	if(n.checked)
		$('column-wrapper').setStyle({display: 'none'});
	else
		$('column-wrapper').setStyle({display: 'block'});
}





function reply(name) {
	$('mg').value = $('mg').value+name+': ';
	$('mg').focus();
}


