<?php
defined('ENV') or die;

if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_GOD)) {

	if($_POST['action'] == 'insert') { // űrlapelküldés, kiértékelés új beszúráshoz
		$feed = $_POST['feed'];
		$feed['timediff'] = (int)$feed['timediff'];

		if(empty($feed['title'])) { $error = 'Írd be a feed elnevezését'; }
		if(empty($feed['feed'])) { $error = 'Add meg feed URL-jét'; }
		if(!is_int($feed['timediff'])) { $error = 'Add meg feed CET-hez képesti időzóna eltérést'; }
		if(empty($feed['site'])) { $error = 'Add meg gazdaoldal elnevezését'; }
		if(empty($feed['link'])) { $error = 'Add meg gazdaoldal URL-jét'; }
		if(empty($feed['feed_schema'])) { $error = 'Add meg a feed feldolgozó sémáját'; }
		if(empty($feed['category_id'])) { $error = 'Válaszd ki a feed kategóriáját'; }
		if(empty($feed['fetchrank'])) { $error = 'Válaszd ki a frissülés gyakoriságát'; }
		if(empty($feed['modtime'])) { $error = 'Add meg a feed utolsó feldolgozásának időpontját'; }
		if(empty($feed['lastpubdate'])) { $error = 'Add meg a feedben talált utolsó elem időpontját'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$feed = $db->escape($feed);

			$feed['public'] = (empty($feed['public']) ? "0" : "1");
			$q_video_embed = empty($feed['video_embed']) ? "NULL" : "'".$feed['video_embed']."'";

			$res = $db->Query("INSERT INTO "._DBPREF."content_feeds (category_id, title, feed, timediff, site, link, feed_schema, video_embed, modtime, lastpubdate, public, fetchrank) VALUES (
			'".$feed['category_id']."', '".$feed['title']."', '".$feed['feed']."', '".$feed['timediff']."', '".$feed['site']."', '".$feed['link']."', '".$feed['feed_schema']."', ".$q_video_embed.", '".$feed['modtime']."', '".$feed['lastpubdate']."', '".$feed['public']."', '".$feed['fetchrank']."')");

			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=tags&mode=show&category_id='.$feed['category_id'].'&order='.$_SESSION['_tags_order'].'&page='.$_SESSION['_tags_page']);
				exit;
			}
		}
	}

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$feed = $_POST['feed'];
		$feed['timediff'] = (int)$feed['timediff'];

		if(empty($feed['title'])) { $error = 'Írd be a feed elnevezését'; }
		if(empty($feed['feed'])) { $error = 'Add meg feed URL-jét'; }
		if(!is_int($feed['timediff'])) { $error = 'Add meg feed CET-hez képesti időzóna eltérést'; }
		if(empty($feed['site'])) { $error = 'Add meg gazdaoldal elnevezését'; }
		if(empty($feed['link'])) { $error = 'Add meg gazdaoldal URL-jét'; }
		if(empty($feed['feed_schema'])) { $error = 'Add meg a feed feldolgozó sémáját'; }
		if(empty($feed['category_id'])) { $error = 'Válaszd ki a feed kategóriáját'; }
		if(empty($feed['fetchrank'])) { $error = 'Válaszd ki a frissülés gyakoriságát'; }
		if(empty($feed['modtime'])) { $error = 'Add meg a feed utolsó feldolgozásának időpontját'; }
		if(empty($feed['lastpubdate'])) { $error = 'Add meg a feedben talált utolsó elem időpontját'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$feed = $db->escape($feed);

			$feed['public'] = (empty($feed['public']) ? "0" : "1");
			$q_video_embed = empty($feed['video_embed']) ? "NULL" : "'".$feed['video_embed']."'";

			$res = $db->Query("UPDATE "._DBPREF."content_feeds SET
			category_id = '".$feed['category_id']."',
			title = '".$feed['title']."',
			feed = '".$feed['feed']."',
			timediff = '".$feed['timediff']."',
			site = '".$feed['site']."',
			link = '".$feed['link']."',
			feed_schema = '".$feed['feed_schema']."',
			video_embed = ".$q_video_embed.",
			modtime = '".$feed['modtime']."',
			lastpubdate = '".$feed['lastpubdate']."',
			public = '".$feed['public']."',
			fetchrank = '".$feed['fetchrank']."'
			WHERE id = '".$db->escape($_GET['id'])."' LIMIT 1");

			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=tags&mode=show&category_id='.$feed['category_id'].'&order='.$_SESSION['_tags_order'].'&page='.$_SESSION['_tags_page']);
				exit;
			}
		}
	}

// már meglévő elem esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$res = $db->Query("SELECT * FROM "._DBPREF."content_feeds WHERE id = '".$_GET['id']."'");
		$feed = $db->fetchArray($res);
	}
	else { // új oldal
		$action = 'insert';
		$_POST['feed']['modtime'] = date('Y-m-d H:i:s', strtotime('-1 day'));
		$_POST['feed']['lastpubdate'] = date('Y-m-d H:i:s', strtotime('-1 day'));
		$feed = $_POST['feed'];
		if(empty($feed['feed_schema'])) $feed['feed_schema'] = 'rss';
	}

?>

	<h1><?= ($action == 'insert') ? 'Új feed hozzáadása' : 'Feedek szerkesztése'; ?></h1>
	<h2>Add meg a feed részleteit, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">
		<input type="hidden" name="action" id="action" value="<?= $action; ?>" />

		<fieldset>

			<label for="alias">Feed URL-je:</label>
			<input name="feed[feed]" type="text" id="feed" class="text required validate-url" value="<?= empty($feed['feed']) ? 'http://' : $feed['feed']; ?>" title="Add meg feed URL-jét" /><br />

			<label for="mail">Feed elnevezése:</label>
			<input type="text" id="title" class="text required" name="feed[title]" value="<?= $feed['title']; ?>" title="Írd be a feed elnevezését" /><br />

			<label for="alias">Időzóna eltérés CET-től:</label>
			<input name="feed[timediff]" type="text" id="timediff" class="text required" maxlength="8" value="<?= empty($feed['timediff']) ? 0 : $feed['timediff']; ?>" title="Add meg feed CET-hez képesti időzóna eltérést" /><br />


			<label for="feed_schema">Feldolgozó séma:</label>
			<input type="text" id="feed_schema" class="text required" name="feed[feed_schema]" value="<?= $feed['feed_schema']; ?>" title="Add meg a feed feldolgozó sémáját" ><br />


			<label for="is_video">Videó feed:</label>
			<input type="checkbox" id="is_video" class="radio" <?php echo (!empty($feed['video_embed'])) ? 'checked="checked"' : ''; ?> onclick="new Element.toggle('video-embed-wrapper');" /><br class="clear" />


<div id="video-embed-wrapper"<?= (empty($feed['video_embed'])) ? ' style="display:none;"' : ''; ?>>
			<label for="video_embed">Videó embed sablon:</label>
			<textarea name="feed[video_embed]" rows="8" class="mceNoEditor" style="height: 100px;" id="embed" title="Add meg a videó beillesztőkódjához szükséges sablont"><?= htmlspecialchars($feed['video_embed'], ENT_QUOTES); ?></textarea><br />
</div>

			<label for="alias">Honlap URL-je:</label>
			<input name="feed[link]" type="text" id="link" class="text required validate-url" value="<?= empty($feed['link']) ? 'http://' : $feed['link']; ?>" title="Add meg a gazdaoldal URL-jét" /><br />

			<label for="alias">Honlap elnevezése:</label>
			<input name="feed[site]" type="text" id="link" class="text required" maxlength="200" value="<?= $feed['site']; ?>" title="Add meg a gazdaoldal elnevezését" /><br />

			<label for="category">Kategória:</label>
			<select name="feed[category_id]" class="required" title="Válaszd ki a feed kategóriáját">
			<option label="" value="">Nincs megadva</option>
			<?php
/*			$res_c = $db->Query("SELECT id, category FROM "._DBPREF."content_categories ORDER BY ordinal");
			while($row_c = $db->fetchArray($res_c)) {
			echo '<option label="'.htmlspecialchars($row_c['category'], ENT_QUOTES).'" value="'.$row_c['id'].'"';
			echo ($feed['category_id'] == $row_c['id']) ? ' selected="selected"' : '';
			echo '>'.htmlspecialchars($row_c['category'], ENT_QUOTES).'</option>'."\n";
			}*/

			foreach($admin->categories AS $category) {
			if($category['id'] != 0 && $category['id'] != 9) {
			echo '<option label="'.$category['category'].'" value="'.$category['id'].'"';
			echo ($feed['category_id'] == $category['id']) ? ' selected="selected"' : '';
			echo '>'.$category['category'].'</option>'."\n";
			}
			}


			?>
			</select><br />

			<label for="fetchrank">Frissülés gyakorisága:</label>
			<select name="feed[fetchrank]" class="required" title="Válaszd ki a frissülés gyakoriságát" tabindex="10">
			<option label="15 percenként" value="1"<?php if($feed['fetchrank'] == 1) { echo ' selected="selected"'; } ?>>15 percenként</option>
			<option label="45 percenként" value="2"<?php if($feed['fetchrank'] == 2) { echo ' selected="selected"'; } ?>>45 percenként</option>
			<option label="90 percenként" value="3"<?php if($feed['fetchrank'] == 3) { echo ' selected="selected"'; } ?>>90 percenként</option>
			</select><br />

			<label for="public">Publikus feed:</label>
			<input type="checkbox" id="public" class="radio" name="feed[public]" value="1" <?php echo (($feed['public'] == 1) || !isset($feed['public'])) ? 'checked="checked"' : ''; ?> /><br class="clear" />

			<label for="modtime">Utoljára feldolgozva:</label>
			<input name="feed[modtime]" type="text" id="modtime" class="text required" style="width: 200px;" value="<?= $feed['modtime']; ?>" title="Add meg a feed utolsó feldolgozásának időpontját" /><br />

			<label for="lastpubdate">Utolsó elem dátuma:</label>
			<input name="feed[lastpubdate]" type="text" id="lastpubdate" class="text required" style="width: 200px;" value="<?= $feed['lastpubdate']; ?>" title="Add meg a feedben talált utolsó elem időpontját" /><br />

		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés"/>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="redirDel(\'content_feeds\', '.$_GET['id'].');" />' : ''; ?>
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>
<?php
}

if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_GOD)) {
?>
	<h1>Címkék szerkesztése</h1>
	<h2>Válaszd ki a módosítandó címkét az alábbi listából.</h2>

	<br class="clear" />

<?php
$query = "";
if(!empty($_GET['q'])) {
	$words = explode(" ", $_GET['q']);
	while(list($k, $v) = each($words)){
		$v = $db->escape($v);
		if(!$j) {
			$query .= "(tag COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR tag COLLATE utf8_hungarian_ci LIKE '".$v."%' OR tag COLLATE utf8_hungarian_ci LIKE '%".$v."' OR tag COLLATE utf8_hungarian_ci LIKE '".$v."')";
			$j = 1;
		}
		else {
			$query .= " AND (tag COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR tag COLLATE utf8_hungarian_ci LIKE '".$v."%' OR tag COLLATE utf8_hungarian_ci LIKE '%".$v."' OR tag COLLATE utf8_hungarian_ci LIKE '".$v."')";
		}
	}
	$query .= " AND ";
}



?>
	<form id="backend-search" action="" method="get">
		<input type="hidden" name="op" value="tags" />
		<input type="hidden" name="mode" value="show" />
		<input type="text" name="q" class="text" value="<?= htmlspecialchars($_GET['q'], ENT_QUOTES); ?>" /> <input type="submit" class="button" value="Keresés" />
	</form>

<?php
// lapozások kiszámítása, ennyi legyen egy oldalon:
$perpage = 30;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
}
$_SESSION['_tags_page'] = $pnow;


if($_GET['order'] == 'content_num') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS id, tag, count(tag_id) AS content_num
	FROM "._DBPREF."content_tags_conn conn
	LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE ".$query." 1 = 1 GROUP BY tag
	ORDER BY content_num ASC LIMIT ".$listfrom.", ".$perpage);
	$flag = 1;
	$_SESSION['_tags_order'] = 'content_num';
}
elseif($_GET['order'] == 'desc') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS id, tag, count(tag_id) AS content_num
	FROM "._DBPREF."content_tags_conn conn
	LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE ".$query." 1 = 1 GROUP BY tag
	ORDER BY id DESC LIMIT ".$listfrom.", ".$perpage);
	$flag = 2;
	$_SESSION['_tags_order'] = 'desc';
}
elseif($_GET['order'] == 'length') {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS id, tag, count(tag_id) AS content_num, LENGTH(tag) AS tag_length
	FROM "._DBPREF."content_tags_conn conn
	LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE ".$query." 1 = 1 GROUP BY tag
	#HAVING LENGTH(tag) > 40
	ORDER BY tag_length ASC, content_num ASC LIMIT ".$listfrom.", ".$perpage);
	$flag = 3;
	$_SESSION['_tags_order'] = 'length';
}
else {
	$res = $env->db->Query("SELECT SQL_CALC_FOUND_ROWS id, tag, count(tag_id) AS content_num
	FROM "._DBPREF."content_tags_conn conn
	LEFT JOIN "._DBPREF."content_tags tags ON conn.tag_id = tags.id
	WHERE ".$query." 1 = 1 GROUP BY tag
	#HAVING COUNT(*) = 1
	ORDER BY tag COLLATE utf8_hungarian_ci ASC LIMIT ".$listfrom.", ".$perpage);
	$flag = 0;
	$_SESSION['_tags_order'] = '';
}

$res_num = $db->Query("SELECT found_rows()");
$rows = $db->fetchArray($res_num);

?>
	<p id="lead">
		<?= ($flag == 0) ? 'ABC szerint' : '<a href="?op=tags&amp;mode=show">ABC szerint</a>'; ?> |
		<?= ($flag == 1) ? 'tartalomszám szerint' : '<a href="?op=tags&amp;mode=show&amp;order=content_num">tartalomszám szerint</a>'; ?> |
		<?= ($flag == 2) ? 'létrehozás szerint' : '<a href="?op=tags&amp;mode=show&amp;order=desc">létrehozás szerint</a>'; ?> |
		<?= ($flag == 3) ? 'hossz szerint' : '<a href="?op=tags&amp;mode=show&amp;order=length">hossz szerint</a>'; ?>
	</p>

<?php

if(!$rows[0]) {
	echo '<p>Nincsenek a feltételnek megfelelő sorok...</p>';
}
else {
$pgs = ceil($rows[0] / $perpage); // oldalak száma

echo $rows[0];

$i = $listfrom + 1; // lista sorszáma
echo '<table cellspacing="0" cellpadding="1" width="100%">';

	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Címke</b></td>
		<td valign="top"><b>Tartalmak</b></td>
		<td>&nbsp;</td>
	</tr>
	';
	while($row = $env->db->fetchArray($res)) {
		$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';

//if(preg_match("/^([0-9]+)$/", $row['tag'])) {

/*
		$db->Query("DELETE FROM "._DBPREF."content_tags WHERE id = '".$row['id']."' LIMIT 1");
		$db->Query("DELETE FROM "._DBPREF."content_tags_conn WHERE tag_id = '".$row['id']."'");
*/
		echo '
		<tr style="background-color: '.$diffcolor.'">
			<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
			<td valign="top" nowrap>'.htmlspecialchars($row['tag'], ENT_QUOTES).'</td>
			<td valign="top">'.$row['content_num'].'</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$env->base.'/tag/'.htmlspecialchars($row['tag'], ENT_QUOTES).'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<!--a href="javascript:redirDel(\'tags\', '.$row['id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a-->
				<a href="?op=tags&amp;mode=delete&amp;id='.$row['id'].'&amp;order='.$_GET['order'].'&amp;page='.$_GET['page'].'&amp;q='.htmlentities($_GET['q'], ENT_QUOTES).'" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
			</td>
		</tr>
		';
	$i++;
}
	}
	echo '</table>';

	echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
$turn = ($pnow <= 15) ? 1 : ($pnow - 15);
$tto = ($pnow <= 15) ? (($pgs < 30) ? $pgs : 30): ($pnow+15);
		for($j = $turn; $j <= $tto; $j++) { // következő, előző oldalak számainak kiírása
			if($pnow == $j) { // ez az oldal, itt állunk most
				echo ' <b>'.$j.'</b>';
			}
			else { // további oldalak linkkel
				if(!empty($_GET['q']))
					echo ' <a href="?op=tags&amp;mode=show&amp;q='.htmlentities($_GET['q'], ENT_QUOTES).'&amp;page='.$j.'">'.$j.'</a> ';
				else
					echo ' <a href="?op=tags&amp;mode=show&amp;order='.$_GET['order'].(($_GET['order'] == 'category') ? '&amp;cid='.$_GET['cid'] : '').'&amp;page='.$j.'">'.$j.'</a> ';
			}
		}
	echo '</p>';
//}
}


if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_GOD)) {

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés
		$db->Query("DELETE FROM "._DBPREF."content_tags WHERE id = '".$_GET['id']."' LIMIT 1");
		$db->Query("DELETE FROM "._DBPREF."content_tags_conn WHERE tag_id = '".$_GET['id']."'");

		header('Location: '.BACKEND.'/?op=tags&mode=show&order='.$_SESSION['_tags_order'].'&page='.$_SESSION['_tags_page'].'&q='.$_GET['q']);
		?>
		<h1>Címke törlése</h1>
		<h2>A címke törlése megtörtént.</h2>
		<?php
		//var_dump($res);
	}
}

?>
