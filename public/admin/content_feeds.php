<?php
defined('ENV') or die;

if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_GOD)) {

	if($_POST['action'] == 'insert') { // űrlapelküldés, kiértékelés új beszúráshoz
		$feed = $_POST['feed'];
		$feed['timediff'] = (int)$feed['timediff'];

		if(empty($feed['title'])) { $error = 'Írd be a feed elnevezését'; }
		if(empty($feed['feed'])) { $error = 'Add meg feed URL-jét'; }
		//if(!is_int($feed['timediff'])) { $error = 'Add meg feed CET-hez képesti időzóna eltérést'; }
		if(empty($feed['site'])) { $error = 'Add meg gazdaoldal elnevezését'; }
		if(empty($feed['link'])) { $error = 'Add meg gazdaoldal URL-jét'; }
		if(empty($feed['feed_schema'])) { $error = 'Add meg a feed feldolgozó sémáját'; }
		if(empty($feed['category_id'])) { $error = 'Válaszd ki a feed kategóriáját'; }
		//if(empty($feed['fetchrank'])) { $error = 'Válaszd ki a frissülés gyakoriságát'; }
		if(empty($feed['modtime'])) { $error = 'Add meg a feed utolsó feldolgozásának időpontját'; }
		if(empty($feed['lastpubdate'])) { $error = 'Add meg a feedben talált utolsó elem időpontját'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$feed = $db->escape($feed);

			$feed['public'] = (empty($feed['public']) ? "0" : "1");
			$q_video_embed = empty($feed['video_embed']) ? "NULL" : "'".$feed['video_embed']."'";

			$res = $db->Query("INSERT INTO "._DBPREF."content_feeds (category_id, title, feed, timediff, site, link, feed_schema, video_embed, modtime, lastpubdate, public, fetchrank) VALUES (
			'".$feed['category_id']."', '".$feed['title']."', '".$feed['feed']."', '".$feed['timediff']."', '".$feed['site']."', '".$feed['link']."', '".$feed['feed_schema']."', ".$q_video_embed.", '".$feed['modtime']."', '".$feed['lastpubdate']."', '".$feed['public']."', '".$feed['fetchrank']."')");

			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=content_feeds&mode=show&category_id='.$feed['category_id'].'&order='.$_SESSION['_feeds_order'].'&page='.$_SESSION['_feeds_page']);
				exit;
			}
		}
	}

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$feed = $_POST['feed'];
		$feed['timediff'] = (int)$feed['timediff'];

		if(empty($feed['title'])) { $error = 'Írd be a feed elnevezését'; }
		if(empty($feed['feed'])) { $error = 'Add meg feed URL-jét'; }
		//if(!is_int($feed['timediff'])) { $error = 'Add meg feed CET-hez képesti időzóna eltérést'; }
		if(empty($feed['site'])) { $error = 'Add meg gazdaoldal elnevezését'; }
		if(empty($feed['link'])) { $error = 'Add meg gazdaoldal URL-jét'; }
		if(empty($feed['feed_schema'])) { $error = 'Add meg a feed feldolgozó sémáját'; }
		if(empty($feed['category_id'])) { $error = 'Válaszd ki a feed kategóriáját'; }
		//if(empty($feed['fetchrank'])) { $error = 'Válaszd ki a frissülés gyakoriságát'; }
		if(empty($feed['modtime'])) { $error = 'Add meg a feed utolsó feldolgozásának időpontját'; }
		if(empty($feed['lastpubdate'])) { $error = 'Add meg a feedben talált utolsó elem időpontját'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$feed = $db->escape($feed);

			$feed['public'] = (empty($feed['public']) ? "0" : "1");
			$q_video_embed = empty($feed['video_embed']) ? "NULL" : "'".$feed['video_embed']."'";

			$res = $db->Query("UPDATE "._DBPREF."content_feeds SET
			category_id = '".$feed['category_id']."',
			title = '".$feed['title']."',
			feed = '".$feed['feed']."',
			timediff = '".$feed['timediff']."',
			site = '".$feed['site']."',
			link = '".$feed['link']."',
			feed_schema = '".$feed['feed_schema']."',
			video_embed = ".$q_video_embed.",
			modtime = '".$feed['modtime']."',
			lastpubdate = '".$feed['lastpubdate']."',
			public = '".$feed['public']."',
			fetchrank = '".$feed['fetchrank']."'
			WHERE id = '".$db->escape($_GET['id'])."' LIMIT 1");

			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=content_feeds&mode=show&category_id='.$feed['category_id'].'&order='.$_SESSION['_feeds_order'].'&page='.$_SESSION['_feeds_page']);
				exit;
			}
		}
	}

// már meglévő elem esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$res = $db->Query("SELECT * FROM "._DBPREF."content_feeds WHERE id = '".$_GET['id']."'");
		$feed = $db->fetchArray($res);
	}
	else { // új oldal
		$action = 'insert';
		$_POST['feed']['modtime'] = date('Y-m-d H:i:s', strtotime('-1 day'));
		$_POST['feed']['lastpubdate'] = date('Y-m-d H:i:s', strtotime('-1 day'));
		$feed = $_POST['feed'];
		if(empty($feed['feed_schema'])) $feed['feed_schema'] = 'rss';
	}

?>

	<h1><?= ($action == 'insert') ? 'Új feed hozzáadása' : 'Feedek szerkesztése'; ?></h1>
	<h2>Add meg a feed részleteit, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">
		<input type="hidden" name="action" id="action" value="<?= $action; ?>" />

		<fieldset>

			<label for="alias">Feed URL-je:</label>
			<input name="feed[feed]" type="text" id="feed" class="text required validate-url" value="<?= empty($feed['feed']) ? 'http://' : $feed['feed']; ?>" title="Add meg feed URL-jét" /><br />

			<label for="mail">Feed elnevezése:</label>
			<input type="text" id="title" class="text required" name="feed[title]" value="<?= $feed['title']; ?>" title="Írd be a feed elnevezését" /><br />

			<!--label for="alias">Időzóna eltérés CET-től:</label>
			<input name="feed[timediff]" type="text" id="timediff" class="text required" maxlength="8" value="<?= empty($feed['timediff']) ? 0 : $feed['timediff']; ?>" title="Add meg feed CET-hez képesti időzóna eltérést" /><br /-->


			<label for="feed_schema">Feldolgozó séma:</label>
			<input type="text" id="feed_schema" class="text required" name="feed[feed_schema]" value="<?= $feed['feed_schema']; ?>" title="Add meg a feed feldolgozó sémáját" ><br />


			<label for="is_video">Videó feed:</label>
			<input type="checkbox" id="is_video" class="radio" <?php echo (!empty($feed['video_embed'])) ? 'checked="checked"' : ''; ?> onclick="new Element.toggle('video-embed-wrapper');" /><br class="clear" />


<div id="video-embed-wrapper"<?= (empty($feed['video_embed'])) ? ' style="display:none;"' : ''; ?>>
			<label for="video_embed">Videó embed sablon:</label>
			<textarea name="feed[video_embed]" rows="8" class="mceNoEditor" style="height: 100px;" id="embed" title="Add meg a videó beillesztőkódjához szükséges sablont"><?= htmlspecialchars($feed['video_embed'], ENT_QUOTES); ?></textarea><br />
</div>

			<label for="alias">Honlap URL-je:</label>
			<input name="feed[link]" type="text" id="link" class="text required validate-url" value="<?= empty($feed['link']) ? 'http://' : $feed['link']; ?>" title="Add meg a gazdaoldal URL-jét" /><br />

			<label for="alias">Honlap elnevezése:</label>
			<input name="feed[site]" type="text" id="link" class="text required" maxlength="200" value="<?= $feed['site']; ?>" title="Add meg a gazdaoldal elnevezését" /><br />

			<label for="category">Kategória:</label>
			<select name="feed[category_id]" class="required" title="Válaszd ki a feed kategóriáját">
			<option label="" value="">Nincs megadva</option>
			<?php
/*			$res_c = $db->Query("SELECT id, category FROM "._DBPREF."content_categories ORDER BY ordinal");
			while($row_c = $db->fetchArray($res_c)) {
			echo '<option label="'.htmlspecialchars($row_c['category'], ENT_QUOTES).'" value="'.$row_c['id'].'"';
			echo ($feed['category_id'] == $row_c['id']) ? ' selected="selected"' : '';
			echo '>'.htmlspecialchars($row_c['category'], ENT_QUOTES).'</option>'."\n";
			}*/

			foreach($admin->categories AS $category) {
			if($category['id'] != 0 && $category['id'] != 9) {
			echo '<option label="'.$category['category'].'" value="'.$category['id'].'"';
			echo ($feed['category_id'] == $category['id']) ? ' selected="selected"' : '';
			echo '>'.$category['category'].'</option>'."\n";
			}
			}


			?>
			</select><br />

			<!--label for="fetchrank">Frissülés gyakorisága:</label>
			<select name="feed[fetchrank]" class="required" title="Válaszd ki a frissülés gyakoriságát" tabindex="10">
			<option label="15 percenként" value="1"<?php if($feed['fetchrank'] == 1) { echo ' selected="selected"'; } ?>>15 percenként</option>
			<option label="45 percenként" value="2"<?php if($feed['fetchrank'] == 2) { echo ' selected="selected"'; } ?>>45 percenként</option>
			<option label="90 percenként" value="3"<?php if($feed['fetchrank'] == 3) { echo ' selected="selected"'; } ?>>90 percenként</option>
			</select><br /-->

			<label for="public">Publikus feed:</label>
			<input type="checkbox" id="public" class="radio" name="feed[public]" value="1" <?php echo (($feed['public'] == 1) || !isset($feed['public'])) ? 'checked="checked"' : ''; ?> /><br class="clear" />

			<label for="modtime">Utoljára feldolgozva:</label>
			<input name="feed[modtime]" type="text" id="modtime" class="text required" style="width: 200px;" value="<?= $feed['modtime']; ?>" title="Add meg a feed utolsó feldolgozásának időpontját" /><br />

			<label for="lastpubdate">Utolsó elem dátuma:</label>
			<input name="feed[lastpubdate]" type="text" id="lastpubdate" class="text required" style="width: 200px;" value="<?= $feed['lastpubdate']; ?>" title="Add meg a feedben talált utolsó elem időpontját" /><br />

		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés"/>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="redirDel(\'content_feeds\', '.$_GET['id'].');" />' : ''; ?>
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>
<?php
}

if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_GOD)) {
?>
	<h1>Feedek szerkesztése</h1>
	<h2>Válaszd ki a módosítandó feedet az alábbi listából.</h2>

	<br class="clear" />

<?php
$order = "";
if(!empty($_GET['q'])) {
	$words = explode(" ", $_GET['q']);
	while(list($k, $v) = each($words)){
		$v = $db->escape($v);
		if(!$j) {
			$order .= "(title LIKE '%".$v."%' OR site LIKE '%".$v."%' OR link LIKE '%".$v."%')";
			$j = 1;
		}
		else {
			$order .= " AND (title LIKE '%".$v."%' OR site LIKE '%".$v."%' OR link LIKE '%".$v."%')";
		}
	}
	$order .= " AND ";
}


if($_GET['category_id'] == 0) {
	$order .= "f.category_id IN (".implode(',', $myCategories).") AND ";
}
else {
	$order .= "f.category_id = '".(int)$_GET['category_id']."' AND ";
}


if($_GET['order'] == 'modtime') { // rendezések
	$order .= "public = '1' ORDER BY modtime";
	$flag = 1;
	$_SESSION['_feeds_order'] = 'modtime';
}
elseif($_GET['order'] == 'public') {
	$order .= "public = '0' ORDER BY title";
	$flag = 2;
	$_SESSION['_feeds_order'] = 'public';
}
elseif($_GET['order'] == 'video') {
	$order .= "public = '1' AND video_embed IS NOT NULL ORDER BY title";
	$flag = 3;
	$_SESSION['_feeds_order'] = 'video';
}
elseif($_GET['order'] == 'lastpubdate') {
	$order .= "public = '1' ORDER BY lastpubdate";
	$flag = 4;
	$_SESSION['_feeds_order'] = 'lastpubdate';
}
else {
	$order .= "public = '1' ORDER BY replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
f.title,'á','azz'),'í','izz'),'cs','czz'),'ly','lzz'),'é','ezz'),'ó','oxx'),'ö','oyy'),'ő','ozz'),'ty','tzz'),'gy','gzz'),'ú','uxx'),'ü','uyy'),'ű','uzz'),'ny','nzz'),'zs','zzz')
 ASC";
	$flag = 0;
	$_SESSION['_feeds_order'] = '';
}

?>
	<form id="backend-search" action="" method="get">
		<input type="hidden" name="op" value="content_feeds" />
		<input type="hidden" name="mode" value="show" />
		<input type="hidden" name="category_id" value="<?= (int)$_GET['category_id']; ?>" />
		<input type="text" name="q" class="text" value="<?= htmlspecialchars($_GET['q'], ENT_QUOTES); ?>" /> <input type="submit" class="button" value="Keresés" />
	</form>

	<p id="lead">
		<?= ($flag == 0) ? 'ABC szerint' : '<a href="?op=content_feeds&amp;mode=show&amp;category_id='.$_GET['category_id'].'">ABC szerint</a>'; ?> |
		<?= ($flag == 1) ? 'utoljára feldolgozva' : '<a href="?op=content_feeds&amp;mode=show&amp;category_id='.$_GET['category_id'].'&amp;order=modtime">utoljára feldolgozva</a>'; ?> |
		<?= ($flag == 4) ? 'utolsó elem' : '<a href="?op=content_feeds&amp;mode=show&amp;category_id='.$_GET['category_id'].'&amp;order=lastpubdate">utolsó elem</a>'; ?> |
		<?= ($flag == 3) ? 'videós feedek' : '<a href="?op=content_feeds&amp;mode=show&amp;category_id='.$_GET['category_id'].'&amp;order=video">videós feedek</a> '; ?> |
		<?= ($flag == 2) ? 'kikapcsolt feedek' : '<a href="?op=content_feeds&amp;mode=show&amp;category_id='.$_GET['category_id'].'&amp;order=public">kikapcsolt feedek</a> '; ?>
	</p>

<?php
// lapozások kiszámítása, ennyi legyen egy oldalon:
$perpage = 20;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
	$_SESSION['_feeds_page'] = (int)$_GET['page'];
}

$res_all = $env->db->Query("SELECT id FROM "._DBPREF."content_feeds f WHERE ".$order);
$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

if(!$env->db->numRows($res_all)) {
	echo '<p>Nincsenek a feltételnek megfelelő sorok...</p>';
}
else {

$i = $listfrom + 1; // lista sorszáma
echo '<table cellspacing="0" cellpadding="1" width="100%">';
$res = $env->db->Query("SELECT f.id, category_id, title, feed, modtime, lastpubdate, fetchrank, category, alias FROM "._DBPREF."content_feeds f
LEFT JOIN "._DBPREF."content_categories c ON f.category_id = c.id WHERE ".$order." LIMIT ".$listfrom.", ".$perpage);

$fetchranks = array('1' => '15', '2' => '45', '3' => '90');

	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Elnevezés</b></td>
		<td valign="top"><b>Kategória</b></td>
		<!--td valign="top"><b>Frissülés</b></td-->
		<td valign="top"><b>Utoljára feldolgozva</b></td>
		<td valign="top"><b>Utolsó elem</b></td>
		<td>&nbsp;</td>
	</tr>
	';
	while($row = $env->db->fetchArray($res)) {
		$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';

//		$res_c = $env->db->Query("SELECT count(id) AS content_num FROM "._DBPREF."content WHERE feed_id = '".$row['id']."'");
//		$row_c = $env->db->fetchArray($res_c);

		$host = preg_replace('/^(http:\/\/)?(www.)?([^\/\?]+)([\/|\?])?(.+)?$/', '$3', $row['feed']);
		$ago = number_format((time()-strtotime($row['modtime']))/60, 0, '.', '');

		echo '
		<tr style="background-color: '.$diffcolor.'">
			<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
			<td valign="top" nowrap><a href="?op=content_feeds&amp;mode=edit&amp;id='.$row['id'].'&category_id='.$row['category_id'].'">'.$row['title'].'</a></td>
			<td valign="top"><a href="'.$env->base.'/'.$row['alias'].'/" onclick="window.open(this.href, \'_blank\'); return false;">'.$row['category'].'</a></td>
			<!--td valign="top">'.$fetchranks[$row['fetchrank']].' percenként</td-->
			<td valign="top">'.$ago.' perce ('.$admin->dateFormat($row['modtime']).')</td>
			<td valign="top">'.$row['lastpubdate'].'</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$row['feed'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<a href="'.$env->base.'/search?q='.$host.'" onclick="window.open(this.href, \'_blank\'); return false;" title="Tartalmak keresése ebből a feedből"><img src="images/icon/search.gif" alt="" /></a>
				<a href="javascript:redirDel(\'content_feeds\', '.$row['id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
			</td>
		</tr>
		';
	$i++;
	}
	echo '</table>';

	echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
		for($j = 1; $j <= $pgs; $j++) { // következő, előző oldalak számainak kiírása
			if($pnow == $j) { // ez az oldal, itt állunk most
				echo ' <b>'.$j.'</b>';
			}
			else { // további oldalak linkkel
				echo ' <a href="?op=content_feeds&amp;mode=show&amp;order='.$_SESSION['_feeds_order'].'&amp;category_id='.$_GET['category_id'].'&amp;page='.$j.'">'.$j.'</a> ';
			}
		}
	echo '</p>';
}
}


if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_GOD)) {

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés
		$res[] = $db->Query("DELETE FROM "._DBPREF."content_feeds WHERE id = '".$_GET['id']."' LIMIT 1");
		$res[] = $db->Query("DELETE FROM "._DBPREF."content WHERE feed_id = '".$_GET['id']."'");
	// itt kéne törölni az érintett tartalmak kommentjeit is
	// update: majd az elhagyott kommentek karbantartó scriptje törli
		?>
		<h1>Feed törlése</h1>
		<h2>A feed és a hozzá tartozó tartalmak törlése megtörtént.</h2>
		<?php
		//var_dump($res);
	}
}

?>
