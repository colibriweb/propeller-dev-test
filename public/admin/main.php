<?php
defined('ENV') or die;

if(!empty($_POST['message'])) {
	$message = $db->escape($_POST['message']);

	$res = $db->Query("INSERT
	INTO "._DBPREF."users_backend_board (user_id, message, datetime) VALUES ('".$_SESSION['user']['id']."', '".$message."', NOW())");

	if($res) { // nincs hiba a beszúrásban
		header('Location: '.BACKEND.'/?op=main');
		exit;
	}
}



if(!empty($_POST['suggest'])) {
	$message = $db->escape($_POST['message']);

	if($admin->checkUrlFormat($_POST['suggest'])) { // link van, keressünk ID-t, majd abból címet

		preg_match('/propeller\.hu\/[a-z]+\/(\d+)-/', $_POST['suggest'], $matches);

		$content_id = $matches[1];
		$link = $_POST['suggest'];
	}
	elseif(is_numeric($_POST['suggest'])) {
		$content_id = $_POST['suggest'];
	}

	$res_suc = $db->Query("SELECT title, CONCAT(id,'-',alias) AS alias, category_id FROM "._DBPREF."content WHERE id = '".$content_id."' LIMIT 1");
	$row_suc = $db->fetchArray($res_suc);
	$title = $db->escape($row_suc['title']);


	if(!$link)
	$link = $env->base.'/'.$env->l['content']['category_alias_'.$row_suc['category_id']].'/'.$row_suc['alias'];

	$res = $db->Query("INSERT INTO "._DBPREF."users_backend_suggest (name, link, title, datetime) VALUES ('".$_SESSION['user']['name']."', '".$db->escape($link)."', '".$title."', NOW())");

	if($res) { // nincs hiba a beszúrásban
		header('Location: '.BACKEND.'/?op=main');
		exit;
	}
}


if(!empty($_GET['remove_suggested'])) {

	$res = $db->Query("DELETE FROM "._DBPREF."users_backend_suggest WHERE id = '".(int)$_GET['remove_suggested']."' LIMIT 1");
	if($res) { // nincs hiba a törlésben
		header('Location: '.BACKEND.'/?op=main');
		exit;
	}
}

?>

<div id="main-left">

	<h1>Szerkesztőségi üzenőfal</h1>
	<h2>Írj valamit az üzenőfalra.</h2>

<br class="clear" />

	<form action="" method="post">
	<fieldset class="msgboard">
		<textarea id="mg" name="message" class="mceNoEditor" rows="4" cols="32"></textarea><br />
		<input type="submit" value="Üzenet elküldése" class="button" />
	</fieldset>
	</form>

<br class="clear" />

<?php

$perpage = 20;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
}

$res_all = $env->db->Query("SELECT id FROM "._DBPREF."users_backend_board");
$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

$res_s = $db->Query("SELECT user_id, message, b.datetime, u.name, alias, avatar FROM "._DBPREF."users_backend_board b
LEFT JOIN "._DBPREF."users u ON b.user_id = u.id ORDER BY b.datetime DESC LIMIT ".$listfrom.", ".$perpage);
while($row_s = $db->fetchArray($res_s)) {
	$msg = nl2br(htmlspecialchars($row_s['message'], ENT_QUOTES));
	$msg = preg_replace("`http://propeller\.hu/([\w|\.|\-|_|/|\#\?\&\=\;]+)`ie", "'<a href=\"\\0\" target=\"_blank\">\\0</a>'", $msg);

	if($row_s['avatar'] != NULL)
	$row_s['avatarsrc'] = ($row_s['avatar'] == 1) ? floor($row_s['user_id']/1000).'/'.$row_s['user_id'].'/'.$row_s['user_id'].'_normal.jpg' : 'default_normal.jpg';

	echo '
	<div class="board-wrapper">
	<div>
		<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row_s['alias'].'">'.htmlspecialchars($row_s['name'], ENT_QUOTES).'</a>
		<i>'.$admin->dateFormat($row_s['datetime']).'</i> &ndash; <a href="javascript:reply(\''.htmlspecialchars($row_s['name'], ENT_QUOTES).'\');" rel="'.htmlspecialchars($row_s['name'], ENT_QUOTES).'"><i>válasz erre</i></a>
	</div>
	'.$msg.'
	</div>
	<br class="clear" />
	';


/*
	echo '<div class="board-item" style="';
	if($row_s['user_id'] == $_SESSION['user']['id']) echo 'background-color:#e2e4e6;';
	if(preg_match("|".$_SESSION['user']['name'].":|si", $row_s['message'])) echo 'border-right: 6px solid #e2e4e6;';
	echo '">
	<div><a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row_s['name'].'"><b>'.$row_s['name'].'</b></a> &ndash; '.$admin->dateFormat($row_s['datetime']).' <a href="javascript:reply(\''.$row_s['name'].'\');">&raquo;</a></div>'.$msg.'</div>';
*/
}

if ($pgs > 0) {
echo '	<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
$turn = ($pnow <= 15) ? 1 : ($pnow - 15);
$tto = ($pnow <= 15) ? (($pgs < 30) ? $pgs : 30): ($pnow+15);
	for($j = $turn; $j <= $tto; $j++) { // következő, előző oldalak számainak kiírása
		if($pnow == $j) { // ez az oldal, itt állunk most
			echo ' <b>'.$j.'</b>';
		}
		else { // további oldalak linkkel
			echo ' <a href="?page='.$j.'">'.$j.'</a> ';
		}
	}
echo '</p>';
}

?>

</div>

<div id="main-right">


<!--h4>Időzített tartalmak</h4>
<div class="main-box">

<?php
/*$res_s = $db->Query("
(SELECT title, link, datetime FROM "._DBPREF."content_topstories WHERE datetime > NOW() ORDER BY datetime DESC)
	UNION
(SELECT title, link, datetime FROM "._DBPREF."content_blocks WHERE datetime > NOW() ORDER BY datetime DESC)
ORDER BY datetime DESC
");
if(!$db->numRows($res_s)) {
	echo '<div>Most épp nincs időzítve semmi...</div>';
}
while($row_s = $db->fetchArray($res_s)) {
	echo '<div><a href="'.$env->base.'/'.$row_s['link'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><b>'.htmlspecialchars($row_s['title'], ENT_QUOTES).'</b></a><br /><span>'.$admin->dateFormatQueue($row_s['datetime']).' &ndash; ';
	if($row_s['category_id'] == 99)
		echo '<a href="'.$env->base.'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban">Fejléc</a>';
	elseif($row_s['category_id'] == 0)
		echo '<a href="'.$env->base.'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban">Címlap</a>';
	else
		echo '<a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row_s['category_id']].'/" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban">'.$env->l['content']['category_'.$row_s['category_id']].'</a>';
	echo '</span></div>';
}*/
?>
</div-->

<!--h4>A szerkesztő figyelmébe</h4>
<div class="main-box">
<div>	<form action="" method="post">
		<input type="text" class="text" name="suggest" value="http://" onclick="Field.activate(this);" /><br />
		<input type="submit" value="Mentés" class="button" />
	</form>
</div>
<div style="height:160px;overflow-y:scroll;overflow-x:hidden;">
<?php
/*
$res_su = $db->Query("SELECT id, name, link, title, datetime FROM "._DBPREF."users_backend_suggest ORDER BY datetime DESC");
while($row_su = $db->fetchArray($res_su)) {
	$rtitle = empty($row_su['title']) ? $row_su['link'] : htmlspecialchars($row_su['title'], ENT_QUOTES);
	echo '
	<div><a href="'.$row_su['link'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><b>'.$rtitle.'</b></a><br />
	<span>'.$admin->dateFormat($row_su['datetime']).' &ndash; '.$row_su['name'].'
	&ndash; <a href="?op=main&amp;remove_suggested='.$row_su['id'].'">Törlés</a></span></div>';

}*/
?>
</div>

</div-->


<?php if($myCategories) { ?>
<h4>Jelentett hozzászólások</h4>
<div class="main-box">
<div style="height:340px;overflow-y:scroll;overflow-x:hidden;">
<?php

$res_su = $db->Query("SELECT name, u.alias, comment_id, category_id, title, CONCAT(c.id,'-',c.alias) AS content_alias, r.datetime FROM "._DBPREF."content_comments_report r
LEFT JOIN "._DBPREF."users u ON r.user_id = u.id
LEFT JOIN "._DBPREF."content c ON r.content_id = c.id
WHERE category_id IN (".implode(',', $myCategories).") ORDER BY r.datetime DESC");
while($row_su = $db->fetchArray($res_su)) {
	echo '
	<div><a href="?op=comments&amp;mode=edit&amp;id='.$row_su['comment_id'].'&amp;category_id='.$row_su['category_id'].'"><b>'.htmlspecialchars($row_su['title'], ENT_QUOTES).'</b></a>


<br />
	<span>'.$admin->dateFormat($row_su['datetime']).' &ndash; <a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row_su['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban">'.$row_su['name'].'</a>
	&ndash; <a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row_su['category_id']].'/'.$row_su['content_alias'].'#c'.$row_su['comment_id'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban">Megnéz</a></span></div>';

}
?>
</div>

</div>
<?php } ?>




