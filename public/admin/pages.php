<?php
defined('ENV') or die;

if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_GOD)) {
	if($_POST['action'] == 'insert') { // űrlapelküldés, kiértékelés új hozzáadáshoz
		$pages = $_POST['pages'];

		if(empty($pages['title'])) { $error = 'Add meg az oldal címét'; }
		if(empty($pages['alias'])) { $error = 'Add meg az oldal alias nevét, amelyen elérhető lesz majd'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$pages['public'] = (empty($pages['public']) ? "0" : "1");
			$pages = $db->escape($pages);

			$res = $db->Query("INSERT
			INTO "._DBPREF."pages (title, alias, body, datetime, modtime, public)
			VALUES ('".$pages['title']."', '".$pages['alias']."', '".$pages['body']."', NOW(), NOW(), '".$pages['public']."')");

			if($res) { // nincs hiba a beszúrásban
				header('Location: '.BACKEND.'/?op=pages&mode=show');
				exit;
			}
		}
	}

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$pages = $_POST['pages'];

		if(empty($pages['title'])) { $error = 'Add meg az oldal címét'; }
		if(empty($pages['alias'])) { $error = 'Add meg az oldal alias nevét, amelyen elérhető lesz majd'; }

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$pages['public'] = (empty($pages['public']) ? "0" : "1");
			$pages = $db->escape($pages);

			$res = $db->Query("UPDATE "._DBPREF."pages SET
			title = '".$pages['title']."',
			alias = '".$pages['alias']."',
			body = '".$pages['body']."',
			modtime = NOW(),
			public = '".$pages['public']."'
			WHERE id = '".(int)$_GET['id']."' LIMIT 1");

			if($res) { // nincs hiba
				header('Location: '.BACKEND.'/?op=pages&mode=show');
				exit;
			}
		}
	}

// már meglévő elem oldal esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$res = $env->db->Query("SELECT * FROM "._DBPREF."pages WHERE id = '".(int)$_GET['id']."' LIMIT 1");
		$pages = $env->db->fetchArray($res);
	}
	else { // új oldal
		$action = 'insert';
		$pages = $_POST['pages'];
	}

?>

	<h1><?= ($action == 'insert') ? 'Új statikus oldal' : 'Statikus oldal szerkesztése'; ?></h1>
	<h2>Add meg a statikus oldal paramétereit, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">
	<input type="hidden" name="action" value="<?= $action; ?>" />

		<fieldset>

			<label for="title">Oldal címe:</label>
			<input name="pages[title]" <?= ($action == 'insert') ? 'onkeyup="getSefURL(this, $(\'alias\'), \'\', \'\');" ' : ''; ?>type="text" id="title" class="text required" maxlength="255" value="<?= htmlspecialchars($pages['title'], ENT_QUOTES); ?>" title="Add meg a statikus oldal címét" /><br />

			<label for="alias">URL alias:</label>
			<input name="pages[alias]" type="text" id="alias" class="text required" maxlength="100" value="<?= $pages['alias']; ?>" title="Add meg az oldal alias-át, amelyen majd elérhető lesz" /><br />

			<label for="pubmail">Publikus:</label>
			<input type="checkbox" id="public" class="radio" name="pages[public]" value="1" <?php echo (($pages['public'] == 1) || !isset($pages['public'])) ? 'checked="checked"' : ''; ?> /><br /><br class="clear" />
			<label for="body">Szöveg:</label>
			<span class="tiny">
				<textarea name="pages[body]" rows="8" style="height: 380px;" id="body"><?= $pages['body']; ?></textarea>
			</span>

		</fieldset>

		<label>&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés"/>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="redirDel(\'pages\', '.$_GET['id'].');" />' : ''; ?>
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>

<?php
}

if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_GOD)) {
?>
	<h1>Statikus oldalak szerkesztése</h1>
	<h2>Válaszd ki egy módosítandó statikus oldalt az alábbi listából.</h2>

	<br class="clear" />

<?php
if($_GET['order'] == 'modtime') { // rendezések
	$order = 'ORDER BY modtime DESC';
	$flag = 1;
}
else {
	$order = "ORDER BY title";
	$flag = 0;
}
?>
	<p id="lead">
		<b>Lista rendezése:</b>
		<?= ($flag == 0) ? 'cím szerint' : '<a href="?op=pages&amp;mode=show">cím szerint</a>'; ?> |
		<?= ($flag == 1) ? 'utolsó módosítás szerint' : '<a href="?op=pages&amp;mode=show&amp;order=modtime">utolsó módosítás szerint</a>'; ?>
	</p>

<?php
// lapozások kiszámítása, ennyi legyen egy oldalon:
$perpage = 30;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
}

$res_all = $env->db->Query("SELECT * FROM "._DBPREF."pages ".$order);
$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

$i = $listfrom + 1; // lista sorszáma
echo '<table cellspacing="0" cellpadding="1" width="100%">';
$res = $env->db->Query("SELECT * FROM "._DBPREF."pages ".$order." LIMIT ".$listfrom.", ".$perpage);
	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Oldal címe</b></td>
		<td valign="top"><b>URL alias</b></td>
		<td valign="top"><b>Állapot</b></td>
		<td valign="top"><b>Utolsó módosítás</b></td>
		<td></td>
	</tr>
	';
while($row = $env->db->fetchArray($res)) {
	$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
	$public = ($row['public'] == 1) ? 'Publikus' : 'Piszkozat';

	echo '
	<tr style="background-color: '.$diffcolor.'">
		<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
		<td valign="top"><a href="?op=pages&amp;mode=edit&amp;id='.$row['id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a></td>
		<td valign="top"><a href="'.$env->base.'/'.$env->l['pages']['url'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">/'.$env->l['pages']['url'].'/'.$row['alias'].'</a></td>
		<td valign="top">'.$public.'</td>
		<td valign="top">'.$admin->dateFormat($row['modtime']).'</td>
		<td valign="top" align="right" nowrap="nowrap">
			<a href="'.$env->base.'/'.$env->l['pages']['url'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
			<a href="javascript:redirDel(\'pages\', '.$row['id'].');" title="Törlés"><img src="images/icon/x.gif" alt="" /></a>
		</td>
	</tr>
	';


$i++;
}
echo '</table>';

echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
	for($j = 1; $j <= $pgs; $j++) { // következő, előző oldalak számainak kiírása
		if($pnow == $j) { // ez az oldal, itt állunk most
			echo ' <b>'.$j.'</b>';
		}
		else { // további oldalak linkkel
			echo ' <a href="?op=pages&amp;mode=show&amp;order='.$_GET['order'].'&amp;page='.$j.'">'.$j.'</a> ';
		}
	}
echo '</p>';
}


if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_GOD)) {

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés
		$res = $db->Query("DELETE FROM "._DBPREF."pages WHERE id = '".$_GET['id']."' LIMIT 1");
		?>
		<h1>Statikus oldal törlése</h1>
		<h2>A kiválasztott statikus oldal törlése megtörtént.</h2>
		<?php
	}
}
?>

