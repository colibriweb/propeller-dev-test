<?php
/**
* Adminisztrátori jogosultságkezelés és azonosítás
* @version 1.0
* @copyright Copyright {@link &copy;} 2007. SWEN INTERNET
* @author Liebig Zsolt <info@swen.hu>
*/

define('ROLE_MODERATOR', 10);
define('ROLE_EDITOR', 20);
define('ROLE_GOD', 30);

class Admin extends Auth {

	/**
	* Adminisztrátori bejelentkezés státusza
	* @var boolean
	*/
	var $admin = false;

	var $categories = array(
		array('id' => 0, 'category' => 'Címlap'),
		array('id' => 1, 'category' => 'Itthon'),
		array('id' => 2, 'category' => 'Nagyvilág'),
		array('id' => 6, 'category' => 'Szórakozás'),
		array('id' => 4, 'category' => 'Technika'),
		array('id' => 7, 'category' => 'Sport'),
	);

	/**
	* Összes elérhető jogosultság
	* @var array
	*/
	var $roles = array(
		'Moderálhatja a hozzászólásokat' => ROLE_MODERATOR,
		'Szerkesztheti az oldalt' => ROLE_EDITOR,
		'Teljes jogosultság' => ROLE_GOD,
/*
		'1_0' => 'Címlap',
		'1_1' => 'Itthon',
		'1_2' => 'Nagyvilág',
//		'1_3' => 'Gazdaság',
		'1_6' => 'Szórakozás',
		'1_4' => 'Technika',
//		'1_5' => 'Kultúra',
		'1_7' => 'Sport',
//		'1_8' => 'Autó',
//		'1_9' => 'Életmód (címlapon)',

		'2_0' => 'Felhasználók kezelése',
		'2_1' => 'Felhasználói jogok kezelése',

		'3_0' => 'Fejléc kiemelések',

		'4_0' => 'Feedek kezelése',
		'4_1' => 'Statikus oldalak kezelése',
		'4_2' => 'Hírlevelek kezelése',

		'5_0' => 'Rendszer',
*/
	);

	/**
	* Felhasználó jogosultsága
	* @var int
	*/
	var $role = 0;

	/**
	* Konstruktor, feltölti az objektumváltozókat, és beállítja a bejelentkezés státuszát a
	* session és/vagy a cookie paraméterei alapján
	*/
	function Admin($env) {

		$this->env = $env;

		$this->isAuthRequired(true);
/*
		if(!$_SESSION['user']) {
			header('HTTP/1.0 404 Not Found');
			header('Location: /statikus/404');
			exit;
		}*/

		$res = $this->env->db->Query("SELECT right_id FROM "._DBPREF."users_backend WHERE user_id = '".$_SESSION['user']['id']."' LIMIT 1");
		if(!$this->env->db->numRows($res)) { // nincs admin joga
			header('HTTP/1.0 404 Not Found');
			header('Location: /statikus/404');
			exit;
		}

		$row = $this->env->db->fetchArray($res);
		$this->role = (int)$row['right_id'];

		$_SESSION['user']['admin'] = 1;
	}

	function hasRight($role = 100) {
		return ($role <= $this->role);
	}

	/**
	 * Lekéri a legalább ROLE_EDITOR státuszú felhasználók email címét az email értesítés számára.
	 */
/*	function getStaffMailsForNotifications() {

		// ezekre az emailcímek nem számítanak szerkesztőnek, "csak van" admin hozzáférésük
		$exceptions = array('zsolt.liebig@swen.hu', 'lajkhu@gmail.com', 'peter.szego@gmail.com');

		$staff = array();
		$res = $this->env->db->Query("
			SELECT u.* FROM "._DBPREF."users_backend b LEFT JOIN "._DBPREF."users u
			ON b.user_id = u.id
			GROUP BY id");
		while($row = $this->env->db->fetchArray($res)) {
			if(!in_array($row['mail'], $exceptions)) {
				$staff[] = $row['mail'];
			}
		}
		return $staff;
	}*/

	function dateFormat($datetime) {

		$time = strtotime($datetime);

		if(date('Y-m-d', $time) == date('Y-m-d', time()+86400))
			return 'holnap, '.date('H:i', $time);
		elseif(date('Y-m-d', $time) == date('Y-m-d', time()-86400))
			return 'tegnap, '.date('H:i', $time);
		elseif(date('Y-m-d', $time) == date('Y-m-d'))
			return 'ma, '.date('H:i', $time);
		else
			return date('Y.m.d. H:i:s', $time);

	}

	function dateFormatQueue($datetime) {

		$time = strtotime($datetime);

		if($time > time()) {
			$diff = $time-time();
			if($diff >= 3600) {
				$h = floor($diff/3600);
				$i = number_format((($diff-($h*3600))/60), 0);
				return $h.'ó '.$i.'p múlva';
			}
			else {
				$i = number_format(($diff/60), 0);
				return $i.'p múlva';
			}
		}
		else {
			$past = time()-$time;
			if($past > 3600) {
				$h = floor($past/3600);
				$i = number_format((($past-($h*3600))/60), 0);
				return $h.'ó '.$i.'p óta';
			}
			else {
				$i = number_format(($past/60), 0);
				return $i.'p óta';
			}
		}

	}

	/**
	* Törli egy művelet után az érintett Smarty cache fájlt
	* @param array $tplFile Cache fájlok
	* @return void
	*/
	function clearCache($tplFiles) {

		/** Rendszerosztály: megjelenítés */
		require(ROOT.'/system/libraries/smarty/Smarty.class.php');
		$smarty = new Smarty();
		$smarty->cache_dir = '../system/cache/smarty_cache_default';

		foreach($tplFiles AS $value) {
			$smarty->clear_cache($value);
		}

		// itt még a mobil verzió template-jeit is törölni kéne
	}

	/**
	* Blogoldal (webcím) formai megfelelőségének ellenőrzése
	* @param string $url Website URL-je
	* @return boolean Helyes formátum esetén true-val, egyébként false-sal tér vissza
	*/
	function checkUrlFormat($url) {

		if(!preg_match("/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i", $url)) {
			return false;
		}
		return true;

	}

}

?>
