<?php
//ob_start('ob_gzhandler');
require('../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);

/* ***************************************************************************** */
switch($_GET['what']) {
/* ***************************************************************************** */
case 'get_title_by_url': // tartalom meta adatainak kiszedése
	if(!$env->isAjax()) { die; }

	require('../system/OpenGraph.class.php');

	$graph = OpenGraph::fetch($_POST['url']);
	$row['title'] = $graph->title;
	$row['description'] = cleanText(str_replace(' Kattintson a részletekért vagy ossza meg!', '', $graph->description));
	$row['link'] = $graph->url;
	$row['image'] = ($graph->image[0] == SHARE_IMAGE) ? '' : $graph->image[0];

	if (! empty($row['title'])) {
		preg_match('/propeller\.(.+)\/(\d+)\-/ui', $_POST['url'], $matches);

		if (isset($matches[2])) {
			$res = $db->Query("SELECT c.title, CONCAT(c.id,'-',c.alias) AS alias, description, excerpt, cat.alias AS category_alias
				FROM "._DBPREF."content c
				LEFT JOIN "._DBPREF."content_categories cat ON c.category_id = cat.id
				WHERE c.id = '".(int)$matches[2]."' LIMIT 1");
			if($db->numRows($res)) { // van ilyen tartalom
				$dbrow = $db->fetchAssoc($res);
				$row['title'] = $dbrow['title'];
				$row['description'] = !empty($dbrow['excerpt']) ? $dbrow['excerpt'] : $env->getExcerpt($env->getSafeText($dbrow['description']));
				$row['link'] = trim($_POST['url']);
			}
			unset($row['category_alias']);
			unset($row['alias']);
		}
	}
	echo json_encode($row);
die;

break;

/* ***************************************************************************** */
case 'get_fullembed_by_id': // videós tartalom embed kódjának lekérdezése
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT title, fullembed FROM "._DBPREF."content c
	LEFT JOIN "._DBPREF."content_videos v ON c.id = v.content_id WHERE c.id = '".(int)$_POST['id']."' LIMIT 1");
	if(!$db->numRows($res)) { // nincs ilyen tartalom
		header('HTTP/1.0 404 Not Found');
		exit;
	}
	$row = $db->fetchAssoc($res);
	echo json_encode($row);

break;

/* ***************************************************************************** */
case 'make_dir': // képtároló könyvtár létrehozása
	if(!$env->isAjax()) { die; }

	if(!in_array($_GET['parent'], array('block'))) { die; }

	$dir = $env->getSefUrl($_POST['value']);
	$dir = str_replace('-', '_', $dir);

	$parent = $env->getSefUrl($_POST['value']);
	$parent = str_replace('-', '_', $parent);

	if(!is_dir(ROOT.'_static/images/'.$_GET['parent'].'/'.$dir)) {
		mkdir(ROOT.'_static/images/'.$_GET['parent'].'/'.$dir, 0755);
		chmod(ROOT.'_static/images/'.$_GET['parent'].'/'.$dir, 0755);
		echo $dir;
	}
	else {
		header('HTTP/1.0 404 Not Found');
		exit;
	}

break;
/* ***************************************************************************** */
case 'get_usernames': // felhasználónevek autocompleter
	if(!$env->isAjax()) { die; }

	if($_POST['type'] == 'authors') {
		$author = $db->escape($_POST['content']['name']);

		$res = $db->Query("SELECT name FROM "._DBPREF."authors WHERE name COLLATE utf8_hungarian_ci LIKE '".$author."%' LIMIT 10");
		if($db->numRows($res)) {
			echo '<ul>'."\n";
			while($row = $db->fetchArray($res)) {
			echo '<li>'.htmlspecialchars($row['name'], ENT_QUOTES).'</li>'."\n";
			}
			echo '</ul>'."\n";
		}

	}
	else {

		if(isset($_POST['content']['name']))
		$name = $db->escape($_POST['content']['name']);
		elseif(isset($_POST['comment']['name']))
		$name = $db->escape($_POST['comment']['name']);
		elseif(isset($_POST['cat']['topusers']))
		$name = $db->escape($_POST['cat']['topusers']);

		$res = $db->Query("SELECT name FROM "._DBPREF."users WHERE name COLLATE utf8_hungarian_ci LIKE '".$name."%' LIMIT 10");
		if($db->numRows($res)) {
			echo '<ul>'."\n";
			while($row = $db->fetchArray($res)) {
			echo '<li>'.$row['name'].'</li>'."\n";
			}
			echo '</ul>'."\n";
		}

	}


break;

case 'tags_autocomplete': // felhasználónevek autocompleter
	if(!$env->isAjax()) { die; }


	$res = $db->Query("SELECT tag FROM "._DBPREF."content_tags WHERE tag LIKE '".$db->escape($_POST['content']['tags'])."%' ORDER BY tag ASC");
	if($db->numRows($res)) {
		echo '<ul>'."\n";
		while($row = $db->fetchArray($res)) {
		echo '<li>'.htmlspecialchars($row['tag'], ENT_QUOTES).'</li>'."\n";
		}
		echo '</ul>'."\n";
	}

break;

case 'get_related': // felhasználónevek autocompleter
	if(!$env->isAjax()) { die; }

	$title = $db->escape($_POST['related']['title'][0]);

	$res = $db->Query("SELECT id, CONCAT(id,'-',alias) AS alias, category_id, title, link, comments, counter, IF(link IS NULL, '1', '2') AS ordd FROM "._DBPREF."content
		WHERE id > ".(_SPD_CONTENT_ID-100000)." AND id <> ".(int)$_GET['not']."
		AND (title COLLATE utf8_hungarian_ci LIKE '%".$title."%' OR title COLLATE utf8_hungarian_ci LIKE '".$title."%' OR title COLLATE utf8_hungarian_ci LIKE '%".$title."')
		AND datetime <= NOW()
		ORDER BY ordd ASC, datetime DESC LIMIT 10");
	if($db->numRows($res)) {
		echo '<ul>'."\n";
		while($row = $db->fetchArray($res)) {
		$link = !empty($row['link']) ? $env->getHost($row['link']) : 'propeller.hu';

		$url = $env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['alias'];
		echo '<li id="'.$row['id'].'" rel="'.$url.'">'.preg_replace('/'.preg_quote(htmlspecialchars($title, ENT_QUOTES),'/').'/i', "<b>\\0</b>", $row['title']).'<span class="informal">'.$row['comments'].' hozzászólás | '.$row['counter'].' megtekintés | '.$link.' </span></li>'."\n";
		}
		echo '</ul>'."\n";
	}

break;

/* ***************************************************************************** */
case 'get_tags_by_username': // felhasználó címkéinek autocompleter
	if(!$env->isAjax()) { die; }

	$res = $db->Query("SELECT tag FROM "._DBPREF."users u
	LEFT JOIN "._DBPREF."content c ON u.id = c.user_id
	LEFT JOIN "._DBPREF."content_tags_conn cn ON c.id = cn.content_id
	LEFT JOIN "._DBPREF."content_tags ct ON cn.tag_id = ct.id
	WHERE u.name = '".$db->escape($_GET['name'])."' AND tag LIKE '".$db->escape($_POST['content']['tag'])."%' LIMIT 10");
	if($db->numRows($res)) {
		$row = $db->fetchArray($res);
		echo '<ul>'."\n";
		echo '<li>'.htmlspecialchars($row['tag'], ENT_QUOTES).'</li>'."\n";
		echo '<ul>'."\n";
	}

break;

/* ***************************************************************************** */
case 'save_block_positions': // dobozok sorrendjének elmentése (címlap)
	if(!$env->isAjax()) { die; }

	foreach($_POST as $block_column => $value) {
		foreach($value as $block_position => $block_id) {
			$db->Query("UPDATE "._DBPREF."content_blocks SET block_column = '".$db->escape($block_column)."', block_position = '".((int)$block_position+1)."' WHERE block_id = '".(int)$block_id."' AND ordinal = '0' LIMIT 1");
		}
	}
	$env->dropFileCache('blocks.cache');

echo 'OK';


break;

/* ***************************************************************************** */
default:

	$env->setHeader(404);

}

?>
