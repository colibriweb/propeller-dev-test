<?php
defined('ENV') or die;

if(($_GET['mode'] == 'feedreport') && $admin->hasRight(ROLE_GOD)) {
	?>
	<h1>Feed napló</h1>
	<h2>Az alábbi lista a feedek feldolgozási gyakoriságát és eredményességét mutatja.</h2>

	<br class="clear" />

	<?php
// lapozások kiszámítása, ennyi legyen egy oldalon:
$perpage = 30;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
}

$res_all = $env->db->Query("SELECT datetime, microtime, feed_num, item_num FROM "._DBPREF."content_feeds_log");
$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

$i = $listfrom + 1; // lista sorszáma

echo '<table cellspacing="0" cellpadding="1" width="100%">';
	$res = $env->db->Query("SELECT datetime, microtime, feed_num, item_num FROM "._DBPREF."content_feeds_log ORDER BY datetime DESC LIMIT ".$listfrom.", ".$perpage);
	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Dátum</b></td>
		<td valign="top"><b>Időköz</b></td>
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Microtime</b></td>
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Feed</b></td>
		<td valign="top"><b>Item</b></td>
		<td valign="top"><b>Feed/Item</b></td>
		<td valign="top"><b>Item/Feed</b></td>
	</tr>
	';
$prev = 0;
	while($row = $env->db->fetchArray($res)) {
		$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));
if($prev == 0)
$datediff = 0;
else
$datediff = number_format(($prev-strtotime($row['datetime']))/60, 0);

$prev = strtotime($row['datetime']);

		$fetchratio = number_format($row['feed_num']/$row['microtime'], 1);
		$feedratio = ($row['feed_num'] == 0) ? '-' : number_format($row['item_num']/$row['feed_num'], 1);

		$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
		echo '
		<tr style="background-color: '.$diffcolor.'">
			<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
			<td valign="top">'.$datetime.'</td>
			<td valign="top">'.$datediff.'</td>
			<td><img src="images/stat.gif" width="'.(($datediff+1)*2).'" height="12" alt="" /></td>
			<td valign="top">'.number_format($row['microtime'], 4).'</td>
			<td><img src="images/stat.gif" width="'.number_format(($row['microtime']+1)*5, 0).'" height="12" alt="" /></td>
			<td valign="top">'.$row['feed_num'].'</td>
			<td valign="top">'.$row['item_num'].'</td>
			<td valign="top">'.$fetchratio.'</td>
			<td valign="top">'.$feedratio.'</td>
		</tr>';
	$i++;
	}
	echo '</table>';

	echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás
		for($j = 1; $j <= $pgs; $j++) { // következő, előző oldalak számainak kiírása
			if($pnow == $j) { // ez az oldal, itt állunk most
				echo ' <b>'.$j.'</b>';
			}
			else { // további oldalak linkkel
				echo ' <a href="?op=system&amp;mode=feedreport&amp;page='.$j.'">'.$j.'</a> ';
			}
		}
	echo '</p>';

}


if(($_GET['mode'] == 'feedcheck') && $admin->hasRight(ROLE_GOD)) {
	?>
	<h1>Feed ellenőrzés</h1>
	<h2>Az alábbi eszköz feldolgoz egy megadott feedet és megmutatja a tartalmát.</h2>

	<br class="clear" />

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post">

		<fieldset>

			<label for="url">Feed URL-je:</label>
			<input type="text" id="url" class="text required" name="url" maxlength="255" value="<?= htmlspecialchars($_POST['url'], ENT_QUOTES); ?>" title="Add meg a feed elérhetőségét" /><br />

			<label for="feed_schema">Feldolgozó séma:</label>
			<input type="text" id="feed_schema" class="text required" name="feed_schema" value="<?= ($_POST['feed_schema']) ? htmlspecialchars($_POST['feed_schema'], ENT_QUOTES) : 'rss'; ?>" title="Add meg a feed feldolgozó sémáját" /><br />

		</fieldset>

		<label for="submit">&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Ellenőrzés" />
		<input type="reset" id="reset" class="button" value="Mezők törlése" />
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>

	<?php

	if(!empty($_POST['url']) && !empty($_POST['feed_schema'])) {

	/** Rendszerosztály: XML feed források feldolgozása */
		require('../system/cron/FeedCron.class.php');

		$feed = new FeedCron($_POST['url']);
		$feed->env = $env;
		$feed->schema = $_POST['feed_schema'];

		if($items = $feed->getFetched()) {
			$bc = $feed->getFileContent($_POST['url']);

			echo '<textarea rows="10" cols="84">'.htmlspecialchars($bc, ENT_QUOTES).'</textarea>';

			echo '<pre>';
			print_r($items);
			echo '</pre>';
		}
		else {
			echo 'Nem tudtam feldolgozni a feedet...';
		}
	}



}

if(($_GET['mode'] == 'feeds')) {
	?>
	<h1>Feedek listája</h1>
	<h2>Az alábbi lista megmutatja a rendszerbe bekötött RSS feedek állapotát.</h2>

	<br class="clear" />

	<p class="lead"></p>

	<?php


$i = 1; // lista sorszáma
echo '<table cellspacing="0" cellpadding="1" width="100%">';
$res = $env->db->Query("SELECT f.id, category_id, title, feed, modtime, fetchrank, category, alias, link, public, video_embed FROM "._DBPREF."content_feeds f
LEFT JOIN "._DBPREF."content_categories c ON f.category_id = c.id ORDER BY public DESC, f.title ASC");

$fetchranks = array('1' => '10', '2' => '30', '3' => '90');

	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Elnevezés</b></td>
		<td valign="top"><b>Kategória</b></td>
		<td valign="top"><b>Utoljára frissült</b></td>
		<td valign="top"><b>Státusz</b></td>
		<td valign="top"><b>Egyebek</b></td>
		<td>&nbsp;</td>
	</tr>
	';
	while($row = $env->db->fetchArray($res)) {
		$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';

//		$res_c = $env->db->Query("SELECT count(id) AS content_num FROM "._DBPREF."content WHERE feed_id = '".$row['id']."'");
//		$row_c = $env->db->fetchArray($res_c);

		$host = preg_replace('/^(http:\/\/)?(www.)?([^\/\?]+)([\/|\?])?(.+)?$/', '$3', $row['feed']);
		$ago = number_format((time()-strtotime($row['modtime']))/60, 0, '.', '');
		$status = ($row['public'] == 1) ? 'aktív' : 'kikapcsolva';
		$others = (!empty($row['video_embed'])) ? 'videós feed' : '-';

		echo '
		<tr style="background-color: '.$diffcolor.'">
			<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
			<td valign="top" nowrap><a href="'.$row['link'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.$row['title'].'</a></td>
			<td valign="top"><a href="'.$env->base.'/'.$row['alias'].'/" onclick="window.open(this.href, \'_blank\'); return false;">'.$row['category'].'</a></td>
			<td valign="top">'.$ago.' perce ('.$admin->dateFormat($row['modtime']).')</td>
			<td valign="top">'.$status.'</td>
			<td valign="top">'.$others.'</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$row['feed'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				<a href="'.$env->base.'/kereses?q='.$host.'" onclick="window.open(this.href, \'_blank\'); return false;" title="Tartalmak keresése ebből a feedből"><img src="images/icon/search.gif" alt="" /></a>
			</td>
		</tr>
		';
	$i++;
	}
	echo '</table>';



}

if(($_GET['mode'] == 'stats')) {
	?>
	<h1>Statisztikák</h1>
	<h2>Az alábbi grafikonok néhány fontosabb adat alakulását mutatják.</h2>

	<br class="clear" />

	<p class="lead"></p>

	<a href="?op=system&mode=stats&id=1">Hozzászólások</a> |
	<!--a href="?op=system&mode=stats&id=2">Hozzászóló felhasználók</a> |-->
	<a href="?op=system&mode=stats&id=3">Regisztrációk</a> |
	<!--a href="?op=system&mode=stats&id=4">Tartalmak száma</a-->
	<a href="?op=system&mode=stats&id=5">Beküldött hírek</a> |
	<a href="?op=system&mode=stats&id=6">Privát üzenetek</a> |
	<a href="?op=system&mode=stats&id=7">Lájkolások</a> |
	<a href="?op=system&mode=stats&id=8">Aktivitások</a> |
	<a href="?op=system&mode=stats&id=9">Saját tartalmak statisztikája</a> |
	<a href="?op=system&mode=stats&id=10">Címke statisztika</a><br />



<?php
if(isset($_GET['id']) && $_GET['id'] == 1) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, date_format(datetime, '%Y. %b') AS dmon, count(id) AS num
FROM "._DBPREF."content_comments GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Hozzászólások száma (hozzászólás/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=
0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />

<?php
unset($dates);
unset($values);
}

if(isset($_GET['id']) && $_GET['id'] == 2) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, date_format(datetime, '%Y. %b') AS dmon, count(distinct user_id) AS num
FROM "._DBPREF."content_comments GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Hozzászóló felhasználók száma (felhasználó/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=
0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />


<?php
unset($dates);
unset($values);
}

if(isset($_GET['id']) && $_GET['id'] == 3) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, count(id) AS num
FROM "._DBPREF."users GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Regisztrációk száma (regisztráció/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />


<?php

unset($dates);
unset($values);
}

if(isset($_GET['id']) && $_GET['id'] == 4) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, date_format(datetime, '%Y. %b') AS dmon, count(id) AS num
FROM "._DBPREF."content GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['dmon'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Tartalmak száma (tartalom/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />



<?php
unset($dates);
unset($values);
}

if(isset($_GET['id']) && $_GET['id'] == 5) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, date_format(datetime, '%Y. %b') AS dmon, count(id) AS num
FROM "._DBPREF."content_temp WHERE user_id IS NOT NULL GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Beküldött tartalmak számának alakulása (beküldés/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />


<?php


unset($dates);
unset($values);
}

if(isset($_GET['id']) && $_GET['id'] == 6) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, count(id) AS num
FROM "._DBPREF."users_messages GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Privát üzenetek száma (üzenet/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />


<?php
}

if(isset($_GET['id']) && $_GET['id'] == 7) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, count(pwid) AS num
FROM "._DBPREF."content_comments_likes GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Hozzászólásokra érkezett lájkolások száma (lájk/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />


<?php
}

if(isset($_GET['id']) && $_GET['id'] == 8) {
$res = $env->db->Query("SELECT date_format(datetime, '%Y-%m-%d') AS datet, count(id) AS num
FROM "._DBPREF."users_activities GROUP BY datet ORDER BY datet DESC LIMIT 0, 180");
while($row = $env->db->fetchArray($res)) {
	$dates[] = $row['datet'];
	$values[] = $row['num'];
}
$dates = array_reverse($dates);
$values = array_reverse($values);
?>


<p class="lead" style="padding-top:24px;"><b>Aktivitások - lájk, hozzászólás, privát üzenet - száma (értesítés/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $values); ?>
&chds=0,<?= max($values); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=0:|<?= $dates[0].'|'.$dates[ceil(count($dates)/4)].'|'.$dates[count($dates)/2].'|'.$dates[ceil(count($dates)/1.333333)].'|'.$dates[count($dates)-1]; ?>|
1:|0|<?= round(max($values)/4); ?>|<?= round(max($values)/2); ?>|<?= round(max($values)/1.333333); ?>|<?= max($values); ?>
" />


<?php
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(isset($_GET['id']) && $_GET['id'] == 9) {

	if (isset($_GET['date']) && isset($_GET['user_id'])) {
		$res = $env->db->Query("SELECT name FROM "._DBPREF."users WHERE id = ".$db->escape($_GET['user_id'])." LIMIT 1");
		$row = $env->db->fetchRow($res);

		?>
		<p class="lead" style="padding-top:24px;"><b><?php echo htmlentities($row[0]) ?> hírei <?php echo htmlentities($_GET['date']) ?> napon (Sz=olvasottság, H=hozzászólás, FB=Facebook):</b></p>

		<?php
		$res = $env->db->Query("SELECT c.id, c.category_id, c.link, c.title, CONCAT(c.id,'-',c.alias) AS content_alias, counter, comments, c.datetime, u.name, u.alias
			FROM "._DBPREF."users_backend_editorlog sz
		LEFT JOIN "._DBPREF."content c ON sz.content_id = c.id
		LEFT JOIN "._DBPREF."users u ON sz.user_id = u.id
		WHERE date_format(sz.datetime, '%Y-%m-%d') = '".$db->escape($_GET['date'])."'
		AND sz.user_id = ".$db->escape($_GET['user_id'])."
		ORDER BY sz.datetime ASC");
		$i = 1; // lista sorszáma
		echo '<table cellspacing="0" cellpadding="1" width="100%">';
			echo '
			<tr class="head">
				<td valign="top">&nbsp;</td>
				<td valign="top"><b>Cím</b></td>
				<td valign="top"><b>Szerkesztő</b></td>
				<td valign="top" align="right"><b>Sz.</b></td>
				<td valign="top" align="right"><b>H.</b></td>
				<td valign="top" align="right"><b>FB.</b></td>
				<td valign="top"><b>Dátum</b></td>
				<td>&nbsp;</td>
			</tr>
			';
        $counter_sum = 0;
        $comments_sum = 0;
		while($row = $env->db->fetchArray($res)) {
			$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
	//		$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));

			echo '
			<tr style="background-color: '.$diffcolor.'">
				<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
				<td valign="top"><a href="?op=content&amp;mode=edit&amp;id='.$row['id'].'&amp;category_id='.$row['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a></td>
				<td valign="top" nowrap="nowrap">';
				echo '<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlentities($row['name'], ENT_QUOTES).'</a>';

				echo '</td>
				<td valign="top"  align="right" nowrap="nowrap">'.number_format($row['counter'], 0, null, ' ').'</td>
				<td valign="top" align="right">'.$row['comments'].'</td>
				<td valign="top" class="shares-count" align="right" nowrap="nowrap" data-url="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['content_alias'].'">0</td>
				<td valign="top" nowrap="nowrap">'.$admin->dateFormat($row['datetime']).'</td>
				<td valign="top" align="right" nowrap="nowrap">
					<a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['content_alias'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
				</td>
			</tr>
			';
		$i++;
        $counter_sum += $row['counter'];
        $comments_sum += $row['comments'];
		}


echo '<tr bgcolor="#eeeeee"><td colspan="3" align="center">Összesen:</td>

        <td valign="top" align="right" nowrap="nowrap">'.number_format($counter_sum, 0, null, ' ').'</td>
        <td valign="top" align="right">'.$comments_sum.'</td>
        <td valign="top" id="shares-count-sum" nowrap="nowrap" align="right">0</td>
        <td valign="top" colspan="2"></td>
</tr>';

		echo '</table>';

	}
	else {
?>





<form method="get" action="/admin/?op=system&mode=stats&id=9" style="display:inline-block;">
<input type="hidden" name="op" value="system">
<input type="hidden" name="mode" value="stats">
<input type="hidden" name="id" value="9">
<p class="lead" style="padding-top:24px;"><b>Saját tartalmak statisztikája (hír/nap/szerkesztő) ebben a hónapban:</b>
<select name="month">
<?php
for ($i=0; $i<=12; $i++) {
	echo '<option value="'.date('Y-m', strtotime("first day of -$i month")).'"';

	if ($_GET['month'] == date('Y-m', strtotime("first day of -$i month"))) echo ' selected="selected"';
	echo '>'.date('Y. m.', strtotime("first day of -$i month")).' hónap</option>'."\n";
}

?>
</select>
<input type="submit" value="Mehet"></p>
</form>

<?php

	if (isset($_GET['month']) && ($_GET['month'] != date('Y-m'))) {
		$month = $_GET['month']; // '2016-04';

		$dstring_m = "'".$month."'";
		$dstring_php = $month."-";
		$today = date("t", strtotime($month));
	}
	else {
		$dstring_m = "date_format(now(), '%Y-%m')";
		$dstring_php = date('Y-m-');
		$today = date('j');
	}





	$editors = array();
	$res_editors = $env->db->Query("SELECT user_id, name FROM "._DBPREF."users_backend_editorlog sz LEFT JOIN
	"._DBPREF."users u ON sz.user_id = u.id
	WHERE date_format(sz.datetime, '%Y-%m') = ".$dstring_m."
	GROUP BY user_id ORDER BY name");
	while($row_editors = $env->db->fetchArray($res_editors)) {
		$editors[] = array($row_editors['user_id'], $row_editors['name']);
	}


	$dat = array();
	$res_dat = $env->db->Query("SELECT date_format(sz.datetime, '%Y-%m-%d') AS nap, sz.user_id, count(sz.id) AS cikkszam, sum(counter) AS counterszam
	FROM "._DBPREF."users_backend_editorlog sz
	LEFT JOIN "._DBPREF."content c ON sz.content_id = c.id
	WHERE date_format(sz.datetime, '%Y-%m') = ".$dstring_m."
	GROUP BY nap, sz.user_id");
	while($row_dat = $env->db->fetchArray($res_dat)) {
		$dat[$row_dat['user_id']][$row_dat['nap']]['cikkszam'] = $row_dat['cikkszam'];
		$dat[$row_dat['user_id']][$row_dat['nap']]['counterszam'] = $row_dat['counterszam'];
		@$dat[$row_dat['user_id']]['all_cikk'] = (int)$dat[(int)@$row_dat['user_id']]['all_cikk'] + (int)$row_dat['cikkszam'];
		@$dat[$row_dat['user_id']]['all_counter'] = (int)$dat[(int)@$row_dat['user_id']]['all_counter'] + (int)$row_dat['counterszam'];


/*
		$res_co = $env->db->Query("SELECT SELECT sum(counter) FROM "._DBPREF."content WHERE id IN ()");
		while($row_co = $env->db->fetchArray($res_co)) {

		$dat[$row_dat['user_id']][$row_dat['nap']]['counterszam'] = $row_co['counterszam'];
		}
*/

	}


echo '<table border="0" width="100%">';
echo '<tr bgcolor="#eeeeee"><td></td>';
foreach($editors AS $key => $value) {
echo '<td colspan="2" align="center">'.$value[1].'</td>';
}
echo '<td colspan="2" align="center">Összesen:</td>';
echo '</tr>';
echo '<tr><td></td>';
foreach($editors AS $key => $value) {
echo '<td width="30" align="center" bgcolor="#eeeeee">Hír</td>';
echo '<td width="70" align="center">Olvasás</td>';
}

echo '<td width="30" align="center" bgcolor="#eeeeee">Hír</td>';
echo '<td width="70" align="center">Olvasás</td>';
echo '</tr>';

			$editorcikkgraph = array_fill(0, date('t', strtotime($dstring_php.'1')), 0);
			$daygraph = [];
			for($i=1; $i<=date('t', strtotime($dstring_php.'1')); $i++) {
				$day = $dstring_php.sprintf("%02d", $i);
				$daygraph[$i-1] = $day;
			}

			$editorcikk_all = 0;
			for($i=1; $i<=$today; $i++) {
			echo '<tr>';
				$day = $dstring_php.sprintf("%02d", $i);
				$daygraph[$i-1] = $day;

				echo '<td width="70" align="center">'.$day.'</td>';
				$editorcikk = 0;
				$editorcounter = 0;
				foreach($editors AS $key => $value) {
					$editorcikk += (int)@$dat[@$value[0]][$day]['cikkszam'];
					$editorcounter += (int)@$dat[@$value[0]][$day]['counterszam'];


echo '<td width="30" align="right" bgcolor="#eeeeee">'.((int)$dat[@$value[0]][$day]['cikkszam'] == 0 ? '-' : '<a href="/admin/?op=system&mode=stats&id=9&date='.$day.'&user_id='.$value[0].'">'.$dat[@$value[0]][$day]['cikkszam'].'</a>').'</td>';
echo '<td width="70" align="right">'.((int)$dat[@$value[0]][$day]['counterszam'] == 0 ? '-' : number_format($dat[@$value[0]][$day]['counterszam'], 0, null, ' ')).'</td>';


				}
				$editorcikkgraph[$i-1] = $editorcikk;
				$editorcikk_all += (int)$editorcikk;

				echo '<td width="30" align="right" bgcolor="#eeeeee">'.(int)$editorcikk.'</td>';
				echo '<td width="70" align="left">'.number_format((int)$editorcounter, 0, null, ' ').'</td>';


			echo '</tr>';
			}


echo '<tr bgcolor="#eeeeee"><td align="center">Összesen:</td>';
foreach($editors AS $key => $value) {
echo '<td width="30" align="right">'.(int)$dat[$value[0]]['all_cikk'].'</td>';
echo '<td width="70" align="right">'.number_format((int)$dat[$value[0]]['all_counter'], 0, null, ' ').'</td>';
}
echo '<td width="30" align="right">'.$editorcikk_all.'</td>';
echo '<td width="70" align="left"> </td>';
echo '</tr>';

echo '</table>';


?>

<p class="lead" style="padding-top:24px;"><b>Saját tartalmak száma (hír/nap):</b></p>
<img src="
http://chart.apis.google.com/chart
?cht=bvg
&chbh=a
&chco=0077CC
&chd=t:<?= implode(',', $editorcikkgraph); ?>
&chds=0,<?= max($editorcikkgraph); ?>
&chs=690x300
&chxt=x,y
&chxp=0,0,25,50,75,100
&chxl=
0:|<?= $daygraph[0].'|'.$daygraph[ceil(count($daygraph)/4)].'|'.$daygraph[count($daygraph)/2].'|'.$daygraph[ceil(count($daygraph)/1.333333)].'|'.$daygraph[count($daygraph)-1]; ?>|
1:|0|<?= round(max($editorcikkgraph)/4); ?>|<?= round(max($editorcikkgraph)/2); ?>|<?= round(max($editorcikkgraph)/1.333333); ?>|<?= max($editorcikkgraph); ?>
" />

	<p class="lead" style="padding-top:24px;"><b>Top hírek ebben a hónapban (Sz=olvasottság, H=hozzászólás, FB=Facebook):</b></p>

<?php

	$res = $env->db->Query("SELECT c.id, c.category_id, c.link, c.title, CONCAT(c.id,'-',c.alias) AS content_alias, counter, comments, c.datetime, u.name, u.alias
		FROM "._DBPREF."users_backend_editorlog sz
	LEFT JOIN "._DBPREF."content c ON sz.content_id = c.id
	LEFT JOIN "._DBPREF."users u ON sz.user_id = u.id
	WHERE date_format(sz.datetime, '%Y-%m') = ".$dstring_m."
	ORDER BY c.counter DESC LIMIT 0, 20");
	$i = 1; // lista sorszáma
	echo '<table cellspacing="0" cellpadding="1" width="100%">';
		echo '
		<tr class="head">
			<td valign="top">&nbsp;</td>
			<td valign="top"><b>Cím</b></td>
			<td valign="top"><b>Szerkesztő</b></td>
			<td valign="top" align="right"><b>Sz.</b></td>
			<td valign="top" align="right"><b>H.</b></td>
			<td valign="top" align="right"><b>FB.</b></td>
			<td valign="top"><b>Dátum</b></td>
			<td>&nbsp;</td>
		</tr>
		';
	while($row = $env->db->fetchArray($res)) {
		$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
//		$datetime = date("Y.m.d. H:i:s", strtotime($row['datetime']));

		echo '
		<tr style="background-color: '.$diffcolor.'">
			<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
			<td valign="top"><a href="?op=content&amp;mode=edit&amp;id='.$row['id'].'&amp;category_id='.$row['category_id'].'">'.htmlspecialchars($row['title'], ENT_QUOTES).'</a></td>
			<td valign="top" nowrap="nowrap">';
			echo '<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;">'.htmlentities($row['name'], ENT_QUOTES).'</a>';

			echo '</td>
			<td valign="top" align="right" nowrap="nowrap">'.number_format($row['counter'], 0, null, ' ').'</td>
			<td valign="top" align="right">'.$row['comments'].'</td>
			<td valign="top" class="shares-count"  align="right" nowrap="nowrap" data-url="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['content_alias'].'">0</td>
			<td valign="top" nowrap="nowrap">'.$admin->dateFormat($row['datetime']).'</td>
			<td valign="top" align="right" nowrap="nowrap">
				<a href="'.$env->base.'/'.$env->l['content']['category_alias_'.$row['category_id']].'/'.$row['content_alias'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
			</td>
		</tr>
		';
	$i++;
	}
	echo '</table>';

	}

	?>
	<script type="text/javascript">
    function myNumberFormat(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

	jQuery(function() {
		// share számok
		var get_fb_container = [];
		jQuery('.shares-count').each(function(key, value) {
			var url = jQuery(this).data('url');

			console.log(url);
			get_fb_container.push(url);
		});

		if (get_fb_container.length) {
            var shares_count_sum = 0;

			jQuery.ajax({
			    url: 'https://graph.facebook.com/?ids=' + get_fb_container.join(',') + '&callback=?',
			    dataType: "json",
			    success: function(data) {
					jQuery.each(data, function(key, value) {
						var url = value.id;
						var count = value.share.share_count;
						var elm = jQuery(".shares-count[data-url='" +url+ "']");

						jQuery(elm).html(myNumberFormat(parseInt(count)));
                        shares_count_sum += parseInt(count);
						//console.log(key + ' > ' + count);
					});

                    if(jQuery('#shares-count-sum').length) {
                        jQuery('#shares-count-sum').html(myNumberFormat(shares_count_sum));
                    }
			    }
			});
		}
	});
	</script>
	<?php
}


if(isset($_GET['id']) && $_GET['id'] == 10) {

?>



<form method="get" action="/admin/?op=system&mode=stats&id=10" style="display:inline-block;">
<input type="hidden" name="op" value="system">
<input type="hidden" name="mode" value="stats">
<input type="hidden" name="id" value="10">
<p class="lead" style="padding-top:24px;"><b>A leggyakrabban használt címkék az elmúlt 30 napban:</b>
<select name="stat_category_id">
<?php
foreach($admin->categories AS $category) {
 if($category['id'] == 0) $category['category'] = 'Minden rovat';
	echo '<option value="'.$category['id'].'"';
	if ($_GET['stat_category_id'] == $category['id']) {
		echo ' selected="selected"';
		$category_name = $category['category'];
	}
	echo '>'.$category['category'].'</option>'."\n";
}
$category_id = isset($_GET['stat_category_id']) ? (int)$_GET['stat_category_id'] : 0;
?>
</select>
<input type="submit" value="Mehet"></p>
</form>

<p><a href="/admin/?op=system&mode=stats&id=10&stat_category_id=<?php echo $category_id ?>&export=csv">Lista exportálása CSV-be</a></p>
<br>
<?php
	require(ROOT.'/modules/content/Content.class.php');
	$content = new Content();
	$content->env = $env;

	$tags = $content->q_tagcloud($category_id);
	$new = array();
	foreach ($tags as $key => $row) {
	    $new[$key] = $row['num'];
	}
	array_multisort($new, SORT_DESC, $tags);

	if(isset($_GET['export']) && $_GET['export'] == 'csv') {
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=propeller_cimkek_'.$env->getSefUrl($category_name).'_'.date('Y-m-d').'.csv');

		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		ob_end_clean();
		ob_start();
		$output = fopen('php://output', 'w');
		fputcsv($output, array('Hír', 'Címke'));

		foreach ($tags as $key => $row) {
			fputcsv($output, array($row['num'], $row['tag']));
		}
   		fclose($output);
		echo ob_get_clean();
		die;
	}

	echo '<table border="0">';
	echo '<tr>';
		echo '<td>&nbsp;</td>';
		echo '<td width="30" align="center" bgcolor="#eeeeee">Hír</td>';
		echo '<td width="70" align="left">Címke</td>';
	echo '</tr>';
	foreach($tags AS $key => $tag) {
		echo '<tr>';
		echo '<td>'.($key+1).'.</td>';
		echo '<td width="30" align="center" bgcolor="#eeeeee">'.$tag['num'].'</td>';
		echo '<td align="left" nowrap><a href="/tag/'.urlencode($tag['tag']).'" target="_blank">'.$tag['tag'].'</a></td>';
		echo '</tr>';
	}

	echo '<table>';
}

}



?>
