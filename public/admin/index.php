<?php
ob_start('ob_gzhandler');
require('../system/MySQL.class.php');
$db = new Database(_DBHOST, _DBPORT, _DBUSER, _DBPASS, _DBNAME, false);

require(ROOT.'/system/Environment.class.php');
$env = new Environment();
$env->db = $db;

require(ROOT.'/modules/users/Auth.class.php');
$auth = new Auth($env);



/* ***************************************************************************** */


$op = empty($_GET['op']) ? 'main' : $_GET['op'];

/** Adminisztrátori jogosultságkezelés és azonosítás */
require('Admin.class.php');
$admin = new Admin($env);

/** Az adminisztrációs felület könyvtára */
define('BACKEND', '/admin');

error_reporting(E_ALL & ~E_NOTICE);
error_reporting(0);

function active($str) {
	global $_GET;
	$mode = $_GET['mode'];

	if (isset($_GET['id'])) {
		$mode = 'show';
	}
	return ($str == $_GET['op'].'-'.$mode) ? ' class="active"' : '';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="hu" lang="hu">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>Admin – Propeller</title>
<script type="/colibrimedia/prop/public/admin/text/javascript" src="js/prototype/prototype.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?= STTC; ?>/js/jquery-1.9.0.min.js"><\/script>')</script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<script src="/colibrimedia/prop/public/admin/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
	selector: "textarea:not(.mceNoEditor)",
	width: 616,
	theme: "modern",
	menubar:false,
	plugins: [
		"advlist autolink link image hr anchor pagebreak",
		"visualblocks visualchars code media noneditable nonbreaking",
		"save template paste textcolor autoresize responsivefilemanager"
	],
	paste_as_text: true,
	paste_auto_cleanup_on_paste : true,
	paste_remove_styles: true,
	paste_remove_styles_if_webkit: true,
	paste_strip_class_attributes: true,
	paste_postprocess : function(pl, o) { // remove extra spaces
		o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
		o.node.innerHTML = o.node.innerHTML.replace(/<br.*?\>/ig, "<\/p><p>");
	},
	browser_spellcheck : true,

	content_css: "admin.css?v=5",
	toolbar: "blockquote bold italic fontsizeselect | responsivefilemanager | link unlink | code removeformat",
  	fontsize_formats: "12px 15px",


	autoresize_min_height: 100,
	autoresize_max_height: 400,
	relative_urls: false,
	convert_urls: true,
	remove_script_host: false,

	image_advtab: false,
	external_filemanager_path:"/admin/js/filemanager/",
	filemanager_title:"Kép kiválasztása",
	filemanager_sort_by: 'date',
	filemanager_descending: 'true',
	external_plugins: { "filemanager" : "/admin/js/filemanager/plugin.min.js"},

	entity_encoding : "raw",
	force_p_newlines : true,
	//invalid_elements : "meta,link,xml,font", // span -t kivettem a fontsizeselect miatt,
	valid_elements: '*[*]',
	language: "hu_HU",
 });
</script>

<base href="<?= $env->base.BACKEND; ?>/" />
<link rel="shortcut icon" href="/colibrimedia/prop/public/admin/images/favicon.ico" />
<link type="text/css" href="/colibrimedia/prop/public/admin/js/validation/validation.css" rel="stylesheet" />
<link type="text/css" href="/colibrimedia/prop/public/admin/js/scriptaculous/autocomplete.css" rel="stylesheet" />
<link type="text/css" href="/colibrimedia/prop/public/admin/admin.css?v=5" rel="stylesheet" />
<script type="text/javascript" src="/colibrimedia/prop/public/admin/js/scriptaculous/scriptaculous.bundle.min.js"></script>
<script type="text/javascript" src="/colibrimedia/prop/public/admin/js/validation/validation.js"></script>
<!-- [END] libraries -->
<script type="text/javascript">// <![CDATA[
var backend = '<?= $env->base.BACKEND; ?>';
var sttc = '<?= STTC; ?>';
// ]]>
</script>
<script type="text/javascript" src="/colibrimedia/prop/public/admin/js/admin.js?v=305"></script>

</head>

<body id="main">

<div id="wrapper">

<table cellspacing="0" cellpadding="0" width="100%">
<tr><td id="horizontal-menu" colspan="3">

	<a id="logo" href="?op=main"><img src="<?= STTC ?>/images/default/logo.png" width="162" alt="" /></a>

	<div id="category-list">

	<?php if(isset($_GET['category_id']) || $op == 'main') { ?>
	<ul>
	<?php
	$myCategories = array();
	foreach($admin->categories AS $category) {
		$active = ($_GET['category_id'] == $category['id']) ? ' class="active"' : '';

		echo '<li><a href="?op='.$_GET['op'].'&amp;mode='.$_GET['mode'].'&amp;category_id='.$category['id'].'&amp;content_type='.htmlentities(@$_GET['content_type'], ENT_QUOTES).'&amp;order='.htmlentities(@$_GET['order'], ENT_QUOTES).'"'.$active.'>'.$category['category'].'</a></li>'."\n";

		if($category['id'] != 0)
		$myCategories[] = $category['id'];
	}
	?>
	</ul>
	<?php } ?>
	</div>

	<div id="logout">
		<a href="<?= $env->base; ?>" onclick="window.open(this.href, '_blank'); return false;"><b>Vissza a címlapra</b></a> | <a
		href="<?= $env->base.'/'.$env->l['users']['url_logout']; ?>" onclick="return confirm('Biztos vagy benne, hogy kijelentkezel?');">Kilépés</a>
	</div>

</td></tr>
<tr><td id="vertical-menu" nowrap="nowrap">


<?php
	$cidStr = !isset($_GET['category_id']) ? $admin->categories[0]['id'] : (int)$_GET['category_id'];


	if ($admin->hasRight(ROLE_EDITOR)) {

		echo '<div>Szerkesztés</div>';
		echo '<a href="?op=blocks&amp;mode=edit&amp;category_id='.$cidStr.'"'.active('blocks-edit').' style="background-image:url(images/icon/edit.gif);">Új kiemelés</a>';
		echo '<a href="?op=blocks&amp;mode=show&amp;category_id='.$cidStr.'"'.active('blocks-show').' style="background-image:url(images/icon/document.gif);">Kiemelések szerkesztése</a>';
		echo '<a href="?op=home&amp;mode=edit"'.active('home-edit').' style="background-image:url(images/icon/skull.gif);">Site beállítások</a>';

		echo '<div>Tartalmak</div>';
		echo '<a href="?op=content&amp;mode=edit&amp;category_id='.$cidStr.'&amp;content_type=0"'.active('content-edit').' style="background-image:url(images/icon/edit.gif);">Új hír vagy videó</a>';
		echo '<a href="?op=content&amp;mode=edit&amp;category_id='.$cidStr.'&amp;content_type=6" style="background-image:url(images/icon/edit.gif);">Új fotósorozat</a>';
		echo '<a href="?op=content&amp;mode=show&amp;category_id='.$cidStr.'&amp;order=own"'.active('content-show').' style="margin-bottom:16px;background-image:url(images/icon/document.gif);">Tartalmak szerkesztése</a>';
	}

	if ($admin->hasRight(ROLE_MODERATOR)) {
		echo '<div>Moderálás</div>';
		echo '<a href="?op=comments&amp;mode=show&amp;category_id='.$cidStr.'"'.active('comments-show').' style="background-image:url(images/icon/quote.gif);">Hozzászólások</a>';
	}
	if (in_array($admin->role, array(ROLE_MODERATOR, ROLE_GOD))) {
		echo '<a href="?op=users&amp;mode=show"'.active('users-show').' style="margin-bottom:16px;background-image:url(images/icon/user.gif);">Felhasználók</a>';
	}

	if ($admin->hasRight(ROLE_GOD)) {

		echo '<div>Karbantartás</div>';

		echo '<a href="?op=pages&amp;mode=edit"'.active('pages-edit').' style="background-image:url(images/icon/edit.gif);">Új statikus oldal</a>';
		echo '<a href="?op=pages&amp;mode=show"'.active('pages-show').' style="margin-bottom:16px;background-image:url(images/icon/document.gif);">Statikus oldalak szerkesztése</a>';

		echo '<a href="?op=content_feeds&amp;mode=edit&amp;category_id='.$cidStr.'"'.active('content_feeds-edit').' style="background-image:url(images/icon/edit.gif);">Új feed hozzáadása</a>';
		echo '<a href="?op=content_feeds&amp;mode=show&amp;category_id='.$cidStr.'"'.active('content_feeds-show').' style="margin-bottom:16px;background-image:url(images/icon/document.gif);">Feedek szerkesztése</a>';

		echo '<a href="?op=tags&amp;mode=show"'.active('tags-show').' style="background-image:url(images/icon/edit.gif);">Címkék szerkesztése</a>';

		echo '<a href="?op=system&amp;mode=feedreport"'.active('system-feedreport').' style="background-image:url(images/icon/caution.gif);">Feed napló</a>';
		echo '<a href="?op=system&amp;mode=feedcheck"'.active('system-feedcheck').' style="background-image:url(images/icon/caution.gif);">Feed ellenőrzés</a>';
	}

	echo '<a href="?op=system&amp;mode=stats"'.active('system-stats').' style="background-image:url(images/icon/info.gif);">Statisztikák</a>';
	echo '<a href="?op=help"'.active('help-').' style="background-image:url(images/icon/info.gif);">Segítség</a>';

?>


	<br class="clear" />

</td>
<td colspan="2" width="100%" id="op-content">

	<?php require($op.'.php'); ?>

</td></tr>
</table>

</div> <!-- / wrapper -->

</body>
</html>
<?php
ob_end_flush();
?>
