<?php
defined('ENV') or die;
?>

<h1>Segítség</h1>
<h2>Mit hogyan az admin felületen?</h2>

<br class="clear" />

<div class="inner">

<ul>
<li><a href="/admin/?op=help#ujhir">Új hír vagy videó beillesztése</a></li>
<li><a href="/admin/?op=help#atszerkesztes">Már bentlévő tartalom átszerkesztése, hogy saját hír legyen</a></li>
<li><a href="/admin/?op=help#ujvendegszerzo">Új vendégszerzős írás beillesztése</a></li>
<li><a href="/admin/?op=help#mtibol">MTI-ből történő hírmásolás</a></li>
</ul>

<br class="clear" />

	<b id="ujhir">Új hír vagy videó beillesztése</b><br>
	<ol>
	<li>Kattints az <a href="/admin/?op=content&mode=edit&category_id=0&content_type=0">Új hír</a> menüpontra.</li>
	<li>Töltsd ki tartalom címét (a "Tartalom linkje" mezőt hagyd üresen).</li>
	<li>Töltsd ki a hír szövegét. Ha több bekezdésből áll a szöveg, akkor egy-egy bekezdést 2 db Shift+Enter leütésével válassz el. Ha valamilyen weboldalról, MTI-ből, Word-dokumentumból másolod a hírt, akkor a szöveget először másold be egy Jegyzettömbbe, hogy elveszítse a formázást. Erről <a href="/admin/?op=help#mtibol">bővebben itt</a>.</li>
	<li>Ha több bekezdésből áll a szöveg, akkor az elsőt jelöld ki, és alakítsd félkövérré ("B" gomb az eszköztáron.)</li>
	<li>HA VIDEÓS HÍRT KÉSZÍTESZ - Töltsd ki a beillesztőkód mezőt (ellenkező esetben hagyd üresen). A beillesztőkód által létrehozott videólejátszó lehetőleg 590 pixel széles legyen.<br>
		<div style="padding-left:20px;">A YouTubeon a videó alatt kattints ide: "Share" &gt; "Embed". A "Video size" résznél írd az első mezőbe, hogy 590. Most másold ki a felette lévő mezőből a beillesztő HTML kódot, ezt másold be a Propeller adminfelületére.</div></li>
	<li>Válaszd ki a kategóriát.</li>
	<li>A "Feed" mezőnél a "Nem RSS-ből érkező tartalom" legyen kiválasztva.</li>
	<li>A "Felhasználónévnél" a "Propeller" szerepeljen.</li>
	<li>A "Kapcsolódó tartalom hozzáadása" gombbal adj hozzá 5 db kapcsolódó cikket. (Kezdj el gépelni egy szót a megjelenő mezők közül a hosszabbikba, és a rendszer javasol kapcsolódókat.)</li>
	<li>Ha minden kész, így kell kinéznie, nyomd meg a Mentést:<br>
		<img src="/admin/admin_ujvideo.jpg" style="margin-top: 5px; border:5px solid #ccc;"></li>
	</ol>


<br class="clear" />
<br class="clear" />

	<b id="atszerkesztes">Már bentlévő tartalom átszerkesztése, hogy saját hír legyen</b><br>
	<ol>
	<li>Keresd ki a <a href="/admin/?op=content&mode=show&category_id=0">Tartalom szerkesztése</a> menüpontból a hírt.</li>
	<li>Töröld ki a tartalom linkjét.</li>
	<li>Ha szükséges, átírhatod a tartalom címét.</li>
	<li>Töltsd ki a hír szövegét az <a href="/admin/?op=help#ujhir">Új hír beillesztése</a> bekezdésben leírtak szerint.</li>
	<li>A kategóriát ne módosítsd.</li>
	<li>A "Feed" mezőnél a "Nem RSS-ből érkező tartalom" legyen kiválasztva.</li>
	<li>A "Felhasználónévnél" a "Propeller" szerepeljen.</li>
	<li>A kapcsolódó tartalomakat átszerkesztheted ha szükséges (az "X" gombbal lehet kivenni ami nem releváns, és a hozzáadással újat hozzáadni).</li>
	<li>Ha minden kész, ennek is az <a href="/admin/?op=help#ujhir">Új hír beillesztése</a> bekezdésben leírtak szerint kell kinéznie, nyomd meg a Mentést.</li>
	</ol>


<br class="clear" />
<br class="clear" />


	<b id="ujvendegszerzo">Új vendégszerzős írás beillesztése</b><br>
	<ol>
	<li>Kattints az <a href="/admin/?op=content&mode=edit&category_id=0&content_type=4">Új vendégszerzős írás</a> menüpontra.</li>
	<li>Töltsd ki tartalom címét.</li>
	<li>Töltsd ki az írás szövegét. Ha több bekezdésből áll a szöveg, akkor egy-egy bekezdést 2 db Shift+Enter leütésével válassz el. Ha valamilyen weboldalról, MTI-ből, Word-dokumentumból másolod a hírt, akkor a szöveget először másold be egy Jegyzettömbbe, hogy elveszítse a formázást. Erről <a href="/admin/?op=help#mtibol">bővebben itt</a>.</li>
	<li>Ha több bekezdésből áll a szöveg, akkor az elsőt jelöld ki, és alakítsd félkövérré ("B" gomb az eszköztáron.)</li>
	<li>Az utolsó bekezdés legyen a vendégszerző neve. Ha linkelni is kell, akkor a névre helyezd el a linket.</li>
	<li>Válaszd ki a kategóriát.</li>
	<li>A "Felhasználónévnél" a "Propeller" szerepeljen.</li>
	<li>A "Kapcsolódó tartalom hozzáadása" gombbal adj hozzá 5 db kapcsolódó cikket. (Kezdj el gépelni egy szót a megjelenő mezők közül a hosszabbikba, és a rendszer javasol kapcsolódókat.)</li>
	<li>Ha minden kész, így kell kinéznie, nyomd meg a Mentést:<br>
		<img src="/admin/admin_ujvendegszerzo.jpg" style="margin-top: 5px; border:5px solid #ccc;"></li>
	</ol>


<br class="clear" />
<br class="clear" />


	<b id="mtibol">MTI-ből történő hírmásolás</b><br>
	<ol>
	<li>Keresd ki az <a href="http://mti.hu/Pages/Default.aspx?lang=hun&menuid=2" target="_blank">MTI felületén</a> a hírt.</li>
	<li>Másold ki a hírt és illeszd be a Jegyzettömbbe. (Ha nem jól tördel a Jegyzettömbe, menj ide: "Formátum" &gt; "Hosszú sorok tördelése".)</li>
	<li>Ahol a bekezdések között csak 1 db sortörés van, oda tegyél még egyet (azaz két bekezdés között mindig legyen 1 üres sor).</li>
	<li>Ha minden kész, így kell kinéznie, másold ki az így keletkezett formázatlan, bekezdésekre tagolt szöveget, ezt másold be a Propeller adminfelületére:<br>
		<img src="/admin/admin_mtibol.jpg" style="margin-top: 5px; border:5px solid #ccc;"></li>
	</ol>

</div>













