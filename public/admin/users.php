<?php
defined('ENV') or die;

//if(($_GET['mode'] == 'edit') && $admin->hasRight(ROLE_MODERATOR)) {
if(($_GET['mode'] == 'edit') && in_array($admin->role, array(ROLE_MODERATOR, ROLE_GOD))) {

	if($_POST['action'] == 'insert') { // űrlapelküldés, kiértékelés új hozzáadáshoz

	}

	if($_POST['action'] == 'update') { // űrlapelküldés, kiértékelés meglévő mentéséhez
		$user = $_POST['user'];
		$user['id'] = (int)$_GET['id'];
		$query = "";

		if(!empty($user['pass']) || !empty($user['repass'])) { // van megadva jelszó vagy re-jelszó
			if(!$auth->checkPassFormat($user['pass'], (int)$env->c['users']['passlength'])) { $error = $env->l['users']['error_easypass']; }
			if($user['pass'] != $user['repass']) { $error = $env->l['users']['error_notsamepass']; }

			$pass = $auth->getHashmark($user['pass']);
			$query .= "pass = '".$pass."', ";
		}

		$res_reserved = $db->Query("SELECT mail, name FROM "._DBPREF."users WHERE id = '".$user['id']."' LIMIT 1");
		$row_reserved = $db->fetchArray($res_reserved);

		if($user['mail'] != $row_reserved['mail']) { // módosul az e-mail cím
			if(!$auth->checkEmailFormat($user['mail'])) { $error = $env->l['users']['error_wrongmail']; }
			if($auth->isReserved('mail', $user['mail'])) { $error = $env->l['users']['error_reservedmail'];	}

			$query .= "mail = '".$user['mail']."', ";
		}

		if($user['name'] != $row_reserved['name']) { // módosul a felhasználónév
			if(!$auth->checkNameFormat($user['name'])) { $error = $env->l['users']['error_wrongname']; }
			if($auth->isReserved('name', $user['name'])) { $error = $env->l['users']['error_reservedname'];	}

			$alias = $auth->getNameAlias($user['name']);
			if($auth->isReserved('alias', $alias)) { $error = $env->l['users']['error_reservedname']; }

			$query .= "name = '".$db->escape($user['name'])."', ";
			$query .= "alias = '".$db->escape($alias)."', ";
		}

		if(empty($error)) { // nincs hiba, megtörténhet a felvétel
			$user = $db->escape($user);

			$query .= "status = '".(int)$user['status']."', ";

			$query .= "newsletter = '".(empty($user['newsletter']) ? "0" : "1")."', ";
//			$query .= "firstname = ".(empty($user['firstname']) ? "NULL" : "'".$user['firstname']."'").", ";
//			$query .= "lastname = ".(empty($user['lastname']) ? "NULL" : "'".$user['lastname']."'").", ";
			$query .= "pubmail = '".(empty($user['pubmail']) ? "0" : "1")."', ";
			$query .= "birthdate = ".((!$user['month'] || !$user['day'] || !$user['year']) ? "NULL" : "'".date($user['year'].'-'.$user['month'].'-'.$user['day'])."'").", ";
			$query .= "profession = ".(empty($user['profession']) ? "NULL" : "'".$user['profession']."'").", ";

/*			$query .= "country_id = ".(empty($user['country_id']) ? "NULL" : "'".$user['country_id']."'").", ";
			$query .= "region_id = ".(empty($user['region_id']) ? "NULL" : "'".$user['region_id']."'").", ";
			$query .= "city_id = ".(empty($user['city_id']) ? "NULL" : "'".$user['city_id']."'").", ";
*/
			$query .= "content_num = '".(int)$user['content_num']."', ";

			$query .= "website = ".(empty($user['website']) ? "NULL" : "'".$user['website']."'").", ";
			$query .= "motto = ".(empty($user['motto']) ? "NULL" : "'".$user['motto']."'").", ";
			$query .= "t_like = ".(empty($user['t_like']) ? "NULL" : "'".$user['t_like']."'").", ";
			$query .= "t_dislike = ".(empty($user['t_dislike']) ? "NULL" : "'".$user['t_dislike']."'").", ";
			$query .= "topics = ".(empty($user['topics']) ? "NULL" : "'".$user['topics']."'")." ";

			$res = $db->Query("UPDATE "._DBPREF."users SET ".$query." WHERE id = '".$user['id']."' LIMIT 1");

			if($res) { // nincs hiba
				if($admin->hasRight(ROLE_GOD)) { // jogok beállítása
					// meglévő jogok törlése
					$res_del = $db->Query("DELETE FROM "._DBPREF."users_backend WHERE user_id = '".$user['id']."'");

					if ((int)$_POST['right'] > 0) {
						$res_rights = $db->Query("INSERT INTO "._DBPREF."users_backend (user_id, right_id) VALUES ('".$user['id']."', '".(int)$_POST['right']."')");
					}
				}
/*
				if(!empty($_FILES['avatar']['name'])) { // kép feltöltése
					require_once('../system/Image.class.php');
					$image = new Image($_FILES['avatar']);
					$image->imgBase = '../images/';
					$image->fileID = $user['id'];

					if(!$image->saveForAvatar()) {
						$error = $env->l['users'][$image->error];
					}
				}
*/
				header('Location: '.BACKEND.'/?op=users&mode=edit&id='.$_GET['id']);
				exit;
			}
		}
	}

// már meglévő elem esetén az adatok kiolvasása ID alapján, és az űrlapmezőkbe való betöltés

/* ***************************************************************************** */

	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // módosító űrlap
		$action = 'update';

		$res = $db->Query("SELECT * FROM "._DBPREF."users WHERE id = '".$_GET['id']."' LIMIT 1");
		$user = $db->fetchArray($res);

		$user['birthyear'] = substr($user['birthdate'], 0, 4);
		$user['birthmonth'] = (int)substr($user['birthdate'], 5, 2);
		$user['birthday'] = (int)substr($user['birthdate'], 8, 2);

		$user['avatarsrc'] = ($user['avatar'] == 1) ? floor($user['id']/1000).'/'.$user['id'].'/'.$user['id'].'_large.jpg' : 'default_large.jpg';

		// jogosultságának kiolvasása
		$userRights = array();
		$res_rights = $db->Query("SELECT * FROM "._DBPREF."users_backend WHERE user_id = '".$_GET['id']."' LIMIT 1");
		$row_r = $db->fetchArray($res_rights);
		$role = $row_r['right_id'];
	}
	else { // új oldal
		$action = 'insert';
		$user = array();
	}

?>

	<h1><?= ($action == 'insert') ? 'Új felhasználó hozzáadása' : 'Felhasználó szerkesztése'; ?></h1>
	<h2>Add meg a felhasználó adatait, majd nyomd meg a mentés gombot.</h2>
	<?php if(isset($error)) { echo '<p id="error"><b>Hiba:</b> '.$error.'</p>'; } ?>

	<form id="form" name="form" class="form" action="<?= $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" id="action" value="<?= $action; ?>" />

		<fieldset>

			<?php if($action == 'update') { ?>
			<a href="<?= $env->base; ?>/<?= $env->l['users']['url_profile'].'/'.$user['alias']; ?>" onclick="window.open(this.href, '_blank'); return false;"><img src="<?= STTC; ?>/avatar/<?= $user['avatarsrc'].'?'.time(); ?>" style="float: right;" alt="" /></a>
			<?php } ?>

			<label for="profession">Foglalkozás:</label>
			<input type="text" id="profession" class="text" name="user[profession]" maxlength="64" value="<?= htmlspecialchars($user['profession'], ENT_QUOTES); ?>" /><br />

			<label for="year">Születési idő:</label>
			<select id="year" name="user[year]" style="width: 60px;">
			<option label="&nbsp;" value="">&nbsp;</option>
			<?php
			for($i = date('Y')-100; $i<=date('Y')-1; $i++) {
				echo '<option label="'.$i.'" value="'.$i.'"';
				echo ($user['birthyear'] == $i) ? ' selected="selected"' : '';
				echo '>'.$i.'</option>'."\n";
			}
			?>
			</select>

			<select id="month" name="user[month]" style="width: 115px;">
			<option label="&nbsp;" value="">&nbsp;</option>
			<?php
			$j = 1;
			foreach ($env->l['months'] as $key => $value) {
				echo '<option label="'.$value.'" value="'.$j.'"';
				echo ($user['birthmonth'] == $j) ? ' selected="selected"' : '';
				echo '>'.$value.'</option>'."\n";
				$j++;
			}
			?>
			</select>

			<select id="day" name="user[day]" style="width: 45px;">
			<option label="&nbsp;" value="">&nbsp;</option>
			<?php
			for($k = 1; $k<=31; $k++) {
				echo '<option label="'.$k.'" value="'.$k.'"';
				echo ($user['birthday'] == $k) ? ' selected="selected"' : '';
				echo '>'.$k.'</option>'."\n";
			}
			?>
			</select><br />


			<label for="mail">E-mail cím:</label>
			<input type="text" id="mail" class="text required validate-email" name="user[mail]" maxlength="255" value="<?= $user['mail']; ?>" title="Írd be az e-mail címet" /><br />

			<label for="pass">Jelszó:</label>
			<input type="password" id="pass" class="text" name="user[pass]" maxlength="32" title="Válassz egy minimum 5 karakter hosszú, nehezen kitalálható jelszót" /><br />

			<label for="repass">Jelszó újra:</label>
			<input type="password" id="repass" class="text" name="user[repass]" maxlength="32" title="Biztonsági okokból ismételd meg az előző mezőben megadott jelszót" /><br />

			<label for="name">Felhasználónév:</label>
			<input type="text" id="name" class="text required" name="user[name]" maxlength="32" value="<?= htmlspecialchars($user['name'], ENT_QUOTES); ?>" title="Válassz egy tetszőleges felhasználónevet" /><br />
<!--
			<label for="avatar">Új avatar:</label>
			<input type="file" size="73" accept="image/gif, image/png, image/jpeg, image/jpg" id="avatar" name="avatar" /><br style="clear:both;" />
-->

			<label for="pubmail">Látható e-mail cím:</label>
			<input type="checkbox" id="pubmail" class="radio" name="user[pubmail]" value="1"<?php echo ($user['pubmail'] == 1) ? ' checked="checked"' : ''; ?>/><br style="clear:both;" />

			<label for="newsletter">Néha jöhet hírlevél:</label>
			<input type="checkbox" id="newsletter" class="radio" name="user[newsletter]" value="1"<?php echo ($user['newsletter'] == 1) ? ' checked="checked"' : ''; ?>/><br style="clear:both;" />

			<label for="status">Állapot:</label>
			<select id="cities-list" class="item-list" name="user[status]">
			<?php
			echo '<option value="0"'.(($user['status'] == 0) ? ' selected="selected"' : '').'>Tiltott</option>'."\n";
			echo '<option value="1"'.(($user['status'] == 1) ? ' selected="selected"' : '').'>Aktiválatlan regisztráció</option>'."\n";
			echo '<option value="2"'.(($user['status'] == 2) ? ' selected="selected"' : '').'>Aktivált regisztráció</option>'."\n";
			?>
			</select><br class="clear" />

			<label for="content_num">Beküldés limit:</label>
			<input type="text" id="content_num" class="text required" name="user[content_num]" maxlength="3" value="<?= (int)$user['content_num']; ?>" /><br />

			<label for="website">Honlap:</label>
			<input type="text" id="website" class="text" name="user[website]" maxlength="200" value="<?= htmlspecialchars(strip_tags($user['website']), ENT_QUOTES); ?>" /><br />

			<label for="motto">Idézet, mottó:</label>
			<textarea name="user[motto]" id="motto" rows="4" cols="18" class="text mceNoEditor"><?= htmlspecialchars(strip_tags($user['motto']), ENT_QUOTES); ?></textarea><br class="clear" />

			<label for="t_like">Szeretem:</label>
			<textarea name="user[t_like]" id="t_like" rows="4" cols="18" class="text mceNoEditor"><?= htmlspecialchars(strip_tags($user['t_like']), ENT_QUOTES); ?></textarea><br class="clear" />

			<label for="t_dislike">Utálom:</label>
			<textarea name="user[t_dislike]" id="t_dislike" rows="4" cols="18" class="text mceNoEditor"><?= htmlspecialchars(strip_tags($user['t_dislike']), ENT_QUOTES); ?></textarea><br class="clear" />

			<label for="topics">Kedvenc témáim:</label>
			<textarea name="user[topics]" id="topics" rows="4" cols="18" class="text mceNoEditor"><?= htmlspecialchars(strip_tags($user['topics']), ENT_QUOTES); ?></textarea><br class="clear" />
<!-- nem használunk vip megjelenést már
			<label for="vip">VIP felhasználó:</label>
			<input type="checkbox" id="vip" class="radio" name="user[vip]" value="1"<?php echo ($user['vip'] == 1) ? ' checked="checked"' : ''; ?>/><br style="clear:both;" />
-->

		</fieldset>
<?php if($admin->hasRight(ROLE_GOD)) { ?>
		<fieldset>

		<?php
echo '<label for="rights">Jogosultságok:</label><div id="rights-wrapper" style="float: left;">';
echo '<table style="width:500px;"><tr><td style="vertical-align: top;" width="49%">';
echo '<input type="radio" class="radio" name="right" value="0" '.(!$role ? 'checked="checked" ' : '').'/> Nincs semmilyen jogosultsága az adminisztrációs felülethez<br />';
foreach($admin->roles AS $key => $value) {
	echo '<input type="radio" class="radio" name="right" value="'.$value.'" '.(($value == $role) ? 'checked="checked" ' : '').'/> '.$key.'<br />';
}
echo '</table>';

		?>
</div>
	</fieldset>
<?php } ?>
		<label>&nbsp;</label>
		<input type="submit" id="submit" class="button" value="Mentés"/>
		<input type="reset" id="reset" class="button" value="<?= ($action == 'update') ? 'Visszaállítás' : 'Mezők törlése'; ?>" />
		<?= ($action == 'update') ? '<input type="button" id="delete" class="button" value="Törlés" onclick="Delete('.$_GET['id'].');" />' : ''; ?>
	</form>
	<script type="text/javascript">
	// <![CDATA[
		var valid = new Validation('form');
	// ]]>
	</script>


<?php
}

//if(($_GET['mode'] == 'show') && $admin->hasRight(ROLE_MODERATOR)) {
if(($_GET['mode'] == 'show') && in_array($admin->role, array(ROLE_MODERATOR, ROLE_GOD))) {

?>
	<h1>Felhasználók szerkesztése</h1>
	<h2>Válaszd ki a módosítandó felhasználót az alábbi listából.</h2>

	<br class="clear" />

<?php

$order = "";
if(!empty($_GET['q'])) {
	$words = explode(" ", $_GET['q']);
	while(list($k, $v) = each($words)){
		$v = $db->escape($v);
		if(!$j) {
			$order .= "(name COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR mail COLLATE utf8_hungarian_ci LIKE '%".$v."%')";
			$j = 1;
		}
		else {
			$order .= " AND (name COLLATE utf8_hungarian_ci LIKE '%".$v."%' OR mail COLLATE utf8_hungarian_ci LIKE '%".$v."%')";
		}
	}
	$order .= " AND ";
}


if(@$_GET['order'] == 'modtime') { // rendezések
	$order .= '1 = 1 ORDER BY modtime DESC';
	$flag = 1;
}
elseif(@$_GET['order'] == 'name') {
	$order .= "1 = 1 ORDER BY name ASC";
	$flag = 0;
}
elseif(@$_GET['order'] == 'status0') {
	$order .= "status = '0' ORDER BY datetime DESC";
	$flag = 3;
}
elseif(@$_GET['order'] == 'status1') {
	$order .= "status = '1' ORDER BY datetime DESC";
	$flag = 4;
}
elseif(@$_GET['order'] == 'status2') {
	$order .= "status = '2' ORDER BY datetime DESC";
	$flag = 5;
}
elseif(@$_GET['order'] == 'admin') {
	$order .= "right_id IS NOT NULL GROUP BY user_id ORDER BY name ASC";
	$flag = 6;
}
else {
	$order .= "1 = 1 ORDER BY datetime DESC";
	$flag = 2;
}
?>
	<form id="backend-search" action="<?= $_SERVER['REQUEST_URI']; ?>" method="get">
		<input type="hidden" name="op" value="users" />
		<input type="hidden" name="mode" value="show" />
		<input type="text" name="q" class="text" value="<?= htmlspecialchars($_GET['q'], ENT_QUOTES); ?>" /> <input type="submit" class="button" value="Keresés" />
	</form>

	<p id="lead">
		<?= ($flag == 0) ? 'név szerint' : '<a href="?op=users&amp;mode=show&amp;order=name">név szerint</a>'; ?> |
		<?= ($flag == 2) ? 'regisztráció szerint' : '<a href="?op=users&amp;mode=show">regisztráció szerint</a>'; ?> |
		<?= ($flag == 1) ? 'utolsó belépés szerint' : '<a href="?op=users&amp;mode=show&amp;order=modtime">utolsó belépés szerint</a>'; ?> |
		<?= ($flag == 3) ? 'tiltottak' : '<a href="?op=users&amp;mode=show&amp;order=status0">tiltottak</a>'; ?> |
		<?= ($flag == 4) ? 'aktiválatlanok' : '<a href="?op=users&amp;mode=show&amp;order=status1">aktiválatlanok</a>'; ?> |
		<?= ($flag == 5) ? 'aktiváltak' : '<a href="?op=users&amp;mode=show&amp;order=status2">aktiváltak</a>'; ?> |
		<?= ($flag == 6) ? 'szerkesztők' : '<a href="?op=users&amp;mode=show&amp;order=admin">szerkesztők</a>'; ?>
	</p>

<?php
// lapozások kiszámítása, ennyi legyen egy oldalon:
$perpage = 20;
if(empty($_GET['page'])) { // első oldalon vagyunk
	$listfrom = 0;
	$pnow = 1;
}
else { // next pages
	$listfrom = $_GET['page'] * $perpage - $perpage;
	$pnow = $_GET['page'];
}

$res_all = $env->db->Query("SELECT id FROM "._DBPREF."users u
LEFT JOIN "._DBPREF."users_backend b ON u.id = b.user_id WHERE ".$order);
$pgs = ceil($env->db->numRows($res_all) / $perpage); // oldalak száma

if(!$env->db->numRows($res_all)) {
	echo '<p>Nincsenek a feltételnek megfelelő sorok...</p>';
}
else {

$i = $listfrom + 1; // lista sorszáma
echo '<table cellspacing="0" cellpadding="1" width="100%">';
$res = $env->db->Query("SELECT id, mail, status, name, alias, datetime, modtime FROM "._DBPREF."users u
LEFT JOIN "._DBPREF."users_backend b ON u.id = b.user_id
WHERE ".$order." LIMIT ".$listfrom.", ".$perpage);
	echo '
	<tr class="head">
		<td valign="top">&nbsp;</td>
		<td valign="top"><b>Felhasználónév</b></td>
		<td valign="top"><b>E-mail cím</b></td>
		<td valign="top"><b>Regisztráció dátuma</b></td>
		<td valign="top"><b>Állapot</b></td>
		<td valign="top"><b>Utolsó belépés</b></td>
		<td valign="top"></td>
	</tr>
	';
$status = array('Tiltott', 'Aktiválatlan', 'Aktivált');
while($row = $env->db->fetchArray($res)) {
	$diffcolor = (($i + 1 ) % 2 == 0) ? '' : '#F8F9FA';
	echo '
	<tr style="background-color: '.$diffcolor.'">
		<td valign="top"><span style="color: #aaa;"><b>'.$i.'.</b></span></td>
		<td valign="top"><a href="?op=users&amp;mode=edit&amp;id='.$row['id'].'">'.htmlspecialchars($row['name'], ENT_QUOTES).'</a></td>
		<td valign="top"><a href="mailto:'.$row['mail'].'">'.$row['mail'].'</a></td>
		<td valign="top">'.$admin->dateFormat($row['datetime']).'</td>
		<td valign="top">'.$status[$row['status']].'</td>
		<td valign="top">'.($row['modtime'] ? $admin->dateFormat($row['modtime']) : '-').'</td>
		<td valign="top" align="right" nowrap="nowrap">
			<a href="'.$env->base.'/'.$env->l['users']['url_profile'].'/'.$row['alias'].'" onclick="window.open(this.href, \'_blank\'); return false;" title="Megnyitás új ablakban"><img src="images/icon/grow.gif" alt="" /></a>
		</td>
	</tr>
	';
$i++;
}
echo '</table>';

echo '<br class="clear" /><p>Oldalak: '; // lapszámozás és lapozás

$turn = ($pnow <= 15) ? 1 : ($pnow - 15);
$tto = ($pnow <= 15) ? (($pgs < 30) ? $pgs : 30): ($pnow+15);

	for($j = $turn; $j <= $tto; $j++) { // következő, előző oldalak számainak kiírása
		if($pnow == $j) { // ez az oldal, itt állunk most
			echo ' <b>'.$j.'</b>';
		}
		else { // további oldalak linkkel
			if(!empty($_GET['q']))
				echo ' <a href="?op=users&amp;mode=show&amp;q='.htmlentities($_GET['q'], ENT_QUOTES).'&amp;page='.$j.'">'.$j.'</a> ';
			else
				echo ' <a href="?op=users&amp;mode=show&amp;order='.@$_GET['order'].'&amp;page='.$j.'">'.$j.'</a> ';
		}
	}
echo '</p>';
}
}


//if(($_GET['mode'] == 'delete') && $admin->hasRight(ROLE_MODERATOR)) {
if(($_GET['mode'] == 'delete') && in_array($admin->role, array(ROLE_MODERATOR, ROLE_GOD))) {


	if(isset($_GET['id']) && is_numeric($_GET['id'])) { // törlés
		$res[] = $db->Query("DELETE FROM "._DBPREF."users WHERE id = '".$_GET['id']."' LIMIT 1");
		$res[] = $db->Query("DELETE FROM "._DBPREF."users_backend WHERE user_id = '".$_GET['id']."'");
		?>
		<h1>Felhasználó törlése</h1>
		<h2>A felhasználó törlése megtörtént.</h2>
		<?php
		//var_dump($res);
	}
}




?>
