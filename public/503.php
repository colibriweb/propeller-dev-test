<?php
header('HTTP/1.1 503 Service Temporarily Unavailable');
header('Status: 503 Service Temporarily Unavailable');
header('Retry-After: 7200');
header("Content-type: text/html; charset=utf-8");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="refresh" content="20; url=http://www.propeller.hu/">
<style type="text/css" media="all"><!--/*--><![CDATA[/*><!--*/
body, div { margin: 0; background-color: #fff; font: normal 14px Arial, Helvetica; }
h1 a { color: #000; text-decoration: none; font: bold 27px Arial, Helvetica; }
#horizon { text-align: center; position: absolute; top: 50%; left: 0px; width: 100%; height: 1px; overflow: visible; visibility: visible; display: block; }
#content { margin-left: -300px; position: absolute; top: -240px; left: 50%; width: 600px; height: 240px; visibility: visible; }
p { color: #828486; margin: 0; padding: 12px 0 0 0; }
.bold { font-weight: bold; color: #333; }
p.foot, p.foot a { padding-top: 80px; font-size: 11px; color: #d2d4d6; }

/*]]>*/--></style>
<script type="text/javascript">
// <![CDATA[
window.onload = function() {
	for(var i=0; i<document.links.length; i++) {
		if(document.links[i].nodeName=='A' & (document.links[i].rel != 'no')) {
			document.links[i].href = document.links[i].href.replace('##kukac##', '@');
			document.links[i].innerHTML = document.links[i].innerHTML.replace('##kukac##', '@');
		}
	}
}
// ]]>
</script>
<title>Propeller</title>
</head>
<body>
	<div id="horizon">
		<div id="content">

			<h1><a href="/">Ajjaj, mi történt?!</a></h1>

			<p>A Propeller karbantartás miatt átmenetileg nem elérhető.<br /></p>

			<p class="bold">Köszönjük türelmed, hamarosan visszajövünk!</p>

			<p> </p>
			<p class="foot">Copyright &copy; 2008-2009. Propeller.hu Kft. &ndash; <a href="mailto:info##kukac##propeller.hu">Kapcsolat</a></p>
		</div>
	</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-241542-5");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>
